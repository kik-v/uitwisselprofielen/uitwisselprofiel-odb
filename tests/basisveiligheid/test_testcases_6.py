from DataGenerator import DataGenerator
from QueryTest import QueryTest

# Opmerkingen: 


# Testcases: 

# Testcase 01

td_01 = [
    {
        "Description": "Testcase 01 (Geen Wlz indicatie + Geen afspraak voedselvoorkeuren + Geen vestiging)",
        "Amount": 10,
        "Human": [
            {
                "NursingProcess": [
                    {
                        "identifier": "1",
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",
                        "location": "",
                        "related_indication": "1"
                    }
                ],
                "Indication": [
                    {
                        "identifier": "1",
                        "type": "WlzIndicatie",
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",
                        "heeftIndicatieBehandeling": True,
                        "ciz": [""] ,
                        "leveringsvorm": [""]
                    }
                ]
            }
        ]
    }
]

# Testcase 01a

td_01_a = [
    {
        "Description": "Testcase 01a (Geen Wlz indicatie + Geen afspraak voedselvoorkeuren + Vestiging nr. 1287)",
        "Amount": 10,
        "Human": [
            {
                "NursingProcess": [
                    {
                        "identifier": "1",
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",
                        "location": "Locatie_BG_Rechts",
                        "related_indication": "1"
                    }
                ],
                "Indication": [
                    {
                        "identifier": "1",
                        "type": "WlzIndicatie",
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",
                        "heeftIndicatieBehandeling": True,
                        "ciz": [""] ,
                        "leveringsvorm": [""]
                    }
                ]
            }
        ]
    }
]

# Testcase 01b

td_01_b = [
    {
        "Description": "Testcase 01b (Geen Wlz indicatie + Geen afspraak voedselvoorkeuren + Vestiging nr. 1254)",
        "Amount": 10,
        "Human": [
            {
                "NursingProcess": [
                    {
                        "identifier": "1",
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",
                        "location": "Locatie_Vleugel_A",
                        "related_indication": "1"
                    }
                ],
                "Indication": [
                    {
                        "identifier": "1",
                        "type": "WlzIndicatie",
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",
                        "heeftIndicatieBehandeling": True,
                        "ciz": [""] ,
                        "leveringsvorm": [""]
                    }
                ]
            }
        ]
    }
]

# Testcase 02

td_02 = [
    {
        "Description": "Testcase 02 (Wlz indicatie 1 t/m 3 (1VV) + Geen afspraak voedselvoorkeuren + Geen vestiging)",
        "Amount": 10,
        "Human": [
            {
                "NursingProcess": [
                    {
                        "identifier": "1",
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",
                        "location": "Locatie_BG_Rechts",
                        "related_indication": "1"
                    }
                ],
                "Indication": [
                    {
                        "identifier": "1",
                        "type": "WlzIndicatie",
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",
                        "heeftIndicatieBehandeling": True,
                        "ciz": ["1VV"] ,
                        "leveringsvorm": [""]
                    }
                ]
            }
        ]
    }
]


# Testcase 02a

td_02_a = [
    {
        "Description": "Testcase 02a (Wlz indicatie 1 t/m 3 (1VV) + Geen afspraak voedselvoorkeuren + Vestiging nr. 1287)",
        "Amount": 10,
        "Human": [
            {
                "NursingProcess": [
                    {
                        "identifier": "1",
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",
                        "location": "Locatie_BG_Rechts",
                        "related_indication": "1"
                    }
                ],
                "Indication": [
                    {
                        "identifier": "1",
                        "type": "WlzIndicatie",
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",
                        "heeftIndicatieBehandeling": True,
                        "ciz": ["1VV"] ,
                        "leveringsvorm": [""]
                    }
                ]
            }
        ]
    }
]

# Testcase 02b

td_02_b = [
    {
        "Description": "Testcase 02b (Wlz indicatie 1 t/m 3 (1VV) + Geen afspraak voedselvoorkeuren + Vestiging nr. 1254)",
        "Amount": 10,
        "Human": [
            {
                "NursingProcess": [
                    {
                        "identifier": "1",
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",
                        "location": "Locatie_Vleugel_A",
                        "related_indication": "1"
                    }
                ],
                "Indication": [
                    {
                        "identifier": "1",
                        "type": "WlzIndicatie",
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",
                        "heeftIndicatieBehandeling": True,
                        "ciz": ["1VV"] ,
                        "leveringsvorm": [""]
                    }
                ]
            }
        ]
    }
]

# Testcase 03

td_03 = [
    {
        "Description": "Testcase 03 (Wlz indicatie 4 t/m 10 (4VV) + Geen afspraak voedselvoorkeuren + Geen vestiging)",
        "Amount": 10,
        "Human": [
            {
                "NursingProcess": [
                    {
                        "identifier": "1",
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",
                        "location": "Locatie_BG_Rechts",
                        "related_indication": "1"
                    }
                ],
                "Indication": [
                    {
                        "identifier": "1",
                        "type": "WlzIndicatie",
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",
                        "heeftIndicatieBehandeling": True,
                        "ciz": ["4VV"] ,
                        "leveringsvorm": [""]
                    }
                ]
            }
        ]
    }
]


# Testcase 03a

td_03_a = [
    {
        "Description": "Testcase 03a (Wlz indicatie 4 t/m 10 (4VV) + Geen afspraak voedselvoorkeuren + Vestiging nr. 1287)",
        "Amount": 10,
        "Human": [
            {
                "NursingProcess": [
                    {
                        "identifier": "1",
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",
                        "location": "Locatie_BG_Rechts",
                        "related_indication": "1"
                    }
                ],
                "Indication": [
                    {
                        "identifier": "1",
                        "type": "WlzIndicatie",
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",
                        "heeftIndicatieBehandeling": True,
                        "ciz": ["4VV"] ,
                        "leveringsvorm": [""]
                    }
                ]
            }
        ]
    }
]

# Testcase 03b

td_03_b = [
    {
        "Description": "Testcase 03b (Wlz indicatie 4 t/m 10 (4VV) + Geen afspraak voedselvoorkeuren + Vestiging nr. 1254)",
        "Amount": 10,
        "Human": [
            {
                "NursingProcess": [
                    {
                        "identifier": "1",
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",
                        "location": "Locatie_Vleugel_A",
                        "related_indication": "1"
                    }
                ],
                "Indication": [
                    {
                        "identifier": "1",
                        "type": "WlzIndicatie",
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",
                        "heeftIndicatieBehandeling": True,
                        "ciz": ["4VV"] ,
                        "leveringsvorm": [""]
                    }
                ]
            }
        ]
    }
]

# Testcase 04

td_04 = [
    {
        "Description": "Testcase 04 (Wlz indicatie 4 t/m 10 (4VV) + Wel afspraak voedselvoorkeuren + Geen vestiging)",
        "Amount": 10,
        "Human": [
            {
                "PolicyAgreement": [
                    {
                        "type": "AfspraakVoedselvoorkeuren",
                        "date": "2024-01-01"
                    }
                ],
                "NursingProcess": [
                    {
                        "identifier": "1",
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",
                        "location": "Locatie_BG_Rechts",
                        "related_indication": "1"
                    }
                ],
                "Indication": [
                    {
                        "identifier": "1",
                        "type": "WlzIndicatie",
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",
                        "heeftIndicatieBehandeling": True,
                        "ciz": ["4VV"] ,
                        "leveringsvorm": [""]
                    }
                ]
            }
        ]
    }
]


# Testcase 04a

td_04_a = [
    {
        "Description": "Testcase 04a (Wlz indicatie 4 t/m 10 (4VV) + Wel afspraak voedselvoorkeuren + Vestiging nr. 1287)",
        "Amount": 10,
        "Human": [
            {
                "PolicyAgreement": [
                    {
                        "type": "AfspraakVoedselvoorkeuren",
                        "date": "2024-01-01"
                    }
                ],
                "NursingProcess": [
                    {
                        "identifier": "1",
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",
                        "location": "Locatie_BG_Rechts",
                        "related_indication": "1"
                    }
                ],
                "Indication": [
                    {
                        "identifier": "1",
                        "type": "WlzIndicatie",
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",
                        "heeftIndicatieBehandeling": True,
                        "ciz": ["4VV"] ,
                        "leveringsvorm": [""]
                    }
                ]
            }
        ]
    }
]

# Testcase 04b

td_04_b = [
    {
        "Description": "Testcase 04b (Wlz indicatie 4 t/m 10 (4VV) + Wel afspraak voedselvoorkeuren + Vestiging nr. 1254)",
        "Amount": 10,
        "Human": [
            {
                "PolicyAgreement": [
                    {
                        "type": "AfspraakVoedselvoorkeuren",
                        "date": "2024-01-01"
                    }
                ],
                "NursingProcess": [
                    {
                        "identifier": "1",
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",
                        "location": "Locatie_Vleugel_A",
                        "related_indication": "1"
                    }
                ],
                "Indication": [
                    {
                        "identifier": "1",
                        "type": "WlzIndicatie",
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",
                        "heeftIndicatieBehandeling": True,
                        "ciz": ["4VV"] ,
                        "leveringsvorm": [""]
                    }
                ]
            }
        ]
    }
]

# Testcase 04c

td_04_c = [
    {
        "Description": "Testcase 04c (Wlz indicatie 4 t/m 10 (4VV) + Meerdere beleidsafspraak rondom levenseinde + Vestiging nr. 1254)",
        "Amount": 10,
        "Human": [
            {
                "PolicyAgreement": [
                    {
                        "type": "AfspraakVoedselvoorkeuren",
                        "date": "2024-01-01"
                    },
                    {
                        "type": "AfspraakVoedselvoorkeuren",
                        "date": "2024-02-01"
                    }
                ],
                "NursingProcess": [
                    {
                        "identifier": "1",
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",
                        "location": "Locatie_Vleugel_A",
                        "related_indication": "1"
                    }
                ],
                "Indication": [
                    {
                        "identifier": "1",
                        "type": "WlzIndicatie",
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",
                        "heeftIndicatieBehandeling": True,
                        "ciz": ["4VV"] ,
                        "leveringsvorm": [""]
                    }
                ]
            }
        ]
    }
]

# Static tests:

def test_if_headers_are_correct_on_6(db_config):
    """ Test of de juiste headers terugkomen in het resultaat
        ODB Basisveiligheid 6. Afspraken voedselvoorkeuren
    """

    # Setup of the test
    test = QueryTest(db_config)

    # Configuration and execution
    test.set_query('Gevalideerde_vragen_technisch/basisveiligheid/Indicator 6.rq')

    # Assertions
    test.verify_header_present('vestiging')
    test.verify_header_present('teller')
    test.verify_header_present('noemer')
    test.verify_header_present('indicator')


def test_if_number_of_rows_returned_is_correct_for_query_6(db_config):
    """ Test of het aantal rijen correct wordt teruggegeven
        ODB Basisveiligheid 6. Afspraken voedselvoorkeuren
    """

    # Setup of the test
    test = QueryTest(db_config)

    # Configuration and execution
    test.set_query('Gevalideerde_vragen_technisch/basisveiligheid/Indicator 6.rq')

    # Assertions
    test.verify_row_count(1)


def test_if_indicator_has_correct_value_for_query_6(db_config):
    """ Test of de indicator de juiste waarde heeft
        ODB Basisveiligheid 6. Afspraken voedselvoorkeuren
    """

    # Setup of the test
    test = QueryTest(db_config)

    # Configuration and execution
    test.set_query('Gevalideerde_vragen_technisch/basisveiligheid/Indicator 6.rq')

    # Assertions
    test.verify_value("indicator","Waarneming kleiner dan 10", where_condition = ("vestiging", "000001287"))


def test_if_dates_can_change_2(db_config):
    """ Test of gewijzigde datum daadwerkelijk een ander resultaat oplevert
        ODB Basisveiligheid 6. Afspraken voedselvoorkeuren
    """

    # Setup of the test
    test = QueryTest(db_config)

    # Configuration and execution
    test.set_query('Gevalideerde_vragen_technisch/basisveiligheid/Indicator 6.rq')

    test.change_reference_date_to("2023-03-01", "2024-03-01")

    # Assertions
    test.verify_value("indicator","Waarneming kleiner dan 10")

# Tests with generated data

# Testcase 01

def test_if_value_returned_is_correct_for_query_6_01(db_config):
    """ Testcase 01 (Geen Wlz + Geen Voedselvoorkeur afspraken + Geen vestiging)
        ODB Basisveiligheid 6. Afspraken voedselvoorkeuren
    """

    # Load defined test data
    dg = DataGenerator(db_config, td_01)

    try:
        # Start a test
        test = QueryTest(db_config)

        # Set the query to be tested
        test.set_query('Gevalideerde_vragen_technisch/basisveiligheid/Indicator 6.rq')

        # Set parameters
        test.change_reference_date_to("2023-03-01", "2024-03-01")

        # Verify actual result of the query
        test.verify_value("indicator", "Waarneming kleiner dan 10")


    finally:
        # Delete previously loaded data
        dg.delete_graph_data()

# Testcase 01a

def test_if_value_returned_is_correct_for_query_6_01_a(db_config):
    """ Testcase 01a (Geen Wlz + Geen Voedselvoorkeur afspraken + Vestiging nr. 1287)
        ODB Basisveiligheid 6. Afspraken voedselvoorkeuren
    """

    # Load defined test data
    dg = DataGenerator(db_config, td_01_a)

    try:
        # Start a test
        test = QueryTest(db_config)

        # Set the query to be tested
        test.set_query('Gevalideerde_vragen_technisch/basisveiligheid/Indicator 6.rq')

        # Set parameters
        test.change_reference_date_to("2023-03-01", "2024-03-01")

        # Verify actual result of the query
        test.verify_value("indicator", "Waarneming kleiner dan 10")


    finally:
        # Delete previously loaded data
        dg.delete_graph_data()

# Testcase 01b

def test_if_value_returned_is_correct_for_query_6_01_b(db_config):
    """ Testcase 01b (Geen Wlz + Geen Voedselvoorkeur afspraken + Vestiging nr. 1254)
        ODB Basisveiligheid 6. Afspraken voedselvoorkeuren
    """

    # Load defined test data
    dg = DataGenerator(db_config, td_01_b)

    try:
        # Start a test
        test = QueryTest(db_config)

        # Set the query to be tested
        test.set_query('Gevalideerde_vragen_technisch/basisveiligheid/Indicator 6.rq')

        # Set parameters
        test.change_reference_date_to("2023-03-01", "2024-03-01")

        # Verify actual result of the query
        test.verify_value("indicator", "Waarneming kleiner dan 10")


    finally:
        # Delete previously loaded data
        dg.delete_graph_data()

# Testcase 02

def test_if_value_returned_is_correct_for_query_6_02(db_config):
    """ Testcase 02 (Wlz indicatie 1 t/m 3 (1VV) + Geen Voedselvoorkeur afspraken + Geen vestiging)
        ODB Basisveiligheid 6. Afspraken voedselvoorkeuren
    """

    # Load defined test data
    dg = DataGenerator(db_config, td_02)

    try:
        # Start a test
        test = QueryTest(db_config)

        # Set the query to be tested
        test.set_query('Gevalideerde_vragen_technisch/basisveiligheid/Indicator 6.rq')

        # Set parameters
        test.change_reference_date_to("2023-03-01", "2024-03-01")

        # Verify actual result of the query
        test.verify_value("indicator", "Waarneming kleiner dan 10")


    finally:
        # Delete previously loaded data
        dg.delete_graph_data()

# Testcase 02a

def test_if_value_returned_is_correct_for_query_6_02_a(db_config):
    """ Testcase 02a (Wlz indicatie 1 t/m 3 (1VV) + Geen Voedselvoorkeur afspraken + Vestiging nr. 1287)
        ODB Basisveiligheid 6. Afspraken voedselvoorkeuren
    """

    # Load defined test data
    dg = DataGenerator(db_config, td_02_a)

    try:
        # Start a test
        test = QueryTest(db_config)

        # Set the query to be tested
        test.set_query('Gevalideerde_vragen_technisch/basisveiligheid/Indicator 6.rq')

        # Set parameters
        test.change_reference_date_to("2023-03-01", "2024-03-01")

        # Verify actual result of the query
        test.verify_value("indicator", "Waarneming kleiner dan 10")


    finally:
        # Delete previously loaded data
        dg.delete_graph_data()

# Testcase 02b

def test_if_value_returned_is_correct_for_query_6_02_b(db_config):
    """ Testcase 02b (Wlz indicatie 1 t/m 3 (1VV) + Geen Voedselvoorkeur afspraken + Vestiging nr. 1254)
        ODB Basisveiligheid 6. Afspraken voedselvoorkeuren
    """

    # Load defined test data
    dg = DataGenerator(db_config, td_02_b)

    try:
        # Start a test
        test = QueryTest(db_config)

        # Set the query to be tested
        test.set_query('Gevalideerde_vragen_technisch/basisveiligheid/Indicator 6.rq')

        # Set parameters
        test.change_reference_date_to("2023-03-01", "2024-03-01")

        # Verify actual result of the query
        test.verify_value("indicator", "Waarneming kleiner dan 10")


    finally:
        # Delete previously loaded data
        dg.delete_graph_data()

# Testcase 03

def test_if_value_returned_is_correct_for_query_6_03(db_config):
    """ Testcase 03 (Wlz indicatie 4 t/m 10 (4VV) + Geen Voedselvoorkeur afspraken + Geen vestiging)
        ODB Basisveiligheid 6. Afspraken voedselvoorkeuren
    """

    # Load defined test data
    dg = DataGenerator(db_config, td_03)

    try:
        # Start a test
        test = QueryTest(db_config)

        # Set the query to be tested
        test.set_query('Gevalideerde_vragen_technisch/basisveiligheid/Indicator 6.rq')

        # Set parameters
        test.change_reference_date_to("2023-03-01", "2024-03-01")

        # Verify actual result of the query
        test.verify_value("indicator", "Waarneming kleiner dan 10")


    finally:
        # Delete previously loaded data
        dg.delete_graph_data()

# Testcase 03a

def test_if_value_returned_is_correct_for_query_6_03_a(db_config):
    """ Testcase 03a (Wlz indicatie 4 t/m 10 (4VV) + Geen Voedselvoorkeur afspraken + Vestiging nr. 1287)
        ODB Basisveiligheid 6. Afspraken voedselvoorkeuren
    """

    # Load defined test data
    dg = DataGenerator(db_config, td_03_a)

    try:
        # Start a test
        test = QueryTest(db_config)

        # Set the query to be tested
        test.set_query('Gevalideerde_vragen_technisch/basisveiligheid/Indicator 6.rq')

        # Set parameters
        test.change_reference_date_to("2023-03-01", "2024-03-01")

        # Verify actual result of the query
        test.verify_value("indicator", "Waarneming kleiner dan 10")


    finally:
        # Delete previously loaded data
        dg.delete_graph_data()

# Testcase 03b

def test_if_value_returned_is_correct_for_query_6_03_b(db_config):
    """ Testcase 03b (Wlz indicatie 4 t/m 10 (4VV) + Geen Voedselvoorkeur afspraken + Vestiging nr. 1254)
        ODB Basisveiligheid 6. Afspraken voedselvoorkeuren
    """

    # Load defined test data
    dg = DataGenerator(db_config, td_03_b)

    try:
        # Start a test
        test = QueryTest(db_config)

        # Set the query to be tested
        test.set_query('Gevalideerde_vragen_technisch/basisveiligheid/Indicator 6.rq')

        # Set parameters
        test.change_reference_date_to("2023-03-01", "2024-03-01")

        # Verify actual result of the query
        test.verify_value("indicator", "Waarneming kleiner dan 10")


    finally:
        # Delete previously loaded data
        dg.delete_graph_data()

# Testcase 04

def test_if_value_returned_is_correct_for_query_6_04(db_config):
    """ Testcase 04 (Wlz indicatie 4 t/m 10 (4VV) + Wel afspraak rondom levenseinde + Geen vestiging)
        ODB Basisveiligheid 6. Afspraken voedselvoorkeuren
    """

    # Load defined test data
    dg = DataGenerator(db_config, td_04)

    try:
        # Start a test
        test = QueryTest(db_config)

        # Set the query to be tested
        test.set_query('Gevalideerde_vragen_technisch/basisveiligheid/Indicator 6.rq')

        # Set parameters
        test.change_reference_date_to("2023-03-01", "2024-03-01")

        # Verify actual result of the query
        test.verify_value("indicator", "92", where_condition=("vestiging","000001287"))


    finally:
        # Delete previously loaded data
        dg.delete_graph_data()

# Testcase 04a

def test_if_value_returned_is_correct_for_query_6_04_a(db_config):
    """ Testcase 04a (Wlz indicatie 4 t/m 10 (4VV) + Wel afspraak rondom levenseinde + Vestiging nr. 1287)
        ODB Basisveiligheid 6. Afspraken voedselvoorkeuren
    """

    # Load defined test data
    dg = DataGenerator(db_config, td_04_a)

    try:
        # Start a test
        test = QueryTest(db_config)

        # Set the query to be tested
        test.set_query('Gevalideerde_vragen_technisch/basisveiligheid/Indicator 6.rq')

        # Set parameters
        test.change_reference_date_to("2023-03-01", "2024-03-01")

        # Verify actual result of the query
        test.verify_value("indicator", "92", where_condition=("vestiging","000001287"))


    finally:
        # Delete previously loaded data
        dg.delete_graph_data()

# Testcase 04b

def test_if_value_returned_is_correct_for_query_6_04_b(db_config):
    """ Testcase 04b (Wlz indicatie 4 t/m 10 (4VV) + Wel afspraak rondom levenseinde + Vestiging nr. 1254)
        ODB Basisveiligheid 6. Afspraken voedselvoorkeuren
    """

    # Load defined test data
    dg = DataGenerator(db_config, td_04_b)

    try:
        # Start a test
        test = QueryTest(db_config)

        # Set the query to be tested
        test.set_query('Gevalideerde_vragen_technisch/basisveiligheid/Indicator 6.rq')

        # Set parameters
        test.change_reference_date_to("2023-03-01", "2024-03-01")

        # Verify actual result of the query
        test.verify_value("indicator", "100", where_condition=("vestiging","000001254"))


    finally:
        # Delete previously loaded data
        dg.delete_graph_data()

# Testcase 04c

def test_if_value_returned_is_correct_for_query_6_04_c(db_config):
    """ Testcase 04c (Wlz indicatie 4 t/m 10 (4VV) + Meerdere afspraken rondom levenseinde + Vestiging nr. 1254)
        ODB Basisveiligheid 6. Afspraken voedselvoorkeuren
    """

    # Load defined test data
    dg = DataGenerator(db_config, td_04_c)

    try:
        # Start a test
        test = QueryTest(db_config)

        # Set the query to be tested
        test.set_query('Gevalideerde_vragen_technisch/basisveiligheid/Indicator 6.rq')

        # Set parameters
        test.change_reference_date_to("2023-03-01", "2024-03-01")

        # Verify actual result of the query
        test.verify_value("indicator", "100", where_condition=("vestiging","000001254"))


    finally:
        # Delete previously loaded data
        dg.delete_graph_data()