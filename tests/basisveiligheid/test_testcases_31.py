from DataGenerator import DataGenerator
from QueryTest import QueryTest

# Opmerkingen: 

# Testcases:

# Testcase 01

td_01 = [
    {
        "Description": "Testcase 01 (Geen Wlz Indicatie + Geen vestiging + Geen Medicatiefout + Geen Medicatie fout meetings)", 
        "Amount": 1,
        "Human": [
            {
                "NursingProcess": [
                    {
                        "identifier": "1",
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",
                        "location": "",
                        "related_indication": "1"
                    },

                ],
                "Indication": [
                    {
                        "identifier": "1",
                        "type": "WlzIndicatie",
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",
                        "heeftIndicatieBehandeling": True,
                        "ciz": [""] ,
                        "leveringsvorm": [""]
                    }
                ]
            }
        ]
    }
]

td_02 = [
    {
        "Description": "Testcase 02 (Wlz Indicatie 1 t/m 3 (1VV) + Geen vestiging + Geen Medicatiefout + Geen Medicatie fout meetings)", 
        "Amount": 1,
        "Human": [
            {
                "NursingProcess": [
                    {
                        "identifier": "1",
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",
                        "location": "",
                        "related_indication": "1"
                    },

                ],
                "Indication": [
                    {
                        "identifier": "1",
                        "type": "WlzIndicatie",
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",
                        "heeftIndicatieBehandeling": True,
                        "ciz": ["1VV"] ,
                        "leveringsvorm": [""]
                    }
                ]
            }
        ]
    }
]

td_02_a = [
    {
        "Description": "Testcase 02a (Wlz Indicatie 4 t/m 10 (4VV) + Geen vestiging + Geen Medicatiefout + Geen Medicatie fout meetings)", 
        "Amount": 1,
        "Human": [
            {
                "NursingProcess": [
                    {
                        "identifier": "1",
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",
                        "location": "",
                        "related_indication": "1"
                    },

                ],
                "Indication": [
                    {
                        "identifier": "1",
                        "type": "WlzIndicatie",
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",
                        "heeftIndicatieBehandeling": True,
                        "ciz": ["4VV"] ,
                        "leveringsvorm": [""]
                    }
                ]
            }
        ]
    }
]

td_03 = [
    {
        "Description": "Testcase 03 (Wlz Indicatie 4 t/m 10 (4VV) + Wel vestiging (Vest. nr. 1287) + Geen Medicatiefout + Geen Medicatie fout meetings)", 
        "Amount": 1,
        "Human": [
            {
                "NursingProcess": [
                    {
                        "identifier": "1",
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",
                        "location": "Locatie_Grotestraat_17",
                        "related_indication": "1"
                    },

                ],
                "Indication": [
                    {
                        "identifier": "1",
                        "type": "WlzIndicatie",
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",
                        "heeftIndicatieBehandeling": True,
                        "ciz": ["4VV"] ,
                        "leveringsvorm": [""]
                    }
                ]
            }
        ]
    }
]

td_03_a = [
    {
        "Description": "Testcase 03a (Wlz Indicatie 4 t/m 10 (4VV) + Wel vestiging (Vest. nr. 1254) + Geen Medicatiefout + Geen Medicatie fout meetings)", 
        "Amount": 1,
        "Human": [
            {
                "NursingProcess": [
                    {
                        "identifier": "1",
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",
                        "location": "Locatie_De_Beuk_1",
                        "related_indication": "1"
                    },

                ],
                "Indication": [
                    {
                        "identifier": "1",
                        "type": "WlzIndicatie",
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",
                        "heeftIndicatieBehandeling": True,
                        "ciz": ["4VV"] ,
                        "leveringsvorm": [""]
                    }
                ]
            }
        ]
    }
]

td_04 = [
    {
        "Description": "Testcase 04 (Wlz Indicatie 4 t/m 10 (4VV) + Wel vestiging (Vest. nr. 1287) + Wel Medicatiefout + Geen Medicatie fout meetings)", 
        "Amount": 1,
        "Human": [
            {
                "ReportMedicationFault": [
                    {
                        "date": "2024-01-01",
                        "medication_fault_meetings": [
                            {
                                "date": "",
                                "location": ""
                            },                       
                        ]
                    },
                ],
                "NursingProcess": [
                    {
                        "identifier": "1",
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",
                        "location": "Locatie_Grotestraat_17",
                        "related_indication": "1"
                    },

                ],
                "Indication": [
                    {
                        "identifier": "1",
                        "type": "WlzIndicatie",
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",
                        "heeftIndicatieBehandeling": True,
                        "ciz": ["4VV"] ,
                        "leveringsvorm": [""]
                    }
                ]
            }
        ]
    }
]

td_04_a = [
    {
        "Description": "Testcase 04 (Wlz Indicatie 4 t/m 10 (4VV) + Wel vestiging (Vest. nr. 1254) + Wel Medicatiefout + Geen Medicatie fout meetings)", 
        "Amount": 1,
        "Human": [
            {
                "ReportMedicationFault": [
                    {
                        "date": "2024-01-01",
                        "medication_fault_meetings": [
                            {
                                "date": "",
                                "location": ""
                            },                       
                        ]
                    },
                ],
                "NursingProcess": [
                    {
                        "identifier": "1",
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",
                        "location": "Locatie_De_Beuk_1",
                        "related_indication": "1"
                    },

                ],
                "Indication": [
                    {
                        "identifier": "1",
                        "type": "WlzIndicatie",
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",
                        "heeftIndicatieBehandeling": True,
                        "ciz": ["4VV"] ,
                        "leveringsvorm": [""]
                    }
                ]
            }
        ]
    }
]

td_05 = [
    {
        "Description": "Testcase 05 (Wlz Indicatie 4 t/m 10 (4VV) + Wel vestiging (Vest. nr. 1287) + Wel Medicatiefout + Wel Medicatie fout meetings (<4))", 
        "Amount": 1,
        "Human": [
            {
                "ReportMedicationFault": [
                    {
                        "date": "2024-01-01",
                        "medication_fault_meetings": [
                            {
                                "date": "2024-01-02",
                                "location": "Locatie_Grotestraat_17"
                            },   
                            {
                                "date": "2024-04-02",
                                "location": "Locatie_Grotestraat_17"
                            },
                            {
                                "date": "2024-07-02",
                                "location": "Locatie_Grotestraat_17"
                            },                                                                           
                        ]
                    },
                ],
                "NursingProcess": [
                    {
                        "identifier": "1",
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",
                        "location": "Locatie_Grotestraat_17",
                        "related_indication": "1"
                    },

                ],
                "Indication": [
                    {
                        "identifier": "1",
                        "type": "WlzIndicatie",
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",
                        "heeftIndicatieBehandeling": True,
                        "ciz": ["4VV"] ,
                        "leveringsvorm": [""]
                    }
                ]
            }
        ]
    }
]

td_05_a = [
    {
        "Description": "Testcase 05a (Wlz Indicatie 4 t/m 10 (4VV) + Wel vestiging (Vest. nr. 1254) + Wel Medicatiefout + Wel Medicatie fout meeting (<4))", 
        "Amount": 1,
        "Human": [
            {
                "ReportMedicationFault": [
                    {
                        "date": "2024-01-01",
                        "medication_fault_meetings": [
                            {
                                "date": "2024-01-02",
                                "location": "Locatie_De_Beuk_1"
                            },   
                            {
                                "date": "2024-04-02",
                                "location": "Locatie_De_Beuk_1"
                            },
                            {
                                "date": "2024-07-02",
                                "location": "Locatie_De_Beuk_1"
                            },            
                        ]
                    },
                ],
                "NursingProcess": [
                    {
                        "identifier": "1",
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",
                        "location": "Locatie_De_Beuk_1",
                        "related_indication": "1"
                    },
                ],
                "Indication": [
                    {
                        "identifier": "1",
                        "type": "WlzIndicatie",
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",
                        "heeftIndicatieBehandeling": True,
                        "ciz": ["4VV"] ,
                        "leveringsvorm": [""]
                    }
                ]
            }
        ]
    }
]

td_05_b = [
    {
        "Description": "Testcase 05b (Wlz Indicatie 4 t/m 10 (4VV) + Wel vestiging (Vest. nr. 1287) + Wel Medicatiefout + Wel Medicatie fout meetings (4))", 
        "Amount": 1,
        "Human": [
            {
                "ReportMedicationFault": [
                    {
                        "date": "2024-01-01",
                        "medication_fault_meetings": [
                            {
                                "date": "2024-01-02",
                                "location": "Locatie_Grotestraat_17"
                            },   
                            {
                                "date": "2024-04-02",
                                "location": "Locatie_Grotestraat_17"
                            },
                            {
                                "date": "2024-07-02",
                                "location": "Locatie_Grotestraat_17"
                            },
                            {
                                "date": "2024-10-02",
                                "location": "Locatie_Grotestraat_17"
                            },                                                                                                       
                        ]
                    },
                ],
                "NursingProcess": [
                    {
                        "identifier": "1",
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",
                        "location": "Locatie_Grotestraat_17",
                        "related_indication": "1"
                    },

                ],
                "Indication": [
                    {
                        "identifier": "1",
                        "type": "WlzIndicatie",
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",
                        "heeftIndicatieBehandeling": True,
                        "ciz": ["4VV"] ,
                        "leveringsvorm": [""]
                    }
                ]
            }
        ]
    }
]

td_05_c = [
    {
        "Description": "Testcase 05c (Wlz Indicatie 4 t/m 10 (4VV) + Wel vestiging (Vest. nr. 1254) + Wel Medicatiefout + Wel Medicatie fout meeting (4))", 
        "Amount": 1,
        "Human": [
            {
                "ReportMedicationFault": [
                    {
                        "date": "2024-01-01",
                        "medication_fault_meetings": [
                            {
                                "date": "2024-01-02",
                                "location": "Locatie_De_Beuk_1"
                            },   
                            {
                                "date": "2024-04-02",
                                "location": "Locatie_De_Beuk_1"
                            },
                            {
                                "date": "2024-07-02",
                                "location": "Locatie_De_Beuk_1"
                            },            
                            {
                                "date": "2024-10-02",
                                "location": "Locatie_De_Beuk_1"
                            },                                        
                        ]
                    },
                ],
                "NursingProcess": [
                    {
                        "identifier": "1",
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",
                        "location": "Locatie_De_Beuk_1",
                        "related_indication": "1"
                    },

                ],
                "Indication": [
                    {
                        "identifier": "1",
                        "type": "WlzIndicatie",
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",
                        "heeftIndicatieBehandeling": True,
                        "ciz": ["4VV"] ,
                        "leveringsvorm": [""]
                    }
                ]
            }
        ]
    }
]

td_05_d = [
    {
        "Description": "Testcase 05c (Wlz Indicatie 4 t/m 10 (4VV) + 2 vestigingen (Vest. nr. 1287 & 1254) + Wel Medicatiefout + Wel Medicatie fout meeting (4))", 
        "Amount": 1,
        "Human": [
            {
                "ReportMedicationFault": [
                    {
                        "date": "2024-01-01",
                        "medication_fault_meetings": [
                            {
                                "date": "2024-01-02",
                                "location": "Locatie_De_Beuk_1"
                            },   
                            {
                                "date": "2024-04-02",
                                "location": "Locatie_De_Beuk_1"
                            },
                            {
                                "date": "2024-07-02",
                                "location": "Locatie_Grotestraat_17"
                            },            
                            {
                                "date": "2024-10-02",
                                "location": "Locatie_Grotestraat_17"
                            },                                        
                        ]
                    },
                ],
                "NursingProcess": [
                    {
                        "identifier": "1",
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",
                        "location": "Locatie_De_Beuk_1",
                        "related_indication": "1"
                    },
                    {
                        "identifier": "2",
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",
                        "location": "Locatie_Grotestraat_17",
                        "related_indication": "1"
                    },
                ],
                "Indication": [
                    {
                        "identifier": "1",
                        "type": "WlzIndicatie",
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",
                        "heeftIndicatieBehandeling": True,
                        "ciz": ["4VV"] ,
                        "leveringsvorm": [""]
                    }
                ]
            }
        ]
    }
]

td_06 = [
    {
        "Description": "Testcase 06 (Wlz Indicatie 1 t/m 3 (1VV) + Wel vestiging (Vest. nr. 1287) + Wel Medicatiefout + Wel Medicatie fout meetings (<4))", 
        "Amount": 1,
        "Human": [
            {
                "ReportMedicationFault": [
                    {
                        "date": "2024-01-01",
                        "medication_fault_meetings": [
                            {
                                "date": "2024-01-02",
                                "location": "Locatie_Grotestraat_17"
                            },   
                            {
                                "date": "2024-04-02",
                                "location": "Locatie_Grotestraat_17"
                            },
                            {
                                "date": "2024-07-02",
                                "location": "Locatie_Grotestraat_17"
                            },                                                                           
                        ]
                    },
                ],
                "NursingProcess": [
                    {
                        "identifier": "1",
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",
                        "location": "Locatie_Grotestraat_17",
                        "related_indication": "1"
                    },

                ],
                "Indication": [
                    {
                        "identifier": "1",
                        "type": "WlzIndicatie",
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",
                        "heeftIndicatieBehandeling": True, 
                        "ciz": ["4VV"] ,
                        "leveringsvorm": [""]
                    }
                ]
            }
        ]
    }
]

td_06_a = [
    {
        "Description": "Testcase 06a (Wlz Indicatie 1 t/m 3 (1VV) + Wel vestiging (Vest. nr. 1254) + Wel Medicatiefout + Wel Medicatie fout meeting (<4))", 
        "Amount": 1,
        "Human": [
            {
                "ReportMedicationFault": [
                    {
                        "date": "2024-01-01",
                        "medication_fault_meetings": [
                            {
                                "date": "2024-01-02",
                                "location": "Locatie_De_Beuk_1"
                            },   
                            {
                                "date": "2024-04-02",
                                "location": "Locatie_De_Beuk_1"
                            },
                            {
                                "date": "2024-07-02",
                                "location": "Locatie_De_Beuk_1"
                            },            
                        ]
                    },
                ],
                "NursingProcess": [
                    {
                        "identifier": "1",
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",
                        "location": "Locatie_De_Beuk_1",
                        "related_indication": "1"
                    },
                ],
                "Indication": [
                    {
                        "identifier": "1",
                        "type": "WlzIndicatie",
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",
                        "heeftIndicatieBehandeling": True,
                        "ciz": ["1VV"] ,
                        "leveringsvorm": [""]
                    }
                ]
            }
        ]
    }
]

td_06_b = [
    {
        "Description": "Testcase 06b (Wlz Indicatie 1 t/m 3 (1VV) + Wel vestiging (Vest. nr. 1287) + Wel Medicatiefout + Wel Medicatie fout meetings (4))", 
        "Amount": 1,
        "Human": [
            {
                "ReportMedicationFault": [
                    {
                        "date": "2024-01-01",
                        "medication_fault_meetings": [
                            {
                                "date": "2024-01-02",
                                "location": "Locatie_Grotestraat_17"
                            },   
                            {
                                "date": "2024-04-02",
                                "location": "Locatie_Grotestraat_17"
                            },
                            {
                                "date": "2024-07-02",
                                "location": "Locatie_Grotestraat_17"
                            },
                            {
                                "date": "2024-10-02",
                                "location": "Locatie_Grotestraat_17"
                            },                                                                                                       
                        ]
                    },
                ],
                "NursingProcess": [
                    {
                        "identifier": "1",
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",
                        "location": "Locatie_Grotestraat_17",
                        "related_indication": "1"
                    },

                ],
                "Indication": [
                    {
                        "identifier": "1",
                        "type": "WlzIndicatie",
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",
                        "heeftIndicatieBehandeling": True,
                        "ciz": ["1VV"] ,
                        "leveringsvorm": [""]
                    }
                ]
            }
        ]
    }
]

td_06_c = [
    {
        "Description": "Testcase 06c (Wlz Indicatie 1 t/m 3 (1VV) + Wel vestiging (Vest. nr. 1254) + Wel Medicatiefout + Wel Medicatie fout meeting (4))", 
        "Amount": 1,
        "Human": [
            {
                "ReportMedicationFault": [
                    {
                        "date": "2024-01-01",
                        "medication_fault_meetings": [
                            {
                                "date": "2024-01-02",
                                "location": "Locatie_De_Beuk_1"
                            },   
                            {
                                "date": "2024-04-02",
                                "location": "Locatie_De_Beuk_1"
                            },
                            {
                                "date": "2024-07-02",
                                "location": "Locatie_De_Beuk_1"
                            },            
                            {
                                "date": "2024-10-02",
                                "location": "Locatie_De_Beuk_1"
                            },                                        
                        ]
                    },
                ],
                "NursingProcess": [
                    {
                        "identifier": "1",
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",
                        "location": "Locatie_De_Beuk_1",
                        "related_indication": "1"
                    },

                ],
                "Indication": [
                    {
                        "identifier": "1",
                        "type": "WlzIndicatie",
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",
                        "heeftIndicatieBehandeling": True,
                        "ciz": ["1VV"] ,
                        "leveringsvorm": [""]
                    }
                ]
            }
        ]
    }
]

td_06_d = [
    {
        "Description": "Testcase 06d (Wlz Indicatie 1 t/m 3 (1VV) + 2 vestigingen (Vest. nr. 1287 & 1254) + Wel Medicatiefout + Wel Medicatie fout meeting (4))", 
        "Amount": 1,
        "Human": [
            {
                "ReportMedicationFault": [
                    {
                        "date": "2024-01-01",
                        "medication_fault_meetings": [
                            {
                                "date": "2024-01-02",
                                "location": "Locatie_De_Beuk_1"
                            },   
                            {
                                "date": "2024-04-02",
                                "location": "Locatie_De_Beuk_1"
                            },
                            {
                                "date": "2024-07-02",
                                "location": "Locatie_Grotestraat_17"
                            },            
                            {
                                "date": "2024-10-02",
                                "location": "Locatie_Grotestraat_17"
                            },                                        
                        ]
                    },
                ],
                "NursingProcess": [
                    {
                        "identifier": "1",
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",
                        "location": "Locatie_De_Beuk_1",
                        "related_indication": "1"
                    },
                    {
                        "identifier": "2",
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",
                        "location": "Locatie_Grotestraat_17",
                        "related_indication": "1"
                    },
                ],
                "Indication": [
                    {
                        "identifier": "1",
                        "type": "WlzIndicatie",
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",
                        "heeftIndicatieBehandeling": True,
                        "ciz": ["1VV"] ,
                        "leveringsvorm": [""]
                    }
                ]
            }
        ]
    }
]

td_07 = [
    {
        "Description": "Testcase 07 (Geen Wlz Indicatie + Wel vestiging (Vest. nr. 1287) + Wel Medicatiefout + Wel Medicatie fout meetings (<4))", 
        "Amount": 1,
        "Human": [
            {
                "ReportMedicationFault": [
                    {
                        "date": "2024-01-01",
                        "medication_fault_meetings": [
                            {
                                "date": "2024-01-02",
                                "location": "Locatie_Grotestraat_17"
                            },   
                            {
                                "date": "2024-04-02",
                                "location": "Locatie_Grotestraat_17"
                            },
                            {
                                "date": "2024-07-02",
                                "location": "Locatie_Grotestraat_17"
                            },                                                                           
                        ]
                    },
                ],
                "NursingProcess": [
                    {
                        "identifier": "1",
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",
                        "location": "Locatie_Grotestraat_17",
                        "related_indication": "1"
                    },

                ],
                "Indication": [
                    {
                        "identifier": "1",
                        "type": "WlzIndicatie",
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",
                        "heeftIndicatieBehandeling": True,
                        "ciz": ["4VV"] ,
                        "leveringsvorm": [""]
                    }
                ]
            }
        ]
    }
]

td_07_a = [
    {
        "Description": "Testcase 07a (Geen Wlz Indicatie + Wel vestiging (Vest. nr. 1254) + Wel Medicatiefout + Wel Medicatie fout meeting (<4))", 
        "Amount": 1,
        "Human": [
            {
                "ReportMedicationFault": [
                    {
                        "date": "2024-01-01",
                        "medication_fault_meetings": [
                            {
                                "date": "2024-01-02",
                                "location": "Locatie_De_Beuk_1"
                            },   
                            {
                                "date": "2024-04-02",
                                "location": "Locatie_De_Beuk_1"
                            },
                            {
                                "date": "2024-07-02",
                                "location": "Locatie_De_Beuk_1"
                            },            
                        ]
                    },
                ],
                "NursingProcess": [
                    {
                        "identifier": "1",
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",
                        "location": "Locatie_De_Beuk_1",
                        "related_indication": "1"
                    },
                ],
                "Indication": [
                    {
                        "identifier": "1",
                        "type": "WlzIndicatie",
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",
                        "heeftIndicatieBehandeling": True,
                        "ciz": [""] ,
                        "leveringsvorm": [""]
                    }
                ]
            }
        ]
    }
]

td_07_b = [
    {
        "Description": "Testcase 07b (Geen Wlz Indicatie + Wel vestiging (Vest. nr. 1287) + Wel Medicatiefout + Wel Medicatie fout meetings (4))", 
        "Amount": 1,
        "Human": [
            {
                "ReportMedicationFault": [
                    {
                        "date": "2024-01-01",
                        "medication_fault_meetings": [
                            {
                                "date": "2024-01-02",
                                "location": "Locatie_Grotestraat_17"
                            },   
                            {
                                "date": "2024-04-02",
                                "location": "Locatie_Grotestraat_17"
                            },
                            {
                                "date": "2024-07-02",
                                "location": "Locatie_Grotestraat_17"
                            },
                            {
                                "date": "2024-10-02",
                                "location": "Locatie_Grotestraat_17"
                            },                                                                                                       
                        ]
                    },
                ],
                "NursingProcess": [
                    {
                        "identifier": "1",
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",
                        "location": "Locatie_Grotestraat_17",
                        "related_indication": "1"
                    },

                ],
                "Indication": [
                    {
                        "identifier": "1",
                        "type": "WlzIndicatie",
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",
                        "heeftIndicatieBehandeling": True,
                        "ciz": [""] ,
                        "leveringsvorm": [""]
                    }
                ]
            }
        ]
    }
]

td_07_c = [
    {
        "Description": "Testcase 07c (Geen Wlz Indicatie + Wel vestiging (Vest. nr. 1254) + Wel Medicatiefout + Wel Medicatie fout meeting (4))", 
        "Amount": 1,
        "Human": [
            {
                "ReportMedicationFault": [
                    {
                        "date": "2024-01-01",
                        "medication_fault_meetings": [
                            {
                                "date": "2024-01-02",
                                "location": "Locatie_De_Beuk_1"
                            },   
                            {
                                "date": "2024-04-02",
                                "location": "Locatie_De_Beuk_1"
                            },
                            {
                                "date": "2024-07-02",
                                "location": "Locatie_De_Beuk_1"
                            },            
                            {
                                "date": "2024-10-02",
                                "location": "Locatie_De_Beuk_1"
                            },                                        
                        ]
                    },
                ],
                "NursingProcess": [
                    {
                        "identifier": "1",
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",
                        "location": "Locatie_De_Beuk_1",
                        "related_indication": "1"
                    },

                ],
                "Indication": [
                    {
                        "identifier": "1",
                        "type": "WlzIndicatie",
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",
                        "heeftIndicatieBehandeling": True,
                        "ciz": [""] ,
                        "leveringsvorm": [""]
                    }
                ]
            }
        ]
    }
]

td_07_d = [
    {
        "Description": "Testcase 07d (Geen Wlz Indicatie + 2 vestigingen (Vest. nr. 1287 & 1254) + Wel Medicatiefout + Wel Medicatie fout meeting (4))", 
        "Amount": 1,
        "Human": [
            {
                "ReportMedicationFault": [
                    {
                        "date": "2024-01-01",
                        "medication_fault_meetings": [
                            {
                                "date": "2024-01-02",
                                "location": "Locatie_De_Beuk_1"
                            },   
                            {
                                "date": "2024-04-02",
                                "location": "Locatie_De_Beuk_1"
                            },
                            {
                                "date": "2024-07-02",
                                "location": "Locatie_Grotestraat_17"
                            },            
                            {
                                "date": "2024-10-02",
                                "location": "Locatie_Grotestraat_17"
                            },                                        
                        ]
                    },
                ],
                "NursingProcess": [
                    {
                        "identifier": "1",
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",
                        "location": "Locatie_De_Beuk_1",
                        "related_indication": "1"
                    },
                    {
                        "identifier": "2",
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",
                        "location": "Locatie_Grotestraat_17",
                        "related_indication": "1"
                    },
                ],
                "Indication": [
                    {
                        "identifier": "1",
                        "type": "WlzIndicatie",
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",
                        "heeftIndicatieBehandeling": True,
                        "ciz": [""] ,
                        "leveringsvorm": [""]
                    }
                ]
            }
        ]
    }
]

td_08 = [
    {
        "Description": "Testcase 08 (Geen Wlz Indicatie + Geen vestiging + Wel Medicatiefout + Wel Medicatie fout meetings (<4))", 
        "Amount": 1,
        "Human": [
            {
                "ReportMedicationFault": [
                    {
                        "date": "2024-01-01",
                        "medication_fault_meetings": [
                            {
                                "date": "2024-01-02",
                                "location": ""
                            },   
                            {
                                "date": "2024-04-02",
                                "location": ""
                            },
                            {
                                "date": "2024-07-02",
                                "location": ""
                            },                                                                           
                        ]
                    },
                ],
                "NursingProcess": [
                    {
                        "identifier": "1",
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",
                        "location": "",
                        "related_indication": "1"
                    },

                ],
                "Indication": [
                    {
                        "identifier": "1",
                        "type": "WlzIndicatie",
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",
                        "heeftIndicatieBehandeling": True,
                        "ciz": ["4VV"] ,
                        "leveringsvorm": [""]
                    }
                ]
            }
        ]
    }
]

td_08_a = [
    {
        "Description": "Testcase 08a (Geen Wlz Indicatie + Geen vestiging + Wel Medicatiefout + Wel Medicatie fout meetings (4))", 
        "Amount": 1,
        "Human": [
            {
                "ReportMedicationFault": [
                    {
                        "date": "2024-01-01",
                        "medication_fault_meetings": [
                            {
                                "date": "2024-01-02",
                                "location": ""
                            },   
                            {
                                "date": "2024-04-02",
                                "location": ""
                            },
                            {
                                "date": "2024-07-02",
                                "location": ""
                            },
                            {
                                "date": "2024-10-02",
                                "location": ""
                            },                
                        ]
                    },
                ],
                "NursingProcess": [
                    {
                        "identifier": "1",
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",
                        "location": "",
                        "related_indication": "1"
                    },
                ],
                "Indication": [
                    {
                        "identifier": "1",
                        "type": "WlzIndicatie",
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",
                        "heeftIndicatieBehandeling": True,
                        "ciz": [""] ,
                        "leveringsvorm": [""]
                    }
                ]
            }
        ]
    }
]

# Static tests

def test_if_headers_are_correct_on_3_1(db_config):
    """ Test of de juiste headers terugkomen in het resultaat
        ODB Basisveiligheid 3.1. Bespreken medicatiefouten in het team
    """

    # Setup of the test
    test = QueryTest(db_config)

    # Configuration and execution
    test.set_query('Gevalideerde_vragen_technisch/basisveiligheid/Indicator 3.1.rq')

    # Assertions
    test.verify_header_present('teller')
    test.verify_header_present('noemer')
    # test.verify_header_present('indicator') Indicator value is 'None' 


def test_if_number_of_rows_returned_is_correct_for_query_3_1(db_config):
    """ Test of het aantal rijen correct wordt teruggegeven
        ODB Basisveiligheid 3.1. Bespreken medicatiefouten in het team
    """

    # Setup of the test
    test = QueryTest(db_config)

    # Configuration and execution
    test.set_query('Gevalideerde_vragen_technisch/basisveiligheid/Indicator 3.1.rq')

    # Assertions
    test.verify_row_count(1)


def test_if_indicator_has_correct_value_for_query_3_1(db_config):
    """ Test of de indicator de juiste waarde heeft
        ODB Basisveiligheid 3.1. Bespreken medicatiefouten in het team  
    """

    # Setup of the test
    test = QueryTest(db_config)

    # Configuration and execution
    test.set_query('Gevalideerde_vragen_technisch/basisveiligheid/Indicator 3.1.rq')

    # Assertions
    test.verify_value_in_list("teller",{"0","0.0"})


def test_if_dates_can_change_for_query_3_1(db_config):
    """ Test of gewijzigde datum daadwerkelijk een ander resultaat oplevert
        ODB Basisveiligheid 3.1. Bespreken medicatiefouten in het team 
    """

    # Setup of the test
    test = QueryTest(db_config)

    # Configuration and execution
    test.set_query('Gevalideerde_vragen_technisch/basisveiligheid/Indicator 3.1.rq')

    test.change_start_period("2023-01-01", "2024-01-01")
    test.change_end_period("2023-10-31", "2024-10-31")

    # Assertions
    test.verify_value_in_list("noemer",{"0","0.0"})

# Tests with generated data

# Testcase 01
def test_if_value_returned_is_correct_for_query_3_1_01(db_config):
    """ Testcase 01 (Geen Wlz Indicatie + Geen vestiging + Geen Medicatiefout + Geen Medicatie fout meetings)
        ODB Basisveiligheid 3.1. Bespreken medicatiefouten in het team
    """

    # Load defined test data
    dg = DataGenerator(db_config, td_01)

    try:
        # Start a test
        test = QueryTest(db_config)

        # Set the query to be tested
        test.set_query('Gevalideerde_vragen_technisch/basisveiligheid/Indicator 3.1.rq')

        # Change measuring period parameters of query
        test.change_start_period("2023-01-01", "2024-01-01")
        test.change_end_period("2023-10-31", "2024-10-31")

        # Verify actual result of the query
        test.verify_value("indicator", None)

    finally:
        # Delete previously loaded data
        dg.delete_graph_data()

# Testcase 02
def test_if_value_returned_is_correct_for_query_3_1_02(db_config):
    """ Testcase 02 (Wlz Indicatie 1 t/m 3 (1VV) + Geen vestiging + Geen Medicatiefout + Geen Medicatie fout meetings)
        ODB Basisveiligheid 3.1. Bespreken medicatiefouten in het team
    """

    # Load defined test data
    dg = DataGenerator(db_config, td_02)

    try:
        # Start a test
        test = QueryTest(db_config)

        # Set the query to be tested
        test.set_query('Gevalideerde_vragen_technisch/basisveiligheid/Indicator 3.1.rq')

        # Change measuring period parameters of query
        test.change_start_period("2023-01-01", "2024-01-01")
        test.change_end_period("2023-10-31", "2024-10-31")

        # Verify actual result of the query
        test.verify_value("indicator", None)

    finally:
        # Delete previously loaded data
        dg.delete_graph_data()

# Testcase 02a
def test_if_value_returned_is_correct_for_query_3_1_02_a(db_config):
    """ Testcase 02a (Wlz Indicatie 4 t/m 10 (4VV) + Geen vestiging + Geen Medicatiefout + Geen Medicatie fout meetings)
        ODB Basisveiligheid 3.1. Bespreken medicatiefouten in het team
    """

    # Load defined test data
    dg = DataGenerator(db_config, td_02_a)

    try:
        # Start a test
        test = QueryTest(db_config)

        # Set the query to be tested
        test.set_query('Gevalideerde_vragen_technisch/basisveiligheid/Indicator 3.1.rq')

        # Change measuring period parameters of query
        test.change_start_period("2023-01-01", "2024-01-01")
        test.change_end_period("2023-10-31", "2024-10-31")

        # Verify actual result of the query
        test.verify_value("indicator", None)

    finally:
        # Delete previously loaded data
        dg.delete_graph_data()

# Testcase 03
def test_if_value_returned_is_correct_for_query_3_1_03(db_config):
    """ Testcase 03 (Wlz Indicatie 4 t/m 10 (4VV) + Wel vestiging (Vest. nr. 1287) + Geen Medicatiefout + Geen Medicatie fout meetings)
        ODB Basisveiligheid 3.1. Bespreken medicatiefouten in het team
    """

    # Load defined test data
    dg = DataGenerator(db_config, td_03)

    try:
        # Start a test
        test = QueryTest(db_config)

        # Set the query to be tested
        test.set_query('Gevalideerde_vragen_technisch/basisveiligheid/Indicator 3.1.rq')

        # Change measuring period parameters of query
        test.change_start_period("2023-01-01", "2024-01-01")
        test.change_end_period("2023-10-31", "2024-10-31")

        # Verify actual result of the query
        test.verify_value("indicator", None)

    finally:
        # Delete previously loaded data
        dg.delete_graph_data()

# Testcase 03a
def test_if_value_returned_is_correct_for_query_3_1_03_a(db_config):
    """ Testcase 03a (Wlz Indicatie 4 t/m 10 (1VV) + Wel vestiging (Vest. nr. 1254) + Geen Medicatiefout + Geen Medicatie fout meetings)
        ODB Basisveiligheid 3.1. Bespreken medicatiefouten in het team
    """

    # Load defined test data
    dg = DataGenerator(db_config, td_03_a)

    try:
        # Start a test
        test = QueryTest(db_config)

        # Set the query to be tested
        test.set_query('Gevalideerde_vragen_technisch/basisveiligheid/Indicator 3.1.rq')

        # Change measuring period parameters of query
        test.change_start_period("2023-01-01", "2024-01-01")
        test.change_end_period("2023-10-31", "2024-10-31")

        # Verify actual result of the query
        test.verify_value("indicator", None)

    finally:
        # Delete previously loaded data
        dg.delete_graph_data()

# Testcase 04
def test_if_value_returned_is_correct_for_query_3_1_04(db_config):
    """ Testcase 04 (Wlz Indicatie 4 t/m 10 (4VV) + Wel vestiging (Vest. nr. 1287) + Wel Medicatiefout + Geen Medicatie fout meetings)
        ODB Basisveiligheid 3.1. Bespreken medicatiefouten in het team
    """

    # Load defined test data
    dg = DataGenerator(db_config, td_04)

    try:
        # Start a test
        test = QueryTest(db_config)

        # Set the query to be tested
        test.set_query('Gevalideerde_vragen_technisch/basisveiligheid/Indicator 3.1.rq')

        # Change measuring period parameters of query
        test.change_start_period("2023-01-01", "2024-01-01")
        test.change_end_period("2023-10-31", "2024-10-31")

        # Verify actual result of the query
        test.verify_value_in_list("indicator", {"0", "0.0"})

    finally:
        # Delete previously loaded data
        dg.delete_graph_data()

# Testcase 04a
def test_if_value_returned_is_correct_for_query_3_1_04_a(db_config):
    """ Testcase 04a (Wlz Indicatie 4 t/m 10 (1VV) + Wel vestiging (Vest. nr. 1254) + Wel Medicatiefout + Geen Medicatie fout meetings)
        ODB Basisveiligheid 3.1. Bespreken medicatiefouten in het team
    """

    # Load defined test data
    dg = DataGenerator(db_config, td_04_a)

    try:
        # Start a test
        test = QueryTest(db_config)

        # Set the query to be tested
        test.set_query('Gevalideerde_vragen_technisch/basisveiligheid/Indicator 3.1.rq')

        # Change measuring period parameters of query
        test.change_start_period("2023-01-01", "2024-01-01")
        test.change_end_period("2023-10-31", "2024-10-31")

        # Verify actual result of the query
        test.verify_value_in_list("indicator", {"0", "0.0"})

    finally:
        # Delete previously loaded data
        dg.delete_graph_data()

# Testcase 05
def test_if_value_returned_is_correct_for_query_3_1_05(db_config):
    """ Testcase 05 (Wlz Indicatie 4 t/m 10 (4VV) + Wel vestiging (Vest. nr. 1287) + Wel Medicatiefout + Wel Medicatie fout meetings (<4))
        ODB Basisveiligheid 3.1. Bespreken medicatiefouten in het team
    """

    # Load defined test data
    dg = DataGenerator(db_config, td_05)

    try:
        # Start a test
        test = QueryTest(db_config)

        # Set the query to be tested
        test.set_query('Gevalideerde_vragen_technisch/basisveiligheid/Indicator 3.1.rq')

        # Change measuring period parameters of query
        test.change_start_period("2023-01-01", "2024-01-01")
        test.change_end_period("2023-10-31", "2024-10-31")

        # Verify actual result of the query
        test.verify_value_in_list("indicator", {"0", "0.0"})

    finally:
        # Delete previously loaded data
        dg.delete_graph_data()

# Testcase 05a
def test_if_value_returned_is_correct_for_query_3_1_05_a(db_config):
    """ Testcase 05a (Wlz Indicatie 4 t/m 10 (4VV) + Wel vestiging (Vest. nr. 1254) + Wel Medicatiefout + Wel Medicatie fout meetings (<4))
        ODB Basisveiligheid 3.1. Bespreken medicatiefouten in het team
    """

    # Load defined test data
    dg = DataGenerator(db_config, td_05_a)

    try:
        # Start a test
        test = QueryTest(db_config)

        # Set the query to be tested
        test.set_query('Gevalideerde_vragen_technisch/basisveiligheid/Indicator 3.1.rq')

        # Change measuring period parameters of query
        test.change_start_period("2023-01-01", "2024-01-01")
        test.change_end_period("2023-10-31", "2024-10-31")

        # Verify actual result of the query
        test.verify_value_in_list("indicator", {"100", "100.0"})

    finally:
        # Delete previously loaded data
        dg.delete_graph_data()

# Testcase 05b
def test_if_value_returned_is_correct_for_query_3_1_05_b(db_config):
    """ Testcase 05b (Wlz Indicatie 4 t/m 10 (4VV) + Wel vestiging (Vest. nr. 1287) + Wel Medicatiefout + Wel Medicatie fout meetings (4))
        ODB Basisveiligheid 3.1. Bespreken medicatiefouten in het team
    """

    # Load defined test data
    dg = DataGenerator(db_config, td_05_b)

    try:
        # Start a test
        test = QueryTest(db_config)

        # Set the query to be tested
        test.set_query('Gevalideerde_vragen_technisch/basisveiligheid/Indicator 3.1.rq')

        # Change measuring period parameters of query
        test.change_start_period("2023-01-01", "2024-01-01")
        test.change_end_period("2023-10-31", "2024-10-31")

        # Verify actual result of the query
        test.verify_value_in_list("indicator", {"100", "100.0"})

    finally:
        # Delete previously loaded data
        dg.delete_graph_data()

# Testcase 05c
def test_if_value_returned_is_correct_for_query_3_1_05_c(db_config):
    """ Testcase 05c (Wlz Indicatie 4 t/m 10 (4VV) + Wel vestiging (Vest. nr. 1254) + Wel Medicatiefout + Wel Medicatie fout meetings (4))
        ODB Basisveiligheid 3.1. Bespreken medicatiefouten in het team
    """

    # Load defined test data
    dg = DataGenerator(db_config, td_05_c)

    try:
        # Start a test
        test = QueryTest(db_config)

        # Set the query to be tested
        test.set_query('Gevalideerde_vragen_technisch/basisveiligheid/Indicator 3.1.rq')

        # Change measuring period parameters of query
        test.change_start_period("2023-01-01", "2024-01-01")
        test.change_end_period("2023-10-31", "2024-10-31")

        # Verify actual result of the query
        test.verify_value_in_list("indicator", {"100", "100.0"})

    finally:
        # Delete previously loaded data
        dg.delete_graph_data()

# Testcase 05d
def test_if_value_returned_is_correct_for_query_3_1_05_d(db_config):
    """ Testcase 05d (Wlz Indicatie 4 t/m 10 (4VV) + 2 vestigingen (Vest. nr. 1287 & 1254) + Wel Medicatiefout + Wel Medicatie fout meetings (4))
        ODB Basisveiligheid 3.1. Bespreken medicatiefouten in het team
    """

    # Load defined test data
    dg = DataGenerator(db_config, td_05_d)

    try:
        # Start a test
        test = QueryTest(db_config)

        # Set the query to be tested
        test.set_query('Gevalideerde_vragen_technisch/basisveiligheid/Indicator 3.1.rq')

        # Change measuring period parameters of query
        test.change_start_period("2023-01-01", "2024-01-01")
        test.change_end_period("2023-10-31", "2024-10-31")

        # Verify actual result of the query
        test.verify_value_in_list("indicator", {"50", "50.0"})

    finally:
        # Delete previously loaded data
        dg.delete_graph_data()

# Testcase 06
def test_if_value_returned_is_correct_for_query_3_1_06(db_config):
    """ Testcase 06 (Wlz Indicatie 1 t/m 3 (1VV) + Wel vestiging (Vest. nr. 1287) + Wel Medicatiefout + Wel Medicatie fout meetings (<4))
        ODB Basisveiligheid 3.1. Bespreken medicatiefouten in het team
    """

    # Load defined test data
    dg = DataGenerator(db_config, td_06)

    try:
        # Start a test
        test = QueryTest(db_config)

        # Set the query to be tested
        test.set_query('Gevalideerde_vragen_technisch/basisveiligheid/Indicator 3.1.rq')

        # Change measuring period parameters of query
        test.change_start_period("2023-01-01", "2024-01-01")
        test.change_end_period("2023-10-31", "2024-10-31")

        # Verify actual result of the query
        test.verify_value_in_list("indicator", {"0", "0.0"})

    finally:
        # Delete previously loaded data
        dg.delete_graph_data()

# Testcase 06a
def test_if_value_returned_is_correct_for_query_3_1_06_a(db_config):
    """ Testcase 06a (Wlz Indicatie 1 t/m 3 (1VV) + Wel vestiging (Vest. nr. 1254) + Wel Medicatiefout + Wel Medicatie fout meetings (<4))
        ODB Basisveiligheid 3.1. Bespreken medicatiefouten in het team
    """

    # Load defined test data
    dg = DataGenerator(db_config, td_06_a)

    try:
        # Start a test
        test = QueryTest(db_config)

        # Set the query to be tested
        test.set_query('Gevalideerde_vragen_technisch/basisveiligheid/Indicator 3.1.rq')

        # Change measuring period parameters of query
        test.change_start_period("2023-01-01", "2024-01-01")
        test.change_end_period("2023-10-31", "2024-10-31")

        # Verify actual result of the query
        test.verify_value("indicator", None)

    finally:
        # Delete previously loaded data
        dg.delete_graph_data()

# Testcase 06b
def test_if_value_returned_is_correct_for_query_3_1_06_b(db_config):
    """ Testcase 06b (Wlz Indicatie 1 t/m 3 (1VV) + Wel vestiging (Vest. nr. 1287) + Wel Medicatiefout + Wel Medicatie fout meetings (4))
        ODB Basisveiligheid 3.1. Bespreken medicatiefouten in het team
    """

    # Load defined test data
    dg = DataGenerator(db_config, td_06_b)

    try:
        # Start a test
        test = QueryTest(db_config)

        # Set the query to be tested
        test.set_query('Gevalideerde_vragen_technisch/basisveiligheid/Indicator 3.1.rq')

        # Change measuring period parameters of query
        test.change_start_period("2023-01-01", "2024-01-01")
        test.change_end_period("2023-10-31", "2024-10-31")

        # Verify actual result of the query
        test.verify_value("indicator", None)

    finally:
        # Delete previously loaded data
        dg.delete_graph_data()

# Testcase 06c
def test_if_value_returned_is_correct_for_query_3_1_06_c(db_config):
    """ Testcase 06c (Wlz Indicatie 1 t/m 3 (1VV) + Wel vestiging (Vest. nr. 1254) + Wel Medicatiefout + Wel Medicatie fout meetings (4))
        ODB Basisveiligheid 3.1. Bespreken medicatiefouten in het team
    """

    # Load defined test data
    dg = DataGenerator(db_config, td_06_c)

    try:
        # Start a test
        test = QueryTest(db_config)

        # Set the query to be tested
        test.set_query('Gevalideerde_vragen_technisch/basisveiligheid/Indicator 3.1.rq')

        # Change measuring period parameters of query
        test.change_start_period("2023-01-01", "2024-01-01")
        test.change_end_period("2023-10-31", "2024-10-31")

        # Verify actual result of the query
        test.verify_value("indicator", None)

    finally:
        # Delete previously loaded data
        dg.delete_graph_data()

# Testcase 06d
def test_if_value_returned_is_correct_for_query_3_1_06_d(db_config):
    """ Testcase 06d (Wlz Indicatie 1 t/m 3 (1VV) + 2 vestigingen (Vest. nr. 1287 & 1254) + Wel Medicatiefout + Wel Medicatie fout meetings (4))
        ODB Basisveiligheid 3.1. Bespreken medicatiefouten in het team
    """

    # Load defined test data
    dg = DataGenerator(db_config, td_06_d)

    try:
        # Start a test
        test = QueryTest(db_config)

        # Set the query to be tested
        test.set_query('Gevalideerde_vragen_technisch/basisveiligheid/Indicator 3.1.rq')

        # Change measuring period parameters of query
        test.change_start_period("2023-01-01", "2024-01-01")
        test.change_end_period("2023-10-31", "2024-10-31")

        # Verify actual result of the query
        test.verify_value("indicator", None)

    finally:
        # Delete previously loaded data
        dg.delete_graph_data()

# Testcase 07
def test_if_value_returned_is_correct_for_query_3_1_07(db_config):
    """ Testcase 07 (Geen Wlz Indicatie + Wel vestiging (Vest. nr. 1287) + Wel Medicatiefout + Wel Medicatie fout meetings (<4))
        ODB Basisveiligheid 3.1. Bespreken medicatiefouten in het team
    """

    # Load defined test data
    dg = DataGenerator(db_config, td_07)

    try:
        # Start a test
        test = QueryTest(db_config)

        # Set the query to be tested
        test.set_query('Gevalideerde_vragen_technisch/basisveiligheid/Indicator 3.1.rq')

        # Change measuring period parameters of query
        test.change_start_period("2023-01-01", "2024-01-01")
        test.change_end_period("2023-10-31", "2024-10-31")

        # Verify actual result of the query
        test.verify_value_in_list("indicator", {"0", "0.0"})

    finally:
        # Delete previously loaded data
        dg.delete_graph_data()

# Testcase 07a
def test_if_value_returned_is_correct_for_query_3_1_07_a(db_config):
    """ Testcase 07a (Geen Wlz Indicatie + Wel vestiging (Vest. nr. 1254) + Wel Medicatiefout + Wel Medicatie fout meetings (<4))
        ODB Basisveiligheid 3.1. Bespreken medicatiefouten in het team
    """

    # Load defined test data
    dg = DataGenerator(db_config, td_07_a)

    try:
        # Start a test
        test = QueryTest(db_config)

        # Set the query to be tested
        test.set_query('Gevalideerde_vragen_technisch/basisveiligheid/Indicator 3.1.rq')

        # Change measuring period parameters of query
        test.change_start_period("2023-01-01", "2024-01-01")
        test.change_end_period("2023-10-31", "2024-10-31")

        # Verify actual result of the query
        test.verify_value("indicator", None)

    finally:
        # Delete previously loaded data
        dg.delete_graph_data()

# Testcase 07b
def test_if_value_returned_is_correct_for_query_3_1_07_b(db_config):
    """ Testcase 07b (Geen Wlz Indicatie + Wel vestiging (Vest. nr. 1287) + Wel Medicatiefout + Wel Medicatie fout meetings (4))
        ODB Basisveiligheid 3.1. Bespreken medicatiefouten in het team
    """

    # Load defined test data
    dg = DataGenerator(db_config, td_07_b)

    try:
        # Start a test
        test = QueryTest(db_config)

        # Set the query to be tested
        test.set_query('Gevalideerde_vragen_technisch/basisveiligheid/Indicator 3.1.rq')

        # Change measuring period parameters of query
        test.change_start_period("2023-01-01", "2024-01-01")
        test.change_end_period("2023-10-31", "2024-10-31")

        # Verify actual result of the query
        test.verify_value("indicator", None)

    finally:
        # Delete previously loaded data
        dg.delete_graph_data()

# Testcase 07c
def test_if_value_returned_is_correct_for_query_3_1_07_c(db_config):
    """ Testcase 07c (Geen Wlz Indicatie + Wel vestiging (Vest. nr. 1254) + Wel Medicatiefout + Wel Medicatie fout meetings (4))
        ODB Basisveiligheid 3.1. Bespreken medicatiefouten in het team
    """

    # Load defined test data
    dg = DataGenerator(db_config, td_07_c)

    try:
        # Start a test
        test = QueryTest(db_config)

        # Set the query to be tested
        test.set_query('Gevalideerde_vragen_technisch/basisveiligheid/Indicator 3.1.rq')

        # Change measuring period parameters of query
        test.change_start_period("2023-01-01", "2024-01-01")
        test.change_end_period("2023-10-31", "2024-10-31")

        # Verify actual result of the query
        test.verify_value("indicator", None)

    finally:
        # Delete previously loaded data
        dg.delete_graph_data()

# Testcase 07d
def test_if_value_returned_is_correct_for_query_3_1_07_d(db_config):
    """ Testcase 07d (Geen Wlz Indicatie + 2 vestigingen (Vest. nr. 1287 & 1254) + Wel Medicatiefout + Wel Medicatie fout meetings (4))
        ODB Basisveiligheid 3.1. Bespreken medicatiefouten in het team
    """

    # Load defined test data
    dg = DataGenerator(db_config, td_07_d)

    try:
        # Start a test
        test = QueryTest(db_config)

        # Set the query to be tested
        test.set_query('Gevalideerde_vragen_technisch/basisveiligheid/Indicator 3.1.rq')

        # Change measuring period parameters of query
        test.change_start_period("2023-01-01", "2024-01-01")
        test.change_end_period("2023-10-31", "2024-10-31")

        # Verify actual result of the query
        test.verify_value("indicator", None)

    finally:
        # Delete previously loaded data
        dg.delete_graph_data()

# Testcase 08
def test_if_value_returned_is_correct_for_query_3_1_08(db_config):
    """ Testcase 08 (Geen Wlz Indicatie + Geen vestiging + Wel Medicatiefout + Wel Medicatie fout meetings (<4))
        ODB Basisveiligheid 3.1. Bespreken medicatiefouten in het team
    """

    # Load defined test data
    dg = DataGenerator(db_config, td_08)

    try:
        # Start a test
        test = QueryTest(db_config)

        # Set the query to be tested
        test.set_query('Gevalideerde_vragen_technisch/basisveiligheid/Indicator 3.1.rq')

        # Change measuring period parameters of query
        test.change_start_period("2023-01-01", "2024-01-01")
        test.change_end_period("2023-10-31", "2024-10-31")

        # Verify actual result of the query
        test.verify_value("indicator", None)

    finally:
        # Delete previously loaded data
        dg.delete_graph_data()

# Testcase 08a
def test_if_value_returned_is_correct_for_query_3_1_08_a(db_config):
    """ Testcase 08a (Geen Wlz Indicatie + Geen vestiging + Wel Medicatiefout + Wel Medicatie fout meetings (4))
        ODB Basisveiligheid 3.1. Bespreken medicatiefouten in het team
    """

    # Load defined test data
    dg = DataGenerator(db_config, td_08_a)

    try:
        # Start a test
        test = QueryTest(db_config)

        # Set the query to be tested
        test.set_query('Gevalideerde_vragen_technisch/basisveiligheid/Indicator 3.1.rq')

        # Change measuring period parameters of query
        test.change_start_period("2023-01-01", "2024-01-01")
        test.change_end_period("2023-10-31", "2024-10-31")

        # Verify actual result of the query
        test.verify_value("indicator", None)

    finally:
        # Delete previously loaded data
        dg.delete_graph_data()