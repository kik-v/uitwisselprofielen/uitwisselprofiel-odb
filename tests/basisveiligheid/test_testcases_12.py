from DataGenerator import DataGenerator
from QueryTest import QueryTest

# Opmerkingen: 

# Testcases: 

td_01 = [
    {
        "Description": "Testcase 01 (Geen Wlz + Geen Decubitus + Geen casuistiekbespreking + Geen vestiging)",
        "Amount": 10,
        "Human": [
            {
                "NursingProcess": [
                    {
                        "identifier": "1",
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",
                        "location": "",
                        "related_indication": "1"
                    }
                ],
                "Indication": [
                    {
                        "identifier": "1",
                        "type": "WlzIndicatie",
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",
                        "heeftIndicatieBehandeling": True,
                        "ciz": [""],
                        "leveringsvorm": [""]
                    }
                ],
            }
        ]
    }
]

td_02 = [
    {
        "Description": "Testcase 02 (Wlz 1 t/m 3 (1VV) + Geen Decubitus + Geen casuistiekbespreking + Geen vestiging)",
        "Amount": 10,
        "Human": [
            {
                "NursingProcess": [
                    {
                        "identifier": "1",
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",
                        "location": "",
                        "related_indication": "1"
                    }
                ],
                "Indication": [
                    {
                        "identifier": "1",
                        "type": "WlzIndicatie",
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",
                        "heeftIndicatieBehandeling": True,
                        "ciz": ["1VV"],
                        "leveringsvorm": [""]
                    }
                ],
            }
        ]
    }
]

td_03 = [
    {
        "Description": "Testcase 03 (Wlz 4 t/m 10 (4VV) + Geen Decubitus + Geen casuistiekbespreking + Geen vestiging)",
        "Amount": 10,
        "Human": [
            {
                "NursingProcess": [
                    {
                        "identifier": "1",
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",
                        "location": "",
                        "related_indication": "1"
                    }
                ],
                "Indication": [
                    {
                        "identifier": "1",
                        "type": "WlzIndicatie",
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",
                        "heeftIndicatieBehandeling": True,
                        "ciz": ["4VV"],
                        "leveringsvorm": [""]
                    }
                ],
            }
        ]
    }
]

td_04 = [
    {
        "Description": "Testcase 04 (Wlz 4 t/m 10 (4VV) + Wel Decubitus (Geen categorie) + Geen casuistiekbespreking + Geen vestiging)",
        "Amount": 10,
        "Human": [
            {
                "Decubitus": [
                    {
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",
                    }
                ],
                "NursingProcess": [
                    {
                        "identifier": "1",
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",
                        "location": "",
                        "related_indication": "1"
                    }
                ],
                "Indication": [
                    {
                        "identifier": "1",
                        "type": "WlzIndicatie",
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",
                        "heeftIndicatieBehandeling": True,
                        "ciz": ["4VV"],
                        "leveringsvorm": [""]
                    }
                ]
            }
        ]
    }
]

td_04_a = [
    {
        "Description": "Testcase 04a (Wlz 4 t/m 10 (4VV) + Wel Decubitus (Geen categorie) + Geen casuistiekbespreking + Vestiging nr 1287)",
        "Amount": 10,
        "Human": [
            {
                "Decubitus": [
                    {
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",
                    }
                ],
                "NursingProcess": [
                    {
                        "identifier": "1",
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",
                        "location": "Locatie_BG_Rechts",
                        "related_indication": "1"
                    }
                ],
                "Indication": [
                    {
                        "identifier": "1",
                        "type": "WlzIndicatie",
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",
                        "heeftIndicatieBehandeling": True,
                        "ciz": ["4VV"],
                        "leveringsvorm": [""]
                    }
                ]
            }
        ]
    }
]

td_04_b = [
    {
        "Description": "Testcase 04b (Wlz 4 t/m 10 (4VV) + Wel Decubitus (Geen categorie) + Geen casuistiekbespreking + Vestiging nr 1254)",
        "Amount": 10,
        "Human": [
            {
                "Decubitus": [
                    {
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",
                    }
                ],
                "NursingProcess": [
                    {
                        "identifier": "2",
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",
                        "location": "Locatie_Vleugel_A",
                        "related_indication": "2"
                    }
                ],
                "Indication": [
                    {
                        "identifier": "2",
                        "type": "WlzIndicatie",
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",
                        "heeftIndicatieBehandeling": True,
                        "ciz": ["4VV"],
                        "leveringsvorm": [""]
                    }
                ]
            }
        ]
    }
]

td_05 = [
    {
        "Description": "Testcase 05 (Wlz 4 t/m 10 (4VV) + Wel Decubitus (Categorie 1) + Geen casuistiekbespreking + Geen vestiging)",
        "Amount": 10,
        "Human": [
            {
                "Decubitus": [
                    {
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",
                        "stage": [
                            {
                                "date": "2024-01-01",
                                "grade": 1
                            }
                        ],
                    }
                ],
                "NursingProcess": [
                    {
                        "identifier": "1",
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",
                        "location": "",
                        "related_indication": "1"
                    }
                ],
                "Indication": [
                    {
                        "identifier": "1",
                        "type": "WlzIndicatie",
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",
                        "heeftIndicatieBehandeling": True,
                        "ciz": ["4VV"],
                        "leveringsvorm": [""]
                    }
                ]
            }
        ]
    }
]

td_05_a = [
    {
        "Description": "Testcase 05a (Wlz 4 t/m 10 (4VV) + Wel Decubitus (Categorie 1) + Geen casuistiekbespreking + Vestiging nr 1287)",
        "Amount": 10,
        "Human": [
            {
                "Decubitus": [
                    {
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",
                        "stage": [                            
                            {
                                "date": "2024-01-01",
                                "grade": 1
                            }
                        ],
                    }
                ],
                "NursingProcess": [
                    {
                        "identifier": "1",
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",
                        "location": "Locatie_BG_Rechts",
                        "related_indication": "1"
                    }
                ],
                "Indication": [
                    {
                        "identifier": "1",
                        "type": "WlzIndicatie",
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",
                        "heeftIndicatieBehandeling": True,
                        "ciz": ["4VV"],
                        "leveringsvorm": [""]
                    }
                ]
            }
        ]
    }
]

td_05_b = [
    {
        "Description": "Testcase 05b (Wlz 4 t/m 10 (4VV) + Wel Decubitus (Categorie 1) + Geen casuistiekbespreking + Vestiging nr 1254)",
        "Amount": 10,
        "Human": [
            {
                "Decubitus": [
                    {
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",
                        "stage": [                            
                            {
                                "date": "2024-01-01",
                                "grade": 1
                            }
                        ],
                    }
                ],
                "NursingProcess": [
                    {
                        "identifier": "2",
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",
                        "location": "Locatie_Vleugel_A",
                        "related_indication": "2"
                    }
                ],
                "Indication": [
                    {
                        "identifier": "2",
                        "type": "WlzIndicatie",
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",
                        "heeftIndicatieBehandeling": True,
                        "ciz": ["4VV"],
                        "leveringsvorm": [""]
                    }
                ]
            }
        ]
    }
]

td_06 = [
    {
        "Description": "Testcase 06 (Wlz 4 t/m 10 (4VV) + Wel Decubitus (Categorie 2+) + Geen casuistiekbespreking + Geen vestiging)",
        "Amount": 10,
        "Human": [
            {
                "Decubitus": [
                    {
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",
                        "stage": [
                            {
                                "date": "2024-01-01",
                                "grade": 2
                            }
                        ],
                    }
                ],
                "NursingProcess": [
                    {
                        "identifier": "1",
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",
                        "location": "",
                        "related_indication": "1"
                    }
                ],
                "Indication": [
                    {
                        "identifier": "1",
                        "type": "WlzIndicatie",
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",
                        "heeftIndicatieBehandeling": True,
                        "ciz": ["4VV"],
                        "leveringsvorm": [""]
                    }
                ]
            }
        ]
    }
]

td_06_a = [
    {
        "Description": "Testcase 06a (Wlz 4 t/m 10 (4VV) + Wel Decubitus (Categorie 2+) + Geen casuistiekbespreking + Vestiging nr 1287)",
        "Amount": 10,
        "Human": [
            {
                "Decubitus": [
                    {
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",
                        "stage": [                            
                            {
                                "date": "2024-01-01",
                                "grade": 2
                            }
                        ],
                    }
                ],
                "NursingProcess": [
                    {
                        "identifier": "1",
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",
                        "location": "Locatie_BG_Rechts",
                        "related_indication": "1"
                    }
                ],
                "Indication": [
                    {
                        "identifier": "1",
                        "type": "WlzIndicatie",
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",
                        "heeftIndicatieBehandeling": True,
                        "ciz": ["4VV"],
                        "leveringsvorm": [""]
                    }
                ]
            }
        ]
    }
]

td_06_b = [
    {
        "Description": "Testcase 06b (Wlz 4 t/m 10 (4VV) + Wel Decubitus (Categorie 2+) + Geen casuistiekbespreking + Vestiging nr 1254)",
        "Amount": 10,
        "Human": [
            {
                "Decubitus": [
                    {
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",
                        "stage": [                            
                            {
                                "date": "2024-01-01",
                                "grade": 2
                            }
                        ],
                    }
                ],
                "NursingProcess": [
                    {
                        "identifier": "2",
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",
                        "location": "Locatie_Vleugel_A",
                        "related_indication": "2"
                    }
                ],
                "Indication": [
                    {
                        "identifier": "2",
                        "type": "WlzIndicatie",
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",
                        "heeftIndicatieBehandeling": True,
                        "ciz": ["4VV"],
                        "leveringsvorm": [""]
                    }
                ]
            }
        ]
    }
]

td_07 = [
    {
        "Description": "Testcase 07 (Wlz 4 t/m 10 (4VV) + Wel Decubitus (Categorie 2+) + Wel casuistiekbespreking + Geen vestiging)",
        "Amount": 10,
        "Human": [
            {
                "Decubitus": [
                    {
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",
                        "stage": [
                            {
                                "date": "2024-01-01",
                                "grade": 2
                            }
                        ],
                        "case_meetings": ["2024-02-01"]
                    }
                ],
                "NursingProcess": [
                    {
                        "identifier": "1",
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",
                        "location": "",
                        "related_indication": "1"
                    }
                ],
                "Indication": [
                    {
                        "identifier": "1",
                        "type": "WlzIndicatie",
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",
                        "heeftIndicatieBehandeling": True,
                        "ciz": ["4VV"],
                        "leveringsvorm": [""]
                    }
                ]
            }
        ]
    }
]

td_07_a = [
    {
        "Description": "Testcase 07a (Wlz 4 t/m 10 (4VV) + Wel Decubitus (Categorie 2+) + Wel casuistiekbespreking + Vestiging nr 1287)",
        "Amount": 10,
        "Human": [
            {
                "Decubitus": [
                    {
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",
                        "stage": [                            
                            {
                                "date": "2024-01-01",
                                "grade": 2
                            }],
                        "case_meetings": ["2024-02-01"]
                    }
                ],
                "NursingProcess": [
                    {
                        "identifier": "1",
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",
                        "location": "Locatie_BG_Rechts",
                        "related_indication": "1"
                    }
                ],
                "Indication": [
                    {
                        "identifier": "1",
                        "type": "WlzIndicatie",
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",
                        "heeftIndicatieBehandeling": True,
                        "ciz": ["4VV"],
                        "leveringsvorm": [""]
                    }
                ]
            }
        ]
    }
]

td_07_b = [
    {
        "Description": "Testcase 07b (Wlz 4 t/m 10 (4VV) + Wel Decubitus (Categorie 2+) + Wel casuistiekbespreking + Vestiging nr 1254)",
        "Amount": 10,
        "Human": [
            {
                "Decubitus": [
                    {
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",
                        "stage": [                            
                            {
                                "date": "2024-01-01",
                                "grade": 2
                            }],
                        "case_meetings": ["2024-02-01"]
                    }
                ],
                "NursingProcess": [
                    {
                        "identifier": "2",
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",
                        "location": "Locatie_Vleugel_A",
                        "related_indication": "2"
                    }
                ],
                "Indication": [
                    {
                        "identifier": "2",
                        "type": "WlzIndicatie",
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",
                        "heeftIndicatieBehandeling": True,
                        "ciz": ["4VV"],
                        "leveringsvorm": [""]
                    }
                ]
            }
        ]
    }
]

td_08 = [
    {
        "Description": "Testcase 08 (Wlz 4 t/m 10 (4VV) + Wel Decubitus (Categorie 2 & 3) + Wel casuistiekbespreking (2x) + Geen vestiging)",
        "Amount": 10,
        "Human": [
            {
                "Decubitus": [
                    {
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",
                        "stage": [
                            {
                                "date": "2024-01-01",
                                "grade": 2
                            },
                            {
                                "date": "2024-06-01",
                                "grade": 3  
                            }],
                        "case_meetings": ["2024-01-02", "2024-06-02"]
                    }
                ],
                "NursingProcess": [
                    {
                        "identifier": "1",
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",
                        "location": "",
                        "related_indication": "1"
                    }
                ],
                "Indication": [
                    {
                        "identifier": "1",
                        "type": "WlzIndicatie",
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",
                        "heeftIndicatieBehandeling": True,
                        "ciz": ["4VV"],
                        "leveringsvorm": [""]
                    }
                ]
            }
        ]
    }
]

td_08_a = [
    {
        "Description": "Testcase 08a (Wlz 4 t/m 10 (4VV) + Wel Decubitus (Categorie 2 & 3) + Wel casuistiekbespreking (2x) + Vestiging nr 1287)",
        "Amount": 10,
        "Human": [
            {
                "Decubitus": [
                    {
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",
                        "stage": [                            
                            {
                                "date": "2024-01-01",
                                "grade": 2
                            },
                            {
                                "date": "2024-06-01",
                                "grade": 3  
                            }],
                        "case_meetings": ["2024-01-02", "2024-06-02"],
                    }
                ],
                "NursingProcess": [
                    {
                        "identifier": "1",
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",
                        "location": "Locatie_BG_Rechts",
                        "related_indication": "1"
                    }
                ],
                "Indication": [
                    {
                        "identifier": "1",
                        "type": "WlzIndicatie",
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",
                        "heeftIndicatieBehandeling": True,
                        "ciz": ["4VV"],
                        "leveringsvorm": [""]
                    }
                ]
            }
        ]
    }
]

td_08_b = [
    {
        "Description": "Testcase 08b (Wlz 4 t/m 10 (4VV) + Wel Decubitus (Categorie 2 & 3) + Wel casuistiekbespreking (2x) + Vestiging nr 1254)",
        "Amount": 10,
        "Human": [
            {
                "Decubitus": [
                    {
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",
                        "stage": [                            
                            {
                                "date": "2024-01-01",
                                "grade": 2
                            },
                            {
                                "date": "2024-06-01",
                                "grade": 3  
                            }],
                        "case_meetings": ["2024-01-02", "2024-06-02"]
                    }
                ],
                "NursingProcess": [
                    {
                        "identifier": "2",
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",
                        "location": "Locatie_Vleugel_A",
                        "related_indication": "2"
                    }
                ],
                "Indication": [
                    {
                        "identifier": "2",
                        "type": "WlzIndicatie",
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",
                        "heeftIndicatieBehandeling": True,
                        "ciz": ["4VV"],
                        "leveringsvorm": [""]
                    }
                ]
            }
        ]
    }
]


# Static tests

def test_if_headers_are_correct_on_1_2(db_config):
    """ Test of de juiste headers terugkomen in het resultaat
        ODB Basisveiligheid 1.2. Casuïstiekbespreking decubitus
    """

    # Setup of the test
    test = QueryTest(db_config)

    # Configuration and execution
    test.set_query('Gevalideerde_vragen_technisch/basisveiligheid/Indicator 1.2.rq')

    # Assertions
    test.verify_header_present('teller')
    test.verify_header_present('noemer')
    # test.verify_header_present('indicator') Indicator waarde is 'None'


def test_if_number_of_rows_returned_is_correct_for_query_1_2(db_config):
    """ Test of het aantal rijen correct wordt teruggegeven
        ODB Basisveiligheid 1.2. Casuïstiekbespreking decubitus
    """

    # Setup of the test
    test = QueryTest(db_config)

    # Configuration and execution
    test.set_query('Gevalideerde_vragen_technisch/basisveiligheid/Indicator 1.2.rq')

    # Assertions
    test.verify_row_count(1)


def test_if_indicator_has_correct_value_for_query_1_2(db_config):
    """ Test of de indicator de juiste waarde heeft
        ODB Basisveiligheid 1.2. Casuïstiekbespreking decubitus
    """

    # Setup of the test
    test = QueryTest(db_config)

    # Configuration and execution
    test.set_query('Gevalideerde_vragen_technisch/basisveiligheid/Indicator 1.2.rq')

    # Assertions
    test.verify_value("teller","0")
    test.verify_value("indicator", None)


def test_if_dates_can_change_1_2(db_config):
    """ Test of gewijzigde datum daadwerkelijk een ander resultaat oplevert
        ODB Basisveiligheid 1.2.Casuïstiekbespreking decubitus
    """

    # Setup of the test
    test = QueryTest(db_config)

    # Configuration and execution
    test.set_query('Gevalideerde_vragen_technisch/basisveiligheid/Indicator 1.2.rq')

    test.change_start_period("2023-01-01", "2022-01-01")
    test.change_end_period("2023-12-31", "2022-12-31")

    # Assertions
    test.verify_value_in_list("indicator",{"0", "0.0"})

# Tests with Generated data

# Testcase 01

def test_if_value_returned_is_correct_for_query_1_2_01(db_config):
    """ Testcase 01 (Geen Wlz + Geen Decubitus + Geen casuistiekbespreking + Geen vestiging)
        ODB Basisveiligheid 1.2 Casuïstiekbespreking decubitus
    """

    # Load defined test data
    dg = DataGenerator(db_config, td_01)

    try:
        # Start a test
        test = QueryTest(db_config)

        # Set the query to be tested
        test.set_query('Gevalideerde_vragen_technisch/basisveiligheid/Indicator 1.2.rq')

        # Set parameters
        test.change_start_period("2023-01-01", "2024-01-01")
        test.change_end_period("2023-12-31","2024-12-31")

        # Verify actual result of the query
        test.verify_value("indicator", None)


    finally:
        # Delete previously loaded data
        dg.delete_graph_data()

# Testcase 02

def test_if_value_returned_is_correct_for_query_1_2_02(db_config):
    """ Testcase 02 (Wlz 1 t/m 3 (1VV) + Geen Decubitus + Geen casuistiekbespreking + Geen vestiging)
        ODB basisveiligheid 1.2 Casuïstiekbespreking decubitus
    """

    # Load defined test data
    dg = DataGenerator(db_config, td_02)

    try:
        # Start a test
        test = QueryTest(db_config)

        # Set the query to be tested
        test.set_query('Gevalideerde_vragen_technisch/basisveiligheid/Indicator 1.2.rq')

        # Set parameters
        test.change_start_period("2023-01-01", "2024-01-01")
        test.change_end_period("2023-12-31","2024-12-31")

        # Verify actual result of the query
        test.verify_value("indicator", None)


    finally:
        # Delete previously loaded data
        dg.delete_graph_data()

# Testcase 03

def test_if_value_returned_is_correct_for_query_1_2_03(db_config):
    """ Testcase 03 (Wlz 4 t/m 10 (4VV) + Geen Decubitus + Geen casuistiekbespreking + Geen vestiging)
        ODB basisveiligheid 1.2 Casuïstiekbespreking decubitus
    """

    # Load defined test data
    dg = DataGenerator(db_config, td_03)

    try:
        # Start a test
        test = QueryTest(db_config)

        # Set the query to be tested
        test.set_query('Gevalideerde_vragen_technisch/basisveiligheid/Indicator 1.2.rq')

        # Set parameters
        test.change_start_period("2023-01-01", "2024-01-01")
        test.change_end_period("2023-12-31","2024-12-31")

        # Verify actual result of the query
        test.verify_value("indicator", None)


    finally:
        # Delete previously loaded data
        dg.delete_graph_data()

# Testcase 04

def test_if_value_returned_is_correct_for_query_1_2_04(db_config):
    """ Testcase 04 (Wlz 4 t/m 10 (4VV) + Wel Decubitus (Geen categorie) + Geen casuistiekbespreking + Geen vestiging)
        ODB basisveiligheid 1.2 Casuïstiekbespreking decubitus
    """

    # Load defined test data
    dg = DataGenerator(db_config, td_04)

    try:
        # Start a test
        test = QueryTest(db_config)

        # Set the query to be tested
        test.set_query('Gevalideerde_vragen_technisch/basisveiligheid/Indicator 1.2.rq')

        # Set parameters
        test.change_start_period("2023-01-01", "2024-01-01")
        test.change_end_period("2023-12-31","2024-12-31")

        # Verify actual result of the query
        test.verify_value("indicator", None)


    finally:
        # Delete previously loaded data
        dg.delete_graph_data()

# Testcase 04a

def test_if_value_returned_is_correct_for_query_1_2_04_a(db_config):
    """ Testcase 04a (Wlz 4 t/m 10 (4VV) + Wel Decubitus (Geen categorie) + Geen casuistiekbespreking + Vestiging nr. 1287)
        ODB basisveiligheid 1.2 Casuïstiekbespreking decubitus
    """

    # Load defined test data
    dg = DataGenerator(db_config, td_04_a)

    try:
        # Start a test
        test = QueryTest(db_config)

        # Set the query to be tested
        test.set_query('Gevalideerde_vragen_technisch/basisveiligheid/Indicator 1.2.rq')

        # Set parameters
        test.change_start_period("2023-01-01", "2024-01-01")
        test.change_end_period("2023-12-31","2024-12-31")

        # Verify actual result of the query
        test.verify_value("indicator", None)


    finally:
        # Delete previously loaded data
        dg.delete_graph_data()

# Testcase 04b

def test_if_value_returned_is_correct_for_query_1_2_04_b(db_config):
    """ Testcase 04b (Wlz 4 t/m 10 (4VV) + Wel Decubitus (Geen categorie) + Geen casuistiekbespreking + Vestiging nr. 1254)
        ODB basisveiligheid 1.2 Casuïstiekbespreking decubitus
    """

    # Load defined test data
    dg = DataGenerator(db_config, td_04_b)

    try:
        # Start a test
        test = QueryTest(db_config)

        # Set the query to be tested
        test.set_query('Gevalideerde_vragen_technisch/basisveiligheid/Indicator 1.2.rq')

        # Set parameters
        test.change_start_period("2023-01-01", "2024-01-01")
        test.change_end_period("2023-12-31","2024-12-31")

        # Verify actual result of the query
        test.verify_value("indicator", None)


    finally:
        # Delete previously loaded data
        dg.delete_graph_data()

# Testcase 05

def test_if_value_returned_is_correct_for_query_1_2_05(db_config):
    """ Testcase 05 (Wlz 4 t/m 10 (4VV) + Wel Decubitus (Categorie 1) + Geen casuistiekbespreking + Geen vestiging)
        ODB basisveiligheid 1.2 Casuïstiekbespreking decubitus
    """

    # Load defined test data
    dg = DataGenerator(db_config, td_05)

    try:
        # Start a test
        test = QueryTest(db_config)

        # Set the query to be tested
        test.set_query('Gevalideerde_vragen_technisch/basisveiligheid/Indicator 1.2.rq')

        # Set parameters
        test.change_start_period("2023-01-01", "2024-01-01")
        test.change_end_period("2023-12-31","2024-12-31")

        # Verify actual result of the query
        test.verify_value("indicator", None)


    finally:
        # Delete previously loaded data
        dg.delete_graph_data()

# Testcase 05a

def test_if_value_returned_is_correct_for_query_1_2_05_a(db_config):
    """ Testcase 05a (Wlz 4 t/m 10 (4VV) + Wel Decubitus (Categorie 1) + Geen casuistiekbespreking + Vestiging nr. 1287)
        ODB basisveiligheid 1.2 Casuïstiekbespreking decubitus
    """

    # Load defined test data
    dg = DataGenerator(db_config, td_05_a)

    try:
        # Start a test
        test = QueryTest(db_config)

        # Set the query to be tested
        test.set_query('Gevalideerde_vragen_technisch/basisveiligheid/Indicator 1.2.rq')

        # Set parameters
        test.change_start_period("2023-01-01", "2024-01-01")
        test.change_end_period("2023-12-31","2024-12-31")

        # Verify actual result of the query
        test.verify_value("indicator", None)


    finally:
        # Delete previously loaded data
        dg.delete_graph_data()

# Testcase 05b

def test_if_value_returned_is_correct_for_query_1_2_05_b(db_config):
    """ Testcase 05b (Wlz 4 t/m 10 (4VV) + Wel Decubitus (Categorie 1) + Geen casuistiekbespreking + Vestiging nr. 1254)
        ODB basisveiligheid 1.2 Casuïstiekbespreking decubitus
    """

    # Load defined test data
    dg = DataGenerator(db_config, td_05_b)

    try:
        # Start a test
        test = QueryTest(db_config)

        # Set the query to be tested
        test.set_query('Gevalideerde_vragen_technisch/basisveiligheid/Indicator 1.2.rq')

        # Set parameters
        test.change_start_period("2023-01-01", "2024-01-01")
        test.change_end_period("2023-12-31","2024-12-31")

        # Verify actual result of the query
        test.verify_value("indicator", None)


    finally:
        # Delete previously loaded data
        dg.delete_graph_data()

# Testcase 06

def test_if_value_returned_is_correct_for_query_1_2_06(db_config):
    """ Testcase 06 (Wlz 4 t/m 10 (4VV) + Wel Decubitus (Categorie 2+) + Geen casuistiekbespreking + Geen vestiging)
        ODB basisveiligheid 1.2 Casuïstiekbespreking decubitus
    """

    # Load defined test data
    dg = DataGenerator(db_config, td_06)

    try:
        # Start a test
        test = QueryTest(db_config)

        # Set the query to be tested
        test.set_query('Gevalideerde_vragen_technisch/basisveiligheid/Indicator 1.2.rq')

        # Set parameters
        test.change_start_period("2023-01-01", "2024-01-01")
        test.change_end_period("2023-12-31","2024-12-31")

        # Verify actual result of the query
        test.verify_value("indicator", None)


    finally:
        # Delete previously loaded data
        dg.delete_graph_data()

# Testcase 06a

def test_if_value_returned_is_correct_for_query_1_2_06_a(db_config):
    """ Testcase 06a (Wlz 4 t/m 10 (4VV) + Wel Decubitus (Categorie 2+) + Geen casuistiekbespreking + Vestiging nr. 1287)
        ODB basisveiligheid 1.2 Casuïstiekbespreking decubitus
    """

    # Load defined test data
    dg = DataGenerator(db_config, td_06_a)

    try:
        # Start a test
        test = QueryTest(db_config)

        # Set the query to be tested
        test.set_query('Gevalideerde_vragen_technisch/basisveiligheid/Indicator 1.2.rq')

        # Set parameters
        test.change_start_period("2023-01-01", "2024-01-01")
        test.change_end_period("2023-12-31","2024-12-31")

        # Verify actual result of the query
        test.verify_value("indicator", "0")


    finally:
        # Delete previously loaded data
        dg.delete_graph_data()

# Testcase 06b

def test_if_value_returned_is_correct_for_query_1_2_06_b(db_config):
    """ Testcase 06b (Wlz 4 t/m 10 (4VV) + Wel Decubitus (Categorie 2+) + Geen casuistiekbespreking + Vestiging nr. 1254)
        ODB basisveiligheid 1.2 Casuïstiekbespreking decubitus
    """

    # Load defined test data
    dg = DataGenerator(db_config, td_06_b)

    try:
        # Start a test
        test = QueryTest(db_config)

        # Set the query to be tested
        test.set_query('Gevalideerde_vragen_technisch/basisveiligheid/Indicator 1.2.rq')

        # Set parameters
        test.change_start_period("2023-01-01", "2024-01-01")
        test.change_end_period("2023-12-31","2024-12-31")

        # Verify actual result of the query
        test.verify_value("indicator", "0")


    finally:
        # Delete previously loaded data
        dg.delete_graph_data()

# Testcase 07

def test_if_value_returned_is_correct_for_query_1_2_07(db_config):
    """ Testcase 07 (Wlz 4 t/m 10 (4VV) + Wel Decubitus (Categorie 2+) + Wel casuistiekbespreking + Geen vestiging)
        ODB basisveiligheid 1.2 Casuïstiekbespreking decubitus
    """

    # Load defined test data
    dg = DataGenerator(db_config, td_07)

    try:
        # Start a test
        test = QueryTest(db_config)

        # Set the query to be tested
        test.set_query('Gevalideerde_vragen_technisch/basisveiligheid/Indicator 1.2.rq')

        # Set parameters
        test.change_start_period("2023-01-01", "2024-01-01")
        test.change_end_period("2023-12-31","2024-12-31")

        # Verify actual result of the query
        test.verify_value("indicator", None)


    finally:
        # Delete previously loaded data
        dg.delete_graph_data()

# Testcase 07a

def test_if_value_returned_is_correct_for_query_1_2_07_a(db_config):
    """ Testcase 07a (Wlz 4 t/m 10 (4VV) + Wel Decubitus (Categorie 2+) + Wel casuistiekbespreking + Vestiging nr. 1287)
        ODB basisveiligheid 1.2 Casuïstiekbespreking decubitus
    """

    # Load defined test data
    dg = DataGenerator(db_config, td_07_a)

    try:
        # Start a test
        test = QueryTest(db_config)

        # Set the query to be tested
        test.set_query('Gevalideerde_vragen_technisch/basisveiligheid/Indicator 1.2.rq')

        # Set parameters
        test.change_start_period("2023-01-01", "2024-01-01")
        test.change_end_period("2023-12-31","2024-12-31")

        # Verify actual result of the query
        test.verify_value("indicator", "100")


    finally:
        # Delete previously loaded data
        dg.delete_graph_data()

# Testcase 07b

def test_if_value_returned_is_correct_for_query_1_2_07_b(db_config):
    """ Testcase 07b (Wlz 4 t/m 10 (4VV) + Wel Decubitus (Categorie 2+) + Wel casuistiekbespreking + Vestiging nr. 1254)
        ODB basisveiligheid 1.2 Casuïstiekbespreking decubitus
    """

    # Load defined test data
    dg = DataGenerator(db_config, td_07_b)

    try:
        # Start a test
        test = QueryTest(db_config)

        # Set the query to be tested
        test.set_query('Gevalideerde_vragen_technisch/basisveiligheid/Indicator 1.2.rq')

        # Set parameters
        test.change_start_period("2023-01-01", "2024-01-01")
        test.change_end_period("2023-12-31","2024-12-31")

        # Verify actual result of the query
        test.verify_value("indicator", "100")


    finally:
        # Delete previously loaded data
        dg.delete_graph_data()

# Testcase 08

def test_if_value_returned_is_correct_for_query_1_2_08(db_config):
    """ Testcase 08 (Wlz 4 t/m 10 (4VV) + Wel Decubitus (Categorie 2 & 3) + Wel casuistiekbespreking (2x) + Geen vestiging)
        ODB basisveiligheid 1.2 Casuïstiekbespreking decubitus
    """

    # Load defined test data
    dg = DataGenerator(db_config, td_08)

    try:
        # Start a test
        test = QueryTest(db_config)

        # Set the query to be tested
        test.set_query('Gevalideerde_vragen_technisch/basisveiligheid/Indicator 1.2.rq')

        # Set parameters
        test.change_start_period("2023-01-01", "2024-01-01")
        test.change_end_period("2023-12-31","2024-12-31")

        # Verify actual result of the query
        test.verify_value("indicator", None)


    finally:
        # Delete previously loaded data
        dg.delete_graph_data()

# Testcase 08a

def test_if_value_returned_is_correct_for_query_1_2_08_a(db_config):
    """ Testcase 08a (Wlz 4 t/m 10 (4VV) + Wel Decubitus (Categorie 2 & 3) + Wel casuistiekbespreking (2x) + Vestiging nr. 1287)
        ODB basisveiligheid 1.2 Casuïstiekbespreking decubitus
    """

    # Load defined test data
    dg = DataGenerator(db_config, td_08_a)

    try:
        # Start a test
        test = QueryTest(db_config)

        # Set the query to be tested
        test.set_query('Gevalideerde_vragen_technisch/basisveiligheid/Indicator 1.2.rq')

        # Set parameters
        test.change_start_period("2023-01-01", "2024-01-01")
        test.change_end_period("2023-12-31","2024-12-31")

        # Verify actual result of the query
        test.verify_value("indicator", "100")


    finally:
        # Delete previously loaded data
        dg.delete_graph_data()

# Testcase 08b

def test_if_value_returned_is_correct_for_query_1_2_08_b(db_config):
    """ Testcase 08b (Wlz 4 t/m 10 (4VV) + Wel Decubitus (Categorie 2 & 3) + Wel casuistiekbespreking (2x) + Vestiging nr. 1254)
        ODB basisveiligheid 1.2 Casuïstiekbespreking decubitus
    """

    # Load defined test data
    dg = DataGenerator(db_config, td_08_b)

    try:
        # Start a test
        test = QueryTest(db_config)

        # Set the query to be tested
        test.set_query('Gevalideerde_vragen_technisch/basisveiligheid/Indicator 1.2.rq')

        # Set parameters
        test.change_start_period("2023-01-01", "2024-01-01")
        test.change_end_period("2023-12-31","2024-12-31")

        # Verify actual result of the query
        test.verify_value("indicator", "100")


    finally:
        # Delete previously loaded data
        dg.delete_graph_data()