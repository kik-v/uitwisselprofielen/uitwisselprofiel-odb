from DataGenerator import DataGenerator
from QueryTest import QueryTest

# Peildatum: 2024-03-01

# Opmerkingen: 


# Testcases: 

# Testcase 01

td_01 = [
    {
        "Description": "Testcase 01 (Geen Wlz + Geen Decubitus + Geen vestiging)",
        "Amount": 10,
        "Human": [
            {
                "NursingProcess": [
                    {
                        "identifier": "1",
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",
                        "location": "",
                        "related_indication": "1"
                    }
                ],
                "Indication": [
                    {
                        "identifier": "1",
                        "type": "WlzIndicatie",
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",
                        "heeftIndicatieBehandeling": True,
                        "ciz": [""],
                        "leveringsvorm": [""]
                    }
                ],
            }
        ]
    }
]

td_02 = [
    {
        "Description": "Testcase 02 (Wlz 1 t/m 3 (1VV) + Geen Decubitus + Geen vestiging)",
        "Amount": 10,
        "Human": [
            {
                "NursingProcess": [
                    {
                        "identifier": "1",
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",
                        "location": "",
                        "related_indication": "1"
                    }
                ],
                "Indication": [
                    {
                        "identifier": "1",
                        "type": "WlzIndicatie",
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",
                        "heeftIndicatieBehandeling": True,
                        "ciz": ["1VV"],
                        "leveringsvorm": [""]
                    }
                ],
            }
        ]
    }
]

td_03 = [
    {
        "Description": "Testcase 03 (Wlz 4 t/m 10 (4VV) + Geen Decubitus + Geen vestiging)",
        "Amount": 10,
        "Human": [
            {
                "NursingProcess": [
                    {
                        "identifier": "1",
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",
                        "location": "",
                        "related_indication": "1"
                    }
                ],
                "Indication": [
                    {
                        "identifier": "1",
                        "type": "WlzIndicatie",
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",
                        "heeftIndicatieBehandeling": True,
                        "ciz": ["4VV"],
                        "leveringsvorm": [""]
                    }
                ],
            }
        ]
    }
]

td_04 = [
    {
        "Description": "Testcase 04 (Wlz 4 t/m 10 (4VV) + Wel Decubitus (Geen categorie) + Geen vestiging)",
        "Amount": 10,
        "Human": [
            {
                "Decubitus": [
                    {
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",
                    }
                ],
                "NursingProcess": [
                    {
                        "identifier": "1",
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",
                        "location": "",
                        "related_indication": "1"
                    }
                ],
                "Indication": [
                    {
                        "identifier": "1",
                        "type": "WlzIndicatie",
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",
                        "heeftIndicatieBehandeling": True,
                        "ciz": ["4VV"],
                        "leveringsvorm": [""]
                    }
                ]
            }
        ]
    }
]

td_04_a = [
    {
        "Description": "Testcase 04a (Wlz 4 t/m 10 (4VV) + Wel Decubitus (Geen categorie) + Vestiging nr 1287)",
        "Amount": 10,
        "Human": [
            {
                "Decubitus": [
                    {
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",                    
                    }
                ],
                "NursingProcess": [
                    {
                        "identifier": "1",
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",
                        "location": "Locatie_BG_Rechts",
                        "related_indication": "1"
                    }
                ],
                "Indication": [
                    {
                        "identifier": "1",
                        "type": "WlzIndicatie",
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",
                        "heeftIndicatieBehandeling": True,
                        "ciz": ["4VV"],
                        "leveringsvorm": [""]
                    }
                ]
            }
        ]
    }
]

td_04_b = [
    {
        "Description": "Testcase 04b (Wlz 4 t/m 10 (4VV) + Wel Decubitus (Geen categorie) + Vestiging nr 1254)",
        "Amount": 10,
        "Human": [
            {
                "Decubitus": [
                    {
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",                    
                    }
                ],
                "NursingProcess": [
                    {
                        "identifier": "2",
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",
                        "location": "Locatie_Vleugel_A",
                        "related_indication": "2"
                    }
                ],
                "Indication": [
                    {
                        "identifier": "2",
                        "type": "WlzIndicatie",
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",
                        "heeftIndicatieBehandeling": True,
                        "ciz": ["4VV"],
                        "leveringsvorm": [""]
                    }
                ]
            }
        ]
    }
]

td_05 = [
    {
        "Description": "Testcase 05 (Wlz 4 t/m 10 (4VV) + Wel Decubitus (Categorie 1) + Geen vestiging)",
        "Amount": 10,
        "Human": [
            {
                "Decubitus": [
                    {
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",
                        "stage": [
                            {
                                "date": "2024-01-01",
                                "grade": 1
                            }
                        ],
                    }
                ],
                "NursingProcess": [
                    {
                        "identifier": "1",
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",
                        "location": "",
                        "related_indication": "1"
                    }
                ],
                "Indication": [
                    {
                        "identifier": "1",
                        "type": "WlzIndicatie",
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",
                        "heeftIndicatieBehandeling": True,
                        "ciz": ["4VV"],
                        "leveringsvorm": [""]
                    }
                ]
            }
        ]
    }
]

td_05_a = [
    {
        "Description": "Testcase 05a (Wlz 4 t/m 10 (4VV) + Wel Decubitus (Categorie 1) + Vestiging nr 1287)",
        "Amount": 10,
        "Human": [
            {
                "Decubitus": [
                    {
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",
                        "stage": [                            
                            {
                                "date": "2024-01-01",
                                "grade": 1
                            }
                        ],
                    }
                ],
                "NursingProcess": [
                    {
                        "identifier": "1",
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",
                        "location": "Locatie_BG_Rechts",
                        "related_indication": "1"
                    }
                ],
                "Indication": [
                    {
                        "identifier": "1",
                        "type": "WlzIndicatie",
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",
                        "heeftIndicatieBehandeling": True,
                        "ciz": ["4VV"],
                        "leveringsvorm": [""]
                    }
                ]
            }
        ]
    }
]

td_05_b = [
    {
        "Description": "Testcase 05b (Wlz 4 t/m 10 (4VV) + Wel Decubitus (Categorie 1) + Vestiging nr 1254)",
        "Amount": 10,
        "Human": [
            {
                "Decubitus": [
                    {
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",
                        "stage": [                            
                            {
                                "date": "2024-01-01",
                                "grade": 1
                            }
                        ],
                    }
                ],
                "NursingProcess": [
                    {
                        "identifier": "2",
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",
                        "location": "Locatie_Vleugel_A",
                        "related_indication": "2"
                    }
                ],
                "Indication": [
                    {
                        "identifier": "2",
                        "type": "WlzIndicatie",
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",
                        "heeftIndicatieBehandeling": True,
                        "ciz": ["4VV"],
                        "leveringsvorm": [""]
                    }
                ]
            }
        ]
    }
]

td_06 = [
    {
        "Description": "Testcase 06 (Wlz 4 t/m 10 (4VV) + Wel Decubitus (Categorie 2+) + Geen vestiging)",
        "Amount": 10,
        "Human": [
            {
                "Decubitus": [
                    {
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",
                        "stage": [
                            {
                                "date": "2024-01-01",
                                "grade": 2
                            }
                        ],
                    }
                ],
                "NursingProcess": [
                    {
                        "identifier": "1",
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",
                        "location": "",
                        "related_indication": "1"
                    }
                ],
                "Indication": [
                    {
                        "identifier": "1",
                        "type": "WlzIndicatie",
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",
                        "heeftIndicatieBehandeling": True,
                        "ciz": ["4VV"],
                        "leveringsvorm": [""]
                    }
                ]
            }
        ]
    }
]

td_06_a = [
    {
        "Description": "Testcase 06a (Wlz 4 t/m 10 (4VV) + Wel Decubitus (Categorie 2+) + Vestiging nr 1287)",
        "Amount": 20,
        "Human": [
            {
                "Decubitus": [
                    {
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",
                        "stage": [                            
                            {
                                "date": "2024-01-01",
                                "grade": 2
                            }
                        ],
                    }
                ],
                "NursingProcess": [
                    {
                        "identifier": "1",
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",
                        "location": "Locatie_BG_Rechts",
                        "related_indication": "1"
                    }
                ],
                "Indication": [
                    {
                        "identifier": "1",
                        "type": "WlzIndicatie",
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",
                        "heeftIndicatieBehandeling": True,
                        "ciz": ["4VV"],
                        "leveringsvorm": [""]
                    }
                ]
            }
        ]
    }
]

td_06_b = [
    {
        "Description": "Testcase 06b (Wlz 4 t/m 10 (4VV) + Wel Decubitus (Categorie 2+) + Vestiging nr 1254)",
        "Amount": 20,
        "Human": [
            {
                "Decubitus": [
                    {
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",
                        "stage": [                            
                            {
                                "date": "2024-01-01",
                                "grade": 2
                            }
                        ],
                    }
                ],
                "NursingProcess": [
                    {
                        "identifier": "2",
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",
                        "location": "Locatie_Vleugel_A",
                        "related_indication": "2"
                    }
                ],
                "Indication": [
                    {
                        "identifier": "2",
                        "type": "WlzIndicatie",
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",
                        "heeftIndicatieBehandeling": True,
                        "ciz": ["4VV"],
                        "leveringsvorm": [""]
                    }
                ]
            }
        ]
    }
]

# Static tests

def test_if_headers_are_correct_on_1_1(db_config):
    """ Test of de juiste headers terugkomen in het resultaat
        ODB Basisveiligheid 1.1. Decubitus
    """

    # Setup of the test
    test = QueryTest(db_config)

    # Configuration and execution
    test.set_query('Gevalideerde_vragen_technisch/basisveiligheid/Indicator 1.1.rq')

    # Assertions
    test.verify_header_present('vestiging')
    test.verify_header_present('teller')
    test.verify_header_present('noemer')
    test.verify_header_present('indicator')


def test_if_number_of_rows_returned_is_correct_for_query_1_1(db_config):
    """ Test of het aantal rijen correct wordt teruggegeven
        ODB Basisveiligheid 1.1. Decubitus
    """

    # Setup of the test
    test = QueryTest(db_config)

    # Configuration and execution
    test.set_query('Gevalideerde_vragen_technisch/basisveiligheid/Indicator 1.1.rq')

    # Assertions
    test.verify_row_count(1)


def test_if_indicator_has_correct_value_for_query_1_1(db_config):
    """ Test of de indicator de juiste waarde heeft
        ODB Basisveiligheid 1.1. Decubitus
    """

    # Setup of the test
    test = QueryTest(db_config)

    # Configuration and execution
    test.set_query('Gevalideerde_vragen_technisch/basisveiligheid/Indicator 1.1.rq')

    # Assertions
    test.verify_value("indicator","Waarneming kleiner dan 10", where_condition=("vestiging","000001287"))


def test_if_dates_can_change_1_1(db_config):
    """ Test of gewijzigde datum daadwerkelijk een ander resultaat oplevert
        ODB Basisveiligheid 1.1. Decubitus
    """

    # Setup of the test
    test = QueryTest(db_config)

    # Configuration and execution
    test.set_query('Gevalideerde_vragen_technisch/basisveiligheid/Indicator 1.1.rq')

    test.change_reference_date_to("2023-03-01","2024-03-01")
    # Assertions
    test.verify_value("indicator","Waarneming kleiner dan 10", where_condition=("vestiging","000001287"))

# Tests with generated testdata

# Testcase 01

def test_if_value_returned_is_correct_for_query_1_1_01(db_config):
    """ Testcase 01 (Geen Wlz + Geen Decubitus + Geen vestiging)
        ODB Basisveiligheid 1.1 Decubitus
    """

    # Load defined test data
    dg = DataGenerator(db_config, td_01)

    try:
        # Start a test
        test = QueryTest(db_config)

        # Set the query to be tested
        test.set_query('Gevalideerde_vragen_technisch/basisveiligheid/Indicator 1.1.rq')

        # Set parameters
        test.change_reference_date_to("2023-03-01", "2024-03-01")

        # Verify actual result of the query
        test.verify_value("indicator","Waarneming kleiner dan 10", where_condition=("vestiging","000001287"))


    finally:
        # Delete previously loaded data
        dg.delete_graph_data()

# Testcase 02

def test_if_value_returned_is_correct_for_query_1_1_02(db_config):
    """ Testcase 02 (Wlz 1 t/m 3 (1VV) + Geen Decubitus + Geen vestiging)
        ODB basisveiligheid 1.1 Decubitus
    """

    # Load defined test data
    dg = DataGenerator(db_config, td_02)

    try:
        # Start a test
        test = QueryTest(db_config)

        # Set the query to be tested
        test.set_query('Gevalideerde_vragen_technisch/basisveiligheid/Indicator 1.1.rq')

        # Set parameters
        test.change_reference_date_to("2023-03-01", "2024-03-01")

        # Verify actual result of the query
        test.verify_value("indicator","Waarneming kleiner dan 10", where_condition=("vestiging","000001287"))


    finally:
        # Delete previously loaded data
        dg.delete_graph_data()

# Testcase 03

def test_if_value_returned_is_correct_for_query_1_1_03(db_config):
    """ Testcase 03 (Wlz 4 t/m 10 (4VV) + Geen Decubitus + Geen vestiging)
        ODB basisveiligheid 1.1 Decubitus
    """

    # Load defined test data
    dg = DataGenerator(db_config, td_03)

    try:
        # Start a test
        test = QueryTest(db_config)

        # Set the query to be tested
        test.set_query('Gevalideerde_vragen_technisch/basisveiligheid/Indicator 1.1.rq')

        # Set parameters
        test.change_reference_date_to("2023-03-01", "2024-03-01")

        # Verify actual result of the query
        test.verify_value("indicator","Waarneming kleiner dan 10", where_condition=("vestiging","000001287"))

    finally:
        # Delete previously loaded data
        dg.delete_graph_data()

# Testcase 04

def test_if_value_returned_is_correct_for_query_1_1_04(db_config):
    """ Testcase 04 (Wlz 4 t/m 10 (4VV) + Wel Decubitus (Geen categorie) + Geen vestiging)
        ODB basisveiligheid 1.1 Decubitus
    """

    # Load defined test data
    dg = DataGenerator(db_config, td_04)

    try:
        # Start a test
        test = QueryTest(db_config)

        # Set the query to be tested
        test.set_query('Gevalideerde_vragen_technisch/basisveiligheid/Indicator 1.1.rq')

        # Set parameters
        test.change_reference_date_to("2023-03-01", "2024-03-01")

        # Verify actual result of the query
        test.verify_value("indicator","Waarneming kleiner dan 10", where_condition=("vestiging","000001287"))


    finally:
        # Delete previously loaded data
        dg.delete_graph_data()

# Testcase 04a

def test_if_value_returned_is_correct_for_query_1_1_04_a(db_config):
    """ Testcase 04a (Wlz 4 t/m 10 (4VV) + Wel Decubitus (Geen categorie) + Vestiging nr. 1287)
        ODB basisveiligheid 1.1 Decubitus
    """

    # Load defined test data
    dg = DataGenerator(db_config, td_04_a)

    try:
        # Start a test
        test = QueryTest(db_config)

        # Set the query to be tested
        test.set_query('Gevalideerde_vragen_technisch/basisveiligheid/Indicator 1.1.rq')

        # Set parameters
        test.change_reference_date_to("2023-03-01", "2024-03-01")

        # Verify actual result of the query
        test.verify_value("indicator","Waarneming kleiner dan 10", where_condition=("vestiging","000001287"))


    finally:
        # Delete previously loaded data
        dg.delete_graph_data()

# Testcase 04b

def test_if_value_returned_is_correct_for_query_1_1_04_b(db_config):
    """ Testcase 04b (Wlz 4 t/m 10 (4VV) + Wel Decubitus (Geen categorie) + Vestiging nr. 1254)
        ODB basisveiligheid 1.1 Decubitus
    """

    # Load defined test data
    dg = DataGenerator(db_config, td_04_b)

    try:
        # Start a test
        test = QueryTest(db_config)

        # Set the query to be tested
        test.set_query('Gevalideerde_vragen_technisch/basisveiligheid/Indicator 1.1.rq')

        # Set parameters
        test.change_reference_date_to("2023-03-01", "2024-03-01")

        # Verify actual result of the query
        test.verify_value("indicator","Waarneming kleiner dan 10", where_condition=("vestiging","000001287"))


    finally:
        # Delete previously loaded data
        dg.delete_graph_data()

# Testcase 05

def test_if_value_returned_is_correct_for_query_1_1_05(db_config):
    """ Testcase 05 (Wlz 4 t/m 10 (4VV) + Wel Decubitus (Categorie 1) + Geen vestiging)
        ODB basisveiligheid 1.1 Decubitus
    """

    # Load defined test data
    dg = DataGenerator(db_config, td_05)

    try:
        # Start a test
        test = QueryTest(db_config)

        # Set the query to be tested
        test.set_query('Gevalideerde_vragen_technisch/basisveiligheid/Indicator 1.1.rq')

        # Set parameters
        test.change_reference_date_to("2023-03-01", "2024-03-01")

        # Verify actual result of the query
        test.verify_value("indicator","Waarneming kleiner dan 10", where_condition=("vestiging","000001287"))


    finally:
        # Delete previously loaded data
        dg.delete_graph_data()

# Testcase 05a

def test_if_value_returned_is_correct_for_query_1_1_05_a(db_config):
    """ Testcase 05a (Wlz 4 t/m 10 (4VV) + Wel Decubitus (Categorie 1) + Vestiging nr. 1287)
        ODB basisveiligheid 1.1 Decubitus
    """

    # Load defined test data
    dg = DataGenerator(db_config, td_05_a)

    try:
        # Start a test
        test = QueryTest(db_config)

        # Set the query to be tested
        test.set_query('Gevalideerde_vragen_technisch/basisveiligheid/Indicator 1.1.rq')

        # Set parameters
        test.change_reference_date_to("2023-03-01", "2024-03-01")

        # Verify actual result of the query
        test.verify_value("indicator","Waarneming kleiner dan 10", where_condition=("vestiging","000001287"))


    finally:
        # Delete previously loaded data
        dg.delete_graph_data()

# Testcase 05b

def test_if_value_returned_is_correct_for_query_1_1_05_b(db_config):
    """ Testcase 05b (Wlz 4 t/m 10 (4VV) + Wel Decubitus (Categorie 1) + Vestiging nr. 1254)
        ODB basisveiligheid 1.1 Decubitus
    """

    # Load defined test data
    dg = DataGenerator(db_config, td_05_b)

    try:
        # Start a test
        test = QueryTest(db_config)

        # Set the query to be tested
        test.set_query('Gevalideerde_vragen_technisch/basisveiligheid/Indicator 1.1.rq')

        # Set parameters
        test.change_reference_date_to("2023-03-01", "2024-03-01")

        # Verify actual result of the query
        test.verify_value("indicator","Waarneming kleiner dan 10", where_condition=("vestiging","000001254"))


    finally:
        # Delete previously loaded data
        dg.delete_graph_data()

# Testcase 06

def test_if_value_returned_is_correct_for_query_1_1_06(db_config):
    """ Testcase 06 (Wlz 4 t/m 10 (4VV) + Wel Decubitus (Categorie 2+) + Geen vestiging)
        ODB basisveiligheid 1.1 Decubitus
    """

    # Load defined test data
    dg = DataGenerator(db_config, td_06)

    try:
        # Start a test
        test = QueryTest(db_config)

        # Set the query to be tested
        test.set_query('Gevalideerde_vragen_technisch/basisveiligheid/Indicator 1.1.rq')

        # Set parameters
        test.change_reference_date_to("2023-03-01", "2024-03-01")

        # Verify actual result of the query
        test.verify_value("indicator","Waarneming kleiner dan 10", where_condition=("vestiging","000001287"))


    finally:
        # Delete previously loaded data
        dg.delete_graph_data()

# Testcase 06a

def test_if_value_returned_is_correct_for_query_1_1_06_a(db_config):
    """ Testcase 06a (Wlz 4 t/m 10 (4VV) + Wel Decubitus (Categorie 2+) + Vestiging nr. 1287)
        ODB basisveiligheid 1.1 Decubitus
    """

    # Load defined test data
    dg = DataGenerator(db_config, td_06_a)

    try:
        # Start a test
        test = QueryTest(db_config)

        # Set the query to be tested
        test.set_query('Gevalideerde_vragen_technisch/basisveiligheid/Indicator 1.1.rq')

        # Set parameters
        test.change_reference_date_to("2023-03-01", "2024-03-01")

        # Verify actual result of the query
        test.verify_value("indicator","100", where_condition=("vestiging","000001287"))


    finally:
        # Delete previously loaded data
        dg.delete_graph_data()

# Testcase 06b

def test_if_value_returned_is_correct_for_query_1_1_06_b(db_config):
    """ Testcase 06b (Wlz 4 t/m 10 (4VV) + Wel Decubitus (Categorie 2+) + Vestiging nr. 1254)
        ODB basisveiligheid 1.1 Decubitus
    """

    # Load defined test data
    dg = DataGenerator(db_config, td_06_b)

    try:
        # Start a test
        test = QueryTest(db_config)

        # Set the query to be tested
        test.set_query('Gevalideerde_vragen_technisch/basisveiligheid/Indicator 1.1.rq')

        # Set parameters
        test.change_reference_date_to("2023-03-01", "2024-03-01")

        # Verify actual result of the query
        test.verify_value("indicator","100", where_condition=("vestiging","000001254"))


    finally:
        # Delete previously loaded data
        dg.delete_graph_data()