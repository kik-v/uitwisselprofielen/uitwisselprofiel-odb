from DataGenerator import DataGenerator
from QueryTest import QueryTest

# Opmerkingen: 


# Testcases: 

# Testcase 01

td_01 = [
    {
        "Description": "Testcase 01 (Geen Wlz Indicatie + Geen vestiging + Geen medicatiereview)", 
        "Amount": 10,
        "Human": [
            {
                "NursingProcess": [
                    {
                        "identifier": "1",
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",
                        "location": "",
                        "related_indication": "1"
                    }
                ],
                "Indication": [
                    {
                        "identifier": "1",
                        "type": "WlzIndicatie",
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",
                        "heeftIndicatieBehandeling": True,
                        "ciz": [""] ,
                        "leveringsvorm": [""]
                    }
                ]
            }
        ]
    }
]

td_02 = [
    {
        "Description": "Testcase 02 (Wlz Indicatie 1 t/m 3 (1VV) + Geen vestiging + Geen medicatiereview)", 
        "Amount": 10,
        "Human": [
            {
                "NursingProcess": [
                    {
                        "identifier": "1",
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",
                        "location": "",
                        "related_indication": "1"
                    }
                ],
                "Indication": [
                    {
                        "identifier": "1",
                        "type": "WlzIndicatie",
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",
                        "heeftIndicatieBehandeling": True,
                        "ciz": ["1VV"] ,
                        "leveringsvorm": [""]
                    }
                ]
            }
        ]
    }
]

td_02_a = [
    {
        "Description": "Testcase 02a (Wlz Indicatie 4 t/m 10 (4VV) + Geen vestiging + Geen medicatiereview)", 
        "Amount": 10,
        "Human": [
            {
                "NursingProcess": [
                    {
                        "identifier": "1",
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",
                        "location": "",
                        "related_indication": "1"
                    }
                ],
                "Indication": [
                    {
                        "identifier": "1",
                        "type": "WlzIndicatie",
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",
                        "heeftIndicatieBehandeling": True,
                        "ciz": ["4VV"] ,
                        "leveringsvorm": [""]
                    }
                ]
            }
        ]
    }
]

td_03 = [
    {
        "Description": "Testcase 03 (Wlz Indicatie 4 t/m 10 (4VV) + Wel vestiging (Vest. nr. 1287) + Geen medicatiereview)", 
        "Amount": 10,
        "Human": [
            {
                "NursingProcess": [
                    {
                        "identifier": "1",
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",
                        "location": "Locatie_Grotestraat_17",
                        "related_indication": "1"
                    }
                ],
                "Indication": [
                    {
                        "identifier": "1",
                        "type": "WlzIndicatie",
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",
                        "heeftIndicatieBehandeling": True,
                        "ciz": ["4VV"] ,
                        "leveringsvorm": [""]
                    }
                ]
            }
        ]
    }
]

td_03_a = [
    {
        "Description": "Testcase 03a (Wlz Indicatie 4 t/m 10 (4VV) + Wel vestiging (Vest. nr. 1254) + Geen medicatiereview)", 
        "Amount": 10,
        "Human": [
            {
                "NursingProcess": [
                    {
                        "identifier": "1",
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",
                        "location": "Locatie_De_Beuk_1",
                        "related_indication": "1"
                    }
                ],
                "Indication": [
                    {
                        "identifier": "1",
                        "type": "WlzIndicatie",
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",
                        "heeftIndicatieBehandeling": True,
                        "ciz": ["4VV"] ,
                        "leveringsvorm": [""]
                    }
                ]
            }
        ]
    }
]

td_04 = [
    {
        "Description": "Testcase 04 (Wlz Indicatie 4 t/m 10 (4VV) + Wel vestiging (Vest. nr. 1287) + Wel medicatiereview)", 
        "Amount": 10,
        "Human": [
            {
                "MedicationReview": [
                    {
                        "date": "2024-01-01",
                    }
                ],
                "NursingProcess": [
                    {
                        "identifier": "1",
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",
                        "location": "Locatie_Grotestraat_17",
                        "related_indication": "1"
                    }
                ],
                "Indication": [
                    {
                        "identifier": "1",
                        "type": "WlzIndicatie",
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",
                        "heeftIndicatieBehandeling": True,
                        "ciz": ["4VV"] ,
                        "leveringsvorm": [""]
                    }
                ]
            }
        ]
    }
]

td_04_a = [
    {
        "Description": "Testcase 04a (Wlz Indicatie 4 t/m 10 (4VV) + Wel vestiging (Vest. nr. 1254) + Wel medicatiereview)", 
        "Amount": 10,
        "Human": [
            {
                "MedicationReview": [
                    {
                        "date": "2024-01-01",
                    }
                ],
                "NursingProcess": [
                    {
                        "identifier": "1",
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",
                        "location": "Locatie_De_Beuk_1",
                        "related_indication": "1"
                    }
                ],
                "Indication": [
                    {
                        "identifier": "1",
                        "type": "WlzIndicatie",
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",
                        "heeftIndicatieBehandeling": True,
                        "ciz": ["4VV"] ,
                        "leveringsvorm": [""]
                    }
                ]
            }
        ]
    }
]

td_04_b = [
    {
        "Description": "Testcase 04b (Wlz Indicatie 4 t/m 10 (4VV) + Wel vestiging (Vest. nr. 1287) + 2 medicatiereviews)", 
        "Amount": 10,
        "Human": [
            {
                "MedicationReview": [
                    {
                        "date": "2024-01-01",
                    },
                    {
                        "date": "2024-03-01",
                    }
                ],
                "NursingProcess": [
                    {
                        "identifier": "1",
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",
                        "location": "Locatie_Grotestraat_17",
                        "related_indication": "1"
                    }
                ],
                "Indication": [
                    {
                        "identifier": "1",
                        "type": "WlzIndicatie",
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",
                        "heeftIndicatieBehandeling": True,
                        "ciz": ["4VV"] ,
                        "leveringsvorm": [""]
                    }
                ]
            }
        ]
    }
]

td_04_c = [
    {
        "Description": "Testcase 04c (Wlz Indicatie 4 t/m 10 (4VV) + Wel vestiging (Vest. nr. 1254) + 2 medicatiereviews)", 
        "Amount": 10,
        "Human": [
            {
                "MedicationReview": [
                    {
                        "date": "2024-01-01",
                    },
                    {
                        "date": "2024-03-01",
                    }
                ],
                "NursingProcess": [
                    {
                        "identifier": "1",
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",
                        "location": "Locatie_De_Beuk_1",
                        "related_indication": "1"
                    }
                ],
                "Indication": [
                    {
                        "identifier": "1",
                        "type": "WlzIndicatie",
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",
                        "heeftIndicatieBehandeling": True,
                        "ciz": ["4VV"] ,
                        "leveringsvorm": [""]
                    }
                ]
            }
        ]
    }
]

td_04_d = [
    {
        "Description": "Testcase 04d (Wlz Indicatie 4 t/m 10 (4VV) + 2 vestigingen (Vest. nr. 1287 & 1254) + 2 medicatiereviews (Tijdens 1e zorgproces in vest. nr. 1287))", 
        "Amount": 10,
        "Human": [
            {
                "MedicationReview": [
                    {
                        "date": "2024-01-01",
                    },
                    {
                        "date": "2024-03-01",
                    }
                ],
                "NursingProcess": [
                    {
                        "identifier": "1",
                        "start_date": "2024-01-01",
                        "end_date": "2024-06-30",
                        "location": "Locatie_Grotestraat_17",
                        "related_indication": "1"
                    },
                    {
                        "identifier": "2",
                        "start_date": "2024-07-01",
                        "end_date": "2024-12-31",
                        "location": "Locatie_De_Beuk_1",
                        "related_indication": "1"
                    },
                ],
                "Indication": [
                    {
                        "identifier": "1",
                        "type": "WlzIndicatie",
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",
                        "heeftIndicatieBehandeling": True,
                        "ciz": ["4VV"] ,
                        "leveringsvorm": [""]
                    }
                ]
            }
        ]
    }
]

td_04_e = [
    {
        "Description": "Testcase 04e (Wlz Indicatie 4 t/m 10 (4VV) + 2 vestigingen (Vest. nr. 1287 & 1254) + 2 medicatiereviews (Tijdens 2e zorgproces in vest. nr. 1254))", 
        "Amount": 10,
        "Human": [
            {
                "MedicationReview": [
                    {
                        "date": "2024-07-01",
                    },
                    {
                        "date": "2024-10-01",
                    }
                ],
                "NursingProcess": [
                    {
                        "identifier": "1",
                        "start_date": "2024-01-01",
                        "end_date": "2024-06-30",
                        "location": "Locatie_Grotestraat_17",
                        "related_indication": "1"
                    },
                    {
                        "identifier": "2",
                        "start_date": "2024-07-01",
                        "end_date": "2024-12-31",
                        "location": "Locatie_De_Beuk_1",
                        "related_indication": "1"
                    },
                ],
                "Indication": [
                    {
                        "identifier": "1",
                        "type": "WlzIndicatie",
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",
                        "heeftIndicatieBehandeling": True,
                        "ciz": ["4VV"] ,
                        "leveringsvorm": [""]
                    }
                ]
            }
        ]
    }
]

td_04_f = [
    {
        "Description": "Testcase 04f (Wlz Indicatie 4 t/m 10 (4VV) + 2 vestigingen (Vest. nr. 1287 & 1254) + 2 medicatiereviews (1 in elk zorgproces))", 
        "Amount": 10,
        "Human": [
            {
                "MedicationReview": [
                    {
                        "date": "2024-01-01",
                    },
                    {
                        "date": "2024-10-01",
                    }
                ],
                "NursingProcess": [
                    {
                        "identifier": "1",
                        "start_date": "2024-01-01",
                        "end_date": "2024-06-30",
                        "location": "Locatie_Grotestraat_17",
                        "related_indication": "1"
                    },
                    {
                        "identifier": "2",
                        "start_date": "2024-07-01",
                        "end_date": "2024-12-31",
                        "location": "Locatie_De_Beuk_1",
                        "related_indication": "1"
                    },
                ],
                "Indication": [
                    {
                        "identifier": "1",
                        "type": "WlzIndicatie",
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",
                        "heeftIndicatieBehandeling": True,
                        "ciz": ["4VV"] ,
                        "leveringsvorm": [""]
                    }
                ]
            }
        ]
    }
]

td_04_g = [
    {
        "Description": "Testcase 04g (Wlz Indicatie 4 t/m 10 (4VV) + 2 vestigingen (Vest. nr. 1287 & 1254) + 1 medicatiereview (in 1e zorgproces vest nr. 1287))", 
        "Amount": 10,
        "Human": [
            {
                "MedicationReview": [
                    {
                        "date": "2024-01-01",
                    }
                ],
                "NursingProcess": [
                    {
                        "identifier": "1",
                        "start_date": "2024-01-01",
                        "end_date": "2024-06-30",
                        "location": "Locatie_Grotestraat_17",
                        "related_indication": "1"
                    },
                    {
                        "identifier": "2",
                        "start_date": "2024-07-01",
                        "end_date": "2024-12-31",
                        "location": "Locatie_De_Beuk_1",
                        "related_indication": "1"
                    },
                ],
                "Indication": [
                    {
                        "identifier": "1",
                        "type": "WlzIndicatie",
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",
                        "heeftIndicatieBehandeling": True,
                        "ciz": ["4VV"] ,
                        "leveringsvorm": [""]
                    }
                ]
            }
        ]
    }
]

td_04_h = [
    {
        "Description": "Testcase 04h (Wlz Indicatie 4 t/m 10 (4VV) + 2 vestigingen (Vest. nr. 1287 & 1254) + 1 medicatiereview (in 2e zorgproces vest nr. 1254))", 
        "Amount": 10,
        "Human": [
            {
                "MedicationReview": [
                    {
                        "date": "2024-10-01",
                    }
                ],
                "NursingProcess": [
                    {
                        "identifier": "1",
                        "start_date": "2024-01-01",
                        "end_date": "2024-06-30",
                        "location": "Locatie_Grotestraat_17",
                        "related_indication": "1"
                    },
                    {
                        "identifier": "2",
                        "start_date": "2024-07-01",
                        "end_date": "2024-12-31",
                        "location": "Locatie_De_Beuk_1",
                        "related_indication": "1"
                    },
                ],
                "Indication": [
                    {
                        "identifier": "1",
                        "type": "WlzIndicatie",
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",
                        "heeftIndicatieBehandeling": True,
                        "ciz": ["4VV"] ,
                        "leveringsvorm": [""]
                    }
                ]
            }
        ]
    }
]

td_05 = [
    {
        "Description": "Testcase 05 (Wlz Indicatie 1 t/m 3 (1VV) + Wel vestiging (Vest. nr. 1287) + Wel medicatiereview)", 
        "Amount": 10,
        "Human": [
            {
                "MedicationReview": [
                    {
                        "date": "2024-01-01",
                    }
                ],
                "NursingProcess": [
                    {
                        "identifier": "1",
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",
                        "location": "Locatie_Grotestraat_17",
                        "related_indication": "1"
                    }
                ],
                "Indication": [
                    {
                        "identifier": "1",
                        "type": "WlzIndicatie",
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",
                        "heeftIndicatieBehandeling": True,
                        "ciz": ["1VV"] ,
                        "leveringsvorm": [""]
                    }
                ]
            }
        ]
    }
]

td_05_a = [
    {
        "Description": "Testcase 05a (Geen Wlz Indicatie + Wel vestiging (Vest. nr. 1287) + Wel medicatiereview)", 
        "Amount": 10,
        "Human": [
            {
                "MedicationReview": [
                    {
                        "date": "2024-01-01",
                    }
                ],
                "NursingProcess": [
                    {
                        "identifier": "1",
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",
                        "location": "Locatie_Grotestraat_17",
                        "related_indication": "1"
                    }
                ],
                "Indication": [
                    {
                        "identifier": "1",
                        "type": "WlzIndicatie",
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",
                        "heeftIndicatieBehandeling": True,
                        "ciz": [""] ,
                        "leveringsvorm": [""]
                    }
                ]
            }
        ]
    }
]

td_06 = [
    {
        "Description": "Testcase 06 (Geen Wlz Indicatie + Geen vestiging + Wel medicatiereview)", 
        "Amount": 10,
        "Human": [
            {
                "MedicationReview": [
                    {
                        "date": "2024-01-01",
                    }
                ],
                "NursingProcess": [
                    {
                        "identifier": "1",
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",
                        "location": "",
                        "related_indication": "1"
                    }
                ],
                "Indication": [
                    {
                        "identifier": "1",
                        "type": "WlzIndicatie",
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",
                        "heeftIndicatieBehandeling": True,
                        "ciz": ["4VV"] ,
                        "leveringsvorm": [""]
                    }
                ]
            }
        ]
    }
]

# Static tests

def test_if_headers_are_correct_on_3_2(db_config):
    """ Test of de juiste headers terugkomen in het resultaat
        ODB Basisveiligheid 3.2. Medicatiereview
    """

    # Setup of the test
    test = QueryTest(db_config)

    # Configuration and execution
    test.set_query('Gevalideerde_vragen_technisch/basisveiligheid/Indicator 3.2.rq')

    # Assertions
    test.verify_header_present('teller')
    test.verify_header_present('noemer')
    test.verify_header_present('indicator')
    test.verify_header_present('vestiging')


def test_if_number_of_rows_returned_is_correct_for_query_3_2(db_config):
    """ Test of het aantal rijen correct wordt teruggegeven
        ODB Basisveiligheid 3.2. Medicatiereview
    """

    # Setup of the test
    test = QueryTest(db_config)

    # Configuration and execution
    test.set_query('Gevalideerde_vragen_technisch/basisveiligheid/Indicator 3.2.rq')

    # Assertions
    test.verify_row_count(1)


def test_if_indicator_has_correct_value_for_query_3_2(db_config):
    """ Test of de indicator de juiste waarde heeft
        ODB Basisveiligheid 3.2. Medicatiereview
    """

    # Setup of the test
    test = QueryTest(db_config)

    # Configuration and execution
    test.set_query('Gevalideerde_vragen_technisch/basisveiligheid/Indicator 3.2.rq')

    # Assertions
    test.verify_value_in_list("indicator",{"0","0.0"})


def test_if_dates_can_change_3_2(db_config):
    """ Test of gewijzigde datum daadwerkelijk een ander resultaat oplevert
        ODB Basisveiligheid 3.2. Medicatiereview
    """

    # Setup of the test
    test = QueryTest(db_config)

    # Configuration and execution
    test.set_query('Gevalideerde_vragen_technisch/basisveiligheid/Indicator 3.2.rq')

    test.change_start_period("2023-01-01", "2024-01-01")
    test.change_end_period("2023-10-31", "2024-10-31")

    # Assertions
    test.verify_value_in_list("indicator",{"0", "0.0"})

# Tests with Generated Data

# Testcase 01
def test_if_value_returned_is_correct_for_query_3_2_01(db_config):
    """ Testcase 01 (Geen Wlz Indicatie + Geen vestiging + Geen Medicatiereview)
        ODB Basisveiligheid 3.2. Medicatiereview
    """

    # Load defined test data
    dg = DataGenerator(db_config, td_01)

    try:
        # Start a test
        test = QueryTest(db_config)

        # Set the query to be tested
        test.set_query('Gevalideerde_vragen_technisch/basisveiligheid/Indicator 3.2.rq')

        # Change measuring period parameters of query
        test.change_start_period("2023-01-01", "2024-01-01")
        test.change_end_period("2023-10-31", "2024-10-31")

        # Verify actual result of the query
        test.verify_value_in_list("indicator", {"0", "0.0"})

    finally:
        # Delete previously loaded data
        dg.delete_graph_data()

# Testcase 02
def test_if_value_returned_is_correct_for_query_3_2_02(db_config):
    """ Testcase 02 (Wlz Indicatie 1 t/m 3 (1VV) + Geen vestiging + Geen Medicatiereview)
        ODB Basisveiligheid 3.2. Medicatiereview
    """

    # Load defined test data
    dg = DataGenerator(db_config, td_02)

    try:
        # Start a test
        test = QueryTest(db_config)

        # Set the query to be tested
        test.set_query('Gevalideerde_vragen_technisch/basisveiligheid/Indicator 3.2.rq')

        # Change measuring period parameters of query
        test.change_start_period("2023-01-01", "2024-01-01")
        test.change_end_period("2023-10-31", "2024-10-31")

        # Verify actual result of the query
        test.verify_value_in_list("indicator", {"0", "0.0"})

    finally:
        # Delete previously loaded data
        dg.delete_graph_data()

# Testcase 02a
def test_if_value_returned_is_correct_for_query_3_2_02_a(db_config):
    """ Testcase 02a (Wlz Indicatie 4 t/m 10 (4VV) + Geen vestiging + Geen Medicatiereview)
        ODB Basisveiligheid 3.2. Medicatiereview
    """

    # Load defined test data
    dg = DataGenerator(db_config, td_02_a)

    try:
        # Start a test
        test = QueryTest(db_config)

        # Set the query to be tested
        test.set_query('Gevalideerde_vragen_technisch/basisveiligheid/Indicator 3.2.rq')

        # Change measuring period parameters of query
        test.change_start_period("2023-01-01", "2024-01-01")
        test.change_end_period("2023-10-31", "2024-10-31")

        # Verify actual result of the query
        test.verify_value_in_list("indicator", {"0", "0.0"})

    finally:
        # Delete previously loaded data
        dg.delete_graph_data()

# Testcase 03
def test_if_value_returned_is_correct_for_query_3_2_03(db_config):
    """ Testcase 03 (Wlz Indicatie 4 t/m 10 (4VV) + Wel vestiging (Vest. nr. 1287) + Geen Medicatiereview)
        ODB Basisveiligheid 3.2. Medicatiereview
    """

    # Load defined test data
    dg = DataGenerator(db_config, td_03)

    try:
        # Start a test
        test = QueryTest(db_config)

        # Set the query to be tested
        test.set_query('Gevalideerde_vragen_technisch/basisveiligheid/Indicator 3.2.rq')

        # Change measuring period parameters of query
        test.change_start_period("2023-01-01", "2024-01-01")
        test.change_end_period("2023-10-31", "2024-10-31")

        # Verify actual result of the query
        test.verify_value_in_list("indicator", {"0", "0.0"})

    finally:
        # Delete previously loaded data
        dg.delete_graph_data()

# Testcase 03a
def test_if_value_returned_is_correct_for_query_3_2_03_a(db_config):
    """ Testcase 03a (Wlz Indicatie 4 t/m 10 (4VV) + Wel vestiging (Vest. nr. 1254) + Geen Medicatiereview)
        ODB Basisveiligheid 3.2. Medicatiereview
    """

    # Load defined test data
    dg = DataGenerator(db_config, td_03_a)

    try:
        # Start a test
        test = QueryTest(db_config)

        # Set the query to be tested
        test.set_query('Gevalideerde_vragen_technisch/basisveiligheid/Indicator 3.2.rq')

        # Change measuring period parameters of query
        test.change_start_period("2023-01-01", "2024-01-01")
        test.change_end_period("2023-10-31", "2024-10-31")

        # Verify actual result of the query
        test.verify_value_in_list("indicator", {"0", "0.0"})

    finally:
        # Delete previously loaded data
        dg.delete_graph_data()

# Testcase 04
def test_if_value_returned_is_correct_for_query_3_2_04(db_config):
    """ Testcase 04 (Wlz Indicatie 4 t/m 10 (4VV) + Wel vestiging (Vest. nr. 1287) + Wel Medicatiereview)
        ODB Basisveiligheid 3.2. Medicatiereview
    """

    # Load defined test data
    dg = DataGenerator(db_config, td_04)

    try:
        # Start a test
        test = QueryTest(db_config)

        # Set the query to be tested
        test.set_query('Gevalideerde_vragen_technisch/basisveiligheid/Indicator 3.2.rq')

        # Change measuring period parameters of query
        test.change_start_period("2023-01-01", "2024-01-01")
        test.change_end_period("2023-10-31", "2024-10-31")

        # Verify actual result of the query
        test.verify_value_in_list("indicator", {"83", "83.0"})

    finally:
        # Delete previously loaded data
        dg.delete_graph_data()

# Testcase 04a
def test_if_value_returned_is_correct_for_query_3_2_04_a(db_config):
    """ Testcase 04a (Wlz Indicatie 4 t/m 10 (4VV) + Wel vestiging (Vest. nr. 1254) + Wel Medicatiereview)
        ODB Basisveiligheid 3.2. Medicatiereview
    """

    # Load defined test data
    dg = DataGenerator(db_config, td_04_a)

    try:
        # Start a test
        test = QueryTest(db_config)

        # Set the query to be tested
        test.set_query('Gevalideerde_vragen_technisch/basisveiligheid/Indicator 3.2.rq')

        # Change measuring period parameters of query
        test.change_start_period("2023-01-01", "2024-01-01")
        test.change_end_period("2023-10-31", "2024-10-31")

        # Verify actual result of the query
        test.verify_value_in_list("indicator", {"0", "0.0"})

    finally:
        # Delete previously loaded data
        dg.delete_graph_data()

# Testcase 04b
def test_if_value_returned_is_correct_for_query_3_2_04_b(db_config):
    """ Testcase 04b (Wlz Indicatie 4 t/m 10 (4VV) + Wel vestiging (Vest. nr. 1287) + 2 Medicatiereviews)
        ODB Basisveiligheid 3.2. Medicatiereview
    """

    # Load defined test data
    dg = DataGenerator(db_config, td_04_b)

    try:
        # Start a test
        test = QueryTest(db_config)

        # Set the query to be tested
        test.set_query('Gevalideerde_vragen_technisch/basisveiligheid/Indicator 3.2.rq')

        # Change measuring period parameters of query
        test.change_start_period("2023-01-01", "2024-01-01")
        test.change_end_period("2023-10-31", "2024-10-31")

        # Verify actual result of the query
        test.verify_value_in_list("indicator", {"83", "83.0"})

    finally:
        # Delete previously loaded data
        dg.delete_graph_data()

# Testcase 04c
def test_if_value_returned_is_correct_for_query_3_2_04_c(db_config):
    """ Testcase 04c (Wlz Indicatie 4 t/m 10 (4VV) + Wel vestiging (Vest. nr. 1254) + 2 Medicatiereviews)
        ODB Basisveiligheid 3.2. Medicatiereview
    """

    # Load defined test data
    dg = DataGenerator(db_config, td_04_c)

    try:
        # Start a test
        test = QueryTest(db_config)

        # Set the query to be tested
        test.set_query('Gevalideerde_vragen_technisch/basisveiligheid/Indicator 3.2.rq')

        # Change measuring period parameters of query
        test.change_start_period("2023-01-01", "2024-01-01")
        test.change_end_period("2023-10-31", "2024-10-31")

        # Verify actual result of the query
        test.verify_value_in_list("indicator", {"0", "0.0"})

    finally:
        # Delete previously loaded data
        dg.delete_graph_data()

# Testcase 04d
def test_if_value_returned_is_correct_for_query_3_2_04_d(db_config):
    """ Testcase 04d (Wlz Indicatie 4 t/m 10 (4VV) + 2 vestigingen (Vest. nr. 1287 & 1254) + 2 Medicatiereviews (Tijdens 2e zorgproces in vest. nr. 1254))
        ODB Basisveiligheid 3.2. Medicatiereview
    """

    # Load defined test data
    dg = DataGenerator(db_config, td_04_d)

    try:
        # Start a test
        test = QueryTest(db_config)

        # Set the query to be tested
        test.set_query('Gevalideerde_vragen_technisch/basisveiligheid/Indicator 3.2.rq')

        # Change measuring period parameters of query
        test.change_start_period("2023-01-01", "2024-01-01")
        test.change_end_period("2023-10-31", "2024-10-31")

        # Verify actual result of the query
        test.verify_value_in_list("indicator", {"0", "0.0"})

    finally:
        # Delete previously loaded data
        dg.delete_graph_data()

# Testcase 04d
def test_if_value_returned_is_correct_for_query_3_2_04_d(db_config):
    """ Testcase 04d (Wlz Indicatie 4 t/m 10 (4VV) + 2 vestigingen (Vest. nr. 1287 & 1254) + 2 Medicatiereviews (Tijdens 1e zorgproces in vest. nr. 1287))
        ODB Basisveiligheid 3.2. Medicatiereview
    """

    # Load defined test data
    dg = DataGenerator(db_config, td_04_d)

    try:
        # Start a test
        test = QueryTest(db_config)

        # Set the query to be tested
        test.set_query('Gevalideerde_vragen_technisch/basisveiligheid/Indicator 3.2.rq')

        # Change measuring period parameters of query
        test.change_start_period("2023-01-01", "2024-01-01")
        test.change_end_period("2023-10-31", "2024-10-31")

        # Verify actual result of the query
        test.verify_value_in_list("indicator", {"0", "0.0"})

    finally:
        # Delete previously loaded data
        dg.delete_graph_data()

# Testcase 04e
def test_if_value_returned_is_correct_for_query_3_2_04_e(db_config):
    """ Testcase 04e (Wlz Indicatie 4 t/m 10 (4VV) + 2 vestigingen (Vest. nr. 1287 & 1254) + 2 Medicatiereviews (Tijdens 2e zorgproces in vest. nr. 1254))
        ODB Basisveiligheid 3.2. Medicatiereview
    """

    # Load defined test data
    dg = DataGenerator(db_config, td_04_e)

    try:
        # Start a test
        test = QueryTest(db_config)

        # Set the query to be tested
        test.set_query('Gevalideerde_vragen_technisch/basisveiligheid/Indicator 3.2.rq')

        # Change measuring period parameters of query
        test.change_start_period("2023-01-01", "2024-01-01")
        test.change_end_period("2023-10-31", "2024-10-31")

        # Verify actual result of the query
        test.verify_value_in_list("indicator", {"0", "0.0"})

    finally:
        # Delete previously loaded data
        dg.delete_graph_data()

# Testcase 04f
def test_if_value_returned_is_correct_for_query_3_2_04_f(db_config):
    """ Testcase 04f (Wlz Indicatie 4 t/m 10 (4VV) + 2 vestigingen (Vest. nr. 1287 & 1254) + 2 Medicatiereviews (1 in elk zorgproces))
        ODB Basisveiligheid 3.2. Medicatiereview
    """

    # Load defined test data
    dg = DataGenerator(db_config, td_04_f)

    try:
        # Start a test
        test = QueryTest(db_config)

        # Set the query to be tested
        test.set_query('Gevalideerde_vragen_technisch/basisveiligheid/Indicator 3.2.rq')

        # Change measuring period parameters of query
        test.change_start_period("2023-01-01", "2024-01-01")
        test.change_end_period("2023-10-31", "2024-10-31")

        # Verify actual result of the query
        test.verify_value_in_list("indicator", {"0", "0.0"})

    finally:
        # Delete previously loaded data
        dg.delete_graph_data()

# Testcase 04g
def test_if_value_returned_is_correct_for_query_3_2_04_g(db_config):
    """ Testcase 04g (Wlz Indicatie 4 t/m 10 (4VV) + 2 vestigingen (Vest. nr. 1287 & 1254) + Wel Medicatiereview (In 1e zorgproces vest. nr. 1287))
        ODB Basisveiligheid 3.2. Medicatiereview
    """

    # Load defined test data
    dg = DataGenerator(db_config, td_04_g)

    try:
        # Start a test
        test = QueryTest(db_config)

        # Set the query to be tested
        test.set_query('Gevalideerde_vragen_technisch/basisveiligheid/Indicator 3.2.rq')

        # Change measuring period parameters of query
        test.change_start_period("2023-01-01", "2024-01-01")
        test.change_end_period("2023-10-31", "2024-10-31")

        # Verify actual result of the query
        test.verify_value_in_list("indicator", {"0", "0.0"})

    finally:
        # Delete previously loaded data
        dg.delete_graph_data()

# Testcase 04h
def test_if_value_returned_is_correct_for_query_3_2_04_h(db_config):
    """ Testcase 04h (Wlz Indicatie 4 t/m 10 (4VV) + 2 vestigingen (Vest. nr. 1287 & 1254) + Wel Medicatiereview (In 2e zorgproces vest. nr. 1254))
        ODB Basisveiligheid 3.2. Medicatiereview
    """

    # Load defined test data
    dg = DataGenerator(db_config, td_04_h)

    try:
        # Start a test
        test = QueryTest(db_config)

        # Set the query to be tested
        test.set_query('Gevalideerde_vragen_technisch/basisveiligheid/Indicator 3.2.rq')

        # Change measuring period parameters of query
        test.change_start_period("2023-01-01", "2024-01-01")
        test.change_end_period("2023-10-31", "2024-10-31")

        # Verify actual result of the query
        test.verify_value_in_list("indicator", {"0", "0.0"})

    finally:
        # Delete previously loaded data
        dg.delete_graph_data()

# Testcase 05
def test_if_value_returned_is_correct_for_query_3_2_05(db_config):
    """ Testcase 05 (Wlz Indicatie 1 t/m 3 (1VV) + Wel vestiging (Vest. nr. 1287) + Wel medicatiereview)
        ODB Basisveiligheid 3.2. Medicatiereview
    """

    # Load defined test data
    dg = DataGenerator(db_config, td_05)

    try:
        # Start a test
        test = QueryTest(db_config)

        # Set the query to be tested
        test.set_query('Gevalideerde_vragen_technisch/basisveiligheid/Indicator 3.2.rq')

        # Change measuring period parameters of query
        test.change_start_period("2023-01-01", "2024-01-01")
        test.change_end_period("2023-10-31", "2024-10-31")

        # Verify actual result of the query
        test.verify_value_in_list("indicator", {"0", "0.0"})

    finally:
        # Delete previously loaded data
        dg.delete_graph_data()

# Testcase 05a
def test_if_value_returned_is_correct_for_query_3_2_05_a(db_config):
    """ Testcase 05a (Geen Wlz Indicatie + Wel vestiging (Vest. nr. 1287) + Wel medicatiereview)
        ODB Basisveiligheid 3.2. Medicatiereview
    """

    # Load defined test data
    dg = DataGenerator(db_config, td_05_a)

    try:
        # Start a test
        test = QueryTest(db_config)

        # Set the query to be tested
        test.set_query('Gevalideerde_vragen_technisch/basisveiligheid/Indicator 3.2.rq')

        # Change measuring period parameters of query
        test.change_start_period("2023-01-01", "2024-01-01")
        test.change_end_period("2023-10-31", "2024-10-31")

        # Verify actual result of the query
        test.verify_value_in_list("indicator", {"0", "0.0"})

    finally:
        # Delete previously loaded data
        dg.delete_graph_data()

# Testcase 06
def test_if_value_returned_is_correct_for_query_3_2_06(db_config):
    """ Testcase 06 (Geen Wlz Indicatie + Geen vestiging + Wel medicatiereview)
        ODB Basisveiligheid 3.2. Medicatiereview
    """

    # Load defined test data
    dg = DataGenerator(db_config, td_06)

    try:
        # Start a test
        test = QueryTest(db_config)

        # Set the query to be tested
        test.set_query('Gevalideerde_vragen_technisch/basisveiligheid/Indicator 3.2.rq')

        # Change measuring period parameters of query
        test.change_start_period("2023-01-01", "2024-01-01")
        test.change_end_period("2023-10-31", "2024-10-31")

        # Verify actual result of the query
        test.verify_value_in_list("indicator", {"0", "0.0"})

    finally:
        # Delete previously loaded data
        dg.delete_graph_data()