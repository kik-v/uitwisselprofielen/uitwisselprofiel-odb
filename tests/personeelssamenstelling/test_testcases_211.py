from DataGenerator import DataGenerator
from QueryTest import QueryTest

# Meetperiode startdatum: 01-01-2024
# Meetperiode einddatum: 31-12-2024

# Opmerkingen:
# Indicator score van oude standaard testdata = 9. 
# Een assertion waarde van 9 wordt gezien alsof de testcase niet wordt meegeteld in de indicator berekening
# Meerdere opeenvolgende arbeidsoverenkomsten in JSON structuur aangepast

# Testcases:

td_01 = [
    {
        "Description": "Testcase 01 (Geen ZVL functie + Geen AOK)",
        "Amount": 10, #Indicator score: 0 
        "Human": [
            {
                "InhuurOvereenkomst": [
                    {
                        "function": [
                            {
                                "label": "INH OK Geen ZVL",
                                "caregiving_role": False,
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31",
                            }
                        ],
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",                        
                        "size": [
                            {
                                "unit": 36,
                                "unit_of_measure": "Uren_per_week_unit",
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31"
                            }
                        ]
                    }
                ]
            }
        ]
    }
]   

td_02 = [
    {
        "Description": "Testcase 02 (Geen ZVL functie + Wel AOK)",
        "Amount": 10, #Indicator score: 0
         "Human": [
            {
                "ArbeidsOvereenkomst": [
                    {
                        "function": [
                            {
                                "label": "AOK Geen ZVL",
                                "caregiving_role": False,
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31",
                            }
                        ],
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",                        
                        "size": [
                            {
                                "unit": 36,
                                "unit_of_measure": "Uren_per_week_unit",
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31"
                            }
                        ]
                    }
                ]
            }
        ]
    }
] 

td_03 = [
    {
        "Description": "Testcase 03 (Wel ZVL functie + Geen AOK - bijvoorbeeld Inhuur/uitzend/stage OK)",
        "Amount": 10, #Indicator score: 0
        "Human": [
            {
                "InhuurOvereenkomst": [
                    {
                        "function": [
                            {
                                "label": "INH OK wel ZVL",
                                "caregiving_role": True,
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31",
                            }
                        ],
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",                        
                        "size": [
                            {
                                "unit": 36,
                                "unit_of_measure": "Uren_per_week_unit",
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31"
                            }
                        ]
                    }
                ]
            }
        ]
    }
] 

td_04 = [
    {
        "Description": "Testcase 04 (Wel ZVL functie + Wel AOK (12m))",
        "Amount": 20, #Indicator score: 10
        "Human": [
            {
                "ArbeidsOvereenkomst": [
                    {
                        "function": [
                            {
                                "label": "AOK Wel ZVL",
                                "caregiving_role": True,
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31",
                            }
                        ],
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",                        
                        "size": [
                            {
                                "unit": 36,
                                "unit_of_measure": "Uren_per_week_unit",
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31"
                            }
                        ]
                    }
                ]
            }
        ]
    }
] 

td_05 = [
    {
        "Description": "Testcase 05 (Wel ZVL functie + Wel AOK (6m))",
        "Amount": 10, #Indicator score: 5 
        "Human": [
            {
                "ArbeidsOvereenkomst": [
                    {
                        "function": [
                            {
                                "label": "AOK Wel ZVL",
                                "caregiving_role": True,
                                "start_date": "2024-01-01",
                                "end_date": "2024-06-30",
                            }
                        ],
                        "start_date": "2024-01-01",
                        "end_date": "2024-06-30",                        
                        "size": [
                            {
                                "unit": 36,
                                "unit_of_measure": "Uren_per_week_unit",
                                "start_date": "2024-01-01",
                                "end_date": "2024-06-30"
                            }
                        ]
                    }
                ]
            }
        ]
    }
] 

td_06 = [
    {
        "Description": "Testcase 06 (Wel ZVL functie + Geen ZVL functie + Wel AOK (6m))",
        "Amount": 1, #Indicator score: 0.5
        "Human": [
            {
                "ArbeidsOvereenkomst": [
                    {
                        "function": [
                            {
                                "label": "AOK Wel ZVL",
                                "caregiving_role": True,
                                "start_date": "2024-01-01",
                                "end_date": "2024-06-30",
                            }
                        ],
                        "start_date": "2024-01-01",
                        "end_date": "2024-06-30",                        
                        "size": [
                            {
                                "unit": 36,
                                "unit_of_measure": "Uren_per_week_unit",
                                "start_date": "2024-01-01",
                                "end_date": "2024-06-30"
                            }
                        ]
                    },
                    {
                        "function": [
                            {
                                "label": "AOK Geen ZVL",
                                "caregiving_role": False,
                                "start_date": "2024-07-01",
                                "end_date": "2024-12-31",
                            }
                        ],
                        "start_date": "2024-07-01",
                        "end_date": "2024-12-31",                        
                        "size": [
                            {
                                "unit": 36,
                                "unit_of_measure": "Uren_per_week_unit",
                                "start_date": "2024-07-01",
                                "end_date": "2024-12-31"
                            }
                        ]
                    }
                ]
            }
        ]
    }
] 

td_06_a = [
    {
        "Description": "Testcase 06a (Wel ZVL functie + Geen ZVL functie + Wel AOK (6m))",
        "Amount": 10, #Indicator score: 5
        "Human": [
            {
                "ArbeidsOvereenkomst": [
                    {
                        "function": [
                            {
                                "label": "AOK Wel ZVL",
                                "caregiving_role": True,
                                "start_date": "2024-01-01",
                                "end_date": "2024-06-30",
                            }
                        ],
                        "start_date": "2024-01-01",
                        "end_date": "2024-06-30",                        
                        "size": [
                            {
                                "unit": 36,
                                "unit_of_measure": "Uren_per_week_unit",
                                "start_date": "2024-01-01",
                                "end_date": "2024-06-30"
                            }
                        ]
                    },
                    {
                        "function": [
                            {
                                "label": "AOK Geen ZVL",
                                "caregiving_role": False,
                                "start_date": "2024-07-01",
                                "end_date": "2024-12-31",
                            }
                        ],
                        "start_date": "2024-07-01",
                        "end_date": "2024-12-31",                        
                        "size": [
                            {
                                "unit": 36,
                                "unit_of_measure": "Uren_per_week_unit",
                                "start_date": "2024-07-01",
                                "end_date": "2024-12-31"
                            }
                        ]
                    }
                ]
            }
        ]
    }
]

td_07 = [
    {
        "Description": "Testcase 07 (Wel ZVL functie + Wel AOK (12m) - 2 opeenvolgende)",
        "Amount": 1, #Indicator score: 1
        "Human": [
            {
                "ArbeidsOvereenkomst": [
                    {
                        "function": [
                            {
                                "label": "AOK Wel ZVL",
                                "caregiving_role": True,
                                "start_date": "2024-01-01",
                                "end_date": "2024-06-30",
                            }
                        ],
                        "start_date": "2024-01-01",
                        "end_date": "2024-06-30",                        
                        "size": [
                            {
                                "unit": 36,
                                "unit_of_measure": "Uren_per_week_unit",
                                "start_date": "2024-01-01",
                                "end_date": "2024-06-30"
                            }
                        ]
                    },
                    {
                        "function": [
                            {
                                "label": "AOK Wel ZVL",
                                "caregiving_role": True,
                                "start_date": "2024-07-01",
                                "end_date": "2024-12-31",
                            }
                        ],
                        "start_date": "2024-07-01",
                        "end_date": "2024-12-31",                        
                        "size": [
                            {
                                "unit": 36,
                                "unit_of_measure": "Uren_per_week_unit",
                                "start_date": "2024-07-01",
                                "end_date": "2024-12-31"
                            }
                        ]
                    }
                ]
            }
        ]
    }
]

td_07_a = [
    {
        "Description": "Testcase 7a (Wel ZVL functie + Wel AOK (12m) - 2 opeenvolgende)",
        "Amount": 10, #Indicator score: 10
        "Human": [
            {
                "ArbeidsOvereenkomst": [
                    {
                        "function": [
                            {
                                "label": "AOK Wel ZVL",
                                "caregiving_role": True,
                                "start_date": "2024-01-01",
                                "end_date": "2024-06-30",
                            }
                        ],
                        "start_date": "2024-01-01",
                        "end_date": "2024-06-30",                        
                        "size": [
                            {
                                "unit": 36,
                                "unit_of_measure": "Uren_per_week_unit",
                                "start_date": "2024-01-01",
                                "end_date": "2024-06-30"
                            }
                        ]
                    },
                    {
                        "function": [
                            {
                                "label": "AOK Wel ZVL",
                                "caregiving_role": True,
                                "start_date": "2024-07-01",
                                "end_date": "2024-12-31",
                            }
                        ],
                        "start_date": "2024-07-01",
                        "end_date": "2024-12-31",                        
                        "size": [
                            {
                                "unit": 36,
                                "unit_of_measure": "Uren_per_week_unit",
                                "start_date": "2024-07-01",
                                "end_date": "2024-12-31"
                            }
                        ]
                    }
                ]
            }
        ]
    }
]


#Static Tests
def test_if_headers_are_correct_for_query_2_1_1(db_config):
    """ Test of de juiste header terugkomt in het resultaat
        ODB personeelssamenstelling 2.1.1. Aantal personeelsleden
    """

    # Setup of the test
    test = QueryTest(db_config)

    # Configuration and execution
    test.set_query('Gevalideerde_vragen_technisch/personeelssamenstelling/Indicator 2.1.1.rq')

    # Assertions
    test.verify_header_present('indicator')


def test_if_number_of_rows_returned_is_correct_for_query_2_1_1(db_config):
    """ Test of het aantal rijen correct wordt teruggegeven
        ODB personeelssamenstelling 2.1.1. Aantal personeelsleden
    """

    # Setup of the test
    test = QueryTest(db_config)

    # Configuration and execution
    test.set_query('Gevalideerde_vragen_technisch/personeelssamenstelling/Indicator 2.1.1.rq')

    # Assertions
    test.verify_row_count(1)


def test_if_indicator_has_correct_value_for_query_2_1_1(db_config):
    """ Test of de indicator de juiste waarde heeft
        ODB personeelssamenstelling 2.1.1. Aantal personeelsleden
    """

    # Setup of the test
    test = QueryTest(db_config)

    # Configuration and execution
    test.set_query('Gevalideerde_vragen_technisch/personeelssamenstelling/Indicator 2.1.1.rq')

    # Assertions
    test.verify_value_in_list("indicator", {"9","9.0"})


def test_if_dates_can_change_2_1_1(db_config):
    """ Test of gewijzigde datum daadwerkelijk een ander resultaat oplevert
        ODB personeelssamenstelling 2.1.1. Aantal personeelsleden
    """

    # Setup of the test
    test = QueryTest(db_config)

    # Configuration and execution
    test.set_query('Gevalideerde_vragen_technisch/personeelssamenstelling/Indicator 2.1.1.rq')

    test.change_start_period("2023-01-01", "2022-01-01")
    test.change_end_period("2023-12-31", "2022-12-31")

    # Assertions
    test.verify_value_in_list("indicator",{"9.115068493150684931506850","9.11506849315068493150685"})


# Tests using Generated Data

# Testcase 01
def test_if_value_returned_is_correct_for_query_2_1_1_01(db_config):
    """ Testcase 01 (Geen ZVL functie + Geen AOK)
        ODB personeelssamenstelling 2.1.1. Aantal personeelsleden
    """

    # Load defined test data
    dg = DataGenerator(db_config, td_01)

    try:
        # Start a test
        test = QueryTest(db_config)

        # Set the query to be tested
        test.set_query('Gevalideerde_vragen_technisch/personeelssamenstelling/Indicator 2.1.1.rq')

        # Change measuring period parameters of query
        test.change_start_period("2023-01-01", "2024-01-01")
        test.change_end_period("2023-12-31", "2024-12-31")

        # Verify actual result of the query
        test.verify_value_in_list("indicator", {"9", "9.0"})

    finally:
        # Delete previously loaded data
        dg.delete_graph_data()


# Testcase 02
def test_if_value_returned_is_correct_for_query_2_1_1_02(db_config):
    """ Testcase 02 (Geen ZVL functie + Wel AOK)
        ODB personeelssamenstelling 2.1.1. Aantal personeelsleden
    """

    # Load defined test data
    dg = DataGenerator(db_config, td_02)

    try:
        # Start a test
        test = QueryTest(db_config)

        # Set the query to be tested
        test.set_query('Gevalideerde_vragen_technisch/personeelssamenstelling/Indicator 2.1.1.rq')

        # Change measuring period parameters of query
        test.change_start_period("2023-01-01", "2024-01-01")
        test.change_end_period("2023-12-31", "2024-12-31")

        # Verify actual result of the query
        test.verify_value_in_list("indicator", {"9", "9.0"})

    finally:
        # Delete previously loaded data
        dg.delete_graph_data()


# Testcase 03
def test_if_value_returned_is_correct_for_query_2_1_1_03(db_config):
    """ Testcase 03 (Wel ZVL functie + Geen AOK)
        ODB personeelssamenstelling 2.1.1. Aantal personeelsleden
    """

    # Load defined test data
    dg = DataGenerator(db_config, td_03)

    try:
        # Start a test
        test = QueryTest(db_config)

        # Set the query to be tested
        test.set_query('Gevalideerde_vragen_technisch/personeelssamenstelling/Indicator 2.1.1.rq')

        # Change measuring period parameters of query
        test.change_start_period("2023-01-01", "2024-01-01")
        test.change_end_period("2023-12-31", "2024-12-31")

        # Verify actual result of the query
        test.verify_value_in_list("indicator", {"9", "9.0"})

    finally:
        # Delete previously loaded data
        dg.delete_graph_data()


# Testcase 04
def test_if_value_returned_is_correct_for_query_2_1_1_04(db_config):
    """ Testcase 04 (Wel ZVL functie + Wel AOK (12m))
        ODB personeelssamenstelling 2.1.1. Aantal personeelsleden
    """

    # Load defined test data
    dg = DataGenerator(db_config, td_04)

    try:
        # Start a test
        test = QueryTest(db_config)

        # Set the query to be tested
        test.set_query('Gevalideerde_vragen_technisch/personeelssamenstelling/Indicator 2.1.1.rq')

        # Change measuring period parameters of query
        test.change_start_period("2023-01-01", "2024-01-01")
        test.change_end_period("2023-12-31", "2024-12-31")

        # Verify actual result of the query
        test.verify_value_in_list("indicator", {"29", "29.0"})

    finally:
        # Delete previously loaded data
        dg.delete_graph_data()


# Testcase 05
def test_if_value_returned_is_correct_for_query_2_1_1_05(db_config):
    """ Testcase 05 (Wel ZVL functie + Wel AOK (6m))
        ODB personeelssamenstelling 2.1.1. Aantal personeelsleden
    """

    # Load defined test data
    dg = DataGenerator(db_config, td_05)

    try:
        # Start a test
        test = QueryTest(db_config)

        # Set the query to be tested
        test.set_query('Gevalideerde_vragen_technisch/personeelssamenstelling/Indicator 2.1.1.rq')

        # Change measuring period parameters of query
        test.change_start_period("2023-01-01", "2024-01-01")
        test.change_end_period("2023-12-31", "2024-12-31")

        # Verify actual result of the query
        test.verify_value_in_list("indicator", {"13.97267759562841530054645","13.972677595628415300546450"})

    finally:
        # Delete previously loaded data
        dg.delete_graph_data()


# Testcase 06
def test_if_value_returned_is_correct_for_query_2_1_1_06(db_config):
    """ Testcase 06 (Wel ZVL functie + Geen ZVL functie + Wel AOK (6m)) 1 human
        ODB personeelssamenstelling 2.1.1. Aantal personeelsleden
    """

    # Load defined test data
    dg = DataGenerator(db_config, td_06)

    try:
        # Start a test
        test = QueryTest(db_config)

        # Set the query to be tested
        test.set_query('Gevalideerde_vragen_technisch/personeelssamenstelling/Indicator 2.1.1.rq')

        # Change measuring period parameters of query
        test.change_start_period("2023-01-01", "2024-01-01")
        test.change_end_period("2023-12-31", "2024-12-31")

        # Verify actual result of the query
        test.verify_value("indicator", "9.497267759562841530054645")

    finally:
        # Delete previously loaded data
        dg.delete_graph_data()

# Testcase 06a
def test_if_value_returned_is_correct_for_query_2_1_1_06_a(db_config):
    """ Testcase 06a (Wel ZVL functie + Geen ZVL functie + Wel AOK (6m)) 10 humans
        ODB personeelssamenstelling 2.1.1. Aantal personeelsleden
    """

    # Load defined test data
    dg = DataGenerator(db_config, td_06_a)

    try:
        # Start a test
        test = QueryTest(db_config)

        # Set the query to be tested
        test.set_query('Gevalideerde_vragen_technisch/personeelssamenstelling/Indicator 2.1.1.rq')

        # Change measuring period parameters of query
        test.change_start_period("2023-01-01", "2024-01-01")
        test.change_end_period("2023-12-31", "2024-12-31")

        # Verify actual result of the query
        test.verify_value_in_list("indicator", {"13.97267759562841530054645", "13.972677595628415300546450"})

    finally:
        # Delete previously loaded data
        dg.delete_graph_data()

# Testcase 07
def test_if_value_returned_is_correct_for_query_2_1_1_07(db_config):
    """ Testcase 07 (Wel ZVL functie + Wel AOK (12m) - 2 opeenvolgende) 1 human
        ODB personeelssamenstelling 2.1.1. Aantal personeelsleden
    """

    # Load defined test data
    dg = DataGenerator(db_config, td_07)

    try:
        # Start a test
        test = QueryTest(db_config)

        # Set the query to be tested
        test.set_query('Gevalideerde_vragen_technisch/personeelssamenstelling/Indicator 2.1.1.rq')

        # Change measuring period parameters of query
        test.change_start_period("2023-01-01", "2024-01-01")
        test.change_end_period("2023-12-31", "2024-12-31")

        # Verify actual result of the query
        test.verify_value_in_list("indicator", {"10.000000000000000000000000", "10.0"})

    finally:
        # Delete previously loaded data
        dg.delete_graph_data()

# Testcase 07a
def test_if_value_returned_is_correct_for_query_2_1_1_07_a(db_config):
    """ Testcase 07.2 (Wel ZVL functie + Wel AOK (12m) - 2 opeenvolgende) 10 humans
        ODB personeelssamenstelling 2.1.1. Aantal personeelsleden
    """

    # Load defined test data
    dg = DataGenerator(db_config, td_07_a)

    try:
        # Start a test
        test = QueryTest(db_config)

        # Set the query to be tested
        test.set_query('Gevalideerde_vragen_technisch/personeelssamenstelling/Indicator 2.1.1.rq')

        # Change measuring period parameters of query
        test.change_start_period("2023-01-01", "2024-01-01")
        test.change_end_period("2023-12-31", "2024-12-31")

        # Verify actual result of the query
        test.verify_value_in_list("indicator", {"19.000000000000000000000000", "19.0"})

    finally:
        # Delete previously loaded data
        dg.delete_graph_data()
