from DataGenerator import DataGenerator
from QueryTest import QueryTest

# Meetperiode startdatum: 2024-01-01
# Meetperiode einddatum: 2024-12-31

#Opmerkingen:

#Testcases

td_01 = [
    {
        "Description": "Testcase 01 (Geen ZVL functie + Geen AOK)",
        "Amount": 10, # Indicator score = 0
        "Human": [
            {
                "InhuurOvereenkomst": [
                    {
                        "function": [
                            {
                                "label": "INH_OK Geen ZVL",
                                "caregiving_role": False,
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31",
                            }
                        ],
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",                        
                        "size": [
                             {
                                "unit": 36,
                                "unit_of_measure": "Uren_per_week_unit",
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31"
                             }
                        ],
                        "booked_hours": [
                            {
                                "unit": 8,
                                "unit_of_measure": "Uur",
                                "start_datetime": "2024-01-01T09:00:00",
                                "end_datetime": "2024-01-01T17:00:00"
                            }
                        ]
                    }
                ]
            }
        ]
    }
] 

td_02 = [
    {
        "Description": "Testcase 02 (Geen ZVL functie + Wel AOK )",
        "Amount": 10, # Indicator score = 0
        "Human": [
            {
                "ArbeidsOvereenkomst": [
                    {
                        "function": [
                            {
                                "label": "AOK Geen ZVL",
                                "caregiving_role": False,
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31",
                            }
                        ],
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",                        
                        "size": [
                             {
                                "unit": 36,
                                "unit_of_measure": "Uren_per_week_unit",
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31"
                             }
                        ],
                        "booked_hours": [
                            {
                                "unit": 8,
                                "unit_of_measure": "Uur",
                                "start_datetime": "2024-01-01T09:00:00",
                                "end_datetime": "2024-01-01T17:00:00"
                            }
                        ]
                    }
                ]
            }
        ]
    }
]                 

td_03 = [
    {
        "Description": "Testcase 03 (Wel ZVL functie + Geen AOK )",
        "Amount": 10, # Indicator score = 0
        "Human": [
            {
                "InhuurOvereenkomst": [
                    {
                        "function": [
                            {
                                "label": "INH_OK Wel ZVL",
                                "caregiving_role": True,
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31",
                            }
                        ],
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",                        
                        "size": [
                             {
                                "unit": 36,
                                "unit_of_measure": "Uren_per_week_unit",
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31"
                             }
                        ],
                        "booked_hours": [
                            {
                                "unit": 8,
                                "unit_of_measure": "Uur",
                                "start_datetime": "2024-01-01T09:00:00",
                                "end_datetime": "2024-01-01T17:00:00"
                            }
                        ]
                    }
                ]
            }
        ]
    }
] 

td_04 = [
    {
        "Description": "Testcase 04 (Wel ZVL functie + Wel AOK )",
        "Amount": 10, # Indicator score =  0.212184629694221602139290 (10 * 36/36 = 10. 10/47 = 0.212...)
        "Human": [
            {
                "ArbeidsOvereenkomst": [
                    {
                        "function": [
                            {
                                "label": "AOK Wel ZVL",
                                "caregiving_role": True,
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31",
                            }
                        ],
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",                        
                        "size": [
                             {
                                "unit": 36,
                                "unit_of_measure": "Uren_per_week_unit",
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31"
                             }
                        ],
                        "booked_hours": [
                            {
                                "unit": 8,
                                "unit_of_measure": "Uur",
                                "start_datetime": "2024-01-01T09:00:00",
                                "end_datetime": "2024-01-01T17:00:00"
                            }
                        ]
                    }
                ]
            }
        ]
    }
] 

td_04_a = [
    {
        "Description": "Testcase 04a (Wel ZVL functie + Wel AOK )",
        "Amount": 10, # Indicator score =  0. (10 * (28/36)/47 = 0.165...)
        "Human": [
            {
                "ArbeidsOvereenkomst": [
                    {
                        "function": [
                            {
                                "label": "AOK Wel ZVL",
                                "caregiving_role": True,
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31",
                            }
                        ],
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",                        
                        "size": [
                             {
                                "unit": 36,
                                "unit_of_measure": "Uren_per_week_unit",
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31"
                             }
                        ],
                        "booked_hours": [
                            {
                                "unit": 8,
                                "unit_of_measure": "Uur",
                                "start_datetime": "2024-01-01T09:00:00",
                                "end_datetime": "2024-01-01T17:00:00"
                            }
                        ]
                    }
                ]
            }
        ]
    }
] 

td_04_b = [
    {
        "Description": "Testcase 04b (Wel ZVL functie + Wel AOK + Niet AOK (18u p/w + 18u p/w) )",
        "Amount": 10, # Indicator score =  0. (10 * (28/36)/47 = 0.165...)
        "Human": [
            {
                "ArbeidsOvereenkomst": [
                    {
                        "function": [
                            {
                                "label": "AOK Wel ZVL",
                                "caregiving_role": True,
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31",
                            }
                        ],
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",                        
                        "size": [
                             {
                                "unit": 36,
                                "unit_of_measure": "Uren_per_week_unit",
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31"
                             }
                        ],
                        "booked_hours": [
                            {
                                "unit": 8,
                                "unit_of_measure": "Uur",
                                "start_datetime": "2024-01-01T09:00:00",
                                "end_datetime": "2024-01-01T17:00:00"
                            }
                        ]
                    }
                ],
                "InhuurOvereenkomst": [
                    {
                        "function": [
                            {
                                "label": "INH_OK Wel ZVL",
                                "caregiving_role": True,
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31",
                            }
                        ],
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",                        
                        "size": [
                             {
                                "unit": 18,
                                "unit_of_measure": "Uren_per_week_unit",
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31"
                             }
                        ],
                        "booked_hours": [
                            {
                                "unit": 8,
                                "unit_of_measure": "Uur",
                                "start_datetime": "2024-01-01T09:00:00",
                                "end_datetime": "2024-01-01T17:00:00"
                            }
                        ]
                    }
                ]
            }
        ]
    }
] 

td_04_c = [
    {
        "Description": "Testcase 04c (Wel ZVL functie + Wel AOK (6m 36u p/w, 6m 18u p/w))",
        "Amount": 10, # Indicator score =  0.212184629694221602139290 (10 * 36/36 = 10. 10/47 = 0.212...)
        "Human": [
            {
                "ArbeidsOvereenkomst": [
                    {
                        "function": [
                            {
                                "label": "AOK Wel ZVL (6m 36u p/w)",
                                "caregiving_role": True,
                                "start_date": "2024-01-01",
                                "end_date": "2024-06-30",
                            }
                        ],
                        "start_date": "2024-01-01",
                        "end_date": "2024-06-30",                        
                        "size": [
                             {
                                "unit": 36,
                                "unit_of_measure": "Uren_per_week_unit",
                                "start_date": "2024-01-01",
                                "end_date": "2024-06-30"
                             }
                        ],
                        "booked_hours": [
                            {
                                "unit": 8,
                                "unit_of_measure": "Uur",
                                "start_datetime": "2024-01-01T09:00:00",
                                "end_datetime": "2024-01-01T17:00:00"
                            }
                        ]
                    },
                    {
                        "function": [
                            {
                                "label": "AOK Wel ZVL (6m 18u p/w)",
                                "caregiving_role": True,
                                "start_date": "2024-07-01",
                                "end_date": "2024-12-31",
                            }
                        ],
                        "start_date": "2024-07-01",
                        "end_date": "2024-12-31",                        
                        "size": [
                             {
                                "unit": 36,
                                "unit_of_measure": "Uren_per_week_unit",
                                "start_date": "2024-07-01",
                                "end_date": "2024-12-31"
                             }
                        ],
                        "booked_hours": [
                            {
                                "unit": 8,
                                "unit_of_measure": "Uur",
                                "start_datetime": "2024-01-01T09:00:00",
                                "end_datetime": "2024-01-01T17:00:00"
                            }
                        ]
                    }
                ]
            }
        ]
    }
] 

#Static Tests

def test_if_headers_are_correct_for_query_2_1_2(db_config):
    """ Test of de juiste header terugkomt in het resultaat
        ODB Personeelssamenstelling 2.1.2. Aantal fte
    """

    # Setup of the test
    test = QueryTest(db_config)

    # Configuration and execution
    test.set_query('Gevalideerde_vragen_technisch/personeelssamenstelling/Indicator 2.1.2.rq')

    # Assertions
    test.verify_header_present('indicator')


def test_if_number_of_rows_returned_is_correct_for_query_2_1_2(db_config):
    """ Test of het aantal rijen correct wordt teruggegeven
        ODB Personeelssamenstelling 2.1.2. Aantal fte
    """

    # Setup of the test
    test = QueryTest(db_config)

    # Configuration and execution
    test.set_query('Gevalideerde_vragen_technisch/personeelssamenstelling/Indicator 2.1.2.rq')

    # Assertions
    test.verify_row_count(1)


def test_if_indicator_has_correct_value_for_query_2_1_2(db_config):
    """ Test of de indicator de juiste waarde heeft
        ODB Personeelssamenstelling 2.1.2. Aantal fte
    """

    # Setup of the test
    test = QueryTest(db_config)

    # Configuration and execution
    test.set_query('Gevalideerde_vragen_technisch/personeelssamenstelling/Indicator 2.1.2.rq')

    # Assertions
    test.verify_value_in_list("indicator", {"0.002364066193853427895981","0.0023640661938534278959810"})


def test_if_dates_can_change_2_1_2(db_config):
    """ Test of gewijzigde datum daadwerkelijk een ander resultaat oplevert
        ODB Personeelssamenstelling 2.1.2. Aantal fte
    """

    # Setup of the test
    test = QueryTest(db_config)

    # Configuration and execution
    test.set_query('Gevalideerde_vragen_technisch/personeelssamenstelling/Indicator 2.1.2.rq')

    test.change_start_period("2023-01-01", "2024-01-01")
    test.change_end_period("2023-12-31", "2024-12-31")

    # Assertions
    test.verify_value_in_list("indicator",{"0","0.0"})

# Tests using generated data

# Testcase 01

def test_if_value_returned_is_correct_for_query_2_1_2_01(db_config):
    """ Testcase 01 (Geen ZVL functie + Geen AOK)
        ODB personeelssamenstelling 2.1.2 Aantal fte
    """

    # Load defined test data
    dg = DataGenerator(db_config, td_01)

    try:
        # Start a test
        test = QueryTest(db_config)

        # Set the query to be tested
        test.set_query('Gevalideerde_vragen_technisch/personeelssamenstelling/Indicator 2.1.2.rq')

        # Change the reference dates of the query
        test.change_start_period("2023-01-01","2024-01-01")
        test.change_end_period("2023-12-31", "2024-12-31")

        # Verify actual result of the query
        test.verify_value_in_list("indicator", {"0", "0.0"})

    finally:
        # Delete previously loaded data
        dg.delete_graph_data()

# Testcase 02

def test_if_value_returned_is_correct_for_query_2_1_2_02(db_config):
    """ Testcase 02 (Geen ZVL functie + Wel AOK)
        ODB personeelssamenstelling 2.1.2 Aantal fte
    """

    # Load defined test data
    dg = DataGenerator(db_config, td_02)

    try:
        # Start a test
        test = QueryTest(db_config)

        # Set the query to be tested
        test.set_query('Gevalideerde_vragen_technisch/personeelssamenstelling/Indicator 2.1.2.rq')

        # Change the reference dates of the query
        test.change_start_period("2023-01-01","2024-01-01")
        test.change_end_period("2023-12-31", "2024-12-31")

        # Verify actual result of the query
        test.verify_value_in_list("indicator", {"0", "0.0"})

    finally:
        # Delete previously loaded data
        dg.delete_graph_data()

# Testcase 03

def test_if_value_returned_is_correct_for_query_2_1_2_03(db_config):
    """ Testcase 03 (Wel ZVL functie + Geen AOK)
        ODB personeelssamenstelling 2.1.2 Aantal fte
    """

    # Load defined test data
    dg = DataGenerator(db_config, td_03)

    try:
        # Start a test
        test = QueryTest(db_config)

        # Set the query to be tested
        test.set_query('Gevalideerde_vragen_technisch/personeelssamenstelling/Indicator 2.1.2.rq')

        # Change the reference dates of the query
        test.change_start_period("2023-01-01","2024-01-01")
        test.change_end_period("2023-12-31", "2024-12-31")

        # Verify actual result of the query
        test.verify_value_in_list("indicator", {"0", "0.0"})

    finally:
        # Delete previously loaded data
        dg.delete_graph_data()

# Testcase 04

def test_if_value_returned_is_correct_for_query_2_1_2_04(db_config):
    """ Testcase 04 (Wel ZVL functie + Wel AOK)
        ODB personeelssamenstelling 2.1.2 Aantal fte
    """

    # Load defined test data
    dg = DataGenerator(db_config, td_04)

    try:
        # Start a test
        test = QueryTest(db_config)

        # Set the query to be tested
        test.set_query('Gevalideerde_vragen_technisch/personeelssamenstelling/Indicator 2.1.2.rq')

        # Change the reference dates of the query
        test.change_start_period("2023-01-01","2024-01-01")
        test.change_end_period("2023-12-31", "2024-12-31")

        # Verify actual result of the query
        test.verify_value_in_list("indicator",{"0.047152139932049244919840", "0.04715213993204924491984"})

    finally:
        # Delete previously loaded data
        dg.delete_graph_data()

# Testcase 04a

def test_if_value_returned_is_correct_for_query_2_1_2_04_a(db_config):
    """ Testcase 04a (Wel ZVL functie + Wel AOK)
        ODB personeelssamenstelling 2.1.2 Aantal fte
    """

    # Load defined test data
    dg = DataGenerator(db_config, td_04_a)

    try:
        # Start a test
        test = QueryTest(db_config)

        # Set the query to be tested
        test.set_query('Gevalideerde_vragen_technisch/personeelssamenstelling/Indicator 2.1.2.rq')

        # Change the reference dates of the query
        test.change_start_period("2023-01-01","2024-01-01")
        test.change_end_period("2023-12-31", "2024-12-31")

        # Verify actual result of the query
        test.verify_value_in_list("indicator",{"0.047152139932049244919840", "0.04715213993204924491984"})

    finally:
        # Delete previously loaded data
        dg.delete_graph_data()

# Testcase 04b

def test_if_value_returned_is_correct_for_query_2_1_2_04_b(db_config):
    """ Testcase 04b (Wel ZVL functie + Wel AOK + Geen AOK)
        ODB personeelssamenstelling 2.1.2 Aantal fte
    """

    # Load defined test data
    dg = DataGenerator(db_config, td_04_b)

    try:
        # Start a test
        test = QueryTest(db_config)

        # Set the query to be tested
        test.set_query('Gevalideerde_vragen_technisch/personeelssamenstelling/Indicator 2.1.2.rq')

        # Change the reference dates of the query
        test.change_start_period("2023-01-01","2024-01-01")
        test.change_end_period("2023-12-31", "2024-12-31")

        # Verify actual result of the query
        test.verify_value_in_list("indicator", {"0.047152139932049244919840", "0.04715213993204924491984"})

    finally:
        # Delete previously loaded data
        dg.delete_graph_data()

# Testcase 04c

def test_if_value_returned_is_correct_for_query_2_1_2_04_c(db_config):
    """ Testcase 04c (Wel ZVL functie + Wel AOK (6m 36u p/w + 6m 18u p/w))
        ODB personeelssamenstelling 2.1.2 Aantal fte
    """

    # Load defined test data
    dg = DataGenerator(db_config, td_04_c)

    try:
        # Start a test
        test = QueryTest(db_config)

        # Set the query to be tested
        test.set_query('Gevalideerde_vragen_technisch/personeelssamenstelling/Indicator 2.1.2.rq')

        # Change the reference dates of the query
        test.change_start_period("2023-01-01","2024-01-01")
        test.change_end_period("2023-12-31", "2024-12-31")

        # Verify actual result of the query
        test.verify_value_in_list("indicator", {"0.047152139932049244919840", "0.04715213993204924491984"})
        # Eerste 6m worden niet meegerekend
    finally:
        # Delete previously loaded data
        dg.delete_graph_data()