from DataGenerator import DataGenerator
from QueryTest import QueryTest

# Meetperiode startdatum: 01-01-2024
# Meetperiode einddatum: 31-12-2024

#Opmerkingen:
# Oude testdata bevat al 1 VWOK


td_01 = [
    {
        "Description": "Testcase 01 (Geen ZVL functie + Geen VWOK)",
        "Amount": 10, #Indicator score: 0
        "Human": [
             {
                "InhuurOvereenkomst": [
                     {
                        "function": [
                            {
                                "label": "Geen ZVL Geen VWOK",
                                "caregiving_role": False,
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31",
                            }
                        ],
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",   
                        "size": [
                            {
                                "unit": 36,
                                "unit_of_measure": "Uren_per_week_unit",
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31"
                            }
                        ]
                    }
                ]
            }
        ]
    }
]

td_02 = [
    {
        "Description": "Testcase 02 (Geen ZVL functie + Wel VWOK)",
        "Amount": 10, #Indicator score: 0
        "Human": [
             {
                "VrijwilligersOvereenkomst": [
                     {
                        "function": [
                            {
                                "label": "Geen ZVL Wel VWOK",
                                "caregiving_role": False,
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31",
                            }
                        ],
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",   
                        "size": [
                            {
                                "unit": 36,
                                "unit_of_measure": "Uren_per_week_unit",
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31"
                            }
                        ]
                    }
                ]
            }
        ]
    }
]

td_03 = [
    {
        "Description": "Testcase 03 (Wel ZVL functie + Geen VWOK)",
        "Amount": 10, #Indicator score: 0
        "Human": [
             {
                "InhuurOvereenkomst": [
                     {
                        "function": [
                            {
                                "label": "Wel ZVL Geen VWOK",
                                "caregiving_role": True,
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31",
                            }
                        ],
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",   
                        "size": [
                            {
                                "unit": 36,
                                "unit_of_measure": "Uren_per_week_unit",
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31"
                            }
                        ]
                    }
                ]
            }
        ]
    }
]

td_04 = [
    {
        "Description": "Testcase 04 (Wel ZVL functie + Wel VWOK (12m))",
        "Amount": 10, #Indicator score: 10
        "Human": [
             {
                "VrijwilligersOvereenkomst": [
                     {
                        "function": [
                            {
                                "label": "Wel ZVL Wel VWOK",
                                "caregiving_role": True,
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31",
                            }
                        ],
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",   
                        "size": [
                            {
                                "unit": 36,
                                "unit_of_measure": "Uren_per_week_unit",
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31"
                            }
                        ]
                    }
                ]
            }
        ]
    }
]

td_04_a = [
    {
        "Description": "Testcase 04a (Wel ZVL functie + Wel VWOK (6m))",
        "Amount": 10, #Indicator score: 10 
          "Human": [
             {
                "VrijwilligersOvereenkomst": [
                     {
                        "function": [
                            {
                                "label": "Wel ZVL Wel VWOK (6m)",
                                "caregiving_role": True,
                                "start_date": "2024-01-01",
                                "end_date": "2024-06-30",
                            }
                        ],
                        "start_date": "2024-01-01",
                        "end_date": "2024-06-30",   
                        "size": [
                            {
                                "unit": 36,
                                "unit_of_measure": "Uren_per_week_unit",
                                "start_date": "2024-01-01",
                                "end_date": "2024-06-30"
                            }
                        ]
                    }
                ]
            }
        ]
    }
]

td_04_b = [
    {
        "Description": "Testcase 04b (Wel ZVL functie + Wel VWOK (6m))",
        "Amount": 10, #Indicator score: 10 
        "Human": [
             {
                "VrijwilligersOvereenkomst": [
                     {
                        "function": [
                            {
                                "label": "Wel ZVL Wel VWOK",
                                "caregiving_role": True,
                                "start_date": "2024-07-01",
                                "end_date": "2024-12-31",
                            }
                        ],
                        "start_date": "2024-07-01",
                        "end_date": "2024-12-31",   
                        "size": [
                            {
                                "unit": 36,
                                "unit_of_measure": "Uren_per_week_unit",
                                "start_date": "2024-07-01",
                                "end_date": "2024-12-31"
                            }
                        ]
                    }
                ]
            }
        ]
    }
]

td_04_c = [
    {
        "Description": "Testcase 04c (Wel ZVL functie + Wel VWOK (6m) + Wel AOK (6m))",
        "Amount": 10, #Indicator score: 10 
        "Human": [
             {
                "VrijwilligersOvereenkomst": [
                     {
                        "function": [
                            {
                                "label": "Wel ZVL Wel VWOK",
                                "caregiving_role": True,
                                "start_date": "2024-01-01",
                                "end_date": "2024-06-30",
                            }
                        ],
                        "start_date": "2024-01-01",
                        "end_date": "2024-06-30",   
                        "size": [
                            {
                                "unit": 36,
                                "unit_of_measure": "Uren_per_week_unit",
                                "start_date": "2024-01-01",
                                "end_date": "2024-06-30"
                            }
                        ]
                    }
                ],  
                "ArbeidsOvereenkomst" :[
                    {
                        "function": [
                            {
                                "label": "Wel ZVL Wel VAOKWOK",
                                "caregiving_role": True,
                                "start_date": "2024-07-01",
                                "end_date": "2024-12-31",
                            }
                        ],
                        "start_date": "2024-07-01",
                        "end_date": "2024-12-31",        
                        "size": [
                             {
                                "unit": 36,
                                "unit_of_measure": "Uren_per_week_unit",
                                "start_date": "2024-07-01",
                                "end_date": "2024-12-31"
                            }
                        ]
                    }    
                ]
            }
        ]
     }
]

td_04_d = [
    {
        "Description": "Testcase 04d (Wel ZVL functie + Wel VWOK (6m) + Wel AOK (6m))",
        "Amount": 1, #Indicator score: 1 
        "Human": [
             {
                "VrijwilligersOvereenkomst": [
                     {
                        "function": [
                            {
                                "label": "Wel ZVL Wel VWOK",
                                "caregiving_role": True,
                                "start_date": "2024-01-01",
                                "end_date": "2024-06-30",
                            }
                        ],
                        "start_date": "2024-01-01",
                        "end_date": "2024-06-30",   
                        "size": [
                            {
                                "unit": 36,
                                "unit_of_measure": "Uren_per_week_unit",
                                "start_date": "2024-01-01",
                                "end_date": "2024-06-30"
                            }
                        ]
                    }
                ],  
                "ArbeidsOvereenkomst" :[
                    {
                        "function": [
                            {
                                "label": "Wel ZVL Wel AOk",
                                "caregiving_role": True,
                                "start_date": "2024-07-01",
                                "end_date": "2024-12-31",
                            }
                        ],
                        "start_date": "2024-07-01",
                        "end_date": "2024-12-31",        
                        "size": [
                             {
                                "unit": 36,
                                "unit_of_measure": "Uren_per_week_unit",
                                "start_date": "2024-07-01",
                                "end_date": "2024-12-31"
                            }
                        ]
                    }    
                ]
            }
        ]
     }
]


def test_if_headers_are_correct_for_query_2_2_3(db_config):
    """ Test of de juiste header terugkomt in het resultaat
        ODB Personeelssamenstelling 2.2.3. Aantal vrijwilligers
    """

    # Setup of the test
    test = QueryTest(db_config)

    # Configuration and execution
    test.set_query('Gevalideerde_vragen_technisch/personeelssamenstelling/Indicator 2.2.3.rq')

    # Assertions
    test.verify_header_present('aantal_vrijwilligers')

def test_if_number_of_rows_returned_is_correct_for_query_2_2_3(db_config):
    """ Test of het aantal rijen correct wordt teruggegeven
        ODB Personeelssamenstelling 2.2.3. Aantal vrijwilligers
    """

    # Setup of the test
    test = QueryTest(db_config)

    # Configuration and execution
    test.set_query('Gevalideerde_vragen_technisch/personeelssamenstelling/Indicator 2.2.3.rq')

    # Assertions
    test.verify_row_count(1)


def test_if_indicator_has_correct_value_for_query_2_2_3(db_config):
    """ Test of de indicator de juiste waarde heeft
        ODB Personeelssamenstelling 2.2.3. Aantal vrijwilligers
    """

    # Setup of the test
    test = QueryTest(db_config)

    # Configuration and execution
    test.set_query('Gevalideerde_vragen_technisch/personeelssamenstelling/Indicator 2.2.3.rq')

    # Assertions
    test.verify_value("aantal_vrijwilligers", "1")
    #.in_row(1)


def test_if_dates_can_change_2_2_3(db_config):
    """ Test of gewijzigde datum daadwerkelijk een ander resultaat oplevert
        ODB Personeelssamenstelling 2.2.3. Aantal vrijwilligers
    """

    # Setup of the test
    test = QueryTest(db_config)

    # Configuration and execution
    test.set_query('Gevalideerde_vragen_technisch/personeelssamenstelling/Indicator 2.2.3.rq')

    test.change_start_period("2023-01-01", "2022-01-01")
    test.change_end_period("2023-12-31", "2022-12-31")

    # Assertions
    test.verify_value("aantal_vrijwilligers","1")

# Tests using Generated Data

# Testcase 01
def test_if_value_returned_is_correct_for_query_2_2_3_01(db_config):
    """ Testcase 01 (Geen ZVL functie + Geen VWOK)
        ODB personeelssamenstelling 2.2.3. Aantal vrijwilligers
    """

    # Load defined test data
    dg = DataGenerator(db_config, td_01)

    try:
        # Start a test
        test = QueryTest(db_config)

        # Set the query to be tested
        test.set_query('Gevalideerde_vragen_technisch/personeelssamenstelling/Indicator 2.2.3.rq')

        # Change measuring period parameters of query
        test.change_start_period("2023-01-01", "2024-01-01")
        test.change_end_period("2023-12-31", "2024-12-31")

        # Verify actual result of the query
        test.verify_value_in_list("aantal_vrijwilligers", {"1", "1.0"})

    finally:
        # Delete previously loaded data
        dg.delete_graph_data()

#Testcase 02
def test_if_value_returned_is_correct_for_query_2_2_3_02(db_config):
    """ Testcase 02 (Geen ZVL functie + Wel VWOK)
        ODB personeelssamenstelling 2.2.3. Aantal vrijwilligers
    """

    # Load defined test data
    dg = DataGenerator(db_config, td_02)

    try:
        # Start a test
        test = QueryTest(db_config)

        # Set the query to be tested
        test.set_query('Gevalideerde_vragen_technisch/personeelssamenstelling/Indicator 2.2.3.rq')

        # Change measuring period parameters of query
        test.change_start_period("2023-01-01", "2024-01-01")
        test.change_end_period("2023-12-31", "2024-12-31")

        # Verify actual result of the query
        test.verify_value_in_list("aantal_vrijwilligers", {"1", "1.0"})

    finally:
        # Delete previously loaded data
        dg.delete_graph_data()

#Testcase 03
def test_if_value_returned_is_correct_for_query_2_2_3_03(db_config):
    """ Testcase 03 (Wel ZVL functie + Geen VWOK)
        ODB personeelssamenstelling 2.2.3. Aantal vrijwilligers
    """

    # Load defined test data
    dg = DataGenerator(db_config, td_03)

    try:
        # Start a test
        test = QueryTest(db_config)

        # Set the query to be tested
        test.set_query('Gevalideerde_vragen_technisch/personeelssamenstelling/Indicator 2.2.3.rq')

        # Change measuring period parameters of query
        test.change_start_period("2023-01-01", "2024-01-01")
        test.change_end_period("2023-12-31", "2024-12-31")

        # Verify actual result of the query
        test.verify_value_in_list("aantal_vrijwilligers", {"1", "1.0"})

    finally:
        # Delete previously loaded data
        dg.delete_graph_data()

#Testcase 04
def test_if_value_returned_is_correct_for_query_2_2_3_04(db_config):
    """ Testcase 04 (Wel ZVL functie + Wel VWOK (12m))
        ODB personeelssamenstelling 2.2.3. Aantal vrijwilligers
    """

    # Load defined test data
    dg = DataGenerator(db_config, td_04)

    try:
        # Start a test
        test = QueryTest(db_config)

        # Set the query to be tested
        test.set_query('Gevalideerde_vragen_technisch/personeelssamenstelling/Indicator 2.2.3.rq')

        # Change measuring period parameters of query
        test.change_start_period("2023-01-01", "2024-01-01")
        test.change_end_period("2023-12-31", "2024-12-31")

        # Verify actual result of the query
        test.verify_value_in_list("aantal_vrijwilligers", {"11", "11.0"})

    finally:
        # Delete previously loaded data
        dg.delete_graph_data()


#Testcase 04a
def test_if_value_returned_is_correct_for_query_2_2_3_04a(db_config):
    """ Testcase 04a (Wel ZVL functie + Wel VWOK (6m))
        ODB personeelssamenstelling 2.2.3. Aantal vrijwilligers
    """

    # Load defined test data
    dg = DataGenerator(db_config, td_04_a)

    try:
        # Start a test
        test = QueryTest(db_config)

        # Set the query to be tested
        test.set_query('Gevalideerde_vragen_technisch/personeelssamenstelling/Indicator 2.2.3.rq')

        # Change measuring period parameters of query
        test.change_start_period("2023-01-01", "2024-01-01")
        test.change_end_period("2023-12-31", "2024-12-31")

        # Verify actual result of the query
        test.verify_value_in_list("aantal_vrijwilligers", {"11", "11.0"})

    finally:
        # Delete previously loaded data
        dg.delete_graph_data()


#Testcase 04b
def test_if_value_returned_is_correct_for_query_2_2_3_04b(db_config):
    """ Testcase 04b (Wel ZVL functie + Wel VWOK (6m))
        ODB personeelssamenstelling 2.2.3. Aantal vrijwilligers
    """

    # Load defined test data
    dg = DataGenerator(db_config, td_04_b)

    try:
        # Start a test
        test = QueryTest(db_config)

        # Set the query to be tested
        test.set_query('Gevalideerde_vragen_technisch/personeelssamenstelling/Indicator 2.2.3.rq')

        # Change measuring period parameters of query
        test.change_start_period("2023-01-01", "2024-01-01")
        test.change_end_period("2023-12-31", "2024-12-31")

        # Verify actual result of the query
        test.verify_value_in_list("aantal_vrijwilligers", {"11", "11.0"})

    finally:
        # Delete previously loaded data
        dg.delete_graph_data()


#Testcase 04c
def test_if_value_returned_is_correct_for_query_2_2_3_04c(db_config):
    """ Testcase 04c (Wel ZVL functie + Wel VWOK (6m) + Wel AOK (6m))
        ODB personeelssamenstelling 2.2.3. Aantal vrijwilligers
    """

    # Load defined test data
    dg = DataGenerator(db_config, td_04_c)

    try:
        # Start a test
        test = QueryTest(db_config)

        # Set the query to be tested
        test.set_query('Gevalideerde_vragen_technisch/personeelssamenstelling/Indicator 2.2.3.rq')

        # Change measuring period parameters of query
        test.change_start_period("2023-01-01", "2024-01-01")
        test.change_end_period("2023-12-31", "2024-12-31")

        # Verify actual result of the query
        test.verify_value_in_list("aantal_vrijwilligers", {"11", "11.0"})

    finally:
        # Delete previously loaded data
        dg.delete_graph_data()

#Testcase 04d
def test_if_value_returned_is_correct_for_query_2_2_3_04d(db_config):
    """ Testcase 04d (Wel ZVL functie + Wel VWOK (6m) + Wel AOK (6m))
        ODB personeelssamenstelling 2.2.3. Aantal vrijwilligers
    """

    # Load defined test data
    dg = DataGenerator(db_config, td_04_d)

    try:
        # Start a test
        test = QueryTest(db_config)

        # Set the query to be tested
        test.set_query('Gevalideerde_vragen_technisch/personeelssamenstelling/Indicator 2.2.3.rq')

        # Change measuring period parameters of query
        test.change_start_period("2023-01-01", "2024-01-01")
        test.change_end_period("2023-12-31", "2024-12-31")

        # Verify actual result of the query
        test.verify_value_in_list("aantal_vrijwilligers", {"2", "2.0"})

    finally:
        # Delete previously loaded data
        dg.delete_graph_data()