from DataGenerator import DataGenerator
from QueryTest import QueryTest

# Meetperiode startdatum: 2024-01-01
# Meetperiode einddatum: 2024-12-31

#Opmerkingen:

#Testcases:

td_01 = [
    {
        "Description": "Testcase 01 (Geen ZVL functie + Geen AOK + Geen ODB Kwalificatieniveau + Geen PIL)",
        "Amount": 10, # Indicator score = 0
        "Human": [
            {
                "InhuurOvereenkomst": [
                    {
                        "function": [
                            {
                                "label": "INH_OK Geen ZVL",
                                "caregiving_role": False,
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31",
                            }
                        ],
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",                        
                        "size": [
                             {
                                "unit": 36,
                                "unit_of_measure": "Uren_per_week_unit",
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31"
                             }
                        ],
                        "booked_hours": [
                            {
                                "unit": 8,
                                "unit_of_measure": "Uur",
                                "start_datetime": "2024-01-01T09:00:00",
                                "end_datetime": "2024-01-01T17:00:00"
                            }
                        ]
                    }
                ]
            }
        ]
    }
]

td_02 = [
    {
        "Description": "Testcase 02 (Geen ZVL functie + Wel AOK + Geen ODB Kwalificatieniveau + Geen PIL)",
        "Amount": 10, # Indicator score = 0
        "Human": [
            {
                "ArbeidsOvereenkomst": [
                    {
                        "function": [
                            {
                                "label": "AOK Geen ZVL",
                                "caregiving_role": False,
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31",
                            }
                        ],
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",                        
                        "size": [
                             {
                                "unit": 36,
                                "unit_of_measure": "Uren_per_week_unit",
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31"
                             }
                        ],
                        "booked_hours": [
                            {
                                "unit": 8,
                                "unit_of_measure": "Uur",
                                "start_datetime": "2024-01-01T09:00:00",
                                "end_datetime": "2024-01-01T17:00:00"
                            }
                        ]
                    }
                ]
            }
        ]
    }
]

td_03 = [
    {
        "Description": "Testcase 03 (Wel ZVL functie + Geen AOK + Geen ODB Kwalificatieniveau + Geen PIL)",
        "Amount": 10, # Indicator score = 0
        "Human": [
            {
                "InhuurOvereenkomst": [
                    {
                        "function": [
                            {
                                "label": "INH_OK Wel ZVL",
                                "caregiving_role": True,
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31",
                            }
                        ],
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",                        
                        "size": [
                             {
                                "unit": 36,
                                "unit_of_measure": "Uren_per_week_unit",
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31"
                             }
                        ],
                        "booked_hours": [
                            {
                                "unit": 8,
                                "unit_of_measure": "Uur",
                                "start_datetime": "2024-01-01T09:00:00",
                                "end_datetime": "2024-01-01T17:00:00"
                            }
                        ]
                    }
                ]
            }
        ]
    }
]

td_04 = [
    {
        "Description": "Testcase 04a (Wel ZVL functie + Geen AOK + Geen PIL + ODB Kwalificatieniveau 1 - INH OK Inzet 36u p/w)",
        "Amount": 10, # Indicator score =  0. (10 * (28/36)/47 = 0.165...)
        "Human": [
            {
                "InhuurOvereenkomst": [
                    {
                        "function": [
                            {
                                "label": "INH OK Wel ZVL ODB 1",
                                "caregiving_role": True,
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31",
                                "qualification_level": [
                                    {
                                        "qualification_level_value": "Kwalificatieniveau 1",
                                        "start_date": "2024-01-01",
                                        "end_date": "2024-12-31" 
                                    }
                                ]
                            }
                        ],
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",                        
                        "size": [
                             {
                                "unit": 36,
                                "unit_of_measure": "Uren_per_week_unit",
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31"
                             }
                        ],
                        "booked_hours": [
                            {
                                "unit": 8,
                                "unit_of_measure": "Uur",
                                "start_datetime": "2024-01-01T09:00:00",
                                "end_datetime": "2024-01-01T17:00:00"
                            }
                        ]
                    }
                ]
            }
        ]
    }
]

td_05 = [
    {
        "Description": "Testcase 05 (Wel ZVL functie + Wel AOK + Geen ODB Kwalificatieniveau + Wel PIL - AOK Inzet 36u p/w)",
        "Amount": 10, # Indicator score =  0.212184629694221602139290 (10 * 36/36 = 10. 10/47 = 0.212...)
        "Human": [
            {
                "ArbeidsOvereenkomst": [
                    {
                        "function": [
                            {
                                "label": "AOK Wel ZVL",
                                "caregiving_role": True,
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31",
                            }
                        ],
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",                        
                        "size": [
                             {
                                "unit": 36,
                                "unit_of_measure": "Uren_per_week_unit",
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31"
                             }
                        ],
                        "booked_hours": [
                            {
                                "unit": 8,
                                "unit_of_measure": "Uur",
                                "start_datetime": "2024-01-01T09:00:00",
                                "end_datetime": "2024-01-01T17:00:00"
                            }
                        ]
                    }
                ]
            }
        ]
    }
]

td_05_a = [
    {
        "Description": "Testcase 05a (Wel ZVL functie + Wel AOK + ODB Kwalificatieniveau 1 - AOK Inzet 36u p/w)",
        "Amount": 10, # Indicator score =  0. (10 * (28/36)/47 = 0.165...)
        "Human": [
            {
                "ArbeidsOvereenkomst": [
                    {
                        "function": [
                            {
                                "label": "AOK Wel ZVL ODB 1",
                                "caregiving_role": True,
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31",
                                "qualification_level": [
                                    {
                                        "qualification_level_value": "Kwalificatieniveau 1",
                                        "start_date": "2024-01-01",
                                        "end_date": "2024-12-31" 
                                    }
                                ]
                            }
                        ],
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",                        
                        "size": [
                             {
                                "unit": 36,
                                "unit_of_measure": "Uren_per_week_unit",
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31"
                             }
                        ],
                        "booked_hours": [
                            {
                                "unit": 8,
                                "unit_of_measure": "Uur",
                                "start_datetime": "2024-01-01T09:00:00",
                                "end_datetime": "2024-01-01T17:00:00"
                            }
                        ]
                    }
                ]
            }
        ]
    }
]

td_05_b = [
    {
        "Description": "Testcase 05b (Wel ZVL functie + Wel AOK + ODB Kwalificatieniveau 1 - AOK Inzet 36u p/w)",
        "Amount": 10, # Indicator score =  0. (10 * (28/36)/47 = 0.165...)
        "Human": [
            {
                "ArbeidsOvereenkomst": [
                    {
                        "function": [
                            {
                                "label": "AOK Wel ZVL ODB 2",
                                "caregiving_role": True,
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31",
                                "qualification_level": [
                                    {
                                        "qualification_level_value": "Kwalificatieniveau 2",
                                        "start_date": "2024-01-01",
                                        "end_date": "2024-12-31" 
                                    }
                                ]
                            }
                        ],
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",                        
                        "size": [
                             {
                                "unit": 36,
                                "unit_of_measure": "Uren_per_week_unit",
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31"
                             }
                        ],
                        "booked_hours": [
                            {
                                "unit": 8,
                                "unit_of_measure": "Uur",
                                "start_datetime": "2024-01-01T09:00:00",
                                "end_datetime": "2024-01-01T17:00:00"
                            }
                        ]
                    }
                ]
            }
        ]
    }
]

td_05_c = [
    {
        "Description": "Testcase 05c (Wel ZVL functie + Wel AOK + ODB Kwalificatieniveau 3 - AOK Inzet 36u p/w)",
        "Amount": 10, # Indicator score =  0. (10 * (28/36)/47 = 0.165...)
        "Human": [
            {
                "ArbeidsOvereenkomst": [
                    {
                        "function": [
                            {
                                "label": "AOK Wel ZVL ODB 3",
                                "caregiving_role": True,
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31",
                                "qualification_level": [
                                    {
                                        "qualification_level_value": "Kwalificatieniveau 3",
                                        "start_date": "2024-01-01",
                                        "end_date": "2024-12-31" 
                                    }
                                ]
                            }
                        ],
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",                        
                        "size": [
                             {
                                "unit": 36,
                                "unit_of_measure": "Uren_per_week_unit",
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31"
                             }
                        ],
                        "booked_hours": [
                            {
                                "unit": 8,
                                "unit_of_measure": "Uur",
                                "start_datetime": "2024-01-01T09:00:00",
                                "end_datetime": "2024-01-01T17:00:00"
                            }
                        ]
                    }
                ]
            }
        ]
    }
]

td_05_d = [
    {
        "Description": "Testcase 05d (Wel ZVL functie + Wel AOK + ODB Kwalificatieniveau 4 - AOK Inzet 36u p/w)",
        "Amount": 10, # Indicator score =  0. (10 * (28/36)/47 = 0.165...)
        "Human": [
            {
                "ArbeidsOvereenkomst": [
                    {
                        "function": [
                            {
                                "label": "AOK Wel ZVL ODB 4",
                                "caregiving_role": True,
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31",
                                "qualification_level": [
                                    {
                                        "qualification_level_value": "Kwalificatieniveau 4",
                                        "start_date": "2024-01-01",
                                        "end_date": "2024-12-31" 
                                    }
                                ]
                            }
                        ],
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",                        
                        "size": [
                             {
                                "unit": 36,
                                "unit_of_measure": "Uren_per_week_unit",
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31"
                             }
                        ],
                        "booked_hours": [
                            {
                                "unit": 8,
                                "unit_of_measure": "Uur",
                                "start_datetime": "2024-01-01T09:00:00",
                                "end_datetime": "2024-01-01T17:00:00"
                            }
                        ]
                    }
                ]
            }
        ]
    }
]

td_05_e = [
    {
        "Description": "Testcase 05e (Wel ZVL functie + Wel AOK + ODB Kwalificatieniveau 5 - AOK Inzet 36u p/w)",
        "Amount": 10, # Indicator score =  0. (10 * (28/36)/47 = 0.165...)
        "Human": [
            {
                "ArbeidsOvereenkomst": [
                    {
                        "function": [
                            {
                                "label": "AOK Wel ZVL ODB 5",
                                "caregiving_role": True,
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31",
                                "qualification_level": [
                                    {
                                        "qualification_level_value": "Kwalificatieniveau 5",
                                        "start_date": "2024-01-01",
                                        "end_date": "2024-12-31" 
                                    }
                                ]
                            }
                        ],
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",                        
                        "size": [
                             {
                                "unit": 36,
                                "unit_of_measure": "Uren_per_week_unit",
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31"
                             }
                        ],
                        "booked_hours": [
                            {
                                "unit": 8,
                                "unit_of_measure": "Uur",
                                "start_datetime": "2024-01-01T09:00:00",
                                "end_datetime": "2024-01-01T17:00:00"
                            }
                        ]
                    }
                ]
            }
        ]
    }
]

td_05_f = [
    {
        "Description": "Testcase 05f (Wel ZVL functie + Wel AOK + ODB Kwalificatieniveau 6 - AOK Inzet 36u p/w)",
        "Amount": 10, # Indicator score =  0. (10 * (28/36)/47 = 0.165...)
        "Human": [
            {
                "ArbeidsOvereenkomst": [
                    {
                        "function": [
                            {
                                "label": "AOK Wel ZVL ODB 6",
                                "caregiving_role": True,
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31",
                                "qualification_level": [
                                    {
                                        "qualification_level_value": "Kwalificatieniveau 6",
                                        "start_date": "2024-01-01",
                                        "end_date": "2024-12-31" 
                                    }
                                ]
                            }
                        ],
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",                        
                        "size": [
                             {
                                "unit": 36,
                                "unit_of_measure": "Uren_per_week_unit",
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31"
                             }
                        ],
                        "booked_hours": [
                            {
                                "unit": 8,
                                "unit_of_measure": "Uur",
                                "start_datetime": "2024-01-01T09:00:00",
                                "end_datetime": "2024-01-01T17:00:00"
                            }
                        ]
                    }
                ]
            }
        ]
    }
]

td_05_g = [
    {
        "Description": "Testcase 05g (Wel ZVL functie + Wel AOK + ODB Kwalificatieniveau Behandelaar - AOK Inzet 36u p/w)",
        "Amount": 10, # Indicator score =  0. (10 * (28/36)/47 = 0.165...)
        "Human": [
            {
                "ArbeidsOvereenkomst": [
                    {
                        "function": [
                            {
                                "label": "AOK Wel ZVL ODB Behandelaar",
                                "caregiving_role": True,
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31",
                                "qualification_level": [
                                    {
                                        "qualification_level_value": "Kwalificatieniveau Behandelaar",
                                        "start_date": "2024-01-01",
                                        "end_date": "2024-12-31" 
                                    }
                                ]
                            }
                        ],
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",                        
                        "size": [
                             {
                                "unit": 36,
                                "unit_of_measure": "Uren_per_week_unit",
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31"
                             }
                        ],
                        "booked_hours": [
                            {
                                "unit": 8,
                                "unit_of_measure": "Uur",
                                "start_datetime": "2024-01-01T09:00:00",
                                "end_datetime": "2024-01-01T17:00:00"
                            }
                        ]
                    }
                ]
            }
        ]
    }
]

td_05_h = [
    {
        "Description": "Testcase 05h (Wel ZVL functie + Wel AOK + ODB Kwalificatieniveau Leerling - AOK Inzet 36u p/w)",
        "Amount": 10, # Indicator score =  0. (10 * (28/36)/47 = 0.165...)
        "Human": [
            {
                "ArbeidsOvereenkomst": [
                    {
                        "function": [
                            {
                                "label": "AOK Wel ZVL ODB Leerling",
                                "caregiving_role": True,
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31",
                                "qualification_level": [
                                    {
                                        "qualification_level_value": "Kwalificatieniveau Leerling",
                                        "start_date": "2024-01-01",
                                        "end_date": "2024-12-31" 
                                    }
                                ]
                            }
                        ],
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",                        
                        "size": [
                             {
                                "unit": 36,
                                "unit_of_measure": "Uren_per_week_unit",
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31"
                             }
                        ],
                        "booked_hours": [
                            {
                                "unit": 8,
                                "unit_of_measure": "Uur",
                                "start_datetime": "2024-01-01T09:00:00",
                                "end_datetime": "2024-01-01T17:00:00"
                            }
                        ]
                    }
                ]
            }
        ]
    }
]

td_05_i = [
    {
        "Description": "Testcase 05i (Wel ZVL functie + Wel AOK + ODB Kwalificatieniveau Overig zorgpersoneel - AOK Inzet 36u p/w)",
        "Amount": 10, # Indicator score =  0. (10 * (28/36)/47 = 0.165...)
        "Human": [
            {
                "ArbeidsOvereenkomst": [
                    {
                        "function": [
                            {
                                "label": "AOK Wel ZVL ODB Overig zorgpersoneel",
                                "caregiving_role": True,
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31",
                                "qualification_level": [
                                    {
                                        "qualification_level_value": "Kwalificatieniveau Overig zorgpersoneel",
                                        "start_date": "2024-01-01",
                                        "end_date": "2024-12-31" 
                                    }
                                ]
                            }
                        ],
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",                        
                        "size": [
                             {
                                "unit": 36,
                                "unit_of_measure": "Uren_per_week_unit",
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31"
                             }
                        ],
                        "booked_hours": [
                            {
                                "unit": 8,
                                "unit_of_measure": "Uur",
                                "start_datetime": "2024-01-01T09:00:00",
                                "end_datetime": "2024-01-01T17:00:00"
                            }
                        ]
                    }
                ]
            }
        ]
    }
]

td_05_j = [
    {
        "Description": "Testcase 05j (Wel ZVL functie + Wel AOK + ODB Kwalificatieniveau 1 + Niet AOK + ODB Kwalificatieniveau 1 (18u p/w + 18u p/w))",
        "Amount": 10, # Indicator score =  0. (10 * (28/36)/47 = 0.165...)
        "Human": [
            {
                "ArbeidsOvereenkomst": [
                    {
                        "function": [
                            {
                                "label": "AOK Wel ZVL ODB Kwalificatieniveau 1",
                                "caregiving_role": True,
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31",
                                "qualification_level": [
                                    {
                                        "qualification_level_value": "Kwalificatieniveau 1",
                                        "start_date": "",
                                        "end_date": "" 
                                    }
                                ]
                            }
                        ],
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",                        
                        "size": [
                             {
                                "unit": 36,
                                "unit_of_measure": "Uren_per_week_unit",
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31"
                             }
                        ],
                        "booked_hours": [
                            {
                                "unit": 18,
                                "unit_of_measure": "Uur",
                                "start_datetime": "2024-12-01T13:00:00",
                                "end_datetime": "2024-12-01T17:00:00"
                            }
                        ]
                    }
                ],
                "InhuurOvereenkomst": [
                    {
                        "function": [
                            {
                                "label": "INH_OK Wel ZVL ODB Kwalificatieniveau 1",
                                "caregiving_role": True,
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31",
                                "qualification_level": [
                                    {
                                        "qualification_level_value": "Kwalificatieniveau 1",
                                        "start_date": "2024-01-01",
                                        "end_date": "2024-12-31" 
                                    }
                                ]
                            }
                        ],
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",                        
                        "size": [
                             {
                                "unit": 18,
                                "unit_of_measure": "Uren_per_week_unit",
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31"
                             }
                        ],
                        "booked_hours": [
                            {
                                "unit": 8,
                                "unit_of_measure": "Uur",
                                "start_datetime": "2024-01-01T09:00:00",
                                "end_datetime": "2024-01-01T17:00:00"
                            }
                        ]
                    }
                ]
            }
        ]
    }
]

td_05_k = [
    {
        "Description": "Testcase 05k (Wel ZVL functie + Wel AOK + ODB Kwalificatieniveau 1 - AOK Inzet 36u p/w + 2 weken verzuim)",
        "Amount": 10, # Indicator score =  0. (10 * (28/36)/47 = 0.165...)
        "Human": [
            {
                "ArbeidsOvereenkomst": [
                    {
                        "function": [
                            {
                                "label": "AOK Wel ZVL ODB Kwalificatieniveau 1",
                                "caregiving_role": True,
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31",
                                "qualification_level": [
                                    {
                                        "qualification_level_value": "Kwalificatieniveau 1",
                                        "start_date": "2024-01-01",
                                        "end_date": "2024-12-31" 
                                    }
                                ]
                            }
                        ],
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",                        
                        "size": [
                             {
                                "unit": 36,
                                "unit_of_measure": "Uren_per_week_unit",
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31"
                             }
                        ],
                        "sickness": [
                            {
                                "unit": 36,
                                "unit_of_measure": "Uren_per_week_unit",
                                "start_date": "2024-03-01",
                                "end_date": "2024-03-15"
                            }
                        ],
                        "booked_hours": [
                            {
                                "unit": 8,
                                "unit_of_measure": "Uur",
                                "start_datetime": "2024-01-01T09:00:00",
                                "end_datetime": "2024-01-01T17:00:00"
                            }
                        ]
                    }
                ]
            }
        ]
    }
]

td_05_l = [
    {
        "Description": "Testcase 05l (Wel ZVL functie + Wel AOK + ODB Kwalificatieniveau 1 - AOK Inzet 36u p/w + 3 maanden zwangerschapsverlof)",
        "Amount": 10, # Indicator score =  0. (10 * (28/36)/47 = 0.165...)
        "Human": [
            {
                "ArbeidsOvereenkomst": [
                    {
                        "function": [
                            {
                                "label": "AOK Wel ZVL ODB Kwalificatieniveau 1",
                                "caregiving_role": True,
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31",
                                "qualification_level": [
                                    {
                                        "qualification_level_value": "Kwalificatieniveau 1",
                                        "start_date": "2024-01-01",
                                        "end_date": "2024-12-31" 
                                    }
                                ]
                            }
                        ],
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",                        
                        "size": [
                             {
                                "unit": 36,
                                "unit_of_measure": "Uren_per_week_unit",
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31"
                             }
                        ],
                        "maternity_leave": [
                            {
                                "start_date": "2024-02-01",
                                "end_date": "2024-05-01"
                            }
                        ],
                        "booked_hours": [
                            {
                                "unit": 8,
                                "unit_of_measure": "Uur",
                                "start_datetime": "2024-01-01T09:00:00",
                                "end_datetime": "2024-01-01T17:00:00"
                            }
                        ]
                    }
                ]
            }
        ]
    }
]

td_05_m = [
    {
        "Description": "Testcase 05m (Wel ZVL functie + Wel AOK + ODB Kwalificatieniveau 1 - AOK Inzet 36u p/w + 2 weken verzuim + 3 maanden zwangerschapsverlof)",
        "Amount": 10, # Indicator score =  0. (10 * (28/36)/47 = 0.165...)
        "Human": [
            {
                "ArbeidsOvereenkomst": [
                    {
                        "function": [
                            {
                                "label": "AOK Wel ZVL ODB Kwalificatieniveau 1",
                                "caregiving_role": True,
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31",
                                "qualification_level": [
                                    {
                                        "qualification_level_value": "Kwalificatieniveau 1",
                                        "start_date": "2024-01-01",
                                        "end_date": "2024-12-31" 
                                    }
                                ]
                            }
                        ],
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",                        
                        "size": [
                             {
                                "unit": 36,
                                "unit_of_measure": "Uren_per_week_unit",
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31"
                             }
                        ],                        
                        "sickness": [
                            {
                                "unit": 36,
                                "unit_of_measure": "Uren_per_week_unit",
                                "start_date": "2024-03-01",
                                "end_date": "2024-03-15"
                            }
                        ],
                        "maternity_leave": [
                            {
                                "start_date": "2024-02-01",
                                "end_date": "2024-05-01"
                            }
                        ],
                        "booked_hours": [
                            {
                                "unit": 8,
                                "unit_of_measure": "Uur",
                                "start_datetime": "2024-01-01T09:00:00",
                                "end_datetime": "2024-01-01T17:00:00"
                            }
                        ]
                    }
                ]
            }
        ]
    }
]

td_06 = [
    {
        "Description": "Testcase 06 (Geen ZVL functie + Wel AOK + ODB Kwalificatieniveau 1 - AOK Inzet 36u p/w )",
        "Amount": 10, # Indicator score =  0. (10 * (28/36)/47 = 0.165...)
        "Human": [
            {
                "ArbeidsOvereenkomst": [
                    {
                        "function": [
                            {
                                "label": "AOK Geen ZVL ODB Kwalificatieniveau 1",
                                "caregiving_role": False,
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31",
                                "qualification_level": [
                                    {
                                        "qualification_level_value": "Kwalificatieniveau 1",
                                        "start_date": "2024-01-01",
                                        "end_date": "2024-12-31" 
                                    }
                                ]
                            }
                        ],
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",                        
                        "size": [
                             {
                                "unit": 36,
                                "unit_of_measure": "Uren_per_week_unit",
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31"
                             }
                        ],
                        "booked_hours": [
                            {
                                "unit": 8,
                                "unit_of_measure": "Uur",
                                "start_datetime": "2024-01-01T09:00:00",
                                "end_datetime": "2024-01-01T17:00:00"
                            }
                        ]
                    }
                ]
            }
        ]
    }
]

td_07 = [
    {
        "Description": "Testcase 07 (Geen ZVL functie + Wel AOK (niet in meetperiode) + Wel PIL + ODB Kwalificatieniveau 1 - AOK Inzet 36u p/w )",
        "Amount": 10, # Indicator score =  0. (10 * (28/36)/47 = 0.165...)
        "Human": [
            {
                "ArbeidsOvereenkomst": [
                    {
                        "function": [
                            {
                                "label": "AOK (niet in meetperiode) Geen ZVL ODB Kwalificatieniveau 1",
                                "caregiving_role": False,
                                "start_date": "2023-01-01",
                                "end_date": "2023-12-31",
                                "qualification_level": [
                                    {
                                        "qualification_level_value": "Kwalificatieniveau 1",
                                        "start_date": "2023-01-01",
                                        "end_date": "2023-12-31" 
                                    }
                                ]
                            }
                        ],
                        "start_date": "2023-01-01",
                        "end_date": "2023-12-31",                        
                        "size": [
                             {
                                "unit": 36,
                                "unit_of_measure": "Uren_per_week_unit",
                                "start_date": "2023-01-01",
                                "end_date": "2023-12-31"
                             }
                        ],
                        "booked_hours": [
                            {
                                "unit": 8,
                                "unit_of_measure": "Uur",
                                "start_datetime": "2024-01-01T09:00:00",
                                "end_datetime": "2024-01-01T17:00:00"
                            }
                        ]
                    }
                ]
            }
        ]
    }
]

td_08 = [
    {
        "Description": "Testcase 08 (Geen ZVL functie + Wel AOK (niet in meetperiode) + Wel PIL + Geen ODB Kwalificatieniveau - AOK Inzet 36u p/w )",
        "Amount": 10, # Indicator score =  0. (10 * (28/36)/47 = 0.165...)
        "Human": [
            {
                "ArbeidsOvereenkomst": [
                    {
                        "function": [
                            {
                                "label": "AOK (niet in meetperiode) Geen ZVL ",
                                "caregiving_role": False,
                                "start_date": "2023-01-01",
                                "end_date": "2023-12-31",
                            }
                        ],
                        "start_date": "2023-01-01",
                        "end_date": "2023-12-31",                        
                        "size": [
                             {
                                "unit": 36,
                                "unit_of_measure": "Uren_per_week_unit",
                                "start_date": "2023-01-01",
                                "end_date": "2023-12-31"
                             }
                        ],
                        "booked_hours": [
                            {
                                "unit": 8,
                                "unit_of_measure": "Uur",
                                "start_datetime": "2024-01-01T09:00:00",
                                "end_datetime": "2024-01-01T17:00:00"
                            }
                        ]
                    }
                ]
            }
        ]
    }
]

# Static tests

def test_if_headers_are_correct_for_query_2_2_1(db_config):
    """ Test of de juiste header terugkomt in het resultaat
        ODB Personeelssamenstelling 2.2.1. Percentage fte per niveau
    """

    # Setup of the test
    test = QueryTest(db_config)

    # Configuration and execution
    test.set_query('Gevalideerde_vragen_technisch/personeelssamenstelling/Indicator 2.2.1.rq')

    # Assertions
    test.verify_header_present('kwalificatie')
    test.verify_header_present('teller')
    test.verify_header_present('noemer')
    test.verify_header_present('indicator')

def test_if_number_of_rows_returned_is_correct_for_query_2_2_1(db_config):
    """ Test of het aantal rijen correct wordt teruggegeven
        ODB Personeelssamenstelling 2.2.1. Percentage fte per niveau
    """

    # Setup of the test
    test = QueryTest(db_config)

    # Configuration and execution
    test.set_query('Gevalideerde_vragen_technisch/personeelssamenstelling/Indicator 2.2.1.rq')

    # Assertions
    test.verify_row_count(1)


def test_if_indicator_has_correct_value_for_query_2_2_1(db_config):
    """ Test of de indicator de juiste waarde heeft
        ODB Personeelssamenstelling 2.2.1. Percentage fte per niveau
    """

    # Setup of the test
    test = QueryTest(db_config)

    # Configuration and execution
    test.set_query('Gevalideerde_vragen_technisch/personeelssamenstelling/Indicator 2.2.1.rq')

    # Assertions
    test.verify_value_in_list("indicator", {"100","100.0"})


def test_if_dates_can_change_2_2_1(db_config):
    """ Test of gewijzigde datum daadwerkelijk een ander resultaat oplevert
        ODB Personeelssamenstelling 2.2.1. Percentage fte per niveau
    """

    # Setup of the test
    test = QueryTest(db_config)

    # Configuration and execution
    test.set_query('Gevalideerde_vragen_technisch/personeelssamenstelling/Indicator 2.2.1.rq')

    test.change_start_period("2023-01-01", "2024-01-01")
    test.change_end_period("2023-12-31", "2024-12-31")

    # Assertions
    test.verify_value("indicator",None)

# Tests using generated data

# Testcase 01

def test_if_value_returned_is_correct_for_query_2_2_1_01(db_config):
    """ Testcase 01 (Geen ZVL functie + Geen AOK + Geen ODB Kwalificatieniveau + Geen PIL)
        ODB personeelssamenstelling 2.2.1. Percentage fte per niveau
    """

    # Load defined test data
    dg = DataGenerator(db_config, td_01)

    try:
        # Start a test
        test = QueryTest(db_config)

        # Set the query to be tested
        test.set_query('Gevalideerde_vragen_technisch/personeelssamenstelling/Indicator 2.2.1.rq')

        # Change the reference dates of the query
        test.change_start_period("2023-01-01","2024-01-01")
        test.change_end_period("2023-12-31", "2024-12-31")

        # Verify actual result of the query
        test.verify_value("indicator", None)

    finally:
        # Delete previously loaded data
        dg.delete_graph_data()

# Testcase 02

def test_if_value_returned_is_correct_for_query_2_2_1_02(db_config):
    """ Testcase 02 (Geen ZVL functie + Wel AOK + Geen ODB Kwalificatieniveau + Geen PIL)
        ODB personeelssamenstelling 2.2.1 Percentage fte per niveau
    """

    # Load defined test data
    dg = DataGenerator(db_config, td_02)

    try:
        # Start a test
        test = QueryTest(db_config)

        # Set the query to be tested
        test.set_query('Gevalideerde_vragen_technisch/personeelssamenstelling/Indicator 2.2.1.rq')

        # Change the reference dates of the query
        test.change_start_period("2023-01-01","2024-01-01")
        test.change_end_period("2023-12-31", "2024-12-31")

        # Verify actual result of the query
        test.verify_value("indicator", None)

    finally:
        # Delete previously loaded data
        dg.delete_graph_data()

# Testcase 03

def test_if_value_returned_is_correct_for_query_2_2_1_03(db_config):
    """ Testcase 03 (Wel ZVL functie + Wel AOK + Geen ODB Kwalificatieniveau + Geen PIL)
        ODB personeelssamenstelling 2.2.1 Percentage fte per niveau
    """

    # Load defined test data
    dg = DataGenerator(db_config, td_03)

    try:
        # Start a test
        test = QueryTest(db_config)

        # Set the query to be tested
        test.set_query('Gevalideerde_vragen_technisch/personeelssamenstelling/Indicator 2.2.1.rq')

        # Change the reference dates of the query
        test.change_start_period("2023-01-01","2024-01-01")
        test.change_end_period("2023-12-31", "2024-12-31")

        # Verify actual result of the query
        test.verify_value("indicator", None)

    finally:
        # Delete previously loaded data
        dg.delete_graph_data()

# Testcase 04

def test_if_value_returned_is_correct_for_query_2_2_1_04(db_config):
    """ Testcase 04 (Wel ZVL functie + Wel AOK + ODB Kwalificatieniveau 1 + Geen PIL)
        ODB personeelssamenstelling 2.2.1 Percentage fte per niveau
    """

    # Load defined test data
    dg = DataGenerator(db_config, td_04)

    try:
        # Start a test
        test = QueryTest(db_config)

        # Set the query to be tested
        test.set_query('Gevalideerde_vragen_technisch/personeelssamenstelling/Indicator 2.2.1.rq')

        # Change the reference dates of the query
        test.change_start_period("2023-01-01","2024-01-01")
        test.change_end_period("2023-12-31", "2024-12-31")

        # Verify actual result of the query
        test.verify_value("indicator", None)

    finally:
        # Delete previously loaded data
        dg.delete_graph_data()

# Testcase 05

def test_if_value_returned_is_correct_for_query_2_2_1_05(db_config):
    """ Testcase 05 (Wel ZVL functie + Wel AOK + Geen ODB Kwalificatieniveau + Wel PIL)
        ODB personeelssamenstelling 2.2.1 Percentage fte per niveau
    """

    # Load defined test data
    dg = DataGenerator(db_config, td_05)

    try:
        # Start a test
        test = QueryTest(db_config)

        # Set the query to be tested
        test.set_query('Gevalideerde_vragen_technisch/personeelssamenstelling/Indicator 2.2.1.rq')

        # Change the reference dates of the query
        test.change_start_period("2023-01-01","2024-01-01")
        test.change_end_period("2023-12-31", "2024-12-31")

        # Verify actual result of the query
        test.verify_value("indicator", None)

    finally:
        # Delete previously loaded data
        dg.delete_graph_data()

# Testcase 05a

def test_if_value_returned_is_correct_for_query_2_2_1_05_a(db_config):
    """ Testcase 05a (Wel ZVL functie + Wel AOK + ODB Kwalificatieniveau 1 + Wel PIL)
        ODB personeelssamenstelling 2.2.1 Percentage fte per niveau
    """

    # Load defined test data
    dg = DataGenerator(db_config, td_05_a)

    try:
        # Start a test
        test = QueryTest(db_config)

        # Set the query to be tested
        test.set_query('Gevalideerde_vragen_technisch/personeelssamenstelling/Indicator 2.2.1.rq')

        # Change the reference dates of the query
        test.change_start_period("2023-01-01","2024-01-01")
        test.change_end_period("2023-12-31", "2024-12-31")

        # Verify actual result of the query
        test.verify_value_in_list("indicator", {"100","100.0"})

    finally:
        # Delete previously loaded data
        dg.delete_graph_data()

# Testcase 05b

def test_if_value_returned_is_correct_for_query_2_2_1_05_b(db_config):
    """ Testcase 05b (Wel ZVL functie + Wel AOK + ODB Kwalificatieniveau 2 + Wel PIL)
        ODB personeelssamenstelling 2.2.1 Percentage fte per niveau
    """

    # Load defined test data
    dg = DataGenerator(db_config, td_05_b)

    try:
        # Start a test
        test = QueryTest(db_config)

        # Set the query to be tested
        test.set_query('Gevalideerde_vragen_technisch/personeelssamenstelling/Indicator 2.2.1.rq')

        # Change the reference dates of the query
        test.change_start_period("2023-01-01","2024-01-01")
        test.change_end_period("2023-12-31", "2024-12-31")

        # Verify actual result of the query
        test.verify_value_in_list("indicator", {"100","100.0"})

    finally:
        # Delete previously loaded data
        dg.delete_graph_data()

# Testcase 05c

def test_if_value_returned_is_correct_for_query_2_2_1_05_c(db_config):
    """ Testcase 05c (Wel ZVL functie + Wel AOK + ODB Kwalificatieniveau 3 + Wel PIL)
        ODB personeelssamenstelling 2.2.1 Percentage fte per niveau
    """

    # Load defined test data
    dg = DataGenerator(db_config, td_05_c)

    try:
        # Start a test
        test = QueryTest(db_config)

        # Set the query to be tested
        test.set_query('Gevalideerde_vragen_technisch/personeelssamenstelling/Indicator 2.2.1.rq')

        # Change the reference dates of the query
        test.change_start_period("2023-01-01","2024-01-01")
        test.change_end_period("2023-12-31", "2024-12-31")

        # Verify actual result of the query
        test.verify_value_in_list("indicator", {"100","100.0"})

    finally:
        # Delete previously loaded data
        dg.delete_graph_data()

# Testcase 05d

def test_if_value_returned_is_correct_for_query_2_2_1_05_d(db_config):
    """ Testcase 05d (Wel ZVL functie + Wel AOK + ODB Kwalificatieniveau 4 + Wel PIL)
        ODB personeelssamenstelling 2.2.1 Percentage fte per niveau
    """

    # Load defined test data
    dg = DataGenerator(db_config, td_05_d)

    try:
        # Start a test
        test = QueryTest(db_config)

        # Set the query to be tested
        test.set_query('Gevalideerde_vragen_technisch/personeelssamenstelling/Indicator 2.2.1.rq')

        # Change the reference dates of the query
        test.change_start_period("2023-01-01","2024-01-01")
        test.change_end_period("2023-12-31", "2024-12-31")

        # Verify actual result of the query
        test.verify_value_in_list("indicator", {"100","100.0"})

    finally:
        # Delete previously loaded data
        dg.delete_graph_data()

# Testcase 05e

def test_if_value_returned_is_correct_for_query_2_2_1_05_e(db_config):
    """ Testcase 05e (Wel ZVL functie + Wel AOK + ODB Kwalificatieniveau 5 + Wel PIL)
        ODB personeelssamenstelling 2.2.1 Percentage fte per niveau
    """

    # Load defined test data
    dg = DataGenerator(db_config, td_05_e)

    try:
        # Start a test
        test = QueryTest(db_config)

        # Set the query to be tested
        test.set_query('Gevalideerde_vragen_technisch/personeelssamenstelling/Indicator 2.2.1.rq')

        # Change the reference dates of the query
        test.change_start_period("2023-01-01","2024-01-01")
        test.change_end_period("2023-12-31", "2024-12-31")

        # Verify actual result of the query
        test.verify_value_in_list("indicator", {"100","100.0"})

    finally:
        # Delete previously loaded data
        dg.delete_graph_data()

# Testcase 05f

def test_if_value_returned_is_correct_for_query_2_2_1_05_f(db_config):
    """ Testcase 05f (Wel ZVL functie + Wel AOK + ODB Kwalificatieniveau 6 + Wel PIL)
        ODB personeelssamenstelling 2.2.1 Percentage fte per niveau
    """

    # Load defined test data
    dg = DataGenerator(db_config, td_05_f)

    try:
        # Start a test
        test = QueryTest(db_config)

        # Set the query to be tested
        test.set_query('Gevalideerde_vragen_technisch/personeelssamenstelling/Indicator 2.2.1.rq')

        # Change the reference dates of the query
        test.change_start_period("2023-01-01","2024-01-01")
        test.change_end_period("2023-12-31", "2024-12-31")

        # Verify actual result of the query
        test.verify_value_in_list("indicator", {"100","100.0"})

    finally:
        # Delete previously loaded data
        dg.delete_graph_data()

# Testcase 05g

def test_if_value_returned_is_correct_for_query_2_2_1_05_g(db_config):
    """ Testcase 05g (Wel ZVL functie + Wel AOK + ODB Kwalificatieniveau Behandelaar + Wel PIL)
        ODB personeelssamenstelling 2.2.1 Percentage fte per niveau
    """

    # Load defined test data
    dg = DataGenerator(db_config, td_05_g)

    try:
        # Start a test
        test = QueryTest(db_config)

        # Set the query to be tested
        test.set_query('Gevalideerde_vragen_technisch/personeelssamenstelling/Indicator 2.2.1.rq')

        # Change the reference dates of the query
        test.change_start_period("2023-01-01","2024-01-01")
        test.change_end_period("2023-12-31", "2024-12-31")

        # Verify actual result of the query
        test.verify_value_in_list("indicator", {"100","100.0"})

    finally:
        # Delete previously loaded data
        dg.delete_graph_data()

# Testcase 05h

def test_if_value_returned_is_correct_for_query_2_2_1_05_h(db_config):
    """ Testcase 05h (Wel ZVL functie + Wel AOK + ODB Kwalificatieniveau Leerling + Wel PIL)
        ODB personeelssamenstelling 2.2.1 Percentage fte per niveau
    """

    # Load defined test data
    dg = DataGenerator(db_config, td_05_h)

    try:
        # Start a test
        test = QueryTest(db_config)

        # Set the query to be tested
        test.set_query('Gevalideerde_vragen_technisch/personeelssamenstelling/Indicator 2.2.1.rq')

        # Change the reference dates of the query
        test.change_start_period("2023-01-01","2024-01-01")
        test.change_end_period("2023-12-31", "2024-12-31")

        # Verify actual result of the query
        test.verify_value_in_list("indicator", {"100","100.0"})

    finally:
        # Delete previously loaded data
        dg.delete_graph_data()

# Testcase 05i

def test_if_value_returned_is_correct_for_query_2_2_1_05_i(db_config):
    """ Testcase 05i (Wel ZVL functie + Wel AOK + ODB Kwalificatieniveau Overig Zorgpersoneel + Wel PIL)
        ODB personeelssamenstelling 2.2.1 Percentage fte per niveau
    """

    # Load defined test data
    dg = DataGenerator(db_config, td_05_i)

    try:
        # Start a test
        test = QueryTest(db_config)

        # Set the query to be tested
        test.set_query('Gevalideerde_vragen_technisch/personeelssamenstelling/Indicator 2.2.1.rq')

        # Change the reference dates of the query
        test.change_start_period("2023-01-01","2024-01-01")
        test.change_end_period("2023-12-31", "2024-12-31")

        # Verify actual result of the query
        test.verify_value_in_list("indicator", {"100","100.0"})

    finally:
        # Delete previously loaded data
        dg.delete_graph_data()

# Testcase 05j

def test_if_value_returned_is_correct_for_query_2_2_1_05_j(db_config):
    """ Testcase 05j (Wel ZVL functie + Wel AOK + ODB Kwalificatieniveau 1 + Niet AOK + ODB Kwalificatieniveau 1 (18u p/w + 18u p/w))
        ODB personeelssamenstelling 2.2.1 Percentage fte per niveau
    """

    # Load defined test data
    dg = DataGenerator(db_config, td_05_j)

    try:
        # Start a test
        test = QueryTest(db_config)

        # Set the query to be tested
        test.set_query('Gevalideerde_vragen_technisch/personeelssamenstelling/Indicator 2.2.1.rq')

        # Change the reference dates of the query
        test.change_start_period("2023-01-01","2024-01-01")
        test.change_end_period("2023-12-31", "2024-12-31")

        # Verify actual result of the query
        test.verify_value_in_list("indicator", {"100","100.0"})

    finally:
        # Delete previously loaded data
        dg.delete_graph_data()
        
# Testcase 05k

def test_if_value_returned_is_correct_for_query_2_2_1_05_k(db_config):
    """ Testcase 05k (Wel ZVL functie + Wel AOK + ODB Kwalificatieniveau 1 - AOK Inzet 36u p/w + 2 weken verzuim)
        ODB personeelssamenstelling 2.2.1 Percentage fte per niveau
    """

    # Load defined test data
    dg = DataGenerator(db_config, td_05_k)

    try:
        # Start a test
        test = QueryTest(db_config)

        # Set the query to be tested
        test.set_query('Gevalideerde_vragen_technisch/personeelssamenstelling/Indicator 2.2.1.rq')

        # Change the reference dates of the query
        test.change_start_period("2023-01-01","2024-01-01")
        test.change_end_period("2023-12-31", "2024-12-31")

        # Verify actual result of the query
        test.verify_value_in_list("indicator", {"100","100.0"})

    finally:
        # Delete previously loaded data
        dg.delete_graph_data()

# Testcase 05l

def test_if_value_returned_is_correct_for_query_2_2_1_05_l(db_config):
    """ Testcase 05l (Wel ZVL functie + Wel AOK + ODB Kwalificatieniveau 1 - AOK Inzet 36u p/w + 3 maanden zwangerschapsverlof)
        ODB personeelssamenstelling 2.2.1 Percentage fte per niveau
    """

    # Load defined test data
    dg = DataGenerator(db_config, td_05_l)

    try:
        # Start a test
        test = QueryTest(db_config)

        # Set the query to be tested
        test.set_query('Gevalideerde_vragen_technisch/personeelssamenstelling/Indicator 2.2.1.rq')

        # Change the reference dates of the query
        test.change_start_period("2023-01-01","2024-01-01")
        test.change_end_period("2023-12-31", "2024-12-31")

        # Verify actual result of the query
        test.verify_value_in_list("indicator", {"100","100.0"})

    finally:
        # Delete previously loaded data
        dg.delete_graph_data()

# Testcase 05m

def test_if_value_returned_is_correct_for_query_2_2_1_05_m(db_config):
    """ Testcase 05m (Wel ZVL functie + Wel AOK + ODB Kwalificatieniveau 1 - AOK Inzet 36u p/w + 2 weken verzuim + 3 maanden zwangerschapsverlof)
        ODB personeelssamenstelling 2.2.1 Percentage fte per niveau
    """

    # Load defined test data
    dg = DataGenerator(db_config, td_05_m)

    try:
        # Start a test
        test = QueryTest(db_config)

        # Set the query to be tested
        test.set_query('Gevalideerde_vragen_technisch/personeelssamenstelling/Indicator 2.2.1.rq')

        # Change the reference dates of the query
        test.change_start_period("2023-01-01","2024-01-01")
        test.change_end_period("2023-12-31", "2024-12-31")

        # Verify actual result of the query
        test.verify_value_in_list("indicator", {"100","100.0"})

    finally:
        # Delete previously loaded data
        dg.delete_graph_data()

# Testcase 06

def test_if_value_returned_is_correct_for_query_2_2_1_06(db_config):
    """ Testcase 06 (Geen ZVL functie + Wel AOK + ODB Kwalificatieniveau 1 - AOK Inzet 36u p/w )
        ODB personeelssamenstelling 2.2.1 Percentage fte per niveau
    """

    # Load defined test data
    dg = DataGenerator(db_config, td_06)

    try:
        # Start a test
        test = QueryTest(db_config)

        # Set the query to be tested
        test.set_query('Gevalideerde_vragen_technisch/personeelssamenstelling/Indicator 2.2.1.rq')

        # Change the reference dates of the query
        test.change_start_period("2023-01-01","2024-01-01")
        test.change_end_period("2023-12-31", "2024-12-31")

        # Verify actual result of the query
        test.verify_value("indicator", None)

    finally:
        # Delete previously loaded data
        dg.delete_graph_data()

# Testcase 07

def test_if_value_returned_is_correct_for_query_2_2_1_07(db_config):
    """ Testcase 07 (Geen ZVL functie + Wel AOK (niet in meetperiode) + Wel PIL + ODB Kwalificatieniveau 1 - AOK Inzet 36u p/w )
        ODB personeelssamenstelling 2.2.1 Percentage fte per niveau
    """

    # Load defined test data
    dg = DataGenerator(db_config, td_07)

    try:
        # Start a test
        test = QueryTest(db_config)

        # Set the query to be tested
        test.set_query('Gevalideerde_vragen_technisch/personeelssamenstelling/Indicator 2.2.1.rq')

        # Change the reference dates of the query
        test.change_start_period("2023-01-01","2024-01-01")
        test.change_end_period("2023-12-31", "2024-12-31")

        # Verify actual result of the query
        test.verify_value("indicator", None)

    finally:
        # Delete previously loaded data
        dg.delete_graph_data()

# Testcase 08

def test_if_value_returned_is_correct_for_query_2_2_1_08(db_config):
    """ Testcase 08 (Geen ZVL functie + Wel AOK (niet in meetperiode) + Wel PIL + Geen ODB Kwalificatieniveau - AOK Inzet 36u p/w )
        ODB personeelssamenstelling 2.2.1 Percentage fte per niveau
    """

    # Load defined test data
    dg = DataGenerator(db_config, td_08)

    try:
        # Start a test
        test = QueryTest(db_config)

        # Set the query to be tested
        test.set_query('Gevalideerde_vragen_technisch/personeelssamenstelling/Indicator 2.2.1.rq')

        # Change the reference dates of the query
        test.change_start_period("2023-01-01","2024-01-01")
        test.change_end_period("2023-12-31", "2024-12-31")

        # Verify actual result of the query
        test.verify_value("indicator", None)

    finally:
        # Delete previously loaded data
        dg.delete_graph_data()