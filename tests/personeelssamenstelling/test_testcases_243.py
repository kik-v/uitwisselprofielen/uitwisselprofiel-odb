from DataGenerator import DataGenerator
from QueryTest import QueryTest

# Peildatum: 2023-12-31

# Opmerkingen:


# Testcases

td_01 = [
    {
        "Description": "Testcase 01 (Geen ZVL functie + Geen AOK + Geen ZVL functie - 1 + Geen AOK - 1 + Geen Kwal. Niv. + Geen Kwal. Niv. - 1)",
        "Amount": 10,
        "Human": [
            {
                "InhuurOvereenkomst": [
                    {
 
                        "function": [
                            {
                                "label": "Inhuur Medewerker niet ZVL",
                                "caregiving_role": False,
                                "start_date": "2023-01-01",
                                "end_date": "2024-12-31",
                            }
                        ],
                        "start_date": "2023-01-31",
                        "end_date": "2024-12-31",
                        "size": [
                            {
                                "unit": 36,
                                "unit_of_measure": "Uren_per_week_unit",
                                "start_date": "2023-01-31",
                                "end_date": "2024-12-31"
                            }
                        ]
                    }
                ]
            }
        ]
    }
]

td_02 = [
    {
        "Description": "Testcase 02 (Geen ZVL functie + Geen AOK + Geen ZVL functie - 1 + Geen AOK - 1 + Wel Kwal. Niv. + Geen Kwal. Niv. - 1)",
        "Amount": 10,
        "Human": [
            {
                "InhuurOvereenkomst": [
                    {
 
                        "function": [
                            {
                                "label": "Inhuur Medewerker niet ZVL",
                                "caregiving_role": False,
                                "start_date": "2023-01-01",
                                "end_date": "2024-12-31",
                                "qualification_level": [
                                    {
                                        "qualification_level_value": "Kwalificatieniveau 4",
                                        "start_date": "2024-01-01",
                                        "end_date": "2024-12-31"
                                    }
                                ]
                            }
                        ],
                        "start_date": "2023-01-01",
                        "end_date": "2024-12-31",
                        "size": [
                            {
                                "unit": 36,
                                "unit_of_measure": "Uren_per_week_unit",
                                "start_date": "2023-01-01",
                                "end_date": "2024-12-31"
                            }
                        ]
                    }
                ]
            }
        ]
    }
]

td_02_a = [
    {
        "Description": "Testcase 02a (Geen ZVL functie + Geen AOK + Geen ZVL functie - 1 + Geen AOK - 1 + Geen Kwal. Niv. + Wel Kwal. Niv. - 1)",
        "Amount": 10,
        "Human": [
            {
                "InhuurOvereenkomst": [
                    {
 
                        "function": [
                            {
                                "label": "Inhuur Medewerker niet ZVL",
                                "caregiving_role": False,
                                "start_date": "2023-01-01",
                                "end_date": "2024-12-31",
                                "qualification_level": [
                                    {
                                        "qualification_level_value": "Kwalificatieniveau 4",
                                        "start_date": "2023-01-01",
                                        "end_date": "2023-12-31"
                                    }
                                ]
                            }
                        ],
                        "start_date": "2023-01-01",
                        "end_date": "2024-12-31",
                        "size": [
                            {
                                "unit": 36,
                                "unit_of_measure": "Uren_per_week_unit",
                                "start_date": "2023-01-01",
                                "end_date": "2024-12-31"
                            }
                        ]
                    }
                ]
            }
        ]
    }
]

td_03 = [
    {
        "Description": "Testcase 03 (Geen ZVL functie + Geen AOK + Geen ZVL functie - 1 + Geen AOK - 1 + Wel Kwal. Niv. + Wel Kwal. Niv. - 1)",
        "Amount": 10,
        "Human": [
            {
                "InhuurOvereenkomst": [
                    {
 
                        "function": [
                            {
                                "label": "Inhuur Medewerker niet ZVL",
                                "caregiving_role": False,
                                "start_date": "2023-01-01",
                                "end_date": "2024-12-31",
                                "qualification_level": [
                                    {
                                        "qualification_level_value": "Kwalificatieniveau 4",
                                        "start_date": "2024-01-01",
                                        "end_date": "2024-12-31"
                                    },
                                    {
                                        "qualification_level_value": "Kwalificatieniveau 3",
                                        "start_date": "2023-01-01",
                                        "end_date": "2023-12-31"
                                    }
                                ]
                            }
                        ],
                        "start_date": "2023-01-01",
                        "end_date": "2024-12-31",
                        "size": [
                            {
                                "unit": 36,
                                "unit_of_measure": "Uren_per_week_unit",
                                "start_date": "2023-01-01",
                                "end_date": "2024-12-31"
                            }
                        ]
                    }
                ]
            }
        ]
    }
]

td_04 = [
    {
        "Description": "Testcase 04 (Geen ZVL functie + Geen AOK + Geen ZVL functie - 1 + Wel AOK - 1 + Wel Kwal. Niv. + Wel Kwal. Niv. - 1)",
        "Amount": 10,
        "Human": [
            {
                "InhuurOvereenkomst": [
                    {
 
                        "function": [
                            {
                                "label": "Inhuur Medewerker niet ZVL, Kwal. Niv. 4 (PD)",
                                "caregiving_role": False,
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31",
                                "qualification_level": [
                                    {
                                        "qualification_level_value": "Kwalificatieniveau 4",
                                        "start_date": "2024-01-01",
                                        "end_date": "2024-12-31"
                                    }
                                ]
                            }
                        ],
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",
                        "size": [
                            {
                                "unit": 36,
                                "unit_of_measure": "Uren_per_week_unit",
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31"
                            }
                        ]
                    }
                ]
            },
            {
                "ArbeidsOvereenkomst":[
                    {
 
                        "function": [
                            {
                                "label": "PIL niet ZVL, Kwal. Niv 3 (PD -1)",
                                "caregiving_role": False,
                                "start_date": "2023-01-01",
                                "end_date": "2023-12-31",
                                "qualification_level": [
                                    {
                                        "qualification_level_value": "Kwalificatieniveau 3",
                                        "start_date": "2023-01-01",
                                        "end_date": "2023-12-31"
                                    }
                                ]
                            }
                        ],
                        "start_date": "2023-01-01",
                        "end_date": "2023-12-31",
                        "size": [
                            {
                                "unit": 36,
                                "unit_of_measure": "Uren_per_week_unit",
                                "start_date": "2023-01-01",
                                "end_date": "2023-12-31"
                            }
                        ]
                    }
                ]
            }
        ]
    }
]

td_04_a = [
    {
        "Description": "Testcase 04a (Geen ZVL functie + Geen AOK + Wel ZVL functie - 1 + Wel AOK - 1 + Wel Kwal. Niv. + Wel Kwal. Niv. - 1)",
        "Amount": 10,
        "Human": [
            {
                "InhuurOvereenkomst": [
                    {
 
                        "function": [
                            {
                                "label": "Inhuur Medewerker niet ZVL, Kwal. Niv. 4 (PD)",
                                "caregiving_role": False,
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31",
                                "qualification_level": [
                                    {
                                        "qualification_level_value": "Kwalificatieniveau 4",
                                        "start_date": "2024-01-01",
                                        "end_date": "2024-12-31"
                                    }
                                ]
                            }
                        ],
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",
                        "size": [
                            {
                                "unit": 36,
                                "unit_of_measure": "Uren_per_week_unit",
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31"
                            }
                        ]
                    }
                ]
            },
            {
                "ArbeidsOvereenkomst":[
                    {
 
                        "function": [
                            {
                                "label": "PIL niet ZVL, Kwal. Niv 3 (PD -1)",
                                "caregiving_role": True,
                                "start_date": "2023-01-01",
                                "end_date": "2023-12-31",
                                "qualification_level": [
                                    {
                                        "qualification_level_value": "Kwalificatieniveau 3",
                                        "start_date": "2023-01-01",
                                        "end_date": "2023-12-31"
                                    }
                                ]
                            }
                        ],
                        "start_date": "2023-01-01",
                        "end_date": "2023-12-31",
                        "size": [
                            {
                                "unit": 36,
                                "unit_of_measure": "Uren_per_week_unit",
                                "start_date": "2023-01-01",
                                "end_date": "2023-12-31"
                            }
                        ]
                    }
                ]
            }
        ]
    }
]

td_05 = [
    {
        "Description": "Testcase 05 (Geen ZVL functie + Wel AOK + Geen ZVL functie - 1 + Wel AOK - 1 + Wel Kwal. Niv. + Wel Kwal. Niv. - 1)",
        "Amount": 10,
        "Human": [
            {
                "ArbeidsOvereenkomst": [
                    {
 
                        "function": [
                            {
                                "label": "PIL niet ZVL, Kwal. Niv. 4 (PD)",
                                "caregiving_role": False,
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31",
                                "qualification_level": [
                                    {
                                        "qualification_level_value": "Kwalificatieniveau 4",
                                        "start_date": "2024-01-01",
                                        "end_date": "2024-12-31"
                                    }
                                ]
                            }
                        ],
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",
                        "size": [
                            {
                                "unit": 36,
                                "unit_of_measure": "Uren_per_week_unit",
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31"
                            }
                        ]
                    },
                    {
                        "function": [
                            {
                                "label": "PIL niet ZVL, Kwal. Niv 3 (PD -1)",
                                "caregiving_role": False,
                                "start_date": "2023-01-01",
                                "end_date": "2023-12-31",
                                "qualification_level": [
                                    {
                                        "qualification_level_value": "Kwalificatieniveau 3",
                                        "start_date": "2023-01-01",
                                        "end_date": "2023-12-31"
                                    }
                                ]
                            }
                        ],
                        "start_date": "2023-01-01",
                        "end_date": "2023-12-31",
                        "size": [
                            {
                                "unit": 36,
                                "unit_of_measure": "Uren_per_week_unit",
                                "start_date": "2023-01-01",
                                "end_date": "2023-12-31"
                            }
                        ]
                    }
                ]
            }
        ]
    }
]

td_05_a = [
    {
        "Description": "Testcase 05a (Geen ZVL functie + Wel AOK + Wel ZVL functie - 1 + Wel AOK - 1 + Wel Kwal. Niv. + Wel Kwal. Niv. - 1)",
        "Amount": 10,
        "Human": [
            {
                "ArbeidsOvereenkomst": [
                    {
                        "function": [
                            {
                                "label": "PIL niet ZVL, Kwal. Niv. 4 (PD)",
                                "caregiving_role": False,
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31",
                                "qualification_level": [
                                    {
                                        "qualification_level_value": "Kwalificatieniveau 4",
                                        "start_date": "2024-01-01",
                                        "end_date": "2024-12-31"
                                    }
                                ]
                            }
                        ],
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",
                        "size": [
                            {
                                "unit": 36,
                                "unit_of_measure": "Uren_per_week_unit",
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31"
                            }
                        ]
                    },
                    #AOK 2 (PD-1)
                    {
                        "function": [
                            {
                                "label": "PIL wel ZVL, Kwal. Niv 3 (PD -1)",
                                "caregiving_role": True,
                                "start_date": "2023-01-01",
                                "end_date": "2023-12-31",
                                "qualification_level": [
                                    {
                                        "qualification_level_value": "Kwalificatieniveau 3",
                                        "start_date": "2023-01-01",
                                        "end_date": "2023-12-31"
                                    }
                                ]
                            }
                        ],
                        "start_date": "2023-01-01",
                        "end_date": "2023-12-31",
                        "size": [
                            {
                                "unit": 36,
                                "unit_of_measure": "Uren_per_week_unit",
                                "start_date": "2023-01-01",
                                "end_date": "2023-12-31"
                            }
                        ]
                    }
                ]
            }
        ]
    }
]

td_06 = [
    {
        "Description": "Testcase 06 (Wel ZVL functie + Wel AOK + Wel ZVL functie - 1 + Wel AOK - 1 + Wel Kwal. Niv. + Wel Kwal. Niv. - 1)",
        "Amount": 10, # Indicator score = 52.6% (10/10+9)*100
        "Human": [
            {
                "ArbeidsOvereenkomst": [
                    {
                        "function": [
                            {
                                "label": "PIL wel ZVL, Kwal. Niv. 4 (PD)",
                                "caregiving_role": True,
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31",
                                "qualification_level": [
                                    {
                                        "qualification_level_value": "Kwalificatieniveau 4",
                                        "start_date": "2024-01-01",
                                        "end_date": "2024-12-31"
                                    }
                                ]
                            }
                        ],
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",
                        "size": [
                            {
                                "unit": 36,
                                "unit_of_measure": "Uren_per_week_unit",
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31"
                            }
                        ]
                    },
                    #AOK 2 (PD-1)
                    {
                        "function": [
                            {
                                "label": "PIL wel ZVL, Kwal. Niv 3 (PD -1)",
                                "caregiving_role": True,
                                "start_date": "2023-01-01",
                                "end_date": "2023-12-31",
                                "qualification_level": [
                                    {
                                        "qualification_level_value": "Kwalificatieniveau 3",
                                        "start_date": "2023-01-01",
                                        "end_date": "2023-12-31"
                                    }
                                ]
                            }
                        ],
                        "start_date": "2023-01-01",
                        "end_date": "2023-12-31",
                        "size": [
                            {
                                "unit": 36,
                                "unit_of_measure": "Uren_per_week_unit",
                                "start_date": "2023-01-01",
                                "end_date": "2023-12-31"
                            }
                        ]
                    }
                ]
            }
        ]
    }
]

td_06_a = [
    {
        "Description": "Testcase 06a (Wel ZVL functie + Wel AOK + Wel ZVL functie - 1 + Wel AOK - 1 + Wel Kwal. Niv. + Wel Kwal. Niv. - 1)",
        "Amount": 10, # Indicator score = 52.6% (10/10+9)*100
        "Human": [
            {
                "ArbeidsOvereenkomst": [
                    {
                        "function": [
                            {
                                "label": "PIL wel ZVL, Kwal. Niv. Behandelaar (PD)",
                                "caregiving_role": True,
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31",
                                "qualification_level": [
                                    {
                                        "qualification_level_value": "Kwalificatieniveau Behandelaar",
                                        "start_date": "2024-01-01",
                                        "end_date": "2024-12-31"
                                    }
                                ]
                            }
                        ],
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",
                        "size": [
                            {
                                "unit": 36,
                                "unit_of_measure": "Uren_per_week_unit",
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31"
                            }
                        ]
                    },
                    #AOK 2 (PD-1)
                    {
                        "function": [
                            {
                                "label": "PIL wel ZVL, Kwal. Niv Leerling (PD -1)",
                                "caregiving_role": True,
                                "start_date": "2023-01-01",
                                "end_date": "2023-12-31",
                                "qualification_level": [
                                    {
                                        "qualification_level_value": "Kwalificatieniveau Leerling",
                                        "start_date": "2023-01-01",
                                        "end_date": "2023-12-31"
                                    }
                                ]
                            }
                        ],
                        "start_date": "2023-01-01",
                        "end_date": "2023-12-31",
                        "size": [
                            {
                                "unit": 36,
                                "unit_of_measure": "Uren_per_week_unit",
                                "start_date": "2023-01-01",
                                "end_date": "2023-12-31"
                            }
                        ]
                    }
                ]
            }
        ]
    }
]

td_07 = [
    {
        "Description": "Testcase 07 (Wel ZVL functie + Wel AOK + Wel ZVL functie - 1 + Wel AOK - 1 + Wel Kwal. Niv. + Wel Kwal. Niv. - 1)",
        "Amount": 10, # Indicator score = 0
        "Human": [
            {
                "ArbeidsOvereenkomst": [
                    {
                        "function": [
                            {
                                "label": "PIL wel ZVL, Kwal. Niv. 4 (PD)",
                                "caregiving_role": True,
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31",
                                "qualification_level": [
                                    {
                                        "qualification_level_value": "Kwalificatieniveau 4",
                                        "start_date": "2024-01-01",
                                        "end_date": "2024-12-31"
                                    }
                                ]
                            }
                        ],
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",
                        "size": [
                            {
                                "unit": 36,
                                "unit_of_measure": "Uren_per_week_unit",
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31"
                            }
                        ]
                    },
                    #AOK 2 (PD-1)
                    {
                        "function": [
                            {
                                "label": "PIL wel ZVL, Kwal. Niv 4 (PD -1)",
                                "caregiving_role": True,
                                "start_date": "2023-01-01",
                                "end_date": "2023-12-31",
                                "qualification_level": [
                                    {
                                        "qualification_level_value": "Kwalificatieniveau 4",
                                        "start_date": "2023-01-01",
                                        "end_date": "2023-12-31"
                                    }
                                ]
                            }
                        ],
                        "start_date": "2023-01-01",
                        "end_date": "2023-12-31",
                        "size": [
                            {
                                "unit": 36,
                                "unit_of_measure": "Uren_per_week_unit",
                                "start_date": "2023-01-01",
                                "end_date": "2023-12-31"
                            }
                        ]
                    }
                ]
            }
        ]
    }
]

td_08 = [
    {
        "Description": "Testcase 08 (Wel ZVL functie + Wel AOK + Wel ZVL functie - 1 + Wel AOK - 1 + Geen Kwal. Niv. + Geen Kwal. Niv. - 1)",
        "Amount": 10, # Indicator score = 0
        "Human": [
            {
                "ArbeidsOvereenkomst": [
                    {
                        "function": [
                            {
                                "label": "PIL wel ZVL, Geen Kwal. Niv. (PD)",
                                "caregiving_role": True,
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31",
                            }
                        ],
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",
                        "size": [
                            {
                                "unit": 36,
                                "unit_of_measure": "Uren_per_week_unit",
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31"
                            }
                        ]
                    },
                    #AOK 2 (PD-1)
                    {
                        "function": [
                            {
                                "label": "PIL wel ZVL, Geen Kwal. Niv. (PD -1)",
                                "caregiving_role": True,
                                "start_date": "2023-01-01",
                                "end_date": "2023-12-31",
                            }
                        ],
                        "start_date": "2023-01-01",
                        "end_date": "2023-12-31",
                        "size": [
                            {
                                "unit": 36,
                                "unit_of_measure": "Uren_per_week_unit",
                                "start_date": "2023-01-01",
                                "end_date": "2023-12-31"
                            }
                        ]
                    }
                ]
            }
        ]
    }
]

td_09 = [
    {
        "Description": "Testcase 09 (Wel ZVL functie + Wel AOK + Geen ZVL functie - 1 + Geen AOK - 1 + Geen Kwal. Niv. + Geen Kwal. Niv. - 1)",
        "Amount": 10, # Indicator score = 0
        "Human": [
            {
                "ArbeidsOvereenkomst": [
                    {
                        "function": [
                            {
                                "label": "PIL wel ZVL, Geen Kwal. Niv. (PD)",
                                "caregiving_role": True,
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31",
                            }
                        ],
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",
                        "size": [
                            {
                                "unit": 36,
                                "unit_of_measure": "Uren_per_week_unit",
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31"
                            }
                        ]
                    }
                ]
            }
        ]
    }
]

td_09_a = [
    {
        "Description": "Testcase 09a (Wel ZVL functie + Wel AOK + Geen ZVL functie - 1 + Geen AOK - 1 + Geen Kwal. Niv. + Geen Kwal. Niv. - 1)",
        "Amount": 10, # Indicator score = 0
        "Human": [
            {
                "ArbeidsOvereenkomst": [
                    {
                        "function": [
                            {
                                "label": "PIL wel ZVL, Geen Kwal. Niv. (PD)",
                                "caregiving_role": True,
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31",
                            }
                        ],
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",
                        "size": [
                            {
                                "unit": 36,
                                "unit_of_measure": "Uren_per_week_unit",
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31"
                            }
                        ]
                    }
                ]
            },
            {
                "InhuurOvereenkomst": [
                    {
                        "function": [
                            {
                                "label": "Inhuur Medewerker niet ZVL",
                                "caregiving_role": False,
                                "start_date": "2023-01-01",
                                "end_date": "2023-12-31",
                            }
                        ],
                        "start_date": "2023-01-01",
                        "end_date": "2023-12-31",
                        "size": [
                            {
                                "unit": 36,
                                "unit_of_measure": "Uren_per_week_unit",
                                "start_date": "2023-01-01",
                                "end_date": "2023-12-31"
                            }
                        ]
                    }
                ]
            }
        ]
    }
]

td_09_b = [
    {
        "Description": "Testcase 09b (Wel ZVL functie + Wel AOK + Wel ZVL functie - 1 + Geen AOK - 1 + Geen Kwal. Niv. + Geen Kwal. Niv. - 1)",
        "Amount": 10, # Indicator score = 0
        "Human": [
            {
                "ArbeidsOvereenkomst": [
                    {
                        "function": [
                            {
                                "label": "PIL wel ZVL, Geen Kwal. Niv. (PD)",
                                "caregiving_role": True,
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31",
                            }
                        ],
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",
                        "size": [
                            {
                                "unit": 36,
                                "unit_of_measure": "Uren_per_week_unit",
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31"
                            }
                        ]
                    }
                ]
            },
            {
                "InhuurOvereenkomst": [
                    {
                        "function": [
                            {
                                "label": "Inhuur Medewerker wel ZVL",
                                "caregiving_role": True,
                                "start_date": "2023-01-01",
                                "end_date": "2023-12-31",
                            }
                        ],
                        "start_date": "2023-01-01",
                        "end_date": "2023-12-31",
                        "size": [
                            {
                                "unit": 36,
                                "unit_of_measure": "Uren_per_week_unit",
                                "start_date": "2023-01-01",
                                "end_date": "2023-12-31"
                            }
                        ]
                    }
                ]
            }
        ]
    }
]

td_10 = [
    {
        "Description": "Testcase 10 (Wel ZVL functie + Geen AOK + Geen ZVL functie - 1 + Geen AOK - 1 + Geen Kwal. Niv. + Geen Kwal. Niv. - 1)",
        "Amount": 10,
        "Human": [
            {
                "InhuurOvereenkomst": [
                    {
                        "function": [
                            {
                                "label": "Inhuur Medewerker niet ZVL",
                                "caregiving_role": True,
                                "start_date": "2023-01-01",
                                "end_date": "2024-12-31",
                            }
                        ],
                        "start_date": "2023-01-31",
                        "end_date": "2024-12-31",
                        "size": [
                            {
                                "unit": 36,
                                "unit_of_measure": "Uren_per_week_unit",
                                "start_date": "2023-01-31",
                                "end_date": "2024-12-31"
                            }
                        ]
                    }
                ]
            }
        ]
    }
]

td_10_a = [
    {
        "Description": "Testcase 10a (Wel ZVL functie + Geen AOK + Geen ZVL functie - 1 + Geen AOK - 1 + Geen Kwal. Niv. + Geen Kwal. Niv. - 1)",
        "Amount": 10,
        "Human": [
            {
                "InhuurOvereenkomst": [
                    {
                        "function": [
                            {
                                "label": "Inhuur Medewerker wel ZVL",
                                "caregiving_role": True,
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31",
                            }
                        ],
                        "start_date": "2024-01-31",
                        "end_date": "2024-12-31",
                        "size": [
                            {
                                "unit": 36,
                                "unit_of_measure": "Uren_per_week_unit",
                                "start_date": "2024-01-31",
                                "end_date": "2024-12-31"
                            }
                        ]
                    },
                    {
                        "function": [
                            {
                                "label": "Inhuur Medewerker niet ZVL",
                                "caregiving_role": False,
                                "start_date": "2023-01-01",
                                "end_date": "2023-12-31",
                            }
                        ],
                        "start_date": "2023-01-01",
                        "end_date": "2023-12-31",
                        "size": [
                            {
                                "unit": 36,
                                "unit_of_measure": "Uren_per_week_unit",
                                "start_date": "2023-01-01",
                                "end_date": "2023-12-31"
                            }
                        ]
                    }
                ]
            }
        ]
    }
]

td_10_b = [
    {
        "Description": "Testcase 10b (Wel ZVL functie + Geen AOK + Wel ZVL functie - 1 + Geen AOK - 1 + Geen Kwal. Niv. + Geen Kwal. Niv. - 1)",
        "Amount": 10,
        "Human": [
            {
                "InhuurOvereenkomst": [
                    {
                        "function": [
                            {
                                "label": "Inhuur Medewerker wel ZVL",
                                "caregiving_role": True,
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31",
                            }
                        ],
                        "start_date": "2024-01-31",
                        "end_date": "2024-12-31",
                        "size": [
                            {
                                "unit": 36,
                                "unit_of_measure": "Uren_per_week_unit",
                                "start_date": "2024-01-31",
                                "end_date": "2024-12-31"
                            }
                        ]
                    },
                    {
                        "function": [
                            {
                                "label": "Inhuur Medewerker wel ZVL",
                                "caregiving_role": True,
                                "start_date": "2023-01-01",
                                "end_date": "2023-12-31",
                            }
                        ],
                        "start_date": "2023-01-01",
                        "end_date": "2023-12-31",
                        "size": [
                            {
                                "unit": 36,
                                "unit_of_measure": "Uren_per_week_unit",
                                "start_date": "2023-01-01",
                                "end_date": "2023-12-31"
                            }
                        ]
                    }
                ]
            }
        ]
    }
]

# Static tests

def test_if_headers_are_correct_for_query_2_4_3(db_config):
    """ Test of de juiste header terugkomt in het resultaat
        ODB Personeelssamenstelling 2.4.3 Percentage doorstroom kwalificatieniveau
    """

    # Setup of the test
    test = QueryTest(db_config)

    # Configuration and execution
    test.set_query('Gevalideerde_vragen_technisch/personeelssamenstelling/Indicator 2.4.3.rq')

    # Assertions
    test.verify_header_present('indicator')
    test.verify_header_present('teller')
    test.verify_header_present('noemer')

def test_if_number_of_rows_returned_is_correct_for_query_2_4_3(db_config):
    """ Test of het aantal rijen correct wordt teruggegeven
        ODB Personeelssamenstelling 2.4.3 Percentage doorstroom kwalificatieniveau
    """

    # Setup of the test
    test = QueryTest(db_config)

    # Configuration and execution
    test.set_query('Gevalideerde_vragen_technisch/personeelssamenstelling/Indicator 2.4.3.rq')

    # Assertions
    test.verify_row_count(1)

def test_if_indicator_has_correct_value_for_query_2_4_3(db_config):
    """ Test of de indicator de juiste waarde heeft
        ODB Personeelssamenstelling 2.4.3 Percentage doorstroom kwalificatieniveau
    """

    # Setup of the test
    test = QueryTest(db_config)

    # Configuration and execution
    test.set_query('Gevalideerde_vragen_technisch/personeelssamenstelling/Indicator 2.4.3.rq')

    # Assertions
    test.verify_value_in_list("indicator", {"0", "0.0"})

def test_if_dates_can_change_2_4_3(db_config):
    """ Test of gewijzigde datum daadwerkelijk een ander resultaat oplevert
        ODB Personeelssamenstelling 2.4.3 Percentage doorstroom kwalificatieniveau
    """

    # Setup of the test
    test = QueryTest(db_config)

    # Configuration and execution
    test.set_query('Gevalideerde_vragen_technisch/personeelssamenstelling/Indicator 2.4.3.rq')

    test.change_reference_date_to("2023-12-31", "2024-12-31")

    # Assertions
    test.verify_value_in_list("indicator",{"0","0.0"})

# Tests using Generated Data

# Testcase 01

def test_if_value_returned_is_correct_for_query_2_4_3_01(db_config):
    """ Testcase 01 (Geen ZVL functie + Geen AOK + Geen ZVL functie - 1 + Geen AOK - 1 + Geen Kwal. Niv. + Geen Kwal. Niv. - 1)
        ODB personeelssamenstelling 2.4.3. Percentage doorstroom kwalificatieniveau
    """

    # Load defined test data
    dg = DataGenerator(db_config, td_01)

    try:
        # Start a test
        test = QueryTest(db_config)

        # Set the query to be tested
        test.set_query('Gevalideerde_vragen_technisch/personeelssamenstelling/Indicator 2.4.3.rq')

        # Change the peildatum of the query
        test.change_reference_date_to("2023-12-31", "2024-12-31")

        # Verify actual result of the query
        test.verify_value_in_list("indicator", {"0", "0.0"})

    finally:
        # Delete previously loaded data
        dg.delete_graph_data()

# Testcase 02

def test_if_value_returned_is_correct_for_query_2_4_3_02(db_config):
    """ Testcase 02 (Geen ZVL functie + Geen AOK + Geen ZVL functie - 1 + Geen AOK - 1 + Wel Kwal. Niv. + Geen Kwal. Niv. - 1)
        ODB personeelssamenstelling 2.4.3. Percentage doorstroom kwalificatieniveau
    """

    # Load defined test data
    dg = DataGenerator(db_config, td_02)

    try:
        # Start a test
        test = QueryTest(db_config)

        # Set the query to be tested
        test.set_query('Gevalideerde_vragen_technisch/personeelssamenstelling/Indicator 2.4.3.rq')

        # Change the peildatum of the query
        test.change_reference_date_to("2023-12-31", "2024-12-31")

        # Verify actual result of the query
        test.verify_value_in_list("indicator", {"0", "0.0"})

    finally:
        # Delete previously loaded data
        dg.delete_graph_data()

# Testcase 02a

def test_if_value_returned_is_correct_for_query_2_4_3_02_a(db_config):
    """ Testcase 02a (Geen ZVL functie + Geen AOK + Geen ZVL functie - 1 + Geen AOK - 1 + Geen Kwal. Niv. + Wel Kwal. Niv. - 1)
        ODB personeelssamenstelling 2.4.3. Percentage doorstroom kwalificatieniveau
    """

    # Load defined test data
    dg = DataGenerator(db_config, td_02_a)

    try:
        # Start a test
        test = QueryTest(db_config)

        # Set the query to be tested
        test.set_query('Gevalideerde_vragen_technisch/personeelssamenstelling/Indicator 2.4.3.rq')

        # Change the peildatum of the query
        test.change_reference_date_to("2023-12-31", "2024-12-31")

        # Verify actual result of the query
        test.verify_value_in_list("indicator", {"0", "0.0"})

    finally:
        # Delete previously loaded data
        dg.delete_graph_data()

# Testcase 03

def test_if_value_returned_is_correct_for_query_2_4_3_03(db_config):
    """ Testcase 03 (Geen ZVL functie + Geen AOK + Geen ZVL functie - 1 + Geen AOK - 1 + Wel Kwal. Niv. + Wel Kwal. Niv. - 1)
        ODB personeelssamenstelling 2.4.3. Percentage doorstroom kwalificatieniveau
    """

    # Load defined test data
    dg = DataGenerator(db_config, td_03)

    try:
        # Start a test
        test = QueryTest(db_config)

        # Set the query to be tested
        test.set_query('Gevalideerde_vragen_technisch/personeelssamenstelling/Indicator 2.4.3.rq')

        # Change the peildatum of the query
        test.change_reference_date_to("2023-12-31", "2024-12-31")

        # Verify actual result of the query
        test.verify_value_in_list("indicator", {"0", "0.0"})

    finally:
        # Delete previously loaded data
        dg.delete_graph_data()

# Testcase 04

def test_if_value_returned_is_correct_for_query_2_4_3_04(db_config):
    """ Testcase 04 (Geen ZVL functie + Geen AOK + Geen ZVL functie - 1 + Wel AOK - 1 + Wel Kwal. Niv. + Wel Kwal. Niv. - 1)
        ODB personeelssamenstelling 2.4.3. Percentage doorstroom kwalificatieniveau
    """

    # Load defined test data
    dg = DataGenerator(db_config, td_04)

    try:
        # Start a test
        test = QueryTest(db_config)

        # Set the query to be tested
        test.set_query('Gevalideerde_vragen_technisch/personeelssamenstelling/Indicator 2.4.3.rq')

        # Change the peildatum of the query
        test.change_reference_date_to("2023-12-31", "2024-12-31")

        # Verify actual result of the query
        test.verify_value_in_list("indicator", {"0", "0.0"})

    finally:
        # Delete previously loaded data
        dg.delete_graph_data()

# Testcase 04a

def test_if_value_returned_is_correct_for_query_2_4_3_04_a(db_config):
    """ Testcase 04a (Geen ZVL functie + Geen AOK + Wel ZVL functie - 1 + Wel AOK - 1 + Wel Kwal. Niv. + Wel Kwal. Niv. - 1)
        ODB personeelssamenstelling 2.4.3. Percentage doorstroom kwalificatieniveau
    """

    # Load defined test data
    dg = DataGenerator(db_config, td_04_a)

    try:
        # Start a test
        test = QueryTest(db_config)

        # Set the query to be tested
        test.set_query('Gevalideerde_vragen_technisch/personeelssamenstelling/Indicator 2.4.3.rq')

        # Change the peildatum of the query
        test.change_reference_date_to("2023-12-31", "2024-12-31")

        # Verify actual result of the query
        test.verify_value_in_list("indicator", {"0", "0.0"})

    finally:
        # Delete previously loaded data
        dg.delete_graph_data()

# Testcase 05

def test_if_value_returned_is_correct_for_query_2_4_3_05(db_config):
    """ Testcase 05 (Geen ZVL functie + Wel AOK + Geen ZVL functie - 1 + Wel AOK - 1 + Wel Kwal. Niv. + Wel Kwal. Niv. - 1)
        ODB personeelssamenstelling 2.4.3. Percentage doorstroom kwalificatieniveau
    """

    # Load defined test data
    dg = DataGenerator(db_config, td_04)

    try:
        # Start a test
        test = QueryTest(db_config)

        # Set the query to be tested
        test.set_query('Gevalideerde_vragen_technisch/personeelssamenstelling/Indicator 2.4.3.rq')

        # Change the peildatum of the query
        test.change_reference_date_to("2023-12-31", "2024-12-31")

        # Verify actual result of the query
        test.verify_value_in_list("indicator", {"0", "0.0"})

    finally:
        # Delete previously loaded data
        dg.delete_graph_data()

# Testcase 05a

def test_if_value_returned_is_correct_for_query_2_4_3_05_a(db_config):
    """ Testcase 05a (Geen ZVL functie + Wel AOK + Wel ZVL functie - 1 + Wel AOK - 1 + Wel Kwal. Niv. + Wel Kwal. Niv. - 1)
        ODB personeelssamenstelling 2.4.3. Percentage doorstroom kwalificatieniveau
    """

    # Load defined test data
    dg = DataGenerator(db_config, td_05_a)

    try:
        # Start a test
        test = QueryTest(db_config)

        # Set the query to be tested
        test.set_query('Gevalideerde_vragen_technisch/personeelssamenstelling/Indicator 2.4.3.rq')

        # Change the peildatum of the query
        test.change_reference_date_to("2023-12-31", "2024-12-31")

        # Verify actual result of the query
        test.verify_value_in_list("indicator", {"0", "0.0"})

    finally:
        # Delete previously loaded data
        dg.delete_graph_data()

# Testcase 06
def test_if_value_returned_is_correct_for_query_2_4_3_06(db_config):
    """ Testcase 06 (Wel ZVL functie + Wel AOK + Geen ZVL functie - 1 + Wel AOK - 1 + Wel Kwal. Niv. + Wel Kwal. Niv. - 1)
        ODB personeelssamenstelling 2.4.3. Percentage doorstroom kwalificatieniveau
    """

    # Load defined test data
    dg = DataGenerator(db_config, td_06)

    try:
        # Start a test
        test = QueryTest(db_config)

        # Set the query to be tested
        test.set_query('Gevalideerde_vragen_technisch/personeelssamenstelling/Indicator 2.4.3.rq')

        # Change the peildatum of the query
        test.change_reference_date_to("2023-12-31", "2024-12-31")

        # Verify actual result of the query
        test.verify_value_in_list("indicator", {"52.631578947368421052631579", "52.631578947368421052631579"})

    finally:
        # Delete previously loaded data
        dg.delete_graph_data()

# Testcase 06a

def test_if_value_returned_is_correct_for_query_2_4_3_06_a(db_config):
    """ Testcase 06a (Wel ZVL functie + Wel AOK + Wel ZVL functie - 1 + Wel AOK - 1 + Wel Kwal. Niv. + Wel Kwal. Niv. - 1)
        ODB personeelssamenstelling 2.4.3. Percentage doorstroom kwalificatieniveau
    """

    # Load defined test data
    dg = DataGenerator(db_config, td_06_a)

    try:
        # Start a test
        test = QueryTest(db_config)

        # Set the query to be tested
        test.set_query('Gevalideerde_vragen_technisch/personeelssamenstelling/Indicator 2.4.3.rq')

        # Change the peildatum of the query
        test.change_reference_date_to("2023-12-31", "2024-12-31")

        # Verify actual result of the query
        test.verify_value_in_list("indicator", {"52.631578947368421052631579", "52.631578947368421052631579"})

    finally:
        # Delete previously loaded data
        dg.delete_graph_data()

# Testcase 07
def test_if_value_returned_is_correct_for_query_2_4_3_07(db_config):
    """ Testcase 07 (Wel ZVL functie + Wel AOK + Geen ZVL functie - 1 + Wel AOK - 1 + Wel Kwal. Niv. + Wel Kwal. Niv. - 1) Kwalificatie niveaus gelijk aan elkaar
        ODB personeelssamenstelling 2.4.3. Percentage doorstroom kwalificatieniveau
    """

    # Load defined test data
    dg = DataGenerator(db_config, td_07)

    try:
        # Start a test
        test = QueryTest(db_config)

        # Set the query to be tested
        test.set_query('Gevalideerde_vragen_technisch/personeelssamenstelling/Indicator 2.4.3.rq')

        # Change the peildatum of the query
        test.change_reference_date_to("2023-12-31", "2024-12-31")

        # Verify actual result of the query
        test.verify_value_in_list("indicator", {"0", "0.0"})

    finally:
        # Delete previously loaded data
        dg.delete_graph_data()

# Testcase 08
def test_if_value_returned_is_correct_for_query_2_4_3_08(db_config):
    """ Testcase 08 (Wel ZVL functie + Wel AOK + Geen ZVL functie - 1 + Wel AOK - 1 + Geen Kwal. Niv. + Geen Kwal. Niv. - 1) 
        ODB personeelssamenstelling 2.4.3. Percentage doorstroom kwalificatieniveau
    """

    # Load defined test data
    dg = DataGenerator(db_config, td_08)

    try:
        # Start a test
        test = QueryTest(db_config)

        # Set the query to be tested
        test.set_query('Gevalideerde_vragen_technisch/personeelssamenstelling/Indicator 2.4.3.rq')

        # Change the peildatum of the query
        test.change_reference_date_to("2023-12-31", "2024-12-31")

        # Verify actual result of the query
        test.verify_value_in_list("indicator", {"0", "0.0"})

    finally:
        # Delete previously loaded data
        dg.delete_graph_data()

# Testcase 09
def test_if_value_returned_is_correct_for_query_2_4_3_09(db_config):
    """ Testcase 09 (Wel ZVL functie + Wel AOK + Geen ZVL functie - 1 + Geen AOK - 1 + Geen Kwal. Niv. + Geen Kwal. Niv. - 1) 
        ODB personeelssamenstelling 2.4.3. Percentage doorstroom kwalificatieniveau
    """

    # Load defined test data
    dg = DataGenerator(db_config, td_09)

    try:
        # Start a test
        test = QueryTest(db_config)

        # Set the query to be tested
        test.set_query('Gevalideerde_vragen_technisch/personeelssamenstelling/Indicator 2.4.3.rq')

        # Change the peildatum of the query
        test.change_reference_date_to("2023-12-31", "2024-12-31")

        # Verify actual result of the query
        test.verify_value_in_list("indicator", {"0", "0.0"})

    finally:
        # Delete previously loaded data
        dg.delete_graph_data()

# Testcase 09_a
def test_if_value_returned_is_correct_for_query_2_4_3_09_a(db_config):
    """ Testcase 09a (Wel ZVL functie + Wel AOK + Geen ZVL functie - 1 + Geen AOK - 1 + Geen Kwal. Niv. + Geen Kwal. Niv. - 1) (InhuurOvereenkomst op PD -1)
        ODB personeelssamenstelling 2.4.3. Percentage doorstroom kwalificatieniveau
    """

    # Load defined test data
    dg = DataGenerator(db_config, td_09_a)

    try:
        # Start a test
        test = QueryTest(db_config)

        # Set the query to be tested
        test.set_query('Gevalideerde_vragen_technisch/personeelssamenstelling/Indicator 2.4.3.rq')

        # Change the peildatum of the query
        test.change_reference_date_to("2023-12-31", "2024-12-31")

        # Verify actual result of the query
        test.verify_value_in_list("indicator", {"0", "0.0"})

    finally:
        # Delete previously loaded data
        dg.delete_graph_data()

# Testcase 09_b
def test_if_value_returned_is_correct_for_query_2_4_3_09_b(db_config):
    """ Testcase 09b (Wel ZVL functie + Wel AOK + Wel ZVL functie - 1 + Geen AOK - 1 + Geen Kwal. Niv. + Geen Kwal. Niv. - 1) (InhuurOvereenkomst op PD -1)
        ODB personeelssamenstelling 2.4.3. Percentage doorstroom kwalificatieniveau
    """

    # Load defined test data
    dg = DataGenerator(db_config, td_09_b)

    try:
        # Start a test
        test = QueryTest(db_config)

        # Set the query to be tested
        test.set_query('Gevalideerde_vragen_technisch/personeelssamenstelling/Indicator 2.4.3.rq')

        # Change the peildatum of the query
        test.change_reference_date_to("2023-12-31", "2024-12-31")

        # Verify actual result of the query
        test.verify_value_in_list("indicator", {"0", "0.0"})

    finally:
        # Delete previously loaded data
        dg.delete_graph_data()

# Testcase 10
def test_if_value_returned_is_correct_for_query_2_4_3_10(db_config):
    """ Testcase 10 (Wel ZVL functie + Geen AOK + Geen ZVL functie - 1 + Geen AOK - 1 + Geen Kwal. Niv. + Geen Kwal. Niv. - 1) 
        ODB personeelssamenstelling 2.4.3. Percentage doorstroom kwalificatieniveau
    """

    # Load defined test data
    dg = DataGenerator(db_config, td_10)

    try:
        # Start a test
        test = QueryTest(db_config)

        # Set the query to be tested
        test.set_query('Gevalideerde_vragen_technisch/personeelssamenstelling/Indicator 2.4.3.rq')

        # Change the peildatum of the query
        test.change_reference_date_to("2023-12-31", "2024-12-31")

        # Verify actual result of the query
        test.verify_value_in_list("indicator", {"0", "0.0"})

    finally:
        # Delete previously loaded data
        dg.delete_graph_data()

# Testcase 10_a
def test_if_value_returned_is_correct_for_query_2_4_3_10_a(db_config):
    """ Testcase 10a (Wel ZVL functie + Geen AOK + Geen ZVL functie - 1 + Geen AOK - 1 + Geen Kwal. Niv. + Geen Kwal. Niv. - 1
        ODB personeelssamenstelling 2.4.3. Percentage doorstroom kwalificatieniveau
    """

    # Load defined test data
    dg = DataGenerator(db_config, td_10_a)

    try:
        # Start a test
        test = QueryTest(db_config)

        # Set the query to be tested
        test.set_query('Gevalideerde_vragen_technisch/personeelssamenstelling/Indicator 2.4.3.rq')

        # Change the peildatum of the query
        test.change_reference_date_to("2023-12-31", "2024-12-31")

        # Verify actual result of the query
        test.verify_value_in_list("indicator", {"0", "0.0"})

    finally:
        # Delete previously loaded data
        dg.delete_graph_data()

# Testcase 10_b
def test_if_value_returned_is_correct_for_query_2_4_3_10_b(db_config):
    """ Testcase 10b (Wel ZVL functie + Geen AOK + Wel ZVL functie - 1 + Geen AOK - 1 + Geen Kwal. Niv. + Geen Kwal. Niv. - 1)
        ODB personeelssamenstelling 2.4.3. Percentage doorstroom kwalificatieniveau
    """

    # Load defined test data
    dg = DataGenerator(db_config, td_10_b)

    try:
        # Start a test
        test = QueryTest(db_config)

        # Set the query to be tested
        test.set_query('Gevalideerde_vragen_technisch/personeelssamenstelling/Indicator 2.4.3.rq')

        # Change the peildatum of the query
        test.change_reference_date_to("2023-12-31", "2024-12-31")

        # Verify actual result of the query
        test.verify_value_in_list("indicator", {"0", "0.0"})

    finally:
        # Delete previously loaded data
        dg.delete_graph_data()