from DataGenerator import DataGenerator
from QueryTest import QueryTest

# Meetperiode startdatum: 01-01-2024
# Meetperiode einddatum: 31-12-2024

#Opmerkingen:

#Testcases

td_01 = [
    {
        "Description": "Testcase 01 (Geen ZVL functie + Geen AOK + Geen WLZ indicatie)",
        "Amount": 10, #Indicator score: 0 
        "Human": [
             {
                 "InhuurOvereenkomst": [
                    {
                        "function": [
                            {
                                "label": "Geen AOK + Geen ZVL",
                                "caregiving_role": False,
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31",
                            }
                        ],
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",                        
                        "size": [
                             {
                                "unit": 36,
                                "unit_of_measure": "Uren_per_week_unit",
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31"
                             }
                        ],
                        "booked_hours": [
                            {
                                "unit": 36,
                                "unit_of_measure": "Uur",
                                "start_datetime": "2024-01-01T09:00:00",
                                "end_datetime": "202-12-31T17:00:00"
                            }
                        ]
                    }
                ],
            }
        ]
    }
]

td_02 = [
    {
        "Description": "Testcase 02 (Geen ZVL functie + Wel AOK)",
        "Amount": 10, #Indicator score: 0
        "Human": [
        {
                 "ArbeidsOvereenkomst": [
                    {
                        "function": [
                            {
                                "label": "Wel AOK + Geen ZVL",
                                "caregiving_role": False,
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31",
                            }
                        ],
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",                        
                        "size": [
                             {
                                "unit": 36,
                                "unit_of_measure": "Uren_per_week_unit",
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31"
                             }
                        ],
                        "booked_hours": [
                            {
                                "unit": 36,
                                "unit_of_measure": "Uur",
                                "start_datetime": "2024-01-01T09:00:00",
                                "end_datetime": "202-12-31T17:00:00"
                            }
                        ]
                    }
                ],
            }
        ]
    }
]

td_03 = [
    {
        "Description": "Testcase 03 (Wel ZVL functie + Geen AOK)",
        "Amount": 10, #Indicator score: 0 
        "Human": [
             {
                 "InhuurOvereenkomst": [
                    {
                        "function": [
                            {
                                "label": "Geen AOK + Geen ZVL",
                                "caregiving_role": False,
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31",
                            }
                        ],
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",                        
                        "size": [
                             {
                                "unit": 36,
                                "unit_of_measure": "Uren_per_week_unit",
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31"
                             }
                        ],
                        "booked_hours": [
                            {
                                "unit": 36,
                                "unit_of_measure": "Uur",
                                "start_datetime": "2024-01-01T09:00:00",
                                "end_datetime": "202-12-31T17:00:00"
                            }
                        ]
                    }
                ],
            }
        ]
    }
]

td_04 = [
    {
        "Description": "Testcase 04 (Wel ZVL functie + Wel AOK)",
        "Amount": 10, #Indicator score: 3.478436552364288559660492 (10 extra zorgpersoneel voor de aanwezige clienten in dummyzorg testdata)
        "Human": [
        {
                 "ArbeidsOvereenkomst": [
                    {
                        "function": [
                            {
                                "label": "Wel AOK + ZVL",
                                "caregiving_role": True,
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31",
                            }
                        ],
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",                        
                        "size": [
                             {
                                "unit": 36,
                                "unit_of_measure": "Uren_per_week_unit",
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31"
                             }
                        ],
                        "booked_hours": [
                            {
                                "unit": 36,
                                "unit_of_measure": "Uur",
                                "start_datetime": "2024-01-01T09:00:00",
                                "end_datetime": "202-12-31T17:00:00"
                            }
                        ]
                    }
                ]
            }
        ]
    }
]

td_04_a = [
    {
        "Description": "Testcase 04a (Wel ZVL functie + Wel AOK + Clienten met Wlz 4vv)",
        "Amount": 10, #Indicator score: 1.304413707136608209872684 (10 extra clienten bij de noemer zowel als Teller verandering door toegevoegde zorgpersoneel)
        "Human": [
        {
                 "ArbeidsOvereenkomst": [
                    {
                        "function": [
                            {
                                "label": "Wel AOK + ZVL",
                                "caregiving_role": True,
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31",
                            }
                        ],
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",                        
                        "size": [
                             {
                                "unit": 36,
                                "unit_of_measure": "Uren_per_week_unit",
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31"
                             }
                        ],
                        "booked_hours": [
                            {
                                "unit": 36,
                                "unit_of_measure": "Uur",
                                "start_datetime": "2024-01-01T09:00:00",
                                "end_datetime": "202-12-31T17:00:00"
                            }
                        ]
                    }
                ],
                "NursingProcess": [
                    {
                        "identifier": "1",
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",
                        "location": "Locatie_BG_Rechts",
                        "related_indication": "1"
                    }
                ],
                "Indication": [
                    {
                        "identifier": "1",
                        "type": "WlzIndicatie",
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",
                        "heeftIndicatieBehandeling": True,
                        "ciz": ["4VV"],
                        "leveringsvorm": [""]
                    }
                ]
            }
        ]
    }
]

td_04_b = [
    {
        "Description": "Testcase 04b (Wel ZVL functie + Wel AOK + Clienten met Wlz 1vv)",
        "Amount": 10, #Indicator score: 3.478436552364288559660492 (Geen verandering van Noemer t.o.v. aantal clienten)
        "Human": [
        {
                 "ArbeidsOvereenkomst": [
                    {
                        "function": [
                            {
                                "label": "Wel AOK + ZVL",
                                "caregiving_role": True,
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31",
                            }
                        ],
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",                        
                        "size": [
                             {
                                "unit": 36,
                                "unit_of_measure": "Uren_per_week_unit",
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31"
                             }
                        ],
                        "booked_hours": [
                            {
                                "unit": 36,
                                "unit_of_measure": "Uur",
                                "start_datetime": "2024-01-01T09:00:00",
                                "end_datetime": "202-12-31T17:00:00"
                            }
                        ]
                    }
                ],
                "NursingProcess": [
                    {
                        "identifier": "1",
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",
                        "location": "Locatie_BG_Rechts",
                        "related_indication": "1"
                    }
                ],
                "Indication": [
                    {
                        "identifier": "1",
                        "type": "WlzIndicatie",
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",
                        "heeftIndicatieBehandeling": True,
                        "ciz": ["1VV"],
                        "leveringsvorm": [""]
                    }
                ]
            }
        ]
    }
]

td_04_c = [
    {
        "Description": "Testcase 04c (Wel ZVL functie + Wel AOK + Clienten zonder Wlz)",
        "Amount": 10, #Indicator score: 3.478436552364288559660492 (Geen verandering van Noemer t.o.v. aantal clienten)
        "Human": [
        {
                 "ArbeidsOvereenkomst": [
                    {
                        "function": [
                            {
                                "label": "Wel AOK + ZVL",
                                "caregiving_role": True,
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31",
                            }
                        ],
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",                        
                        "size": [
                             {
                                "unit": 36,
                                "unit_of_measure": "Uren_per_week_unit",
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31"
                             }
                        ],
                        "booked_hours": [
                            {
                                "unit": 36,
                                "unit_of_measure": "Uur",
                                "start_datetime": "2024-01-01T09:00:00",
                                "end_datetime": "202-12-31T17:00:00"
                            }
                        ]
                    }
                ],
                "NursingProcess": [
                    {
                        "identifier": "1",
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",
                        "location": "Locatie_BG_Rechts",
                        "related_indication": "1"
                    }
                ],
                "Indication": [
                    {
                        "identifier": "1",
                        "type": "WlzIndicatie",
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",
                        "heeftIndicatieBehandeling": True,
                        "ciz": [""],
                        "leveringsvorm": [""]
                    }
                ]
            }
        ]
    }
]

td_05 = [
    {
        "Description": "Testcase 05 (Clienten + Geen WLZ indicatie)",
        "Amount": 10, #Indicator score: 0 
        "Human": [
                    {
                "NursingProcess": [
                    {
                        "identifier": "1",
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",
                        "location": "Locatie_BG_Rechts",
                        "related_indication": "1"
                    }
                ],
                "Indication": [
                    {
                        "identifier": "1",
                        "type": "WlzIndicatie",
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",
                        "heeftIndicatieBehandeling": True,
                        "ciz": [],
                        "leveringsvorm": [""]
                    }
                ]
            }
        ]
    }
]

td_06 = [
    {
        "Description": "Testcase 06 (Clienten + WLZ indicatie 1 t/m 3)",
        "Amount": 10, #Indicator score: 0 
        "Human": [
                    {
                "NursingProcess": [
                    {
                        "identifier": "1",
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",
                        "location": "Locatie_BG_Rechts",
                        "related_indication": "1"
                    }
                ],
                "Indication": [
                    {
                        "identifier": "1",
                        "type": "WlzIndicatie",
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",
                        "heeftIndicatieBehandeling": True,
                        "ciz": ["1VV"], #, "2VV", "3VV", "7VV", "8VV", "10VV", "9BVV"]
                        "leveringsvorm": [""] 
                    }
                ]
            }
        ]
    }
]

td_07 = [
    {
        "Description": "Testcase 07 (Clienten + WLZ indicatie 4 t/m 10)",
        "Amount": 10, #Indicator score: 0 Geen zorgpersoneel = Teller: 0. 
        "Human": [
                    {
                "NursingProcess": [
                    {
                        "identifier": "1",
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",
                        "location": "Locatie_BG_Rechts",
                        "related_indication": "1"
                    }
                ],
                "Indication": [
                    {
                        "identifier": "1",
                        "type": "WlzIndicatie",
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",
                        "heeftIndicatieBehandeling": True,
                        "ciz": ["4VV"], #, "5VV", "6VV", "7VV", "8VV", "10VV", "9BVV"]
                        "leveringsvorm": [""] 
                    }
                ]
            }
        ]
    }
]

# Static tests

def test_if_headers_are_correct_for_query_2_5_1(db_config):
    """ Test of de juiste header terugkomt in het resultaat
        ODB Personeelssamenstelling 2.5.1. Aantal fte zorg client ratio 
    """

    # Setup of the test
    test = QueryTest(db_config)

    # Configuration and execution
    test.set_query('Gevalideerde_vragen_technisch/personeelssamenstelling/Indicator 2.5.1.rq')

    # Assertions
    test.verify_header_present('indicator')
    test.verify_header_present('teller')
    test.verify_header_present('noemer')

def test_if_number_of_rows_returned_is_correct_for_query_2_5_1(db_config):
    """ Test of het aantal rijen correct wordt teruggegeven
        ODB Personeelssamenstelling 2.5.1. Aantal fte zorg client ratio
    """

    # Setup of the test
    test = QueryTest(db_config)

    # Configuration and execution
    test.set_query('Gevalideerde_vragen_technisch/personeelssamenstelling/Indicator 2.5.1.rq')

    # Assertions
    test.verify_row_count(1)

def test_if_indicator_has_correct_value_for_query_2_5_1(db_config):
    """ Test of de indicator de juiste waarde heeft
        ODB Personeelssamenstelling 2.5.1. Aantal fte zorg client ratio
    """

    # Setup of the test
    test = QueryTest(db_config)

    # Configuration and execution
    test.set_query('Gevalideerde_vragen_technisch/personeelssamenstelling/Indicator 2.5.1.rq')

    # Assertions
    test.verify_value("indicator","0.118203309692671394799050")
    # test.verify_value("teller","0.002364066193853427895981")
    # test.verify_value("noemer","3")

def test_if_dates_can_change_2_5_1(db_config):
    """ Test of gewijzigde datum daadwerkelijk een ander resultaat oplevert
        ODB Personeelssamenstelling 2.5.1. Aantal fte zorg client ratio
    """

    # Setup of the test
    test = QueryTest(db_config)

    # Configuration and execution
    test.set_query('Gevalideerde_vragen_technisch/personeelssamenstelling/Indicator 2.5.1.rq')

    test.change_start_period("2023-01-01", "2024-01-01")
    test.change_end_period("2023-12-31", "2024-12-31")

    # Assertions
    test.verify_value_in_list("indicator", {"0", "0.0"})

# Tests using Generated Data

# Testcase 01
def test_if_value_returned_is_correct_for_query_2_5_1_01(db_config):
    """ Testcase 01 (Geen ZVL functie + Geen AOK)
        ODB personeelssamenstelling 2.5.1. Aantal fte zorg client ratio
    """

    # Load defined test data
    dg = DataGenerator(db_config, td_01)

    try:
        # Start a test
        test = QueryTest(db_config)

        # Set the query to be tested
        test.set_query('Gevalideerde_vragen_technisch/personeelssamenstelling/Indicator 2.5.1.rq')

        # Change measuring period parameters of query
        test.change_start_period("2023-01-01", "2024-01-01")
        test.change_end_period("2023-12-31", "2024-12-31")


        # Verify actual result of the query
        test.verify_value_in_list("indicator", {"0", "0.0"})

    finally:
        # Delete previously loaded data
        dg.delete_graph_data()

# Testcase 02
def test_if_value_returned_is_correct_for_query_2_5_1_02(db_config):
    """ Testcase 02 (Geen ZVL functie + Wel AOK)
        ODB personeelssamenstelling 2.5.1. Aantal fte zorg client ratio
    """

    # Load defined test data
    dg = DataGenerator(db_config, td_02)

    try:
        # Start a test
        test = QueryTest(db_config)

        # Set the query to be tested
        test.set_query('Gevalideerde_vragen_technisch/personeelssamenstelling/Indicator 2.5.1.rq')

        # Change measuring period parameters of query
        test.change_start_period("2023-01-01", "2024-01-01")
        test.change_end_period("2023-12-31", "2024-12-31")

        # Verify actual result of the query
        test.verify_value_in_list("indicator", {"0", "0.0"})

    finally:
        # Delete previously loaded data
        dg.delete_graph_data()

# Testcase 03
def test_if_value_returned_is_correct_for_query_2_5_1_03(db_config):
    """ Testcase 03 (Wel ZVL functie + Geen AOK)
        ODB personeelssamenstelling 2.5.1. Aantal fte zorg client ratio
    """

    # Load defined test data
    dg = DataGenerator(db_config, td_03)

    try:
        # Start a test
        test = QueryTest(db_config)

        # Set the query to be tested
        test.set_query('Gevalideerde_vragen_technisch/personeelssamenstelling/Indicator 2.5.1.rq')

        # Change measuring period parameters of query
        test.change_start_period("2023-01-01", "2024-01-01")
        test.change_end_period("2023-12-31", "2024-12-31")

        # Verify actual result of the query
        test.verify_value_in_list("indicator", {"0", "0.0"})

    finally:
        # Delete previously loaded data
        dg.delete_graph_data()

# Testcase 04
def test_if_value_returned_is_correct_for_query_2_5_1_04(db_config):
    """ Testcase 04 (Wel ZVL functie + Wel AOK)
        ODB personeelssamenstelling 2.5.1. Aantal fte zorg client ratio
    """

    # Load defined test data
    dg = DataGenerator(db_config, td_04)

    try:
        # Start a test
        test = QueryTest(db_config)

        # Set the query to be tested
        test.set_query('Gevalideerde_vragen_technisch/personeelssamenstelling/Indicator 2.5.1.rq')

        # Change measuring period parameters of query
        test.change_start_period("2023-01-01", "2024-01-01")
        test.change_end_period("2023-12-31", "2024-12-31")

        # Verify actual result of the query
        test.verify_value("indicator", "10.580244513441377702300664")
        # test.verify_value("teller", "0.212184629694221602139290")
        # test.verify_value("noemer", "3.008219178082191780821918")

    finally:
        # Delete previously loaded data
        dg.delete_graph_data()

# Testcase 04a
def test_if_value_returned_is_correct_for_query_2_5_1_04_a(db_config):
    """ Testcase 04a (Wel ZVL functie + Wel AOK + clienten met Wlz 4VV)
        ODB personeelssamenstelling 2.5.1. Aantal fte zorg client ratio
    """

    # Load defined test data
    dg = DataGenerator(db_config, td_04_a)

    try:
        # Start a test
        test = QueryTest(db_config)

        # Set the query to be tested
        test.set_query('Gevalideerde_vragen_technisch/personeelssamenstelling/Indicator 2.5.1.rq')

        # Change measuring period parameters of query
        test.change_start_period("2023-01-01", "2024-01-01")
        test.change_end_period("2023-12-31", "2024-12-31")

        # Verify actual result of the query
        test.verify_value("indicator", "1.763374085573562950383444")
        # test.verify_value("teller", "0.212184629694221602139290")
        # test.verify_value("noemer", ""13.035616438356164383561644")

    finally:
        # Delete previously loaded data
        dg.delete_graph_data()

# Testcase 04b
def test_if_value_returned_is_correct_for_query_2_5_1_04_b(db_config):
    """ Testcase 04b (Wel ZVL functie + Wel AOK + Clienten met Wlz 1VV)
        ODB personeelssamenstelling 2.5.1. Aantal fte zorg client ratio
    """

    # Load defined test data
    dg = DataGenerator(db_config, td_04_b)

    try:
        # Start a test
        test = QueryTest(db_config)

        # Set the query to be tested
        test.set_query('Gevalideerde_vragen_technisch/personeelssamenstelling/Indicator 2.5.1.rq')

        # Change measuring period parameters of query
        test.change_start_period("2023-01-01", "2024-01-01")
        test.change_end_period("2023-12-31", "2024-12-31")

        # Verify actual result of the query
        test.verify_value("indicator", "10.580244513441377702300664")
        # test.verify_value("teller", "0.212184629694221602139290")
        # test.verify_value("noemer", "3.008219178082191780821918")

    finally:
        # Delete previously loaded data
        dg.delete_graph_data()

# Testcase 04c
def test_if_value_returned_is_correct_for_query_2_5_1_04_c(db_config):
    """ Testcase 04c (Wel ZVL functie + Wel AOK + Clienten geen Wlz indicatie)
        ODB personeelssamenstelling 2.5.1. Aantal fte zorg client ratio
    """

    # Load defined test data
    dg = DataGenerator(db_config, td_04_c)

    try:
        # Start a test
        test = QueryTest(db_config)

        # Set the query to be tested
        test.set_query('Gevalideerde_vragen_technisch/personeelssamenstelling/Indicator 2.5.1.rq')

        # Change measuring period parameters of query
        test.change_start_period("2023-01-01", "2024-01-01")
        test.change_end_period("2023-12-31", "2024-12-31")

        # Verify actual result of the query
        test.verify_value("indicator", "10.580244513441377702300664")
        # test.verify_value("teller", "0.212184629694221602139290")
        # test.verify_value("noemer", "3.008219178082191780821918")

    finally:
        # Delete previously loaded data
        dg.delete_graph_data()

# Testcase 05
def test_if_value_returned_is_correct_for_query_2_5_1_05(db_config):
    """ Testcase 05 (Clienten met geen wlz indicatie)
        ODB personeelssamenstelling 2.5.1. Aantal fte zorg client ratio
    """

    # Load defined test data
    dg = DataGenerator(db_config, td_05)

    try:
        # Start a test
        test = QueryTest(db_config)

        # Set the query to be tested
        test.set_query('Gevalideerde_vragen_technisch/personeelssamenstelling/Indicator 2.5.1.rq')

        # Change measuring period parameters of query
        test.change_start_period("2023-01-01", "2024-01-01")
        test.change_end_period("2023-12-31", "2024-12-31")

        # Verify actual result of the query
        test.verify_value_in_list("indicator", {"0", "0.0"})

    finally:
        # Delete previously loaded data
        dg.delete_graph_data()

# Testcase 06
def test_if_value_returned_is_correct_for_query_2_5_1_06(db_config):
    """ Testcase 06 (Clienten met Wlz indicatie 1 t/m 3)
        ODB personeelssamenstelling 2.5.1. Aantal fte zorg client ratio
    """

    # Load defined test data
    dg = DataGenerator(db_config, td_06)

    try:
        # Start a test
        test = QueryTest(db_config)

        # Set the query to be tested
        test.set_query('Gevalideerde_vragen_technisch/personeelssamenstelling/Indicator 2.5.1.rq')

        # Change measuring period parameters of query
        test.change_start_period("2023-01-01", "2024-01-01")
        test.change_end_period("2023-12-31", "2024-12-31")

        # Verify actual result of the query
        test.verify_value_in_list("indicator", {"0", "0.0"})

    finally:
        # Delete previously loaded data
        dg.delete_graph_data()

# Testcase 07
def test_if_value_returned_is_correct_for_query_2_5_1_07(db_config):
    """ Testcase 07 (Clienten met Wlz indicatie 4 t/m 10)
        ODB personeelssamenstelling 2.5.1. Aantal fte zorg client ratio
    """

    # Load defined test data
    dg = DataGenerator(db_config, td_07)

    try:
        # Start a test
        test = QueryTest(db_config)

        # Set the query to be tested
        test.set_query('Gevalideerde_vragen_technisch/personeelssamenstelling/Indicator 2.5.1.rq')

        # Change measuring period parameters of query
        test.change_start_period("2023-01-01", "2024-01-01")
        test.change_end_period("2023-12-31", "2024-12-31")

        # Verify actual result of the query
        test.verify_value_in_list("indicator", {"0", "0.0"})

    finally:
        # Delete previously loaded data
        dg.delete_graph_data()