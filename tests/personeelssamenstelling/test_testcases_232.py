from DataGenerator import DataGenerator
from QueryTest import QueryTest

# Meetperiode startdatum: 01-01-2024
# Meetperiode einddatum: 31-12-2024

#Opmerkingen:
# Meerdere verzuimperiodes worden niet bij elkaar opgeteld 
# e.g. 08 = 1 verzuim periode, 08_a & 08_b = 2 verzuim periodes (query issue?)

       

td_01 = [
    {
        "Description": "Testcase 01 (Geen ZVL functie + Geen AOK (PNIL) + Geen verzuim)",
        "Amount": 10, #Indicator score: 0 
        "Human": [
            {
                "InhuurOvereenkomst": [
                    {
                        "function": [
                            {
                                "label": "INH OK Geen ZVL",
                                "caregiving_role": False,
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31",
                            }
                        ],
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",  
                        "location": "Locatie_De_Beuk_1",                      
                        "size": [
                             {
                                "unit": 36,
                                "unit_of_measure": "Uren_per_week_unit",
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31"
                             }
                        ]
                    }
                ]
            }
        ]
    }
]  

td_02 = [
    {
        "Description": "Testcase 02 (Geen ZVL functie + Wel AOK (PIL) + Geen verzuim)",
        "Amount": 10, # Indicator score: 0
        "Human": [
            {
                "ArbeidsOvereenkomst": [
                    {   
                        "function": [
                            {       
                                "label": "AOK Geen ZVL",                       
                                "caregiving_role": False,
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31",
                            }
                        ],
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",
                        "location": "Locatie_De_Beuk_1",   
                        "size": [
                             {
                                "unit": 36,
                                "unit_of_measure": "Uren_per_week_unit",
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31"
                            }
                        ]
                    }
                ]
            }
        ]
     }
]

td_03 = [
    {
        "Description": "Testcase 03 (Wel ZVL functie + Geen AOK + Geen verzuim)",
        "Amount": 10, # Indicator score: 0
        "Human": [
            {
                "StageOvereenkomst": [
                    {
                        "function": [
                            {
                                "label": "ST OK Wel ZVL",
                                "caregiving_role": True,
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31",
                            }
                        ],
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",  
                        "location": "Locatie_De_Beuk_1",                      
                        "size": [
                             {
                                "unit": 36,
                                "unit_of_measure": "Uren_per_week_unit",
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31"
                             }
                        ]
                    }
                ]
            }
        ]
    }
]  

td_04 = [ 
    {
        "Description": "Testcase 04 (Wel ZVL functie + Wel AOK (PIL) + Geen verzuim)",
        "Amount": 10, # Indicator score: 0
        "Human": [
            {
                "ArbeidsOvereenkomst": [
                    {   
                        "function": [
                            {       
                                "label": "AOK Wel ZVL",                       
                                "caregiving_role": True,
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31",
                            }
                        ],
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",
                        "location": "Locatie_De_Beuk_1",   
                        "size": [
                             {
                                "unit": 36,
                                "unit_of_measure": "Uren_per_week_unit",
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31"
                            }
                        ]
                    }
                ]
            }
        ]
     }
]

td_04_a = [ 
    {
        "Description": "Testcase 04a (Wel ZVL functie + Wel AOK (PNIL) + Geen verzuim)",
        "Amount": 10, # Indicator score: 0
        "Human": [
            {
                "InhuurOvereenkomst": [
                    {   
                        "function": [
                            {       
                                "label": "INH OK Wel ZVL",                       
                                "caregiving_role": True,
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31",
                            }
                        ],
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",
                        "location": "Locatie_De_Beuk_1",   
                        "size": [
                             {
                                "unit": 36,
                                "unit_of_measure": "Uren_per_week_unit",
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31"
                            }
                        ]
                    }
                ]
            }
        ]
    }
]

td_05 = [
    {
        "Description": "Testcase 05 (Geen ZVL functie + Geen AOK (PNIL) + Wel verzuim (1 week))",
        "Amount": 10, # Indicator score: 0
        "Human": [
            {
                "StageOvereenkomst": [
                    {   
                        "function": [
                            {       
                                "label": "ST OK Geen ZVL + 1 week verzuim",                       
                                "caregiving_role": False,
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31",
                            }
                        ],
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",
                        "location": "Locatie_De_Beuk_1",   
                        "size": [
                             {
                                "unit": 36,
                                "unit_of_measure": "Uren_per_week_unit",
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31"
                            }
                        ],
                        "sickness": [
                            {
                                "unit": 36,
                                "unit_of_measure": "Uren_per_week_unit",
                                "start_date": "2024-01-01",
                                "end_date": "2024-01-08"
                            }
                        ]
                    }
                ]
            }
        ]
    }
]

td_05_a = [
    {
        "Description": "Testcase 05a (Geen ZVL functie + Geen AOK (PNIL) + Wel verzuim (2 aparte weken))",
        "Amount": 10, # Indicator score: 0
        "Human": [
            {
                "StageOvereenkomst": [
                    {   
                        "function": [
                            {       
                                "label": "ST OK Geen ZVL + 2 weken verzuim",                       
                                "caregiving_role": False,
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31",
                            }
                        ],
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",
                        "location": "Locatie_De_Beuk_1",   
                        "size": [
                             {
                                "unit": 36,
                                "unit_of_measure": "Uren_per_week_unit",
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31"
                            }
                        ],
                        "sickness": [
                            {
                                "unit": 36,
                                "unit_of_measure": "Uren_per_week_unit",
                                "start_date": "2024-01-01",
                                "end_date": "2024-01-08"
                            },
                            {
                                "unit": 36,
                                "unit_of_measure": "Uren_per_week_unit",
                                "start_date": "2024-06-01",
                                "end_date": "2024-06-08"
                            }
                        ]
                    }
                ]
            }
        ]
    }
]

td_06 = [
    {
        "Description": "Testcase 06 (Geen ZVL functie + Wel AOK (PIL) + Wel verzuim (1 week))",
        "Amount": 10, # Indicator score: 0
        "Human": [
            {
                "ArbeidsOvereenkomst": [
                    {   
                        "function": [
                            {       
                                "label": "AOK Geen ZVL + 1 week verzuim",                       
                                "caregiving_role": False,
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31",
                            }
                        ],
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",
                        "location": "Locatie_De_Beuk_1",   
                        "size": [
                             {
                                "unit": 36,
                                "unit_of_measure": "Uren_per_week_unit",
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31"
                            }
                        ],
                        "sickness": [
                            {
                                "unit": 36,
                                "unit_of_measure": "Uren_per_week_unit",
                                "start_date": "2024-01-01",
                                "end_date": "2024-01-08"
                            }
                        ]
                    }
                ]
            }
        ]
    }
]

td_06_a = [
    {
        "Description": "Testcase 06a (Geen ZVL functie + Wel AOK (PIL) + Wel verzuim (2 aparte weken))",
        "Amount": 10, # Indicator score: 0
        "Human": [
            {
                "ArbeidsOvereenkomst": [
                    {   
                        "function": [
                            {       
                                "label": "AOK Geen ZVL + 2 weken verzuim",                       
                                "caregiving_role": False,
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31",
                            }
                        ],
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",
                        "location": "Locatie_De_Beuk_1",   
                        "size": [
                             {
                                "unit": 36,
                                "unit_of_measure": "Uren_per_week_unit",
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31"
                            }
                        ],
                        "sickness": [
                            {
                                "unit": 36,
                                "unit_of_measure": "Uren_per_week_unit",
                                "start_date": "2024-01-01",
                                "end_date": "2024-01-08"
                            },
                            {
                                "unit": 36,
                                "unit_of_measure": "Uren_per_week_unit",
                                "start_date": "2024-06-01",
                                "end_date": "2024-06-08"
                            }
                        ]
                    }
                ]
            }
        ]
    }
]

td_07 = [
    {
        "Description": "Testcase 07 (Wel ZVL functie + Geen AOK (PNIL) + Wel verzuim (1 week))",
        "Amount": 10, # Indicator score: 0
        "Human": [
            {
                "InhuurOvereenkomst": [
                    {   
                        "function": [
                            {       
                                "label": "INH OK Wel ZVL + 1 week verzuim",                       
                                "caregiving_role": True,
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31",
                            }
                        ],
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",
                        "location": "Locatie_De_Beuk_1",   
                        "size": [
                             {
                                "unit": 36,
                                "unit_of_measure": "Uren_per_week_unit",
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31"
                            }
                        ],
                        "sickness": [
                            {
                                "unit": 36,
                                "unit_of_measure": "Uren_per_week_unit",
                                "start_date": "2024-01-01",
                                "end_date": "2024-01-08"
                            }
                        ]
                    }
                ]
            }
        ]
    }
]

td_07_a = [
    {
        "Description": "Testcase 07a (Wel ZVL functie + Geen AOK (PNIL) + Wel verzuim (2 aparte weken))",
        "Amount": 10, # Indicator score: 0
        "Human": [
            {
                "InhuurOvereenkomst": [
                    {   
                        "function": [
                            {       
                                "label": "INH OK Wel ZVL + 2 weken verzuim",                       
                                "caregiving_role": True,
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31",
                            }
                        ],
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",
                        "location": "Locatie_De_Beuk_1",   
                        "size": [
                             {
                                "unit": 36,
                                "unit_of_measure": "Uren_per_week_unit",
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31"
                            }
                        ],
                        "sickness": [
                            {
                                "unit": 36,
                                "unit_of_measure": "Uren_per_week_unit",
                                "start_date": "2024-01-01",
                                "end_date": "2024-01-08"
                            },
                            {
                                "unit": 36,
                                "unit_of_measure": "Uren_per_week_unit",
                                "start_date": "2024-06-01",
                                "end_date": "2024-06-08"
                            }
                        ]
                    }
                ]
            }
        ]
    }
]

td_08 = [
    {
        "Description": "Testcase 08 (Wel ZVL functie + Wel AOK (PIL) + Wel verzuim (1 week))",
        "Amount": 20, # Indicator score: 0.6315
        "Human": [
            {
                "ArbeidsOvereenkomst": [
                    {   
                        "function": [
                            {       
                                "label": "AOK Wel ZVL + 1 week verzuim",                       
                                "caregiving_role": True,
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31",
                            }
                        ],
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",
                        "location": "Locatie_De_Beuk_1",   
                        "size": [
                             {
                                "unit": 36,
                                "unit_of_measure": "Uren_per_week_unit",
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31"
                            }
                        ],
                        "sickness": [
                            {
                                "unit": 36,
                                "unit_of_measure": "Uren_per_week_unit",
                                "start_date": "2024-01-01",
                                "end_date": "2024-01-08"
                            }
                        ]
                    }
                ]
            }
        ]
    }
]

td_08_a = [
    {
        "Description": "Testcase 08a (Wel ZVL functie + Wel AOK (PIL) + Wel verzuim (2 aparte weken) + Wel verlof (1 week zw. verlof))",
        "Amount": 10, #Indicator score: 0.6842
        "Human": [
            {
                "ArbeidsOvereenkomst": [
                    {   
                        "function": [
                            {       
                                "label": "AOK Wel ZVL + 2 weken verzuim + 1 week zw. verlof",                       
                                "caregiving_role": True,
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31",
                            }
                        ],
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",
                        "location": "Locatie_De_Beuk_1",   
                        "size": [
                             {
                                "unit": 36,
                                "unit_of_measure": "Uren_per_week_unit",
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31"
                            }
                        ],
                        "sickness": [
                            {
                                "unit": 36,
                                "unit_of_measure": "Uren_per_week_unit",
                                "start_date": "2024-01-01",
                                "end_date": "2024-01-08"
                            },
                            {
                                "unit": 36,
                                "unit_of_measure": "Uren_per_week_unit",
                                "start_date": "2024-06-01",
                                "end_date": "2024-06-08"
                            }
                        ],
                        "maternity_leave": [
                            {   
                                "start_date": "2024-08-01",
                                "end_date": "2024-10-01"
                            }
                        ]
                    }
                ]
            }
        ]
    }
]

td_08_b = [
    {
        "Description": "Testcase 08b (Wel ZVL functie + Wel AOK (PIL) + Wel verzuim (2 aparte weken))",
        "Amount": 10, #Indicator score: 0.6842
        "Human": [
            {
                "ArbeidsOvereenkomst": [
                    {   
                        "function": [
                            {       
                                "label": "AOK Wel ZVL + 2 weken verzuim + 1 week zw. verlof",                       
                                "caregiving_role": True,
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31",
                            }
                        ],
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",
                        "location": "Locatie_De_Beuk_1",   
                        "size": [
                             {
                                "unit": 36,
                                "unit_of_measure": "Uren_per_week_unit",
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31"
                            }
                        ],
                        "sickness": [
                            {
                                "unit": 36,
                                "unit_of_measure": "Uren_per_week_unit",
                                "start_date": "2024-01-01",
                                "end_date": "2024-01-08"
                            },
                            {
                                "unit": 36,
                                "unit_of_measure": "Uren_per_week_unit",
                                "start_date": "2024-06-01",
                                "end_date": "2024-06-08"
                            }
                        ]
                    }
                ]
            }
        ]
    }
]

td_08_c = [
    {
        "Description": "Testcase 08c (Wel ZVL functie + Wel AOK (PIL) + Wel verzuim (2 aparte weken))",
        "Amount": 1, #Indicator score: 0.6842
        "Human": [
            {
                "ArbeidsOvereenkomst": [
                    {   
                        "function": [
                            {       
                                "label": "AOK Wel ZVL + 2 weken verzuim + 1 week zw. verlof",                       
                                "caregiving_role": True,
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31",
                            }
                        ],
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",
                        "location": "Locatie_De_Beuk_1",   
                        "size": [
                             {
                                "unit": 36,
                                "unit_of_measure": "Uren_per_week_unit",
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31"
                            }
                        ],
                        "sickness": [
                            {
                                "unit": 36,
                                "unit_of_measure": "Uren_per_week_unit",
                                "start_date": "2024-01-01",
                                "end_date": "2024-01-08"
                            },
                            {
                                "unit": 36,
                                "unit_of_measure": "Uren_per_week_unit",
                                "start_date": "2024-06-01",
                                "end_date": "2024-06-08"
                            }
                        ],
                        "maternity_leave": [
                            {   
                                "start_date": "2024-08-01",
                                "end_date": "2024-10-01"
                            }
                        ]
                    }
                ]
            }
        ]
    }
]

td_08_d = [
    {
        "Description": "Testcase 08d (Wel ZVL functie + Wel AOK (PIL) + Wel verzuim (4 aparte weken))",
        "Amount": 1, #Indicator score: 0.6842
        "Human": [
            {
                "ArbeidsOvereenkomst": [
                    {
                        "function": [
                            {
                                "label": "Wel AOK (PIL) Wel ZVL & 4 weken verzuim",
                                "caregiving_role": True,
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31",
                            }
                        ],
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",
                        "location": "Locatie_De_Beuk_1",
                        "size": [
                             {
                                "unit": 36,
                                "unit_of_measure": "Uren_per_week_unit",
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31",
                            }
                        ],
                        "sickness": [
                            {
                                "unit": 36,
                                "unit_of_measure": "Uren_per_week_unit",
                                "start_date": "2024-01-01",
                                "end_date": "2024-01-08"
                            },
                            {
                                "unit": 36,
                                "unit_of_measure": "Uren_per_week_unit",
                                "start_date": "2024-06-01",
                                "end_date": "2024-06-08"
                            },
                            {
                                "unit": 36,
                                "unit_of_measure": "Uren_per_week_unit",
                                "start_date": "2024-01-01",
                                "end_date": "2024-01-08"
                            },
                            {
                                "unit": 36,
                                "unit_of_measure": "Uren_per_week_unit",
                                "start_date": "2024-05-01",
                                "end_date": "2024-05-08"
                            }
                        ]
                    }
                ]
            }
        ]
    }
]


# Static tests

def test_if_headers_are_correct_for_query_2_3_2(db_config):
    """ Test of de juiste header terugkomt in het resultaat
        ODB Personeelssamenstelling 2.3.2. Verzuimfrequentie
    """

    # Setup of the test
    test = QueryTest(db_config)

    # Configuration and execution
    test.set_query('Gevalideerde_vragen_technisch/personeelssamenstelling/Indicator 2.3.2.rq')

    # Assertions
    test.verify_header_present('teller')
    test.verify_header_present('noemer')
    test.verify_header_present('verzuimfrequentie')

def test_if_number_of_rows_returned_is_correct_for_query_2_3_2(db_config):
    """
        verzuimfrequentie: ODB Personeelssamenstelling 2.3.2
        Beschrijving: Test of het aantal rijen correct wordt teruggegeven
    """

    # Setup of the test
    test = QueryTest(db_config)

    # Configuration and execution
    test.set_query('Gevalideerde_vragen_technisch/personeelssamenstelling/Indicator 2.3.2.rq')

    # Assertions
    test.verify_row_count(1)


def test_if_verzuimfrequentie_has_correct_value_for_query_2_3_2(db_config):
    """ Test of de verzuimfrequentie de juiste waarde heeft
        ODB Personeelssamenstelling 2.3.2. Verzuimfrequentie
    """

    # Setup of the test
    test = QueryTest(db_config)

    # Configuration and execution
    test.set_query('Gevalideerde_vragen_technisch/personeelssamenstelling/Indicator 2.3.2.rq')

    # Assertions
    test.verify_value_in_list("verzuimfrequentie", {"0.222222222222222222222222", "0.222222222222222222222222"})


def test_if_dates_can_change_2_3_2(db_config):
    """ Test of gewijzigde datum daadwerkelijk een ander resultaat oplevert
        ODB Personeelssamenstelling 2.3.2. Verzuimfrequentie
    """

    # Setup of the test
    test = QueryTest(db_config)

    # Configuration and execution
    test.set_query('Gevalideerde_vragen_technisch/personeelssamenstelling/Indicator 2.3.2.rq')

    test.change_start_period("2023-01-01", "2022-01-01")
    test.change_end_period("2023-12-31", "2022-12-31")

    # Assertions
    test.verify_value_in_list("verzuimfrequentie", {"0", "0.0"})

# Tests using Generated Data

# Testcase 01
def test_if_value_returned_is_correct_for_query_2_3_2_01(db_config):
    """ Testcase 01 (Geen ZVL functie + Geen AOK (PNIL) + Geen verzuim)
        ODB personeelssamenstelling 2.3.2. Verzuimfrequentie
    """

    # Load defined test data
    dg = DataGenerator(db_config, td_01)

    try:
        # Start a test
        test = QueryTest(db_config)

        # Set the query to be tested
        test.set_query('Gevalideerde_vragen_technisch/personeelssamenstelling/Indicator 2.3.2.rq')

        # Change measuring period parameters of query
        test.change_start_period("2023-01-01", "2024-01-01")
        test.change_end_period("2023-12-31", "2024-12-31")

        # Verify actual result of the query
        test.verify_value_in_list("verzuimfrequentie", {"0", "0.0"})

    finally:
        # Delete previously loaded data
        dg.delete_graph_data()


# Testcase 02
def test_if_value_returned_is_correct_for_query_2_3_2_02(db_config):
    """ Testcase 02 (Geen ZVL functie + Wel AOK (PIL) + Geen verzuim)
        ODB personeelssamenstelling 2.3.2. Verzuimfrequentie
    """

    # Load defined test data
    dg = DataGenerator(db_config, td_02)

    try:
        # Start a test
        test = QueryTest(db_config)

        # Set the query to be tested
        test.set_query('Gevalideerde_vragen_technisch/personeelssamenstelling/Indicator 2.3.2.rq')

        # Change measuring period parameters of query
        test.change_start_period("2023-01-01", "2024-01-01")
        test.change_end_period("2023-12-31", "2024-12-31")

        # Verify actual result of the query
        test.verify_value_in_list("verzuimfrequentie", {"0", "0.0"})

    finally:
        # Delete previously loaded data
        dg.delete_graph_data()


# Testcase 03
def test_if_value_returned_is_correct_for_query_2_3_2_03(db_config):
    """ Testcase 03 (Wel ZVL functie + Geen AOK (PNIL) + Geen verzuim)
        ODB personeelssamenstelling 2.3.2. Verzuimfrequentie
    """

    # Load defined test data
    dg = DataGenerator(db_config, td_03)

    try:
        # Start a test
        test = QueryTest(db_config)

        # Set the query to be tested
        test.set_query('Gevalideerde_vragen_technisch/personeelssamenstelling/Indicator 2.3.2.rq')

        # Change measuring period parameters of query
        test.change_start_period("2023-01-01", "2024-01-01")
        test.change_end_period("2023-12-31", "2024-12-31")

        # Verify actual result of the query
        test.verify_value_in_list("verzuimfrequentie", {"0", "0.0"})

    finally:
        # Delete previously loaded data
        dg.delete_graph_data()


# Testcase 04
def test_if_value_returned_is_correct_for_query_2_3_2_04(db_config):
    """ Testcase 04 (Wel ZVL functie + Wel AOK (PIL) + Geen verzuim)
        ODB personeelssamenstelling 2.3.2. Verzuimfrequentie
    """

    # Load defined test data
    dg = DataGenerator(db_config, td_04)

    try:
        # Start a test
        test = QueryTest(db_config)

        # Set the query to be tested
        test.set_query('Gevalideerde_vragen_technisch/personeelssamenstelling/Indicator 2.3.2.rq')

        # Change measuring period parameters of query
        test.change_start_period("2023-01-01", "2024-01-01")
        test.change_end_period("2023-12-31", "2024-12-31")

        # Verify actual result of the query
        test.verify_value_in_list("verzuimfrequentie", {"0", "0.0"})

    finally:
        # Delete previously loaded data
        dg.delete_graph_data()


# Testcase 04a
def test_if_value_returned_is_correct_for_query_2_3_2_04a(db_config):
    """ Testcase 04a (Wel ZVL functie + Wel AOK (PNIL) + Geen verzuim)
        ODB personeelssamenstelling 2.3.2. Verzuimfrequentie
    """

    # Load defined test data
    dg = DataGenerator(db_config, td_04_a)

    try:
        # Start a test
        test = QueryTest(db_config)

        # Set the query to be tested
        test.set_query('Gevalideerde_vragen_technisch/personeelssamenstelling/Indicator 2.3.2.rq')

        # Change measuring period parameters of query
        test.change_start_period("2023-01-01", "2024-01-01")
        test.change_end_period("2023-12-31", "2024-12-31")

        # Verify actual result of the query
        test.verify_value_in_list("verzuimfrequentie", {"0", "0.0"})

    finally:
        # Delete previously loaded data
        dg.delete_graph_data()

# Testcase 05
def test_if_value_returned_is_correct_for_query_2_3_2_05(db_config):
    """ Testcase 05 (Geen ZVL functie + Geen AOK (PNIL) + Wel verzuim (1 week 36u))
        ODB personeelssamenstelling 2.3.2. Verzuimfrequentie
    """

    # Load defined test data
    dg = DataGenerator(db_config, td_05)

    try:
        # Start a test
        test = QueryTest(db_config)

        # Set the query to be tested
        test.set_query('Gevalideerde_vragen_technisch/personeelssamenstelling/Indicator 2.3.2.rq')

        # Change measuring period parameters of query
        test.change_start_period("2023-01-01", "2024-01-01")
        test.change_end_period("2023-12-31", "2024-12-31")

        # Verify actual result of the query
        test.verify_value_in_list("verzuimfrequentie", {"0", "0.0"})

    finally:
        # Delete previously loaded data
        dg.delete_graph_data()


# Testcase 05a
def test_if_value_returned_is_correct_for_query_2_3_2_05a(db_config):
    """ Testcase 05a (Geen ZVL functie + Geen AOK (PNIL) + Wel verzuim (2 aparte weken 36u))
        ODB personeelssamenstelling 2.3.2. Verzuimfrequentie
    """

    # Load defined test data
    dg = DataGenerator(db_config, td_05_a)

    try:
        # Start a test
        test = QueryTest(db_config)

        # Set the query to be tested
        test.set_query('Gevalideerde_vragen_technisch/personeelssamenstelling/Indicator 2.3.2.rq')

        # Change measuring period parameters of query
        test.change_start_period("2023-01-01", "2024-01-01")
        test.change_end_period("2023-12-31", "2024-12-31")

        # Verify actual result of the query
        test.verify_value_in_list("verzuimfrequentie", {"0", "0.0"})

    finally:
        # Delete previously loaded data
        dg.delete_graph_data()

# Testcase 06
def test_if_value_returned_is_correct_for_query_2_3_2_06(db_config):
    """ Testcase 06 (Geen ZVL functie + Wel AOK (PIL) + Wel verzuim (1 week 36u))
        ODB personeelssamenstelling 2.3.2. Verzuimfrequentie
    """

    # Load defined test data
    dg = DataGenerator(db_config, td_06)

    try:
        # Start a test
        test = QueryTest(db_config)

        # Set the query to be tested
        test.set_query('Gevalideerde_vragen_technisch/personeelssamenstelling/Indicator 2.3.2.rq')

        # Change measuring period parameters of query
        test.change_start_period("2023-01-01", "2024-01-01")
        test.change_end_period("2023-12-31", "2024-12-31")

        # Verify actual result of the query
        test.verify_value_in_list("verzuimfrequentie", {"0", "0.0"})

    finally:
        # Delete previously loaded data
        dg.delete_graph_data()


# Testcase 06a
def test_if_value_returned_is_correct_for_query_2_3_2_06a(db_config):
    """ Testcase 06a (Geen ZVL functie + Wel AOK (PIL) + Wel verzuim (2 aparte weken 36u))
        ODB personeelssamenstelling 2.3.2. Verzuimfrequentie
    """

    # Load defined test data
    dg = DataGenerator(db_config, td_06_a)

    try:
        # Start a test
        test = QueryTest(db_config)

        # Set the query to be tested
        test.set_query('Gevalideerde_vragen_technisch/personeelssamenstelling/Indicator 2.3.2.rq')

        # Change measuring period parameters of query
        test.change_start_period("2023-01-01", "2024-01-01")
        test.change_end_period("2023-12-31", "2024-12-31")

        # Verify actual result of the query
        test.verify_value_in_list("verzuimfrequentie", {"0", "0.0"})

    finally:
        # Delete previously loaded data
        dg.delete_graph_data()

# Testcase 07
def test_if_value_returned_is_correct_for_query_2_3_2_07(db_config):
    """ Testcase 07 (Wel ZVL functie + Geen AOK (PNIL) + Wel verzuim (1 week 36u))
        ODB personeelssamenstelling 2.3.2. Verzuimfrequentie
    """

    # Load defined test data
    dg = DataGenerator(db_config, td_07)

    try:
        # Start a test
        test = QueryTest(db_config)

        # Set the query to be tested
        test.set_query('Gevalideerde_vragen_technisch/personeelssamenstelling/Indicator 2.3.2.rq')

        # Change measuring period parameters of query
        test.change_start_period("2023-01-01", "2024-01-01")
        test.change_end_period("2023-12-31", "2024-12-31")

        # Verify actual result of the query
        test.verify_value_in_list("verzuimfrequentie", {"0", "0.0"})

    finally:
        # Delete previously loaded data
        dg.delete_graph_data()


# Testcase 07a
def test_if_value_returned_is_correct_for_query_2_3_2_07a(db_config):
    """ Testcase 07a (Wel ZVL functie + Geen AOK (PNIL) + Wel verzuim (2 aparte weken 36u))
        ODB personeelssamenstelling 2.3.2. Verzuimfrequentie
    """

    # Load defined test data
    dg = DataGenerator(db_config, td_07_a)

    try:
        # Start a test
        test = QueryTest(db_config)

        # Set the query to be tested
        test.set_query('Gevalideerde_vragen_technisch/personeelssamenstelling/Indicator 2.3.2.rq')

        # Change measuring period parameters of query
        test.change_start_period("2023-01-01", "2024-01-01")
        test.change_end_period("2023-12-31", "2024-12-31")

        # Verify actual result of the query
        test.verify_value_in_list("verzuimfrequentie", {"0", "0.0"})

    finally:
        # Delete previously loaded data
        dg.delete_graph_data()

# Testcase 08
def test_if_value_returned_is_correct_for_query_2_3_2_08(db_config):
    """ Testcase 08 (Wel ZVL functie + Wel AOK (PIL) + Wel verzuim (1 week 36u))
        ODB personeelssamenstelling 2.3.2. Verzuimfrequentie
    """

    # Load defined test data
    dg = DataGenerator(db_config, td_08)

    try:
        # Start a test
        test = QueryTest(db_config)

        # Set the query to be tested
        test.set_query('Gevalideerde_vragen_technisch/personeelssamenstelling/Indicator 2.3.2.rq')

        # Change measuring period parameters of query
        test.change_start_period("2023-01-01", "2024-01-01")
        test.change_end_period("2023-12-31", "2024-12-31")

        # Verify actual result of the query
        test.verify_value_in_list("verzuimfrequentie", {"0.689655172413793103448276", "0.689655172413793103448276"})

    finally:
        # Delete previously loaded data
        dg.delete_graph_data()


# Testcase 08a
def test_if_value_returned_is_correct_for_query_2_3_2_08a(db_config):
    """ Testcase 08a (Wel ZVL functie + Wel AOK (PIL) + Wel verzuim (2 aparte weken 36u))
        ODB personeelssamenstelling 2.3.2. Verzuimfrequentie
    """

    # Load defined test data
    dg = DataGenerator(db_config, td_08_a)

    try:
        # Start a test
        test = QueryTest(db_config)

        # Set the query to be tested
        test.set_query('Gevalideerde_vragen_technisch/personeelssamenstelling/Indicator 2.3.2.rq')

        # Change measuring period parameters of query
        test.change_start_period("2023-01-01", "2024-01-01")
        test.change_end_period("2023-12-31", "2024-12-31")

        # Verify actual result of the query
        test.verify_value_in_list("verzuimfrequentie", {"1.052631578947368421052632", "1.052631578947368421052632"})
        # test.verify_value_in_list("teller", {"20", "20.0"})
        # test.verify_value_in_list("noemer", {"19", "19.0"})
        
    finally:
        # Delete previously loaded data
        dg.delete_graph_data()

# Testcase 08b
def test_if_value_returned_is_correct_for_query_2_3_2_08b(db_config):
    """ Testcase 08b (Wel ZVL functie + Wel AOK (PIL) + Wel verzuim (2 aparte weken 36u) + Zwangerschapsverlof)
        ODB personeelssamenstelling 2.3.2. Verzuimfrequentie
    """

    # Load defined test data
    dg = DataGenerator(db_config, td_08_b)

    try:
        # Start a test
        test = QueryTest(db_config)

        # Set the query to be tested
        test.set_query('Gevalideerde_vragen_technisch/personeelssamenstelling/Indicator 2.3.2.rq')

        # Change measuring period parameters of query
        test.change_start_period("2023-01-01", "2024-01-01")
        test.change_end_period("2023-12-31", "2024-12-31")

        # Verify actual result of the query
        test.verify_value_in_list("verzuimfrequentie", {"1.052631578947368421052632", "1.052631578947368421052632"})
        # test.verify_value_in_list("teller", {"20", "20.0"})
        # test.verify_value_in_list("noemer", {"19", "19.0"})
    finally:
        # Delete previously loaded data
        dg.delete_graph_data()

# Testcase 08c
def test_if_value_returned_is_correct_for_query_2_3_2_08c(db_config):
    """ Testcase 08c (Wel ZVL functie + Wel AOK (PIL) + Wel verzuim (2 aparte weken 36u)) 1 human
        ODB personeelssamenstelling 2.3.2. Verzuimfrequentie
    """

    # Load defined test data
    dg = DataGenerator(db_config, td_08_c)

    try:
        # Start a test
        test = QueryTest(db_config)

        # Set the query to be tested
        test.set_query('Gevalideerde_vragen_technisch/personeelssamenstelling/Indicator 2.3.2.rq')

        # Change measuring period parameters of query
        test.change_start_period("2023-01-01", "2024-01-01")
        test.change_end_period("2023-12-31", "2024-12-31")

        # Verify actual result of the query
        test.verify_value_in_list("verzuimfrequentie", {"0.2", "0.2"})
        
    finally:
        # Delete previously loaded data
        dg.delete_graph_data()

# Testcase 08d
def test_if_value_returned_is_correct_for_query_2_3_2_08d(db_config):
    """ Testcase 08d (Wel ZVL functie + Wel AOK (PIL) + Wel verzuim (4 aparte weken 36u) + Zwangerschapsverlof) 1 human
        ODB personeelssamenstelling 2.3.2. Verzuimfrequentie
    """

    # Load defined test data
    dg = DataGenerator(db_config, td_08_d)

    try:
        # Start a test
        test = QueryTest(db_config)

        # Set the query to be tested
        test.set_query('Gevalideerde_vragen_technisch/personeelssamenstelling/Indicator 2.3.2.rq')

        # Change measuring period parameters of query
        test.change_start_period("2023-01-01", "2024-01-01")
        test.change_end_period("2023-12-31", "2024-12-31")

        # Verify actual result of the query
        test.verify_value_in_list("verzuimfrequentie", {"0.4", "0.4"})
        
    finally:
        # Delete previously loaded data
        dg.delete_graph_data()