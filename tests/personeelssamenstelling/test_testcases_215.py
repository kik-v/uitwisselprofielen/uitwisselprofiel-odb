from DataGenerator import DataGenerator
from QueryTest import QueryTest

# Meetperiode startdatum: 01-01-2024"2024-01-01"
# Meetperiode einddatum: "2024-12-31"

#Opmerkingen:
# PNIL kostensoorten: 
# WBedOvpUik, WBedOvpUit, WBedOvpMaf, WBedOvpZzp, WBedOvpPay, WBedOvpOip, PM418000

# PIL kostensoorten:
# WPerLes, WPerSol, PM411000, PM412000, PM413000, PM414000, PM415000, PM422100, PM422300, PM422400, PM422410, PM422500, PM422600, PM422900


td_01 = [
    {
        "Description": "Testcase 01 (Wel matching rubrieken PNIL + Wel matching rubrieken PIL + Valt wel in meetperiode)",
        "Amount": 1,
        "AccountingItem": [
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "WBedOvpUik" ,
                "financial_entity_value": 10000  
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "WPerLes" ,
                "financial_entity_value": 10000  
            }
        ] 
    }
]

td_01_a = [
    {
        "Description": "Testcase 01a (Wel matching rubrieken PNIL (0.5) + Wel matching rubrieken PIL(1) + Valt wel in meetperiode) ",
        "Amount": 1, #Indicator score: 50%
        "AccountingItem": [
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "WBedOvpUit" ,
                "financial_entity_value": 5000  
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "WPerSol" ,
                "financial_entity_value": 10000  
            }
        ] 
    }
]

td_01_b = [
    {
        "Description": "Testcase 01b (Wel matching rubrieken PNIL (1) + Wel matching rubrieken PIL(0.5) + Valt wel in meetperiode) ",
        "Amount": 1, #Indicator score: 200%
        "AccountingItem": [
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "WBedOvpZzp" ,
                "financial_entity_value": 10000  
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "PM411000" ,
                "financial_entity_value": 5000  
            }
        ] 
    }
]

td_01_c = [
    {
        "Description": "Testcase 01c (Wel matching rubrieken PNIL (0.1) + Wel matching rubrieken PIL(1) + Valt wel in meetperiode) ",
        "Amount": 1, #Indicator score: 10%
        "AccountingItem": [
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "WBedOvpMaf" ,
                "financial_entity_value": 1000  
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "PM412000" ,
                "financial_entity_value": 10000  
            }
        ] 
    }
]

td_01_d = [
    {
        "Description": "Testcase 01d (Wel matching rubrieken PNIL (0.1) + Wel matching rubrieken PIL(1) + Valt wel in meetperiode) ",
        "Amount": 1, #Indicator score: 25%
        "AccountingItem": [
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "WBedOvpPay" ,
                "financial_entity_value": 1000  
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "PM413000" ,
                "financial_entity_value": 10000  
            }
        ] 
    }
]

td_01_e = [
    {
        "Description": "Testcase 01e (Wel matching rubrieken PNIL (0.1) + Wel matching rubrieken PIL(1) + Valt wel in meetperiode) ",
        "Amount": 1, #Indicator score: 66%
        "AccountingItem": [
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "WBedOvpOip" ,
                "financial_entity_value": 8000  
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "PM414000" ,
                "financial_entity_value": 12000  
            }
        ] 
    }
]

td_01_f = [
    {
        "Description": "Testcase 01f (Wel matching rubrieken PNIL (0.1) + Wel matching rubrieken PIL(1) + Valt wel in meetperiode) ",
        "Amount": 1, #Indicator score: 75%
        "AccountingItem": [
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "PM418000" ,
                "financial_entity_value": 7500  
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "PM415000" ,
                "financial_entity_value": 10000
            }
        ] 
    }
]

td_01_g = [
    {
        "Description": "Testcase 01g (Wel matching rubrieken PNIL (0.1) + Wel matching rubrieken PIL(1) + Valt wel in meetperiode) ",
        "Amount": 1, #Indicator score: 90%
        "AccountingItem": [
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "PM418000" ,
                "financial_entity_value": 4500  
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "WBedOvpOip" ,
                "financial_entity_value": 4500  
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "PM422100" ,
                "financial_entity_value": 5000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "PM422300" ,
                "financial_entity_value": 5000
            }            
        ] 
    }
]

td_01_h = [
    {
        "Description": "Testcase 01h (Wel matching rubrieken PNIL (0.1) + Wel matching rubrieken PIL(1) + Valt wel in meetperiode) ",
        "Amount": 1, #Indicator score: 100%
        "AccountingItem": [
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "PM418000" ,
                "financial_entity_value": 9500  
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "PM422400" ,
                "financial_entity_value": 2000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "PM422410" ,
                "financial_entity_value": 2000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "PM422500" ,
                "financial_entity_value": 2000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "PM422600" ,
                "financial_entity_value": 2000
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "PM422900" ,
                "financial_entity_value": 2000
            }           
        ] 
    }
]

td_02 = [
    {
        "Description": "Testcase 02 (Geen matching rubrieken PNIL + Wel matching rubrieken PIL + Valt wel in meetperiode) ",
        "Amount": 1, #Indicator score: 0%
        "AccountingItem": [
        #     {
        #         "costsite": "kp_Grotestraat",
        #         "date": "2024-01-01",
        #         "accounting_item_type": "WBedOvpZzp" ,
        #         "financial_entity_value": 10000  
        #     },
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "WPerSol" ,
                "financial_entity_value": 5000  
            }
        ] 
    }
]

td_03 = [
    {
        "Description": "Testcase 03 (Wel matching rubrieken PNIL + Geen matching rubrieken PIL + Valt wel in meetperiode) ",
        "Amount": 1, #Indicator score: nvt
        "AccountingItem": [
            {
                "costsite": "kp_Grotestraat",
                "date": "2024-01-01",
                "accounting_item_type": "WBedOvpZzp" ,
                "financial_entity_value": 10000  
            },
            # {
            #     "costsite": "kp_Grotestraat",
            #     "date": "2024-01-01",
            #     "accounting_item_type": "WPerSol" ,
            #     "financial_entity_value": 5000  
            # }
        ] 
    }
]

td_04 = [
    {
        "Description": "Testcase 03 (Wel matching rubrieken PNIL + Wel matching rubrieken PIL + Valt niet in meetperiode) ",
        "Amount": 1, #Indicator score:  nvt
        "AccountingItem": [
            {
                "costsite": "kp_Grotestraat",
                "date": "2023-01-01",
                "accounting_item_type": "WBedOvpZzp" ,
                "financial_entity_value": 10000  
            },
            {
                "costsite": "kp_Grotestraat",
                "date": "2023-01-01",
                "accounting_item_type": "WPerSol" ,
                "financial_entity_value": 5000  
            }
        ] 
    }
]


# Static Tests
def test_if_headers_are_correct_for_query_2_1_5(db_config):
    """ Test of de juiste header terugkomt in het resultaat
        ODB Personeelssamenstelling 2.1.5. Percentage kosten uitzendkrachten Personeel Niet In Loondienst (PNIL)
    """

    # Setup of the test
    test = QueryTest(db_config)

    # Configuration and execution
    test.set_query('Gevalideerde_vragen_technisch/personeelssamenstelling/Indicator 2.1.5.rq')

    # Assertions
    test.verify_header_present('kosten_pnil')
    test.verify_header_present('kosten_pil')
    # test.verify_header_present('percentage_pnil_tov_pil')


def test_if_number_of_rows_returned_is_correct_for_query_2_1_5(db_config):
    """ Test of het aantal rijen correct wordt teruggegeven
        ODB Personeelssamenstelling 2.1.5. Percentage kosten uitzendkrachten Personeel Niet In Loondienst (PNIL) 
    """

    # Setup of the test
    test = QueryTest(db_config)

    # Configuration and execution
    test.set_query('Gevalideerde_vragen_technisch/personeelssamenstelling/Indicator 2.1.5.rq')

    # Assertions
    test.verify_row_count(1)


def test_if_indicator_has_correct_value_for_query_2_1_5(db_config):
    """ Test of de indicator de juiste waarde heeft
        ODB Personeelssamenstelling 2.1.5. Percentage kosten uitzendkrachten Personeel Niet In Loondienst (PNIL)
    """

    # Setup of the test
    test = QueryTest(db_config)

    # Configuration and execution
    test.set_query('Gevalideerde_vragen_technisch/personeelssamenstelling/Indicator 2.1.5.rq')

    # Assertions
    test.verify_value("percentage_pnil_tov_pil", None)


def test_if_dates_can_change_2_1_5(db_config):
    """ Test of gewijzigde datum daadwerkelijk een ander resultaat oplevert
        ODB Personeelssamenstelling 2.1.5. Percentage kosten uitzendkrachten Personeel Niet In Loondienst (PNIL)
    """

    # Setup of the test
    test = QueryTest(db_config)

    # Configuration and execution
    test.set_query('Gevalideerde_vragen_technisch/personeelssamenstelling/Indicator 2.1.5.rq')

    test.change_start_period("2023-01-01", "2022-01-01")
    test.change_end_period("2023-12-31", "2022-12-31")

    # Assertions
    test.verify_value("percentage_pnil_tov_pil", "169.600")


# Tests using Generated Data

# Testcase 01
def test_if_value_returned_is_correct_for_query_2_1_5_01(db_config):
    """ Testcase 01 (Wel matching rubrieken PNIL + Wel matching rubrieken PIL + Valt wel in meetperiode)
        ODB personeelssamenstelling 2.1.5. Percentage kosten uitzendkrachten Personeel Niet In Loondienst (PNIL)
    """

    # Load defined test data
    dg = DataGenerator(db_config, td_01)

    try:
        # Start a test
        test = QueryTest(db_config)

        # Set the query to be tested
        test.set_query('Gevalideerde_vragen_technisch/personeelssamenstelling/Indicator 2.1.5.rq')

        # Change measuring period parameters of query
        test.change_start_period("2023-01-01", "2024-01-01")
        test.change_end_period("2023-12-31", "2024-12-31")

        # Verify actual result of the query
        test.verify_value_in_list("percentage_pnil_tov_pil", {"100", "100.0"})

    finally:
        # Delete previously loaded data
        dg.delete_graph_data()

# Testcase 01a
def test_if_value_returned_is_correct_for_query_2_1_5_01_a(db_config):
    """ Testcase 01a (Wel matching rubrieken PNIL + Wel matching rubrieken PIL + Valt wel in meetperiode)
        ODB personeelssamenstelling 2.1.5. Percentage kosten uitzendkrachten Personeel Niet In Loondienst (PNIL)
    """

    # Load defined test data
    dg = DataGenerator(db_config, td_01_a)

    try:
        # Start a test
        test = QueryTest(db_config)

        # Set the query to be tested
        test.set_query('Gevalideerde_vragen_technisch/personeelssamenstelling/Indicator 2.1.5.rq')

        # Change measuring period parameters of query
        test.change_start_period("2023-01-01", "2024-01-01")
        test.change_end_period("2023-12-31", "2024-12-31")

        # Verify actual result of the query
        test.verify_value_in_list("percentage_pnil_tov_pil", {"50", "50.0"})

    finally:
        # Delete previously loaded data
        dg.delete_graph_data()

# Testcase 01b
def test_if_value_returned_is_correct_for_query_2_1_5_01_b(db_config):
    """ Testcase 01b (Wel matching rubrieken PNIL + Wel matching rubrieken PIL + Valt wel in meetperiode)
        ODB personeelssamenstelling 2.1.5. Percentage kosten uitzendkrachten Personeel Niet In Loondienst (PNIL)
    """

    # Load defined test data
    dg = DataGenerator(db_config, td_01_b)

    try:
        # Start a test
        test = QueryTest(db_config)

        # Set the query to be tested
        test.set_query('Gevalideerde_vragen_technisch/personeelssamenstelling/Indicator 2.1.5.rq')

        # Change measuring period parameters of query
        test.change_start_period("2023-01-01", "2024-01-01")
        test.change_end_period("2023-12-31", "2024-12-31")

        # Verify actual result of the query
        test.verify_value_in_list("percentage_pnil_tov_pil", {"200", "200"})

    finally:
        # Delete previously loaded data
        dg.delete_graph_data()

# Testcase 01c
def test_if_value_returned_is_correct_for_query_2_1_5_01_c(db_config):
    """ Testcase 01c (Wel matching rubrieken PNIL + Wel matching rubrieken PIL + Valt wel in meetperiode)
        ODB personeelssamenstelling 2.1.5. Percentage kosten uitzendkrachten Personeel Niet In Loondienst (PNIL)
    """

    # Load defined test data
    dg = DataGenerator(db_config, td_01_c)

    try:
        # Start a test
        test = QueryTest(db_config)

        # Set the query to be tested
        test.set_query('Gevalideerde_vragen_technisch/personeelssamenstelling/Indicator 2.1.5.rq')

        # Change measuring period parameters of query
        test.change_start_period("2023-01-01", "2024-01-01")
        test.change_end_period("2023-12-31", "2024-12-31")

        # Verify actual result of the query
        test.verify_value_in_list("percentage_pnil_tov_pil", {"10.0", "10.0"})

    finally:
        # Delete previously loaded data
        dg.delete_graph_data()

# Testcase 01d
def test_if_value_returned_is_correct_for_query_2_1_5_01_d(db_config):
    """ Testcase 01d (Wel matching rubrieken PNIL + Wel matching rubrieken PIL + Valt wel in meetperiode)
        ODB personeelssamenstelling 2.1.5. Percentage kosten uitzendkrachten Personeel Niet In Loondienst (PNIL)
    """

    # Load defined test data
    dg = DataGenerator(db_config, td_01_d)

    try:
        # Start a test
        test = QueryTest(db_config)

        # Set the query to be tested
        test.set_query('Gevalideerde_vragen_technisch/personeelssamenstelling/Indicator 2.1.5.rq')

        # Change measuring period parameters of query
        test.change_start_period("2023-01-01", "2024-01-01")
        test.change_end_period("2023-12-31", "2024-12-31")

        # Verify actual result of the query
        test.verify_value_in_list("percentage_pnil_tov_pil", {"10.0", "10.0"})

    finally:
        # Delete previously loaded data
        dg.delete_graph_data()

# Testcase 01e
def test_if_value_returned_is_correct_for_query_2_1_5_01_e(db_config):
    """ Testcase 01e (Wel matching rubrieken PNIL + Wel matching rubrieken PIL + Valt wel in meetperiode)
        ODB personeelssamenstelling 2.1.5. Percentage kosten uitzendkrachten Personeel Niet In Loondienst (PNIL)
    """

    # Load defined test data
    dg = DataGenerator(db_config, td_01_e)

    try:
        # Start a test
        test = QueryTest(db_config)

        # Set the query to be tested
        test.set_query('Gevalideerde_vragen_technisch/personeelssamenstelling/Indicator 2.1.5.rq')

        # Change measuring period parameters of query
        test.change_start_period("2023-01-01", "2024-01-01")
        test.change_end_period("2023-12-31", "2024-12-31")

        # Verify actual result of the query
        test.verify_value_in_list("percentage_pnil_tov_pil", {"66.666666666666666666666700", "66.666666666666666666666700"})

    finally:
        # Delete previously loaded data
        dg.delete_graph_data()

# Testcase 01f
def test_if_value_returned_is_correct_for_query_2_1_5_01_f(db_config):
    """ Testcase 01f (Wel matching rubrieken PNIL + Wel matching rubrieken PIL + Valt wel in meetperiode)
        ODB personeelssamenstelling 2.1.5. Percentage kosten uitzendkrachten Personeel Niet In Loondienst (PNIL)
    """

    # Load defined test data
    dg = DataGenerator(db_config, td_01_f)

    try:
        # Start a test
        test = QueryTest(db_config)

        # Set the query to be tested
        test.set_query('Gevalideerde_vragen_technisch/personeelssamenstelling/Indicator 2.1.5.rq')

        # Change measuring period parameters of query
        test.change_start_period("2023-01-01", "2024-01-01")
        test.change_end_period("2023-12-31", "2024-12-31")

        # Verify actual result of the query
        test.verify_value_in_list("percentage_pnil_tov_pil", {"75.00", "75.00"})

    finally:
        # Delete previously loaded data
        dg.delete_graph_data()

# Testcase 01g
def test_if_value_returned_is_correct_for_query_2_1_5_01_g(db_config):
    """ Testcase 01g (Wel matching rubrieken PNIL + Wel matching rubrieken PIL + Valt wel in meetperiode)
        ODB personeelssamenstelling 2.1.5. Percentage kosten uitzendkrachten Personeel Niet In Loondienst (PNIL)
    """

    # Load defined test data
    dg = DataGenerator(db_config, td_01_g)

    try:
        # Start a test
        test = QueryTest(db_config)

        # Set the query to be tested
        test.set_query('Gevalideerde_vragen_technisch/personeelssamenstelling/Indicator 2.1.5.rq')

        # Change measuring period parameters of query
        test.change_start_period("2023-01-01", "2024-01-01")
        test.change_end_period("2023-12-31", "2024-12-31")

        # Verify actual result of the query
        test.verify_value_in_list("percentage_pnil_tov_pil", {"90.0", "90.0"})

    finally:
        # Delete previously loaded data
        dg.delete_graph_data()

# Testcase 01h
def test_if_value_returned_is_correct_for_query_2_1_5_01_h(db_config):
    """ Testcase 01h (Wel matching rubrieken PNIL + Wel matching rubrieken PIL + Valt wel in meetperiode)
        ODB personeelssamenstelling 2.1.5. Percentage kosten uitzendkrachten Personeel Niet In Loondienst (PNIL)
    """

    # Load defined test data
    dg = DataGenerator(db_config, td_01_h)

    try:
        # Start a test
        test = QueryTest(db_config)

        # Set the query to be tested
        test.set_query('Gevalideerde_vragen_technisch/personeelssamenstelling/Indicator 2.1.5.rq')

        # Change measuring period parameters of query
        test.change_start_period("2023-01-01", "2024-01-01")
        test.change_end_period("2023-12-31", "2024-12-31")

        # Verify actual result of the query
        test.verify_value_in_list("percentage_pnil_tov_pil", {"95.00", "95.0"})

    finally:
        # Delete previously loaded data
        dg.delete_graph_data()

# Testcase 02
def test_if_value_returned_is_correct_for_query_2_1_5_02(db_config):
    """ Testcase 02 (Geen matching rubrieken PNIL + Wel matching rubrieken PIL + Valt wel in meetperiode)
        ODB personeelssamenstelling 2.1.5. Percentage kosten uitzendkrachten Personeel Niet In Loondienst (PNIL)
    """

    # Load defined test data
    dg = DataGenerator(db_config, td_02)

    try:
        # Start a test
        test = QueryTest(db_config)

        # Set the query to be tested
        test.set_query('Gevalideerde_vragen_technisch/personeelssamenstelling/Indicator 2.1.5.rq')

        # Change measuring period parameters of query
        test.change_start_period("2023-01-01", "2024-01-01")
        test.change_end_period("2023-12-31", "2024-12-31")

        # Verify actual result of the query
        test.verify_value_in_list("percentage_pnil_tov_pil", {"0", "0"})

    finally:
        # Delete previously loaded data
        dg.delete_graph_data()


# Testcase 03
def test_if_value_returned_is_correct_for_query_2_1_5_03(db_config):
    """ Testcase 03 (Wel matching rubrieken PNIL + Geen matching rubrieken PIL + Valt wel in meetperiode)
        ODB personeelssamenstelling 2.1.5. Percentage kosten uitzendkrachten Personeel Niet In Loondienst (PNIL)
    """

    # Load defined test data
    dg = DataGenerator(db_config, td_03)

    try:
        # Start a test
        test = QueryTest(db_config)

        # Set the query to be tested
        test.set_query('Gevalideerde_vragen_technisch/personeelssamenstelling/Indicator 2.1.5.rq')

        # Change measuring period parameters of query
        test.change_start_period("2023-01-01", "2024-01-01")
        test.change_end_period("2023-12-31", "2024-12-31")

        # Verify actual result of the query
        test.verify_value("percentage_pnil_tov_pil", None)

    finally:
        # Delete previously loaded data
        dg.delete_graph_data()


# Testcase 04
def test_if_value_returned_is_correct_for_query_2_1_5_04(db_config):
    """ Testcase 04 (Wel matching rubrieken PNIL + Wel matching rubrieken PIL + Valt niet in meetperiode)
        ODB personeelssamenstelling 2.1.5. Percentage kosten uitzendkrachten Personeel Niet In Loondienst (PNIL)
    """

    # Load defined test data
    dg = DataGenerator(db_config, td_04)

    try:
        # Start a test
        test = QueryTest(db_config)

        # Set the query to be tested
        test.set_query('Gevalideerde_vragen_technisch/personeelssamenstelling/Indicator 2.1.5.rq')

        # Change measuring period parameters of query
        test.change_start_period("2023-01-01", "2024-01-01")
        test.change_end_period("2023-12-31", "2024-12-31")

        # Verify actual result of the query
        test.verify_value("percentage_pnil_tov_pil", None)

    finally:
        # Delete previously loaded data
        dg.delete_graph_data()