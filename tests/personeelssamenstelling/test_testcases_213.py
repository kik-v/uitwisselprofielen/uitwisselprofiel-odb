from DataGenerator import DataGenerator
from QueryTest import QueryTest

# Peildatum: 2024-07-01

#Opmerkingen:
# 

#Testcases 

td_01 = [
    {
        "Description": "Testcase 01 (Geen ZVL functie + Geen AOK)",
        "Amount": 10, #Indicator score: 0 
        "Human": [
            {
                "InhuurOvereenkomst": [
                    {
                        "function": [
                            {
                                "label": "INH OK Geen ZVL",
                                "caregiving_role": False,
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31",
                            }
                        ],
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",                        
                        "size": [
                             {
                                "unit": 36,
                                "unit_of_measure": "Uren_per_week_unit",
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31"
                             }
                        ]
                    }
                ]
            }
        ]
    }
]   

td_02 = [
    {
        "Description": "Testcase 02 (Geen ZVL functie + Wel AOK)",
        "Amount": 10, #Indicator score: 0
        "Human": [
            {
                "ArbeidsOvereenkomst": [
                    {
                        "function": [
                            {
                                "label": "AOK Geen ZVL",
                                "caregiving_role": False,
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31",
                            }
                        ],
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",                        
                        "size": [
                             {
                                "unit": 36,
                                "unit_of_measure": "Uren_per_week_unit",
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31"
                             }
                        ]
                    }
                ]
            }
        ]
    }
]   

td_03 = [ 
    {
        "Description": "Testcase 03 (Wel ZVL functie + Geen AOK - bijvoorbeeld Inhuur/uitzend/stage OK)",
        "Amount": 10, #Indicator score: 0
        "Human": [
            {
                "InhuurOvereenkomst": [
                    {
                        "function": [
                            {
                                "label": "INH OK Wel ZVL",
                                "caregiving_role": True,
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31",
                            }
                        ],
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",                        
                        "size": [
                             {
                                "unit": 36,
                                "unit_of_measure": "Uren_per_week_unit",
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31"
                             }
                        ]
                    }
                ]
            }
        ]
    }
]   

td_04 = [
    {
        "Description": "Testcase 04 (Wel ZVL functie + Wel AOK Bepaaldetijd (12m))",
        "Amount": 10, #Indicator score: 52.631578947368421052631579 (10/19)
        "Human": [
            {
                "ArbeidsOvereenkomstBepaaldeTijd": [
                    {
                        "function": [
                            {
                                "label": "AOK BPT Wel ZVL",
                                "caregiving_role": True,
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31",
                            }
                        ],
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",                        
                        "size": [
                             {
                                "unit": 36,
                                "unit_of_measure": "Uren_per_week_unit",
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31"
                             }
                        ]
                    }
                ]
            }
        ]
    }
]   

td_04_a = [ 
    {
        "Description": "Testcase 04a (Wel ZVL functie + Wel AOK Onbepaaldetijd (12m))",
        "Amount": 10, #Indicator score: 0 (0/19)
        "Human": [
            {
                "ArbeidsOvereenkomstOnbepaaldeTijd": [
                    {
                        "function": [
                            {
                                "label": "AOK OBPT Wel ZVL",
                                "caregiving_role": True,
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31",
                            }
                        ],
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",                        
                        "size": [
                             {
                                "unit": 36,
                                "unit_of_measure": "Uren_per_week_unit",
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31"
                             }
                        ]
                    }
                ]
            }
        ]
    }
]   

td_04_b = [
    {
        "Description": "Testcase 04b (Wel ZVL functie + Wel AOK Bepaaldetijd (12m, 18u p/w))",
        "Amount": 10, #Indicator score: 52.631578947368421052631579 (10/19)
        "Human": [
            {
                "ArbeidsOvereenkomstBepaaldeTijd": [
                    {
                        "function": [
                            {
                                "label": "AOK BPT Wel ZVL",
                                "caregiving_role": True,
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31",
                            }
                        ],
                        "start_date": "2024-01-01",
                        "end_date": "2024-12-31",                        
                        "size": [
                             {
                                "unit": 18,
                                "unit_of_measure": "Uren_per_week_unit",
                                "start_date": "2024-01-01",
                                "end_date": "2024-12-31"
                             }
                        ]
                    }
                ]
            }
        ]
    }
]   

td_05 = [
    {
        "Description": "Testcase 05 (Wel ZVL functie + Wel AOK Bepaaldetijd (6m))",
        "Amount": 10, #Indicator score: 0 (Peildatum niet in AOK)
        "Human": [
            {
                "ArbeidsOvereenkomstBepaaldeTijd": [
                    {
                        "function": [
                            {
                                "label": "AOK BPT Wel ZVL",
                                "caregiving_role": True,
                                "start_date": "2024-01-01",
                                "end_date": "2024-06-30",
                            }
                        ],
                        "start_date": "2024-01-01",
                        "end_date": "2024-06-30",                        
                        "size": [
                             {
                                "unit": 36,
                                "unit_of_measure": "Uren_per_week_unit",
                                "start_date": "2024-01-01",
                                "end_date": "2024-06-30"
                             }
                        ]
                    }
                ]
            }
        ]
    }
]  

td_05_a = [
    {
        "Description": "Testcase 05a (Wel ZVL functie + Wel AOK Onbepaaldetijd (6m))",
        "Amount": 10, #Indicator score: 0 (Peildatum niet in AOK)
        "Human": [
            {
                "ArbeidsOvereenkomstOnbepaaldeTijd": [
                    {
                        "function": [
                            {
                                "label": "AOK BPT Wel ZVL",
                                "caregiving_role": True,
                                "start_date": "2024-01-01",
                                "end_date": "2024-06-30",
                            }
                        ],
                        "start_date": "2024-01-01",
                        "end_date": "2024-06-30",                        
                        "size": [
                             {
                                "unit": 36,
                                "unit_of_measure": "Uren_per_week_unit",
                                "start_date": "2024-01-01",
                                "end_date": "2024-06-30"
                             }
                        ]
                    }
                ]
            }
        ]
    }
] 

td_05_b = [
    {
        "Description": "Testcase 05b (Wel ZVL functie + Wel AOK Bepaaldetijd (6m))",
        "Amount": 10, #Indicator score: 52.631578947368421052631579
        "Human": [
            {
                "ArbeidsOvereenkomstBepaaldeTijd": [
                    {
                        "function": [
                            {
                                "label": "AOK BPT Wel ZVL",
                                "caregiving_role": True,
                                "start_date": "2024-07-01",
                                "end_date": "2024-12-31",
                            }
                        ],
                        "start_date": "2024-07-01",
                        "end_date": "2024-12-31",                        
                        "size": [
                             {
                                "unit": 36,
                                "unit_of_measure": "Uren_per_week_unit",
                                "start_date": "2024-07-01",
                                "end_date": "2024-12-31"
                             }
                        ]
                    }
                ]
            }
        ]
    }
] 

td_05_c = [
    {
        "Description": "Testcase 05c (Wel ZVL functie + Wel AOK Bepaaldetijd (6m))",
        "Amount": 9, # Indicator score: 50 (9/18)
        "Human": [
            {
                "ArbeidsOvereenkomstBepaaldeTijd": [
                    {
                        "function": [
                            {
                                "label": "AOK BPT Wel ZVL",
                                "caregiving_role": True,
                                "start_date": "2024-07-01",
                                "end_date": "2024-12-31",
                            }
                        ],
                        "start_date": "2024-07-01",
                        "end_date": "2024-12-31",                        
                        "size": [
                             {
                                "unit": 36,
                                "unit_of_measure": "Uren_per_week_unit",
                                "start_date": "2024-07-01",
                                "end_date": "2024-12-31"
                             }
                        ]
                    }
                ]
            }
        ]
    }
] 

td_06 = [
    {
        "Description": "Testcase 06 (Wel ZVL functie + Geen ZVL functie + Wel AOK (6m))",
        "Amount": 10, # Indicator score: 0 (BPT AOK niet op peildatum)
        "Human": [
            {
                "ArbeidsOvereenkomstBepaaldeTijd": [
                    {
                        "function": [
                            {
                                "label": "AOK BPT Wel ZVL",
                                "caregiving_role": True,
                                "start_date": "2024-01-01",
                                "end_date": "2024-06-30",
                            }
                        ],
                        "start_date": "2024-01-01",
                        "end_date": "2024-06-30",                        
                        "size": [
                             {
                                "unit": 36,
                                "unit_of_measure": "Uren_per_week_unit",
                                "start_date": "2024-01-01",
                                "end_date": "2024-06-30"
                             }
                        ]
                    },
                    {
                        "function": [
                            {
                                "label": "AOK BPT Geen ZVL",
                                "caregiving_role": False,
                                "start_date": "2024-07-01",
                                "end_date": "2024-12-31",
                            }
                        ],
                        "start_date": "2024-07-01",
                        "end_date": "2024-12-31",                        
                        "size": [
                             {
                                "unit": 36,
                                "unit_of_measure": "Uren_per_week_unit",
                                "start_date": "2024-07-01",
                                "end_date": "2024-12-31"
                             }
                        ]
                    }
                ]
            }
        ]
    }
]  

td_07 = [
    {
        "Description": "Testcase 7 (Wel ZVL functie + Wel AOK (12m) - 2 opeenvolgende Bepaaldetijd)",
        "Amount": 20, # Indicator score: 68.965517241379310344827586 (20/29)
        "Human": [
            {
                "ArbeidsOvereenkomstBepaaldeTijd": [
                    {
                        "function": [
                            {
                                "label": "AOK BPT Wel ZVL",
                                "caregiving_role": True,
                                "start_date": "2024-01-01",
                                "end_date": "2024-06-30",
                            }
                        ],
                        "start_date": "2024-01-01",
                        "end_date": "2024-06-30",                        
                        "size": [
                             {
                                "unit": 36,
                                "unit_of_measure": "Uren_per_week_unit",
                                "start_date": "2024-01-01",
                                "end_date": "2024-06-30"
                             }
                        ]
                    },
                    {
                        "function": [
                            {
                                "label": "AOK BPT Wel ZVL",
                                "caregiving_role": True,
                                "start_date": "2024-07-01",
                                "end_date": "2024-12-31",
                            }
                        ],
                        "start_date": "2024-07-01",
                        "end_date": "2024-12-31",                        
                        "size": [
                             {
                                "unit": 36,
                                "unit_of_measure": "Uren_per_week_unit",
                                "start_date": "2024-07-01",
                                "end_date": "2024-12-31"
                             }
                        ]
                    }
                ]
            }
        ]
    }
] 


td_07_a = [
    {
        "Description": "Testcase 07a (Wel ZVL functie + Wel AOK (12m) - 2 opeenvolgende Bepaaldetijd + Onbepaaldetijd)",
        "Amount": 10, # Indicator score: 0 (Peildatum valt tijdens onbepaaldetijd AOK)
        "Human": [
            {
                "ArbeidsOvereenkomstBepaaldeTijd": [
                    {
                        "function": [
                            {
                                "label": "AOK BPT Wel ZVL",
                                "caregiving_role": True,
                                "start_date": "2024-01-01",
                                "end_date": "2024-06-30",
                            }
                        ],
                        "start_date": "2024-01-01",
                        "end_date": "2024-06-30",                        
                        "size": [
                             {
                                "unit": 36,
                                "unit_of_measure": "Uren_per_week_unit",
                                "start_date": "2024-01-01",
                                "end_date": "2024-06-30"
                             }
                        ]
                    },
                ],
                "ArbeidsOvereenkomstOnbepaaldeTijd": [
                    {
                        "function": [
                            {
                                "label": "AOK OBPT Wel ZVL",
                                "caregiving_role": True,
                                "start_date": "2024-07-01",
                                "end_date": "2024-12-31",
                            }
                        ],
                        "start_date": "2024-07-01",
                        "end_date": "2024-12-31",                        
                        "size": [
                             {
                                "unit": 36,
                                "unit_of_measure": "Uren_per_week_unit",
                                "start_date": "2024-07-01",
                                "end_date": "2024-12-31"
                             }
                        ]
                    }
                ]
            }
        ]
    }
] 

td_07_b = [
    {
        "Description": "Testcase 07b (Wel ZVL functie + Wel AOK (12m) - 2 opeenvolgende Onbepaaldetijd + Bepaaldetijd)",
        "Amount": 10, # Indicator score: 52.631578947368421052631579 (10/19)
        "Human": [
            {
                "ArbeidsOvereenkomstOnbepaaldeTijd": [
                    {
                        "function": [
                            {
                                "label": "AOK OBPT Wel ZVL",
                                "caregiving_role": True,
                                "start_date": "2024-01-01",
                                "end_date": "2024-06-30",
                            }
                        ],
                        "start_date": "2024-01-01",
                        "end_date": "2024-06-30",                        
                        "size": [
                             {
                                "unit": 36,
                                "unit_of_measure": "Uren_per_week_unit",
                                "start_date": "2024-01-01",
                                "end_date": "2024-06-30"
                             }
                        ]
                    },
                ],
                "ArbeidsOvereenkomstBepaaldeTijd": [
                    {
                        "function": [
                            {
                                "label": "AOK BPT Wel ZVL",
                                "caregiving_role": True,
                                "start_date": "2024-07-01",
                                "end_date": "2024-12-31",
                            }
                        ],
                        "start_date": "2024-07-01",
                        "end_date": "2024-12-31",                        
                        "size": [
                             {
                                "unit": 36,
                                "unit_of_measure": "Uren_per_week_unit",
                                "start_date": "2024-07-01",
                                "end_date": "2024-12-31"
                             }
                        ]
                    }
                ]
            }
        ]
    }
] 

td_07_c = [
    {
        "Description": "Testcase 07c (Wel ZVL functie + Wel AOK (12m) - 2 opeenvolgende Onbepaaldetijd)",
        "Amount": 10, # Indicator score: 0
        "Human": [
            {
                "ArbeidsOvereenkomstOnbepaaldeTijd": [
                    {
                        "function": [
                            {
                                "label": "AOK OBPT Wel ZVL",
                                "caregiving_role": True,
                                "start_date": "2024-01-01",
                                "end_date": "2024-06-30",
                            }
                        ],
                        "start_date": "2024-01-01",
                        "end_date": "2024-06-30",                        
                        "size": [
                             {
                                "unit": 36,
                                "unit_of_measure": "Uren_per_week_unit",
                                "start_date": "2024-01-01",
                                "end_date": "2024-06-30"
                             }
                        ]
                    },
                ],
                "ArbeidsOvereenkomstOnbepaaldeTijd": [
                    {
                        "function": [
                            {
                                "label": "AOK OBPT Wel ZVL",
                                "caregiving_role": True,
                                "start_date": "2024-07-01",
                                "end_date": "2024-12-31",
                            }
                        ],
                        "start_date": "2024-07-01",
                        "end_date": "2024-12-31",                        
                        "size": [
                             {
                                "unit": 36,
                                "unit_of_measure": "Uren_per_week_unit",
                                "start_date": "2024-07-01",
                                "end_date": "2024-12-31"
                             }
                        ]
                    }
                ]
            }
        ]
    }
] 

td_07_d = [
    {
        "Description": "Testcase 7d (Wel ZVL functie + Wel AOK (12m) - 2 opeenvolgende Bepaaldetijd)",
        "Amount": 1, # Indicator score: 10 (1/10 * 100)
        "Human": [
            {
                "ArbeidsOvereenkomstBepaaldeTijd": [
                    {
                        "function": [
                            {
                                "label": "AOK OBPT Wel ZVL",
                                "caregiving_role": True,
                                "start_date": "2024-01-01",
                                "end_date": "2024-06-30",
                            }
                        ],
                        "start_date": "2024-01-01",
                        "end_date": "2024-06-30",                        
                        "size": [
                             {
                                "unit": 36,
                                "unit_of_measure": "Uren_per_week_unit",
                                "start_date": "2024-01-01",
                                "end_date": "2024-06-30"
                             }
                        ]
                    },
                ],
                "ArbeidsOvereenkomstBepaaldeTijd": [
                    {
                        "function": [
                            {
                                "label": "AOK BPT Wel ZVL",
                                "caregiving_role": True,
                                "start_date": "2024-07-01",
                                "end_date": "2024-12-31",
                            }
                        ],
                        "start_date": "2024-07-01",
                        "end_date": "2024-12-31",                        
                        "size": [
                             {
                                "unit": 36,
                                "unit_of_measure": "Uren_per_week_unit",
                                "start_date": "2024-07-01",
                                "end_date": "2024-12-31"
                             }
                        ]
                    }
                ]
            }
        ]
    }
] 
            
#Static Tests
def test_if_headers_are_correct_for_query_2_1_3(db_config):
    """ Test of de juiste header terugkomt in het resultaat
        ODB personeelssamenstelling 2.1.3. Percentage personeel met een arbeidsovereenkomst voor bepaalde tijd
    """

    # Setup of the test
    test = QueryTest(db_config)

    # Configuration and execution
    test.set_query('Gevalideerde_vragen_technisch/personeelssamenstelling/Indicator 2.1.3.rq')

    # Assertions
    test.verify_header_present('teller')
    test.verify_header_present('noemer')
    test.verify_header_present('indicator')


def test_if_number_of_rows_returned_is_correct_for_query_2_1_3(db_config):
    """ Test of het aantal rijen correct wordt teruggegeven
        ODB personeelssamenstelling 2.1.3. Percentage personeel met een arbeidsovereenkomst voor bepaalde tijd
    """

    # Setup of the test
    test = QueryTest(db_config)

    # Configuration and execution
    test.set_query('Gevalideerde_vragen_technisch/personeelssamenstelling/Indicator 2.1.3.rq')

    # Assertions
    test.verify_row_count(1)


def test_if_indicator_has_correct_value_for_query_2_1_3(db_config):
    """ Test of de indicator de juiste waarde heeft
        ODB personeelssamenstelling 2.1.3. Percentage personeel met een arbeidsovereenkomst voor bepaalde tijd
    """

    # Setup of the test
    test = QueryTest(db_config)

    # Configuration and execution
    test.set_query('Gevalideerde_vragen_technisch/personeelssamenstelling/Indicator 2.1.3.rq')

    # Assertions
    test.verify_value("indicator","0")
    # test.verify_value("teller","0")
    # test.verify_value("noemer","9")


def test_if_dates_can_change_2_1_3(db_config):
    """ Test of gewijzigde datum daadwerkelijk een ander resultaat oplevert
        ODB personeelssamenstelling 2.1.3. Percentage personeel met een arbeidsovereenkomst voor bepaalde tijd
    """

    # Setup of the test
    test = QueryTest(db_config)

    # Configuration and execution
    test.set_query('Gevalideerde_vragen_technisch/personeelssamenstelling/Indicator 2.1.3.rq')

    test.change_reference_date_to("2023-12-31", "2022-12-31")

    # Assertions
    test.verify_value("indicator","0")
    # test.verify_value("teller","0")
    # test.verify_value("noemer","8")


# Tests using Generated Data

# Testcase 01
def test_if_value_returned_is_correct_for_query_2_1_3_01(db_config):
    """ Testcase 01 (Geen ZVL functie + Geen AOK)
        ODB personeelssamenstelling 2.1.3. Percentage personeel met een arbeidsovereenkomst voor bepaalde tijd
    """

    # Load defined test data
    dg = DataGenerator(db_config, td_01)

    try:
        # Start a test
        test = QueryTest(db_config)

        # Set the query to be tested
        test.set_query('Gevalideerde_vragen_technisch/personeelssamenstelling/Indicator 2.1.3.rq')

        # Change measuring period parameters of query
        test.change_reference_date_to("2023-12-31", "2024-12-31")

        # Verify actual result of the query
        test.verify_value_in_list("indicator", {"0", "0.0"})

    finally:
        # Delete previously loaded data
        dg.delete_graph_data()


# Testcase 02
def test_if_value_returned_is_correct_for_query_2_1_3_02(db_config):
    """ Testcase 02 (Geen ZVL functie + Wel AOK)
        ODB personeelssamenstelling 2.1.3 Percentage personeel met een arbeidsovereenkomst voor bepaalde tijd
    """

    # Load defined test data
    dg = DataGenerator(db_config, td_02)

    try:
        # Start a test
        test = QueryTest(db_config)

        # Set the query to be tested
        test.set_query('Gevalideerde_vragen_technisch/personeelssamenstelling/Indicator 2.1.3.rq')

        # Change measuring period parameters of query
        test.change_reference_date_to("2023-12-31", "2024-12-31")

        # Verify actual result of the query
        test.verify_value_in_list("indicator", {"0", "0.0"})

    finally:
        # Delete previously loaded data
        dg.delete_graph_data()


# Testcase 03
def test_if_value_returned_is_correct_for_query_2_1_3_03(db_config):
    """ Testcase 03 (Wel ZVL functie + Geen AOK)
        ODB personeelssamenstelling 2.1.3 Percentage personeel met een arbeidsovereenkomst voor bepaalde tijd
    """

    # Load defined test data
    dg = DataGenerator(db_config, td_03)

    try:
        # Start a test
        test = QueryTest(db_config)

        # Set the query to be tested
        test.set_query('Gevalideerde_vragen_technisch/personeelssamenstelling/Indicator 2.1.3.rq')

        # Change measuring period parameters of query
        test.change_reference_date_to("2023-12-31", "2024-12-31")

        # Verify actual result of the query
        test.verify_value_in_list("indicator", {"0", "0.0"})

    finally:
        # Delete previously loaded data
        dg.delete_graph_data()


# Testcase 04
def test_if_value_returned_is_correct_for_query_2_1_3_04(db_config):
    """ Testcase 04 (Wel ZVL functie + Wel AOK (12m))
        ODB personeelssamenstelling 2.1.3 Percentage personeel met een arbeidsovereenkomst voor bepaalde tijd
    """

    # Load defined test data
    dg = DataGenerator(db_config, td_04)

    try:
        # Start a test
        test = QueryTest(db_config)

        # Set the query to be tested
        test.set_query('Gevalideerde_vragen_technisch/personeelssamenstelling/Indicator 2.1.3.rq')

        # Change measuring period parameters of query
        test.change_reference_date_to("2023-12-31", "2024-12-31")

        # Verify actual result of the query
        test.verify_value("indicator", "52.631578947368421052631579")

    finally:
        # Delete previously loaded data
        dg.delete_graph_data()


# Testcase 04_a
def test_if_value_returned_is_correct_for_query_2_1_3_04a(db_config):
    """ Testcase 04a (Wel ZVL functie + Wel AOK (12m, 32u p/w))
        ODB personeelssamenstelling 2.1.3. Aantal fte
    """

    # Load defined test data
    dg = DataGenerator(db_config, td_04_a)

    try:
        # Start a test
        test = QueryTest(db_config)

        # Set the query to be tested
        test.set_query('Gevalideerde_vragen_technisch/personeelssamenstelling/Indicator 2.1.3.rq')

        # Change measuring period parameters of query
        test.change_reference_date_to("2023-12-31", "2024-12-31")

        # Verify actual result of the query
        test.verify_value_in_list("indicator", {"0","0.0"})

    finally:
        # Delete previously loaded data
        dg.delete_graph_data()


# Testcase 04_b
def test_if_value_returned_is_correct_for_query_2_1_3_04b(db_config):
    """ Testcase 04b (Wel ZVL functie + Wel AOK (12m, 18u p/w))",
        ODB personeelssamenstelling 2.1.3. Percentage personeel met een arbeidsovereenkomst voor bepaalde tijd
    """

    # Load defined test data
    dg = DataGenerator(db_config, td_04_b)

    try:
        # Start a test
        test = QueryTest(db_config)

        # Set the query to be tested
        test.set_query('Gevalideerde_vragen_technisch/personeelssamenstelling/Indicator 2.1.3.rq')

        # Change measuring period parameters of query
        test.change_reference_date_to("2023-12-31", "2024-12-31")

        # Verify actual result of the query
        test.verify_value("indicator", "52.631578947368421052631579")

    finally:
        # Delete previously loaded data
        dg.delete_graph_data()

# Testcase 05
def test_if_value_returned_is_correct_for_query_2_1_3_05(db_config):
    """ Testcase 05 (Wel ZVL functie + Wel AOK Bepaaldetijd(6m))
        ODB personeelssamenstelling 2.1.3. Percentage personeel met een arbeidsovereenkomst voor bepaalde tijd
    """

    # Load defined test data
    dg = DataGenerator(db_config, td_05)

    try:
        # Start a test
        test = QueryTest(db_config)

        # Set the query to be tested
        test.set_query('Gevalideerde_vragen_technisch/personeelssamenstelling/Indicator 2.1.3.rq')

        # Change measuring period parameters of query
        test.change_reference_date_to("2023-12-31", "2024-12-31")

        # Verify actual result of the query
        test.verify_value_in_list("indicator", {"0", "0.0"}) # Peildatum valt niet tijdens AOK tijdsbestrek

    finally:
        # Delete previously loaded data
        dg.delete_graph_data()

# Testcase 05a
def test_if_value_returned_is_correct_for_query_2_1_3_05_a(db_config):
    """ Testcase 05a (Wel ZVL functie + Wel AOK Bepaaldetijd(6m))
        ODB personeelssamenstelling 2.1.3. Percentage personeel met een arbeidsovereenkomst voor bepaalde tijd
    """

    # Load defined test data
    dg = DataGenerator(db_config, td_05_a)

    try:
        # Start a test
        test = QueryTest(db_config)

        # Set the query to be tested
        test.set_query('Gevalideerde_vragen_technisch/personeelssamenstelling/Indicator 2.1.3.rq')

        # Change measuring period parameters of query
        test.change_reference_date_to("2023-12-31", "2024-12-31")

        # Verify actual result of the query
        test.verify_value_in_list("indicator", {"0", "0.0"})

    finally:
        # Delete previously loaded data
        dg.delete_graph_data()

# Testcase 05b
def test_if_value_returned_is_correct_for_query_2_1_3_05_b(db_config):
    """ Testcase 05b (Wel ZVL functie + Wel AOK Bepaaldetijd(6m))
        ODB personeelssamenstelling 2.1.3. Percentage personeel met een arbeidsovereenkomst voor bepaalde tijd
    """

    # Load defined test data
    dg = DataGenerator(db_config, td_05_b)

    try:
        # Start a test
        test = QueryTest(db_config)

        # Set the query to be tested
        test.set_query('Gevalideerde_vragen_technisch/personeelssamenstelling/Indicator 2.1.3.rq')

        # Change measuring period parameters of query
        test.change_reference_date_to("2023-12-31", "2024-12-31")

        # Verify actual result of the query
        test.verify_value("indicator", "52.631578947368421052631579") 

    finally:
        # Delete previously loaded data
        dg.delete_graph_data()

# Testcase 05c
def test_if_value_returned_is_correct_for_query_2_1_3_05_c(db_config):
    """ Testcase 05c (Wel ZVL functie + Wel AOK Bepaaldetijd(6m))
        ODB personeelssamenstelling 2.1.3. Percentage personeel met een arbeidsovereenkomst voor bepaalde tijd
    """

    # Load defined test data
    dg = DataGenerator(db_config, td_05_c)

    try:
        # Start a test
        test = QueryTest(db_config)

        # Set the query to be tested
        test.set_query('Gevalideerde_vragen_technisch/personeelssamenstelling/Indicator 2.1.3.rq')

        # Change measuring period parameters of query
        test.change_reference_date_to("2023-12-31", "2024-12-31")

        # Verify actual result of the query
        test.verify_value_in_list("indicator", {"50", "50.0"})

    finally:
        # Delete previously loaded data
        dg.delete_graph_data()

# Testcase 06
def test_if_value_returned_is_correct_for_query_2_1_3_06(db_config):
    """ Testcase 06 (Wel ZVL functie + Geen ZVL functie + Wel AOK (6m))
        ODB personeelssamenstelling 2.1.3. Percentage personeel met een arbeidsovereenkomst voor bepaalde tijd
    """

    # Load defined test data
    dg = DataGenerator(db_config, td_06)

    try:
        # Start a test
        test = QueryTest(db_config)

        # Set the query to be tested
        test.set_query('Gevalideerde_vragen_technisch/personeelssamenstelling/Indicator 2.1.3.rq')

        # Change measuring period parameters of query
        test.change_reference_date_to("2023-12-31", "2024-12-31")

        # Verify actual result of the query
        test.verify_value_in_list("indicator", {"0","0.0"})

    finally:
        # Delete previously loaded data
        dg.delete_graph_data()


# Testcase 07
def test_if_value_returned_is_correct_for_query_2_1_3_07(db_config):
    """ Testcase 07 (Wel ZVL functie + Wel AOK (12m) - 2 opeenvolgende Bepaaldetijd)
        ODB personeelssamenstelling 2.1.3. Percentage personeel met een arbeidsovereenkomst voor bepaalde tijd
    """

    # Load defined test data
    dg = DataGenerator(db_config, td_07)

    try:
        # Start a test
        test = QueryTest(db_config)

        # Set the query to be tested
        test.set_query('Gevalideerde_vragen_technisch/personeelssamenstelling/Indicator 2.1.3.rq')

        # Change measuring period parameters of query
        test.change_reference_date_to("2023-12-31", "2024-12-31")

        # Verify actual result of the query
        test.verify_value("indicator", "68.965517241379310344827586")

    finally:
        # Delete previously loaded data
        dg.delete_graph_data()

# Testcase 07a
def test_if_value_returned_is_correct_for_query_2_1_3_07_a(db_config):
    """ Testcase 07a (Wel ZVL functie + Wel AOK (12m) - 2 opeenvolgende Bepaaldetijd + Onbepaaldetijd)
        ODB personeelssamenstelling 2.1.3. Percentage personeel met een arbeidsovereenkomst voor bepaalde tijd
    """

    # Load defined test data
    dg = DataGenerator(db_config, td_07_a)

    try:
        # Start a test
        test = QueryTest(db_config)

        # Set the query to be tested
        test.set_query('Gevalideerde_vragen_technisch/personeelssamenstelling/Indicator 2.1.3.rq')

        # Change measuring period parameters of query
        test.change_reference_date_to("2023-12-31", "2024-12-31")

        # Verify actual result of the query
        test.verify_value_in_list("indicator", {"0","0.0"})

    finally:
        # Delete previously loaded data
        dg.delete_graph_data()

# Testcase 07b
def test_if_value_returned_is_correct_for_query_2_1_3_07_b(db_config):
    """ Testcase 07b (Wel ZVL functie + Wel AOK (12m) - 2 opeenvolgende Onbepaaldetijd + Bepaaldetijd)
        ODB personeelssamenstelling 2.1.3. Percentage personeel met een arbeidsovereenkomst voor bepaalde tijd
    """

    # Load defined test data
    dg = DataGenerator(db_config, td_07_b)

    try:
        # Start a test
        test = QueryTest(db_config)

        # Set the query to be tested
        test.set_query('Gevalideerde_vragen_technisch/personeelssamenstelling/Indicator 2.1.3.rq')

        # Change measuring period parameters of query
        test.change_reference_date_to("2023-12-31", "2024-12-31")

        # Verify actual result of the query
        test.verify_value("indicator", "52.631578947368421052631579")

    finally:
        # Delete previously loaded data
        dg.delete_graph_data()

# Testcase 07c
def test_if_value_returned_is_correct_for_query_2_1_3_07_c(db_config):
    """ Testcase 07c (Wel ZVL functie + Wel AOK (12m) - 2 opeenvolgende Onbepaaldetijd)
        ODB personeelssamenstelling 2.1.3. Percentage personeel met een arbeidsovereenkomst voor bepaalde tijd
    """

    # Load defined test data
    dg = DataGenerator(db_config, td_07_c)

    try:
        # Start a test
        test = QueryTest(db_config)

        # Set the query to be tested
        test.set_query('Gevalideerde_vragen_technisch/personeelssamenstelling/Indicator 2.1.3.rq')

        # Change measuring period parameters of query
        test.change_reference_date_to("2023-12-31", "2024-12-31")

        # Verify actual result of the query
        test.verify_value_in_list("indicator", {"0","0.0"})

    finally:
        # Delete previously loaded data
        dg.delete_graph_data()

# Testcase 07d
def test_if_value_returned_is_correct_for_query_2_1_3_07_d(db_config):
    """ Testcase 07d (Wel ZVL functie + Wel AOK (12m) - 2 opeenvolgende Bepaaldetijd)
        ODB personeelssamenstelling 2.1.3. Percentage personeel met een arbeidsovereenkomst voor bepaalde tijd
    """

    # Load defined test data
    dg = DataGenerator(db_config, td_07_d)

    try:
        # Start a test
        test = QueryTest(db_config)

        # Set the query to be tested
        test.set_query('Gevalideerde_vragen_technisch/personeelssamenstelling/Indicator 2.1.3.rq')

        # Change measuring period parameters of query
        test.change_reference_date_to("2023-12-31", "2024-12-31")

        # Verify actual result of the query
        test.verify_value_in_list("indicator", {"10", "10.0"})

    finally:
        # Delete previously loaded data
        dg.delete_graph_data()
