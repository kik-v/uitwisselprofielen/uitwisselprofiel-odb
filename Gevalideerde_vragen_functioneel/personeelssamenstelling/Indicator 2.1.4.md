---
title: 2.1.4. Percentage inzet uitzendkrachten Personeel Niet in Loondienst (PNIL)
description: "Aantal fte uitzendkrachten/PNIL ingezet gedurende de periode gedeeld door het totaal aantal ingezette fte door personeelsleden gedurende dezelfde periode."
weight: 4
---
## Indicator
**Definitie:** Aantal fte uitzendkrachten/PNIL ingezet gedurende de periode gedeeld door het totaal aantal ingezette fte door personeelsleden gedurende dezelfde periode.

**Teller:** Aantal ingezette fte uitzendkrachten/PNIL over de periode.

**Noemer:** Aantal ingezette fte van personeelsleden met een arbeidsovereenkomst en fte uitzendkrachten/PNIL over de periode.

## Omschrijving
De teller van deze indicator betreft het aantal geregistreerde uren van personen die zorgverlener zijn, maar niet in loondienst, en binnen de meetperiode over een uitzend- of inhuur-overeenkomst beschikten.

De noemer van indicator betreft de som van de volgende twee berekeningen:

* het aantal geregistreerde uren van personen die zorgverlener zijn en binnen de meetperiode over een arbeidsovereenkomst beschikten.
* de teller.

Op basis van deze uren kan het aantal ingezette fte’s van deze personen berekend worden. Om het aantal fte te bepalen wordt gewerkt met 36 uren per week. Meeruren, overuren en oproepuren worden meegerekend.

Deze indicator wordt berekend op organisatieniveau op basis van zorg-gerelateerde functies. 

Op de (deel)resultaten van deze indicator kan een verdeelsleutel worden toegepast.

## Uitgangspunten

* De noemer betreft het totaal aantal geregistreerde uren door personeel in loondienst (PIL) en personeel niet in loondienst (PNIL).
* Personen die zorg verlenen en tegelijkertijd meerdere arbeids- inhuur- en/of uitzendovereenkomsten hebben tellen mee in de berekening wanneer de uren geregistreerd zijn als zorgverlener.
* Om deze indicator te kunnen berekenen dienen de uren beschikbaar te zijn vanuit de urenregistratie van de zorgaanbieder.
* De geregistreerde gewerkte uren, zijn uren exclusief verzuim en (zwangerschaps-)verlof en inclusief meeruren, overuren en oproepuren.
* Voor de berekening van fte, wordt rekening gehouden met 5 weken verlof. Alle gewerkte uren per jaar worden bij elkaar opgeteld en gedeeld door 47 (weken) om de uren per week te berekenen. Om het aantal fte te bepalen wordt gerekende met 36 uren per week. In geval van 1 fte van 36 uur/week, wordt dus geacht 47 weken * 36 uur/week = 1692 uren per jaar te werken.

## Berekening

Deze indicator wordt als volgt berekend:

1. Selecteer alle personen die zorgverlener waren.
2. Selecteer van deze personen de personen met een of meerdere van de volgende overeenkomsten in de meetperiode:
   * Arbeidsovereenkomst;
   * Inhuurovereenkomst;
   * Uitzendovereenkomst.
3. Bereken per persoon het totaal aantal gewerkte uren per type overeenkomst dat deze persoon geregistreerd heeft in de meetperiode.
4. Bereken het totaal aantal uren van 2a, 2b en 2c (noemer).
5. Bereken het totaal aantal uren van 2b en 2c (teller).
6. Bereken de indicator door het resultaat uit ‘5’ te delen door het resultaat uit ‘4’ en te vermenigvuldigen met 100%.

| Teller | Noemer | Indicator |
|---|---|---|
| Stap 5 | Stap 4 | Stap 6 |
