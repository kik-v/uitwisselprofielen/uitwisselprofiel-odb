---
title: 2.4.1. Percentage instroom
description: "Het percentage instroom betreft het aantal personen dat op de peildatum minus 1 jaar niet werkzaam is als personeelslid in loondienst bij de organisatie, maar dat wel is op de peildatum, gedeeld door het gemiddelde aantal personeelsleden in loondienst op de peildatum en op de peildatum minus 1 jaar."
weight: 12
---
## Indicator
**Definitie:** Het percentage instroom betreft het aantal personen dat op de peildatum minus 1 jaar niet werkzaam is als personeelslid in loondienst bij de organisatie, maar dat wel is op de peildatum, gedeeld door het gemiddelde aantal personeelsleden in loondienst op de peildatum en op de peildatum minus 1 jaar.

**Teller:** Aantal personeelsleden met een arbeidsovereenkomst op de peildatum dat niet werkzaam was als personeelslid met een arbeidsovereenkomst op de peildatum minus 1 jaar.

**Noemer:** Gemiddeld aantal personeelsleden met een arbeidsovereenkomst op de peildatum en aantal personeelsleden met een arbeidsovereenkomst op de peildatum minus 1 jaar.

## Omschrijving
De indicator betreft het aantal nieuwe personeelsleden in loondienst na 1 jaar ten op zichten van het gemiddelde van 1) het aantal personeelsleden op het meetmoment en 2) het aantal personeelsleden een jaar eerder. De teller van deze indicator betreft het aantal personeelsleden die zorgverlener zijn op de peildatum en een jaar eerder nog niet over een arbeidsovereenkomst beschikten. De noemer betreft het gemiddelde van 1) het aantal medewerkers dat zorgverlener is en op de peildatum over een arbeidsovereenkomst beschikte en 2) het aantal medewerkers dat zorgverlener is en een jaar eerder over een arbeidsovereenkomst beschikte.

Een persoon telt in de berekening als ‘1’ mee wanneer de persoon de peildatum beschikte over een of meerdere opeenvolgende arbeidsovereenkomsten.

Deze indicator wordt berekend op organisatieniveau op basis van zorg-gerelateerde functies. 

Op de (deel)resultaten van deze indicator kan een verdeelsleutel worden toegepast.

## Uitgangspunten

* Personen met een of meerdere arbeidsovereenkomsten en die zorgverlener zijn op de peildatum en een jaar eerder nog niet worden geïncludeerd in de berekening van de teller.
* Personen die tegelijkertijd meerdere arbeidsovereenkomsten hebben tellen 1 keer mee, wanneer voor beide arbeidsovereenkomsten sprake is van een zorgverlener.

## Berekening

Deze indicator wordt als volgt berekend:

1. Selecteer alle personen met een arbeidsovereenkomst op de peildatum die zorgverlener zijn.
2. Selecteer alle personen met een arbeidsovereenkomst op de peildatum minus 1 jaar die zorgverlener zijn.
3. Selecteer alle personen uit ‘1’ die niet voorkomen bij ‘2’.
4. Bereken het totaal aantal personen uit ‘3’ (teller).
5. Bereken het gemiddelde aantal personen door het resultaat uit ‘1’ en ‘2’ bij elkaar op te tellen en te delen door 2 (noemer).
6. Bereken de indicator door het resultaat bij ‘4’ te delen door het resultaat bij ‘5’ en vermenigvuldig dit met 100%.

| Teller | Noemer | Indicator |
|---|---|---|
| Stap 4 | Stap 5 | Stap 6 |
