---
title: 2.2.3. Aantal vrijwilligers
description: "Aantal vrijwilligers over de periode."
weight: 9
---
## Indicator
**Definitie:** Aantal vrijwilligers over de periode.

**Teller:** Aantal vrijwilligers.

**Noemer:** Niet van toepassing.

## Omschrijving
Deze indicator betreft het aantal personen die zorgverlener zijn en binnen de meetperiode over een vrijwilligersovereenkomst beschikten.

Het aantal vrijwilligers is het aantal personen dat beschikt over een vrijwilligersovereenkomst gedurende (een deel van) de meetperiode of het aantal vrijwilligers met de functie vrijwilliger.

Deze indicator wordt berekend op organisatieniveau op basis van zorg-gerelateerde functies. 

Op de (deel)resultaten van deze indicator kan een verdeelsleutel worden toegepast.

## Uitgangspunten

Voor de berekening van deze indicator worden de onderstaande uitgangspunten gehanteerd:

* Personen met een of meerdere vrijwilligersovereenkomsten gedurende (een deel van de) meetperiode worden geïncludeerd.
* Vrijwilligers die zorgverlener waren en tegelijkertijd meerdere vrijwilligersovereenkomsten hebben tellen 1 keer mee.  


## Berekening

Deze indicator wordt als volgt berekend:

1. Selecteer alle personen met een vrijwilligersovereenkomst of functie vrijwilliger gedurende de meetperiode.
2. Selecteer alle vrijwilligers die zorgverlener waren.
3. Bereken op basis van stap ‘2’ de indicator door het totaal aantal vrijwilligers te berekenen.

| Teller | Noemer | Indicator |
|---|---|---|
| Stap 3 | Niet van toepassing | Stap 3 |
