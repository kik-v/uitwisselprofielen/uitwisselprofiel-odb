---
title: 2.4.2. Percentage uitstroom
description: "Het percentage uitstroom betreft het aantal personen dat op de peildatum minus 1 jaar wel werkzaam was als personeelslid in loondienst bij de organisatie, maar dat niet is op de peildatum, gedeeld door het gemiddelde aantal personeelsleden in loondienst op de peildatum en de peildatum minus 1 jaar."
weight: 13
---
## Indicator
**Definitie:** Het percentage uitstroom betreft het aantal personen dat op de peildatum minus 1 jaar wel werkzaam was als personeelslid in loondienst bij de organisatie, maar dat niet is op de peildatum, gedeeld door het gemiddelde aantal personeelsleden in loondienst op de peildatum en de peildatum minus 1 jaar.

**Teller:** Aantal personeelsleden met een arbeidsovereenkomst op de peildatum minus 1 jaar dat niet werkzaam is als personeelslid met een arbeidsovereenkomst op de peildatum.

**Noemer:** Gemiddeld aantal personeelsleden met een arbeidsovereenkomst op de peildatum en aantal personeelsleden met een arbeidsovereenkomst op de peildatum minus 1 jaar.

## Omschrijving
De indicator betreft het aantal uitgestroomde personeelsleden in loondienst na 1 jaar ten op zichten van het gemiddelde van 1) het aantal personeelsleden op de peildatum en 2) het aantal personeelsleden een jaar eerder. De teller van deze indicator betreft het aantal personeelsleden die zorgverlener zijn en op de peildatum niet meer over een arbeidsovereenkomst beschikte en een jaar voorafgaand aan de peildatum nog wel. De noemer betreft het gemiddelde van 1) het aantal medewerkers dat zorgverlener is en op de peildatum over een arbeidsovereenkomst beschikte en 2) het aantal medewerkers dat zorgverlener is en een jaar eerder dan de peildatum over een arbeidsovereenkomst beschikte.

Een persoon telt in de berekening als ‘1’ mee wanneer de persoon op de peildatum beschikte over een of meerdere opeenvolgende arbeidsovereenkomsten.

Deze indicator wordt berekend op organisatieniveau op basis van zorg-gerelateerde functies. 

Op de (deel)resultaten van deze indicator kan een verdeelsleutel worden toegepast.

## Uitgangspunten

* Personen met een of meerdere arbeidsovereenkomsten en zorgverlener zijn op de peildatum worden geïncludeerd in de berekening van de teller.
* Personen die tegelijkertijd meerdere arbeidsovereenkomsten hebben tellen 1 keer mee, wanneer voor beide arbeidsovereenkomsten sprake is van een zorgverlener.

## Berekening

Deze indicator wordt als volgt berekend:

1. Selecteer alle personen die zorgverlener zijn.
2. Selecteer alle personen met een arbeidsovereenkomst op de peildatum.
3. Selecteer alle personen met een arbeidsovereenkomst een jaar eerder dan de peildatum.
4. Selecteer alle personen uit ‘3’ die niet voorkomen bij ‘2’.
5. Bereken het totaal aantal personen uit ‘4’ (teller).
6. Bereken het gemiddeld aantal personen door het resultaat uit ‘2’ en ‘3’ bij elkaar op te tellen en te delen door 2 (noemer).
7. Bereken de indicator door het resultaat bij ‘5’ te delen door het resultaat bij ‘6’ en vermenigvuldig met 100%.

| Teller | Noemer | Indicator |
|---|---|---|
| Stap 5 | Stap 6 | Stap 7 |
