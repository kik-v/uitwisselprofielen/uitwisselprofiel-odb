---
title: 2.3.2. Verzuimfrequentie
description: "De verhouding tussen het aantal verzuimgevallen en het aantal personeelsleden in loondienst."
weight: 11
---
## Indicator
**Definitie:** De verhouding tussen het aantal verzuimgevallen en het aantal personeelsleden in loondienst.

**Teller:** Aantal ziekmeldingen.

**Noemer:** Gemiddeld aantal personeelsleden.

## Omschrijving
Het aantal nieuwe ziekmeldingen ten op zichte van het gemiddeld aantal personeelsleden in loondienst in de meetperiode. De noemer van deze indicator betreft het resultaat van indicator 2.1.1.

Deze indicator wordt berekend op organisatieniveau op basis van zorg-gerelateerde functies. 

Op de (deel)resultaten van deze indicator kan een verdeelsleutel worden toegepast.

## Uitgangspunten

* Zie de uitgangspunten voor de noemer de uitgangspunten bij indicator 2.1.1.
* De zorgaanbieder geeft zelf aan welke meldingen nieuwe ziekmeldingen betreffen.
* Zwangerschaps- en bevallingsverlof worden uitgesloten.

## Berekening

1. Selecteer alle personen die (o.b.v. hun functie) zorgverlener zijn.
2. Selecteer alle personen met een arbeidsovereenkomst (personeel in loondienst).
3. Tel van alle personen het totaal aantal nieuwe ziekmeldingen  in de meetperiode bij elkaar op.
4. Deel de uitkomst van '3' door de uitkomst van indicator 2.1.1.

| Teller | Noemer | Indicator |
|---|---|---|
| Stap 3 | indicator 2.1.1 | Stap 4 |
