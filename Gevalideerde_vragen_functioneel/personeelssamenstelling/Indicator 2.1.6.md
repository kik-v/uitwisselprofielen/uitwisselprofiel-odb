---
title: 2.1.6. Gemiddelde contractomvang
description: "Aantal fte’s van personeelsleden met een arbeidsovereenkomst over de periode gedeeld door gemiddeld aantal personeelsleden met een arbeidsovereenkomst over de periode."
weight: 6
---
## Indicator
**Definitie:** Aantal fte’s van personeelsleden met een arbeidsovereenkomst die zorgverlener zijn over de periode gedeeld door gemiddeld aantal personeelsleden met een arbeidsovereenkomst die zorgverlener zijn over de periode.

**Teller:** Aantal ingezette fte van personeelsleden met een arbeidsovereenkomst die zorgverlener zijn over de periode.

**Noemer:** Gemiddeld aantal personeelsleden met een arbeidsovereenkomst die zorgverlener zijn over de periode.

## Omschrijving
Deze indicator betreft het aantal fte's van personen met een arbeidsovereenkomst die zorgverlener zijn over de periode gedeeld door het gemiddeld aantal personen met een arbeidsovereenkomst die zorgverlener zijn in de meetperiode. 

Deze indicator wordt berekend door het resultaat van indicator 2.1.2 te delen door het resultaat van 2.1.1. Deze indicator wordt berekend op organisatieniveau op basis van zorg-gerelateerde functies. 

Op de (deel)resultaten van deze indicator kan een verdeelsleutel worden toegepast.


## Uitgangspunten
Zie de uitgangspunten bij indicator 2.1.1 en indicator 2.1.2.


## Berekening
Deze indicator wordt als volgt berekend:

1. Deel het resultaat van indicator 2.1.2 (uitgedrukt in fte) door het resultaat van indicator 2.1.1 (uitgedrukt in het aantal personen).

| Teller | Noemer | Indicator |
|---|---|---|
| Indicator 2.1.2 | Indicator 2.1.1 | Stap 1 |
