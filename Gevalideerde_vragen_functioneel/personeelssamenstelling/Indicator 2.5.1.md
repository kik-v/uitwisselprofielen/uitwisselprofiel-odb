---
title: 2.5.1. Aantal fte zorg client ratio
description: "Aantal ingezette fte’s over de periode gedeeld door het aantal cliënten over de periode. Bij de fte’s betreft het de ingezette fte’s (incl. meeruren, overuren, oproepuren)."
weight: 15
---
## Indicator
**Definitie:** Aantal ingezette fte’s over de periode gedeeld door het aantal cliënten over de periode. Bij de fte’s betreft het de ingezette fte’s (incl. meeruren, overuren, oproepuren).

**Teller:** Som ingezette fte’s personeelsleden met een arbeidsovereenkomst over de periode.

**Noemer:** Aantal cliëntdagen van cliënten met een Wlz-indicatie met zorgprofiel VV4 tot en met VV10.

## Omschrijving
Deze indicator betreft de verhouding tussen het aantal ingezette fte’s van personeel in loondienst dat zorgverlener is ten opzichte van het aantal cliëntdagen gedurende de meetperiode.

Deze teller betreft het aantal geregistreerde uren van personeelsleden in loondienst, dat zorgverlener is en binnen de meetperiode over een arbeidsovereenkomst beschikte. Op basis van deze uren kan het aantal ingezette fte’s van deze personen berekend worden. Om het aantal fte te bepalen wordt gewerkt met 36 uren per week. Meeruren, overuren en oproepuren worden meegerekend.

De noemer betreft het totaal aantal geregistreerde cliëntdagen in de meetperiode van cliënten met een Wlz-indicatie met zorgprofiel VV4 t/m VV10.

Deze indicator wordt berekend op organisatieniveau op basis van zorg-gerelateerde functies. 

NB: Op de teller van deze indicator kan een verdeelsleutel worden toegepast. Op de de noemer van deze indicator dient geen verdeelsleutel te worden toegepast. Immers, de noemer betreft reeds cliënten met een Wlz-indicatie met een zorgprofiel VV4 tot en met VV10.

## Uitgangspunten

* Zie de uitgangspunten bij indicator 2.1.2.
* Het aantal cliëntdagen wordt uitgedrukt in jaren.

## Berekening

Deze indicator wordt als volgt berekend:

1. Zie voor de berekening van de teller de berekening van indicator 2.1.2 (dit is de teller van deze indicator 2.5.1).
2. Selecteer in de meetperiode cliënten met een Wlz-indicatie met zorgprofiel VV4 t/m VV10.
3. Bereken per cliënt uit ‘2’ het aantal cliëntdagen dat gerelateerd is aan een Wlz-indicatie en over een datum beschikte in de meetperiode.
4. Bereken het totaal aantal cliëntjaren door alle cliëntdagen uit ‘3’ bij elkaar op te tellen en te delen door 365 (noemer).
5. Bereken de indicator door het resultaat uit ‘1’ te delen door het resultaat uit ‘4’.

| Teller | Noemer | Indicator |
|---|---|---|
| Stap 1 | Stap 4 | Stap 5 |
