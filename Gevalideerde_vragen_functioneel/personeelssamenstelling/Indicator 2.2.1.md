---
title: 2.2.1. Percentage fte per niveau
description: "Aantal over de periode ingezette fte’s van personeelsleden op een bepaald kwalificatieniveau (ODB kwalificatieniveau)."
weight: 7
---
## Indicator
**Definitie:** Aantal over de periode ingezette fte’s van personeelsleden op een bepaald kwalificatieniveau (ODB kwalificatieniveau).

**Teller:** Aantal ingezette fte’s van personeelsleden met een arbeidsovereenkomst over de periode met kwalificatieniveau X.

**Noemer:** Aantal ingezette fte’s van personeelsleden met een arbeidsovereenkomst over de periode.

## Omschrijving
Deze indicator betreft de verhouding tussen het aantal geregistreerde uren van medewerkers op een bepaald kwalificatieniveau (ODB kwalificatieniveau) ten op zichte van het totaal aantal geregistreerde uren voor alle kwalificatieniveaus samen. De medewerkers dienen zorgverlener te zijn. Deze medewerkers moeten bovendien binnen de meetperiode over een arbeidsovereenkomst beschikken. 

Op basis van deze uren kan het aantal ingezette fte’s van deze personen berekend worden. Om het aantal fte te bepalen wordt gewerkt met 36 uren per week. Meeruren, overuren en oproepuren worden meegerekend.

Deze indicator wordt berekend op organisatieniveau op basis van zorg-gerelateerde functies. 

Op de (deel)resultaten van deze indicator kan een verdeelsleutel worden toegepast.

## Uitgangspunten

* De noemer betreft het totaal aantal geregistreerde uren van medewerkers die tot de groep personeel in loondienst (PIL) behoren.
* Uren van personen die tegelijkertijd over meerdere arbeidsovereenkomsten beschikken, tellen mee in de berekening wanneer de uren geregistreerd zijn als zorgverlener én zij beschikten over een kwalificatieniveau (ODB kwalificatieniveau).
* Om deze indicator te kunnen berekenen dienen de uren beschikbaar te zijn vanuit de urenregistratie van de zorgaanbieder.
* De geregistreerde gewerkte uren, zijn uren exclusief verzuim en (zwangerschaps-)verlof en inclusief meeruren, overuren en oproepuren.
* Voor de berekening van fte, wordt rekening gehouden met 5 weken verlof. Alle gewerkte uren per jaar worden bij elkaar opgeteld en gedeeld door 47 (weken) om de uren per week te berekenen. Om het aantal fte te bepalen wordt gerekende met 36 uren per week. In geval van 1 fte van 36 uur/week, wordt dus geacht 47 weken * 36 uur/week = 1692 uren per jaar te werken.

## Berekening

Deze indicator wordt als volgt berekend:

1. Bereken op basis van de resultaten van indicator 2.1.2 voor elk kwalificatieniveau (ODB kwalificatieniveau) het aantal fte (9 tellers).
2. Bepaal voor elk van de negen resultaten uit stap '1' de indicator door elke teller te delen door het resultaat van indicator 2.1.2 en te vermenigvuldigen met 100%.

| ODB Kwalificatieniveau | Teller | Noemer | Indicator |
|---|---|---|---|
| 1 | Stap 1 | Indicator 2.1.2 | Stap 2 |
| 2 | Stap 1 | Indicator 2.1.2 | Stap 2 |
| 3 | Stap 1 | Indicator 2.1.2 | Stap 2 |
| 4 | Stap 1 | Indicator 2.1.2 | Stap 2 |
| 5 | Stap 1 | Indicator 2.1.2 | Stap 2 |
| 6 | Stap 1 | Indicator 2.1.2 | Stap 2 |
| Behandelaren/(para-) medisch | Stap 1 | Indicator 2.1.2 | Stap 2 |
| Overig zorgpersoneel | Stap 1 | Indicator 2.1.2 | Stap 2 |
| Leerlingen | Stap 1 | Indicator 2.1.2 | Stap 2 |
