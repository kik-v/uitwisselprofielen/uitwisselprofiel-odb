---
title: 2.1.1. Aantal personeelsleden
description: "Aantal personeelsleden met een arbeidsovereenkomst over de periode."
weight: 1
---
## Indicator
**Definitie:** Aantal personeelsleden met een arbeidsovereenkomst over de periode.

**Teller:** Gemiddeld aantal personeelsleden met een arbeidsovereenkomst over de periode.

**Noemer:** Niet van toepassing.

## Toelichting
Deze indicator betreft het aantal personen dat zorgverlener is en binnen de meetperiode over een arbeidsovereenkomst beschikte. Een persoon telt in de berekening als ‘1’ mee wanneer de persoon gedurende de gehele meetperiode beschikte over een of meerdere opeenvolgende arbeidsovereenkomsten. Wanneer een persoon gedurende een gedeelte van de meetperiode beschikte over een of meerdere opeenvolgende arbeidsovereenkomsten telt een persoon naar rato van de meetperiode mee. Bijvoorbeeld, bij een meetperiode van een jaar telt een persoon die gedurende 6 maanden over een arbeidsovereenkomst beschikte als 0,5 mee in de berekening van de indicator.

Deze indicator wordt berekend op organisatieniveau op basis van zorg-gerelateerde functies. 

Op de (deel)resultaten van deze indicator kan een verdeelsleutel worden toegepast.


## Uitgangspunten

* Personen met een of meerdere opeenvolgende arbeidsovereenkomsten gedurende (een deel van de) meetperiode worden geïncludeerd.
* Personen die zorg verlenen en tegelijkertijd meerdere arbeidsovereenkomsten hebben tellen 1 keer mee, wanneer voor beide overeenkomsten sprake is van een zorgverlener.
* De ratio van meetellen wordt bepaald op basis van het aantal kalenderdagen.


## Berekening

Deze indicator wordt als volgt berekend:

1. Selecteer alle personen die zorgverlener waren gedurende de meetperiode.
2. Selecteer alle personen met een arbeidsovereenkomst gedurende de meetperiode.
3. Bereken per persoon het aantal dagen dat de arbeidsovereenkomst (met zorgverlenerfunctie) duurde in de meetperiode.
4. Bereken van elke persoon de relatieve bijdrage door voor elke persoon het aantal dagen dat de arbeidsovereenkomst in die periode duurde te delen door het aantal dagen dat de meetperiode duurde.
5. Bereken het totaal door alle relatieve bijdragen bij elkaar op tellen.

| Teller | Noemer | Indicator |
|---|---|---|
| Stap 5 |Niet van toepassing| Stap 5 |
