---
title: 2.1.3. Percentage personeel met een arbeidsovereenkomst voor bepaalde tijd
description: "Aantal personeelsleden met een arbeidsovereenkomst voor bepaalde tijd op een moment gedeeld door het aantal personeelsleden met een arbeidsovereenkomst voor bepaalde en onbepaalde tijd op datzelfde moment."
weight: 3
---
## Indicator
**Definitie:** Aantal personeelsleden met een arbeidsovereenkomst voor bepaalde tijd op een moment gedeeld door het aantal personeelsleden met een arbeidsovereenkomst voor bepaalde en onbepaalde tijd op datzelfde moment.

**Teller:** Aantal personeelsleden met een arbeidsovereenkomst voor bepaalde tijd op de peildatum.

**Noemer:** Aantal personeelsleden met een arbeidsovereenkomst op de peildatum.

## Toelichting
Deze indicator betreft de verhouding tussen het aantal personen dat zorgverlener is en op een bepaalde peildatum over een tijdelijke arbeidsovereenkomst beschikte, ten op zichte van alle personeelsleden met een arbeidsovereenkomst (die op datzelfde moment zorgverlener waren). Een persoon die op de peildatum zowel een contract voor bepaalde als onbepaalde heeft, telt 1x in de teller en 2x in de noemer mee (deze zou als enige personeelslid voor een indicator van 50% zorgen). Personen kunnen dus meerdere malen meegeteld worden, omdat er arbeidsovereenkomsten geteld worden.

Deze indicator wordt berekend op organisatieniveau op basis van zorg-gerelateerde functies. 

Op de (deel)resultaten van deze indicator kan een verdeelsleutel worden toegepast.

## Uitgangspunten

* Alle personen met een of meerdere arbeidsovereenkomsten op de peildatum worden geïncludeerd.
* Personen die op de peildatum beschikken over meerdere arbeidsovereenkomsten tellen alle mee, wanneer voor deze overeenkomsten sprake is van een functie zorgverlener.

## Berekening

Deze indicator wordt als volgt berekend:

1. Selecteer alle personen die zorgverlener waren.
2. Selecteer alle personen met een of meerdere arbeidsovereenkomsten op de peildatum.
3. Bereken het totaal aantal personen met een of meerdere arbeidsovereenkomsten voor bepaalde tijd (teller).
4. Bereken het totaal aantal personen met een of meerdere arbeidsovereenkomsten voor bepaalde en onbepaalde tijd (noemer).
5. Bereken de indicator door 3 en 4 door elkaar te delen en te vermenigvuldigen met 100%.

| Teller | Noemer | Indicator |
|---|---|---|
| Stap 3 | Stap 4 | Stap 5 |
