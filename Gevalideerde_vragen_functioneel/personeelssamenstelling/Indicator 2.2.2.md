---
title: 2.2.2. Aantal stagiairs
description: "Aantal stagiairs over de periode."
weight: 8
---
## Indicator
**Definitie:** Aantal stagiairs over de periode.

**Teller:** Aantal stagiairs.

**Noemer:** Niet van toepassing.

## Omschrijving
Deze indicator betreft het aantal stagiairs dat zorgverlener is en binnen de meetperiode. Een persoon telt in de berekening als ‘1’ mee wanneer de persoon in de meetperiode beschikte over een of meerdere stageovereenkomsten. Bijvoorbeeld, bij een meetperiode van een jaar (12 maanden) telt een persoon die gedurende 6 maanden over een stage-overeenkomst beschikte als ‘1’ mee in de berekening van de indicator.

Deze indicator wordt berekend op organisatieniveau op basis van zorg-gerelateerde functies. 

Op de (deel)resultaten van deze indicator kan een verdeelsleutel worden toegepast.

## Uitgangspunten

* Personen met een of meerdere stageovereenkomsten gedurende (een deel van de) meetpeiode worden geïncludeerd.
* Stagiairs die zorgverlener zijn en gedurende de meetperiode meerdere stageovereenkomsten hadden tellen 1 keer mee, wanneer voor beide stageovereenkomsten sprake was van een zorgverlener.

## Berekening

Deze indicator wordt als volgt berekend:

1. Selecteer alle personen met een stageovereenkomst gedurende de meetperiode.
2. Selecteer alle personen die zorgverlener zijn.
3. Bereken op basis van stap ‘2’ de indicator door het totaal aantal stagiairs te berekenen.

| Teller | Noemer | Indicator |
|---|---|---|
| Stap 3 | Niet van toepassing | Stap 3 |
