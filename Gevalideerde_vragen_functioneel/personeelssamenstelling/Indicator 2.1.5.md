---
title: 2.1.5. Percentage kosten uitzendkrachten Personeel Niet In Loondienst
description: "Percentage kosten van personeel niet in loondienst (PNIL)."
weight: 5
---
## Indicator

**Definitie:** Percentage kosten van personeel niet in loondienst (PNIL).

**Getal 1:** Kosten van personeel niet in loondienst (PNIL).

**Getal 2:** Kosten personeel in loondienst.

## Toelichting

Deze indicator betreft de kosten van personeelsleden niet in loondienst (PNIL) ten op zichte van personeel in loondienst (PIL).

Deze indicator betreft de kosten van zorggerelateerd personeel en wordt voor de totale organisatie berekend.

Op de (deel)resultaten van deze indicator kan een verdeelsleutel worden toegepast.

### Kosten personeel niet in loondienst (PNIL)

Kosten van personeel niet in loondienst (PNIL) betreft alle vergoedingen aan personeel die niet in loondienst en als uitzendkracht binnen de instelling werkzaam zijn.

Bij vergoedingen is geen sprake van een dienstbetrekking. De instelling is niet inhoudingsplichtig. Het personeel voert de door haar opgedragen werkzaamheden echter wel uit onder de (directe) leiding van de instelling, en maakt daarbij veelal gebruik van faciliteiten, materialen en gereedschappen van de instelling.

Voorbeelden hiervan zijn:

* uitzendkrachten en freelance medewerkers;
* oproep- en invalkrachten voor zover de instelling daarvoor niet inhoudingsplichtig is;
* medische en andere specialisten niet in loondienst.

Bij uitbestede werkzaamheden worden werkzaamheden niet uitgevoerd onder leiding van de instelling. Hiervoor koopt de instelling diensten in en geeft aan derden opdracht tot uit voering en leiding van dergelijke werkzaamheden (Bron: Rekeningschema-voor-zorginstellingen-Prismant - Utrecht, september 2004 Publicatienummer 203.25).

Ter illustratie staan in onderstaande tabellen de verwijzingen naar de betreffende (hoofd- en sub-) rubrieken conform het Referentiegrootboekschema (RGS) en het Prismant rekeningschema.

| Specificatie rubrieken RGS m.b.t. PNIL|
|----------------|
| WBedOvpUik           W.B.A       4012010      Uitzendkrachten    Uitzendkrachten overige personeelskosten |
| WBedOvpUikUik        W.B.A010    4012010.01    Uitzendkrachten    Uitzendkrachten overige personeelskosten |
| WBedOvpUikFor        W.B.A020    4012010.02    Uitzendkrachten  formatief    Uitzendkrachten  formatief |
| WBedOvpUikPrj        W.B.A030    4012010.03    Uitzendkrachten projectmatig    Uitzendkrachten projectmatig |
| WBedOvpUikBfo        W.B.A040    4012010.04    Uitzendkrachten boven formatief    Uitzendkrachten boven formatief |
| WBedOvpUikPro        W.B.A050    4012010.05    Uitzendkrachten programma's    Uitzendkrachten programma's |
| WBedOvpUit           W.B.C       4012020    Uitzendbedrijven    Uitzendbedrijven overige personeelskosten |
| WBedOvpMaf           W.B.D       4012030    Management fee    Management fee overige personeelskosten |
| WBedOvpZzp           W.B.E       4012040    Ingehuurde ZZP-ers    Ingehuurde ZZP-ers overige personeelskosten |
| WBedOvpPay           W.B.F       4012050    Ingehuurde payrollers    Ingehuurde payrollers overige personeelskosten |
| WBedOvpOip           W.B.G       4012060    Overig ingeleend personeel    Overig ingeleend personeel overige personeelskosten |

| Specificatie rubrieken Prismant m.b.t. PNIL|
|----------------|
| 418000 Vergoedingen voor niet in loondienst verrichte arbeid |
| 418100 Uitzendkrachten |
| 418200 Overig personeel niet in loondienst |

### Kosten personeel in loondienst (PIL)

Loonkosten van personeel in loondienst betreft de salariskosten van in dienstbetrekking verrichte arbeid op basis van een arbeidsovereenkomst. De instelling treedt op als werkgever en is uit dien hoofde inhoudingsplichtig voor de loonbelasting en premieheffing. (Bron: Rekeningschema-voor-zorginstellingen-Prismant - Utrecht, september 2004 Publicatienummer 203.25, pagina 29).

Op grond van hiervan worden tot de salarissen gerekend:

* Brutosalaris (volgens inschalingtabellen) inclusief doorbetaalde salarissen tijdens ziekte;
* Vakantiebijslag;
* Vergoedingen voor overwerk, onregelmatige dienst, bereikbaarheids-, aanwezigheids- en
slaapdienst;
* Eindejaarsuitkeringen e.d. (Bron: Rekeningschema-voor-zorginstellingen-Prismant - Utrecht, september 2004 Publicatienummer 203.25, pagina 30).

Ter illustratie staan in onderstaande tabellen de verwijzingen naar de betreffende (hoofd- en sub-) rubrieken conform het Referentiegrootboekschema (RGS) en het Prismant rekeningschema.

| Specificatie rubrieken RGS|
|----------------|
| WPerLes S.A 4001000 Lonen en salarissen |
| WPerSol S.B 4002000 Sociale lasten |

| Specificatie rubrieken Prismant|
|----------------|
| 411 Algemene en administratieve functies |
| 412 Hotelfuncties |
| 413 Patiënt- c.q. bewonergebonden functies |
| 414 Leerling personeel |
| 415 Terrein- en gebouwgebonden functies |
| 422100 Berekende sociale kosten vakantiebijslag |
| 422300 Aandeel werknemer premies sociale verzekeringen |
| 422400 Aandeel werkgever premies sociale verzekeringen |
| 422410 Korting / vrijlating basispremie WAO / werkgeversdeel Awf (Algemeen werkloosheidsfonds) |
| 422500 Ziektekostenverzekering |
| 422600 Pensioenkosten |
| 422900 Overgeboekte sociale kosten |

## Uitgangspunten

* De loonkosten van personeel in loondienst worden geïncludeerd.
* De sociale kosten die rechtstreeks aan een personeelslid in loondienst zijn gerelateerd worden geïncludeerd.
* Voor de volledigheid zijn hoofd- en subrubrieken opgenomen in de tabellen met de verwijzingen naar de betreffende RGS en Prismant-rubrieken. Voor het berekenen van de indciator dient elke unieke boeking een keer mee te tellen (en niet dubbel, omdat zowel, hoofdrubriek als subrubriek worden beschreven in bovenstaande tabellen). Daarnaast hangt de selectie van de boekingen af van een speicifieke implemenatie bij een zorgaanbieder. Immers, de inrichting van de boekhouding kan afwijken van RGS en Prismant (zoals in bovenstaande tabellen beschreven). De zorgaanbieder kan nl. afwijkende grootboeken hebben gedefineerd, boekingen onder andere rubrieken hebben ingedeeld of een een ander referentieschema gebruiken, etc.  

## Berekening

Deze indicator wordt als volgt berekend:

1. Bereken de kosten die vallen onder personeel niet in loondienst (PNIL) voor de betreffende meetperiode voor de totale organisatie.
2. Bereken de kosten die vallen onder personeel in loondienst (PIL) voor de betreffende meetperiode voor de totale organisatie.
3. Bereken de indicator door de resultaten uit stap 1 door de resultaten uit stap 2 te delen en te vermenigvuldig met 100%.

| Getal 1: Kosten PNIL (Euro) | Getal 2: Kosten PIL (Euro) | Indicator |
|----|----|----|
| Stap 1 | Stap 2 | Stap 3 |

NB: Bij deze indicator is geen sprake van een teller en een noemer, maar van een verhouding.
