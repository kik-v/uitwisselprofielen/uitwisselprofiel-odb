---
title: 2.4.3. Percentage doorstroom kwalificatieniveau
description: "Het percentage doorstroom betreft het aantal personen dat op twee opeenvolgende peilmomenten werkzaam is als personeelslid en van kwalificatieniveau is gewisseld op de peildatum ten opzichte van de peildatum minus 1 jaar, gedeeld door het gemiddelde aantal personeelsleden in loondienst op de peildatum en de peildatum minus 1 jaar."
weight: 14

---
## Indicator
**Definitie:** Het percentage doorstroom betreft het aantal personen dat op twee opeenvolgende peilmomenten werkzaam is als personeelslid en van kwalificatieniveau is gewisseld op de peildatum ten opzichte van de peildatum minus 1 jaar, gedeeld door het gemiddelde aantal personeelsleden op de peildatum en de peildatum minus 1 jaar.

**Teller:** Aantal personeelsleden met een arbeidsovereenkomst op de peildatum en op de peildatum minus 1 jaar, dat op de peildatum een ander kwalificatieniveau heeft dan op de peildatum minus 1 jaar.

**Noemer:** Gemiddeld aantal personeelsleden met een arbeidsovereenkomst op de peildatum en aantal personeelsleden met een arbeidsovereenkomst op de peildatum minus 1 jaar.

## Omschrijving
De indicator betreft het aantal personeelsleden in loondienst dat na 1 jaar van kwalificatieniveau (ODB-kwalificatieniveau) is gewisseld ten op zichten van het gemiddelde van:

1. het aantal personeelsleden op de peildatum, en;
2. het aantal personeelsleden een jaar eerder dan de peildatum.

De teller van deze indicator betreft het aantal personeelsleden dat zorgverlener is en op de peildatum en een jaar eerder over een arbeidsovereenkomst beschikte en bij wie het kwalificatieniveau is gewijzigd.

De noemer betreft het gemiddelde van 1) het aantal medewerkers dat zorgverlener is en op de peildatum over een arbeidsovereenkomst beschikte en 2) het aantal medewerkers dat zorgverlener is en een jaar eerder dan de peildatum over een arbeidsovereenkomst beschikte.

Een persoon telt in de berekening van de teller als ‘1’ mee wanneer de persoon een jaar voorafgaand aan de peildatum beschikte over een ander kwalificatieniveau dan op de peildatum.

Deze indicator wordt berekend op organisatieniveau op basis van zorg-gerelateerde functies. 

Op de (deel)resultaten van deze indicator kan een verdeelsleutel worden toegepast.

## Uitgangspunten

* Personen met een of meerdere arbeidsovereenkomsten op de peildatum of op de peildatum een jaar eerder dan de peildatum worden geïncludeerd in de berekening van de teller.
* Personen die zorgverlener zijn op zowel de peildatum alsook een jaar eerder dan de peildatum worden geïncludeerd.
* Personen die tegelijkertijd meerdere arbeidsovereenkomsten hebben tellen 1 keer mee, wanneer voor beide arbeidsovereenkomsten sprake is van een zorgverlener  . 
* Personen die op de peildatum en op de peildatum een jaar eerder beschikten over een kwalificatieniveau (ODB kwalificatieniveau uit de lijst van het kwaliteitskader) worden geïncludeerd.

## Berekening

Deze indicator wordt als volgt berekend:

1. Selecteer alle personen die zorgverlener zijn.
2. Selecteer alle personen met een arbeidsovereenkomst op de peildatum.
3. Selecteer van alle personen uit ‘2’ de personen met een arbeidsovereenkomst 1 jaar eerder dan de peildatum.
4. Selecteer van alle personen uit ‘3’ het kwalificatieniveau op de peildatum en op de peildatum minus 1 jaar.
5. Selecteer van alle personen uit ‘4’ de personen bij wie het kwalificatieniveau op beide momenten niet gelijk is.
6. Bereken het totaal aantal personen uit ‘5’ (teller).
7. Selecteer alle personen met een arbeidsovereenkomsten op de peildatum.
8. Selecteer alle personen met een arbeidsovereenkomsten 1 jaar eerder dan de peildatum.
9. Bereken het gemiddelde aantal personen door het resultaat uit ‘7’ en ‘8’ bij elkaar op te tellen en te delen door 2 (noemer).
10. Bereken de indicator door het resultaat bij ‘6’ te delen door het resultaat bij ‘9’ en te vermenigvuldigen met 100%.

| Teller | Noemer | Indicator |
|---|---|---|
| Stap 6 | Stap 9 | Stap 10 |
