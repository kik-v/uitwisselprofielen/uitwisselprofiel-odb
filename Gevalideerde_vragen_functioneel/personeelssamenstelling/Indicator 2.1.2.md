---
title: 2.1.2. Aantal fte
description: "Aantal ingezette fte’s van personeelsleden met een arbeidsovereenkomst over de periode. Het betreft de ingezette fte’s inclusief meeruren, overuren, oproepuren."
weight: 2
---
## Indicator
**Definitie**: Aantal ingezette fte’s van personeelsleden met een arbeidsovereenkomst over de periode. Het betreft de ingezette fte’s inclusief meeruren, overuren, oproepuren.

**Teller**: Aantal ingezette fte met een arbeidsovereenkomst over de periode.

**Noemer**: Niet van toepassing.

## Toelichting
Deze indicator betreft het aantal ingezette (geregistreerde) uren van personen die zorgverlener zijn en binnen de meetperiode over een arbeidsovereenkomst beschikten. Op basis van deze uren kan het aantal ingezette fte’s van deze personen berekend worden. Om het aantal fte te bepalen wordt gewerkt met 36 uren per week. Meeruren, overuren en oproepuren worden meegerekend.

Deze indicator wordt berekend op organisatieniveau op basis van zorg-gerelateerde functies. 

Op de (deel)resultaten van deze indicator kan een verdeelsleutel worden toegepast.

## Uitgangspunten

* Personen die tegelijkertijd een of meerdere arbeidsovereenkomsten hebben tellen mee in de berekening wanneer de uren geregistreerd zijn als zorgverlener (o.b.v. de functie in de arbeidsovereenkomst).
* Om deze indicator te kunnen berekenen dienen de uren beschikbaar te zijn vanuit de urenregistratie.
* De geregistreerde ingezette uren, zijn uren exclusief verzuim en (zwangerschaps-)verlof en inclusief meeruren, overuren en oproepuren.
* Voor de berekening van fte, wordt rekening gehouden met 5 weken verlof. Alle gewerkte uren per jaar worden bij elkaar opgeteld en gedeeld door 47 (weken) om de uren per week te berekenen. Om het aantal fte te bepalen wordt gerekende met 36 uren per week. In geval van 1 fte van 36 uur/week, wordt dus geacht 47 weken * 36 uur/week = 1692 uren per jaar te werken.

## Berekening

Deze indicator wordt als volgt berekend:

1. Selecteer alle personen die zorgverlener zijn.
2. Selecteer alle personen met een arbeidsovereenkomst gedurende de meetperiode.
3. Tel per persoon het aantal uren dat de persoon heeft geregistreerd (is ingezet) in de meetperiode.
4. Bereken het totaal uren door alle geselecteerde uren van alle geselecteerde personen bij elkaar op te tellen.
5. Bereken het aantal fte door het totaal uit stap 4 te delen door het aantal uren van een fte, in dit geval 36 uren per week, en deel vervolgens door 47 weken.

| Teller | Noemer | Indicator |
|---|---|---|
| Stap 5 |Niet van toepassing| Stap 5 |
