---
title: Gevalideerde vragen functioneel 
---
In deze sectie staan de functionele beschrijvingen van de indicatoren personeelssamenstelling en basisveiligheid. Per indicator uit het handboek is het volgende beschreven:
- Definitie, inclusief teller en noemer;
- De beschrijving van de berekening;
- Specifieke uitagangspunten die worden gehanteerd bij de berekening. 

De gegevens die benodigd zijn voor de berekening van deze indicatoren dienen nog nader getoetst te worden op basis van proefimplementaties bij zorgaanbieders.

## Voorbeeldberekeningen

Voorbeeldberekeningen in de vorm van SPARQL-queries voor het berekenen van indicatoren staan hier:

[Klik hier voor Personele samenstelling](/Gevalideerde_vragen_technisch/personeelssamenstelling/)

[Klik hier voor Basisveiligheid](/Gevalideerde_vragen_technisch/basisveiligheid/)

## Benodigde gegevens in de modelgegevensset

De concepten, eigenschappen en relaties die nodig zijn om de indicatoren Personele samenstelling en basisveiligheid te berekenen staan [hier](/Gevalideerde_vragen_technisch/modelgegevensset).

## Algemene uitgangspunten

De algemene uitgangspunten voor de berekening van de indicatoren Personele samenstelling en Basisveiligheid uit het uitwisselprofiel Zorginstituut staan beschreven in Algemene uitgangspunten.

## Verdeelsleutels

Voor het gebruik van verdeelsleutels verwijzen wij u naar: [klik hier](https://kik-v-publicatieplatform.nl/documentatie/Verdeelsleutels/).