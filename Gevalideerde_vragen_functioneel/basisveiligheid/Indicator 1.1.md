---
title: 1.1. Decubitus
description: "Percentage cliënten met decubitus categorie 2 of hoger per vestiging."
weight: 1
---
## Indicator

**Definitie:** Percentage cliënten met decubitus categorie 2 of hoger per vestiging.

**Teller:** Aantal cliënten op de vestiging met decubitus categorie 2 of hoger.

**Noemer:** Totaal aantal cliënten op de vestiging exclusief cliënten met decubitus bij wie de categorie niet is geregistreerd.

## Toelichting

Deze indicator betreft per vestiging het aantal cliënten met decubitus categorie 2 of hoger ten op zichten van het totaal aantal cliënten per vestiging. Het betreft cliënten met een Wlz-indicatie met een zorgprofiel VV4 t/m VV10. De indicator wordt per vestiging berekend. Cliënten bij wie wel sprake is van decubitus, maar bij wie geen categorie is geregisteerd worden niet geïncludeerd (in zowel de noemer als de teller). Cliënten die gedurende de meetperiode van vestiging wisselen worden ingedeeld bij de meest recente vestiging (binnen de meetperiode). 

Voor het beschrijven van de wond wordt gebruik gemaakt van de indeling in 4 categorieën, gebaseerd op het internationale NPUAP/EPUAP decubitus classificatiesysteem.

Deze indicator wordt berekend op vestigingsniveau op een peildatum. De peildatum is 1 maart 2024.

## Uitgangspunten

* Alle cliënten met een Wlz-indicatie VV4 tot en met VV10 op de peildatum worden geïncludeerd.
* Cliënten met decubitus bij wie geen enkele categorie is geregistreerd, worden uitgesloten in de berekening (van zowel de teller als de noemer) van deze indicator.  
* Cliënten worden ingedeeld op de vestiging waar de client (o.b.v. de indicatie) zorg ontving op de peildatum. 


## Berekening

Deze indicator wordt als volgt berekend:

1. Selecteer alle cliënten met een Wlz-indicatie met een zorgprofiel VV4 tot en met VV10 op de peildatum.
2. Verwijder uit het resultaat van stap 1 alle cliënten met decubitus, maar bij wie nooit een decubitus categorie is geregistreerd (noemer).
3. Bereken op basis van stap 2 het totaal aantal cliënten bij wie de decubitus-categorie 2 of hoger is geregistreerd op de peildatum (teller).
4. Bepaal per client de vestiging op de peildatum.  
5. Bereken per vestiging de indicator door het resultaat uit ‘3’ te delen door het resultaat uit ‘2’ en te vermenigvuldigen met 100%.

| Teller | Noemer | Indicator |
|---|---|---|
| Stap 3 | Stap 2 | Stap 5 |
