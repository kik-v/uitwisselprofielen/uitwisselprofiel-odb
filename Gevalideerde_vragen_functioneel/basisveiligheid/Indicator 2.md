---
title: 2. Gezamenlijke afspraken over behandeling rond het levenseinde
description: "Percentage cliënten op de vestiging waarbij beleidsafspraken rondom het levenseinde zijn vastgelegd in het zorgdossier."
weight: 2
---
## Indicator

**Definitie:** Percentage cliënten op de vestiging waarbij beleidsafspraken rondom het levenseinde zijn vastgelegd in het zorgdossier.

**Teller:** Aantal cliënten waarbij beleidsafspraken zijn vastgelegd in het zorgdossier.

**Noemer:** Totaal aantal cliënten op de vestiging.

## Toelichting

De indicator betreft per vestiging het totaal aantal cliënten waarbij beleidsafspraken rondom het levenseinde zijn vastgelegd in het zorgdossier ten op zichten van het totaal aantal cliënten per vestiging. Het betreft cliënten met een Wlz-indicatie met een zorgprofiel VV4 t/m VV10. De indicator wordt per vestiging berekend. 

Deze indicator wordt berekend op vestigingsniveau op een peildatum. De peildatum is 1 maart 2024.

## Uitgangspunten

* Alle cliënten met een Wlz-indicatie VV4 tot en met VV10 op de peildatum worden geïncludeerd.
* In geval van meerdere afspraken bij een cliënt telt deze cliënt één keer mee.
* Cliënten worden ingedeeld op vestiging waar de cliënt (o.b.v. de indicatie) zorg ontving op de peildatum.

## Berekening

De indicator wordt als volgt berekend:

1. Selecteer alle cliënten met een Wlz-indicatie met een zorgprofiel VV4 tot en met VV10 op de peildatum.
2. Bepaal o.b.v. stap 1 per client de vestiging op de peildatum.  
3. Bepaal per cliënt of er minimaal één beleidsafspraak rond het levenseinde is geregistreerd.
4. Bereken per vestiging het aantal cliënten.
5. Bereken per vestiging het aantal cliënten bij wie minimaal een afspraak rond het levenseinde is geregistreerd.
6. Bereken de indicator door per vestiging het resultaat uit stap ‘5’ te delen door het resultaat uit stap ‘4’ en vermenigvuldig met 100%.

| Teller | Noemer | Indicator |
|---|---|---|
| Stap 5 | Stap 4 | Stap 6 |
