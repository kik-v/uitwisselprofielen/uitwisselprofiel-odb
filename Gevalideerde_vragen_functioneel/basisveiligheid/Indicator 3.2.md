---
title: 3.2. Medicatiereview
description: "Percentage cliënten –met een ZZP V&V indicatie met behandeling- waarbij een formele medicatiereview heeft plaatsgevonden in het bijzijn van een zorgmedewerker die medicijnen aan de cliënt verstrekt."
weight: 3
---
## Indicator
**Definitie:** Percentage cliënten –met een ZZP V&V indicatie met behandeling- waarbij een formele medicatiereview heeft plaatsgevonden in het bijzijn van een zorgmedewerker die medicijnen aan de cliënt verstrekt.

**Teller:** Aantal cliënten –met een ZZP V&V indicatie met behandeling- waarbij een formele medicatiereview is uitgevoerd in het afgelopen jaar in het bijzijn van de apotheker, specialist ouderengeneeskunde en een zorgmedewerker die medicijnen aan de cliënt verstrekt.

**Noemer:** Alle cliënten met een ZZP V&V indicatie met behandeling op de vestiging.

## Toelichting

De indicator betreft per vestiging het totaal aantal cliënten bij wie in de verslagperiode een medicatiereview heeft plaatsgevonden ten op zichten van het totaal aantal cliënten per vestiging. Het betreft cliënten met een wlz-indicatie met een zorgprofiel VV4 t/m VV10. De indicator wordt per vestiging berekend. Cliënten die gedurende de verslagperiode van vestiging wisselen worden ingedeeld bij de meest recente vestiging (binnen de verslagperiode). 

Bij de medicatiereview dienen de apotheker, specialist ouderengeneeskunde en een zorgmedewerker die medicijnen aan de cliënt verstrekt aanwezig te zijn geweest.

Deze indicator wordt berekend op vestigingsniveau. 

## Uitgangspunten

* Alle cliënten met een Wlz-indicatie VV4 tot en met VV10 **met behandeling** binnen de verslagperiode en die langer dan 6 maanden in zorg zijn worden geïncludeerd.
* Cliënten worden ingedeeld op de meeste recente vestiging waar de client (o.b.v. de indicatie) zorg ontving binnen de verslagperiode. 
* In geval van meerdere afspraken bij een cliënt telt deze cliënt een keer mee.
* De datum van de medicatiereview moet binnen de verslagperiode liggen.
* In de berekening van deze indicator wordt geen rekening gehouden met de voorwaarden waaraan een medicatiereview dient te voldoen. Voorwaarden betreffen dat de medicatiereview in het bijzijn van de apotheker, specialist ouderengeneeskunde en een zorgmedewerker die medicijnen aan de cliënt verstrekt heeft plaatsgevonden. Bij de berekening van deze indicator wordt er dan ook vanuit gegaan dat de zorgaanbieder medicatiereviews aanlevert die aan de betreffende voorwaarden voldoen.


## Berekening

De indicator wordt als volgt berekend:

1. Selecteer alle cliënten met een Wlz-indicatie met een zorgprofiel VV4 tot en met VV10 met behandeling binnen de verslagperiode en die langer dan 6 maanden in zorg zijn.
2. Bepaal o.b.v. stap 1 per client de meest recente vestiging binnen de verslagperiode.  
3. Bepaal per cliënt of er minimaal een medicatiereview heeft plaatsgevonden.
4. Bereken per (meest recente) vestiging het aantal cliënten.
5. Bereken per (meest recente) vestiging het aantal cliënten bij wie minimaal een medicatiereview heeft plaatsgevonden binnen de verslagperiode.
6. Bereken de indicator door per vestiging het resultaat uit stap ‘5’ te delen door het resultaat uit stap ‘4’ en vermenigvuldig met 100%.

| Teller | Noemer | Indicator |
|---|---|---|
| Stap 5 | Stap 4 | Stap 6 |
