---
title: 8. Clientervaring score - Gemiddelde totaalscore gebaseerd op zes vragen
description: "Clientervaring score: Gemiddelde totaalscore gebaseerd op zes vragen."
weight: 8
---
## Indicator

**Definitie:** Clientervaring score: Gemiddelde totaalscore gebaseerd op zes vragen.

**Teller:** Per vestiging: Bij elkaar opgetelde totaalscores per respondent.

**Noemer:** Per vestiging: Aantal respondenten.

## Toelichting

De indicator wordt per vestiging ingevuld. De meetperiode betreft 1-1-2023 t/m 31-12-2023.

De zorgaanbieder vult handmatig de teller en de noemer in.


## Uitgangspunten

* De teller betreft de bij elkaar opgetelde totaalscores per respondent.
* De noemer betreft het aantal respondenten.

## Berekening

Niet van toepassing.

