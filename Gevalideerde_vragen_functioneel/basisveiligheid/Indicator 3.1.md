---
title: 3.1. Bespreken medicatiefouten in het team
description: "Percentage vestigingen waar medicatiefouten tenminste één keer per kwartaal multidisciplinair worden besproken met medewerkers van de vestiging op basis van meldingen die zijn gedaan."
weight: 3
---
## Indicator
**Definitie:** Percentage vestigingen waar medicatiefouten tenminste één keer per kwartaal multidisciplinair worden besproken met medewerkers van de vestiging op basis van meldingen die zijn gedaan.

**Teller:** Het aantal vestigingen waar meldingen van medicatiefouten ten minste eens per kwartaal zijn besproken op basis van gedane meldingen.

**Noemer:** Het totaal aantal vestigingen in de zorgorganisatie waar meldingen van medicatiefouten zijn geweest.

## Toelichting

De indicator geeft aan in hoeverre geregistreerde medicatiefouten ook daadwerkelijk worden besproken op de vestigingen van een zorginstelling waar deze fouten plaatsvinden en gemeld zijn. Elk kwartaal dat er medicatiefouten plaatsvinden en zijn gemeld, dienen deze vervolgens ook besproken te zijn. De indicator betreft dan ook het aantal vestigingen waar in een bepaald kwartaal meldingen van medicatiefouten zijn besproken ten op zichten van alle vestigingen waar meldingen zijn gedaan van medicatiefouten.

De noemer van deze indicator geeft het totaal aantal vestigingen waar meldingen van medicatiefouten zijn geregistreerd. Wanneer bijvoorbeeld alleen in het eerste kwartaal van een jaar (januari tot en met maart) een medicatiefout is geregistreerd telt deze vestiging als ‘1’ mee in de noemer. De teller geeft het aantal vestigingen weer waar minstens een keer per kwartaal medicatiefouten multidisciplinair zijn besproken.

Deze indicator wordt berekend op vestigingsniveau. 

## Uitgangspunten

* Vestigingen waar in de periode van januari tot en met oktober van het verslagjaar medicatiefouten zijn geregistreerd van cliënten met een Wlz-indicatie met een zorgprofiel VV4 tot en met VV10 worden geïncludeerd in de noemer.
* Vestigingen waar medicatiefouten tenminste één keer per kwartaal in de periode januari tot en met december van het verslagjaar multidisciplinair zijn besproken met medewerkers van de vestiging op basis van meldingen die zijn gedaan tellen mee in de teller.
* In de berekening van deze indicator wordt geen rekening gehouden met de voorwaarde waaraan een bespreking dient te voldoen. De voorwaarde betreft dat de bespreking met bepaalde personen plaats heeft gevonden. Bij de berekening van deze indicator wordt er dan ook vanuit gegaan dat de zorgaanbieder besprekingen aanlevert die aan deze voorwaarde voldoen.

## Berekening

Deze indicator wordt als volgt berekend:

1. Selecteer de vestigingen waar in de verslagperiode cliënten met een Wlz-indicatie met een zorgprofiel VV4 t/m VV10 zijn geregistreerd.
2. Selecteer van deze vestigingen de delen (bijvoorbeeld kwartalen) van de verslagperiode (bijvoorbeeld een jaar) waarin cliënten met een Wlz-indicatie met een zorgprofiel VV4 t/m VV10 zijn geregistreerd.
3. Selecteer van deze delen de vestigingen waar bij deze cliënten medicatiefouten zijn geregistreerd.
4. Bereken het totaal aantal unieke vestigingen uit ‘3’ (noemer).
5. Selecteer van de delen uit ‘3’ de delen waar tijdens het deel of in de periode, met een gelijke duur als het deel, na het deel besprekingen van medicatiefouten zijn geregistreerd.
6. Selecteer de vestigingen waarbij per vestiging het aandeel delen uit ‘5’ overeenkomt met het aantal delen uit ‘3’.
7. Bereken het totaal aantal unieke vestigingen uit ‘6’ (teller).
8. Bereken de indicator door het resultaat uit ‘7’ te delen door het resultaat uit ‘4’ en bereken het percentage door te vermenigvuldigen met 100%.

| Teller | Noemer | Indicator |
|---|---|---|
| Stap 7 | Stap 4 | Stap 8 |

## Voorbeeld met getallen

* In geval van 3 besprekingen is teller 0, dit is onafhankelijk van de noemer.
* In geval van 4 besprekingen en dan een per kwartaal is teller 1.
* Bij een medicatiefout in de periode januari tot en met oktober is de noemer 1.
* en bij geen 0 en in november ook 0.

