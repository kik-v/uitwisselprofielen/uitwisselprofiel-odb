---
title: 1.2. Casuïstiekbespreking decubitus
description: "Percentage vestigingen waar een casuïstiekbespreking over decubitus heeft plaatsgevonden."
weight: 1
---
## Indicator
**Definitie:**
Percentage vestigingen waar een casuïstiekbespreking over decubitus heeft plaatsgevonden, waarbij de volgende items zijn doorgenomen:
- Waar is het huidletsel ontstaan?
- Wat is de vestiging?
- Zagen we al risico’s tijdens de anamnese?
- Interventies tot op heden, duur van de wondgenezing etc.

**Teller:** Aantal vestigingen met cliënten met decubitus categorie 2 en hoger en waar dit jaar een casuïstiekbespreking heeft plaatsgevonden op basis van de beschreven items.

**Noemer:** Totaal aantal vestigingen in de zorgorganisatie waar decubitus categorie 2 of hoger voorkomt.

## Toelichting

De indicator betreft het aantal vestigingen met cliënten met decubitus categorie 2 of hoger waar tevens een casuïstiekbespreking heeft plaatsgevonden ten op zichten van het totaal aantal vestigingen met cliënten met decubitus categorie 2 of hoger. Het betreft cliënten met een Wlz-indicatie met een zorgprofiel VV4 t/m VV10. De indicator wordt per vestiging berekend. 

Voor het beschrijven van de wond wordt gebruik gemaakt van de indeling in 4 categorieën, gebaseerd op het internationale NPUAP/EPUAP decubitus classificatiesysteem.

Deze indicator wordt berekend op vestigingsniveau. De meetperiode betreft 1 janauri tot en met 31 december van het verslagjaar.

## Uitgangspunten

* Vestigingen met cliënten bij wie een of meerdere gevallen van decubitus met categorie 2 of hoger voorkwamen binnen de meetperiode worden geïncludeerd in de noemer.
* De vestigingen waar in de meetperiode een casuïstiekbespreking over decubitus heeft plaatsgevonden worden geïncludeerd in de teller.
* In de berekening van de indicator wordt geen rekening gehouden met de voorwaarden waaraan een casuistiekbespreking over decubitus dient te voldoen. Voorwaarden betreft dat er bepaalde items besproken dienen te zijn om als casuistiekbespreking aangemerkt te worden. Bij de berekening van deze indicator wordt er dan ook vanuit gegaan dat de zorgaanbieder casusitiekbesprekingen aanlevert die aan de betreffende voorwaarden voldoen.

## Berekening

Deze indicator wordt als volgt berekend:

1. Selecteer alle vestigingen met cliënten met een wlz-indicatie met een zorgprofiel VV4 t/m VV10 binnen de meetperiode.
2. Selecteer o.b.v. stap 1 de vestigingen met cliënten met decubitus categorie 2 of hoger.
3. Selecteer van deze vestigingen de vestigingen waar in de meetperiode minimaal eenr casuïstiekbespreking over decubitus heeft plaatsgevonden.
4. Bereken het totaal aantal unieke vestigingen uit ‘2’ (noemer).
5. Bereken het totaal aantal unieke vestigingen uit ‘3’ (teller).
6. Bereken de indicator door het resultaat uit ‘5’ te delen door het resultaat uit ‘4’ en te vermenigvuldigen met 100%.

| Teller | Noemer | Indicator |
|---|---|---|
| Stap 5 | Stap 4 | Stap 6 |
