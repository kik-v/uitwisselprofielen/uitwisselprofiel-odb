# Indicator: ODB Personele samenstelling 2.3.2
# Parameters: -
# Ontologie: versie 2.0.0 of nieuwer

PREFIX onz-g: <http://purl.org/ozo/onz-g#>
PREFIX onz-pers: <http://purl.org/ozo/onz-pers#>
PREFIX xsd: <http://www.w3.org/2001/XMLSchema#>
PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
PREFIX time: <http://www.w3.org/2006/time#>

# Verzuimfrequentie. Let op: datums periode op twee plekken aanpassen!

SELECT 
	?teller 
	?noemer 
	(?teller/?noemer AS ?verzuimfrequentie)
{
	{ 
		SELECT (COUNT(?ziekte) as ?teller)
		{
			SELECT DISTINCT ?ziekte
			WHERE
			{ 
			  		# definieer periode waarin overeenkomst geldig moet zijn
			  		BIND("2023-01-01"^^xsd:date AS ?start_periode)
			        BIND("2023-12-31"^^xsd:date AS ?eind_periode)

                    # selecteer alle functies die onz-pers:ZorgverlenerFunctie zijn
                    ?functie 
                        a onz-pers:ZorgverlenerFunctie ;
                        onz-g:startDatum ?start_functie .
                    OPTIONAL {?functie a onz-pers:ZorgverlenerFunctie ;
                                    onz-g:eindDatum ?eind_functie}
                    FILTER(?start_functie <= ?eind_periode && ((?eind_functie >= ?start_periode) || (!BOUND(?eind_functie))))

                    BIND(IF(?start_functie < ?start_periode, ?start_periode, ?start_functie) AS ?start_functie_reken)
                    BIND(IF(!BOUND(?eind_functie), ?eind_periode, ?eind_functie) AS ?eind_functie_reken)

                    # selecteer alle bijbehorende arbeidsovereenkomsten 
                    ?overeenkomst 
                        onz-g:isAbout ?functie ;
                        a onz-pers:ArbeidsOvereenkomst ;
                        onz-pers:heeftOpdrachtnemer ?persoon .
			  
			        # selecteer personen met ziekteperiode 
			        ?persoon onz-g:isParticipantIn ?ziekte .
			   		?ziekte 
			            a onz-pers:ZiektePeriode ;
			            onz-g:startDatum ?start_ziekte .     
			        FILTER (?start_ziekte >= ?start_functie_reken && ?start_ziekte <= ?eind_functie_reken)
			}
		}
	} 
	 
	{
	    SELECT
            (SUM(?factor_totaal) AS ?noemer)
        {
            SELECT #alle factoren bij elkaar optellen, max per persoon beperken tot 1
                ?persoon
                (IF(SUM(?dagen_indicator/?dagen_periode) > 1, 1,SUM(?dagen_indicator/?dagen_periode))AS ?factor_totaal)
            {
                SELECT DISTINCT #functies die op dezelfde dag starten, samenvoegen op de functie met de meeste dagen
                    ?persoon
                    ?dagen_periode
                    ?start_functie_corr
                    (MAX(?dagen_functie) AS ?dagen_indicator)
                { 
                    # definieer periode waarin overeenkomst geldig moet zijn
                    BIND("2023-01-01"^^xsd:date AS ?start_periode_datum)
                    BIND("2023-12-31"^^xsd:date AS ?eind_periode_datum)
                    ?start_periode_datum ^time:inXSDDate/time:inTemporalPosition/time:numericPosition ?start_periode .
                    ?eind_periode_datum ^time:inXSDDate/time:inTemporalPosition/time:numericPosition ?eind_periode .
                    BIND(?eind_periode - ?start_periode + 1 AS ?dagen_periode)

                    # selecteer werkovereenkomsten waarbij werknemer een zorgverlenerfunctie heeft
                    ?functie 
                        a onz-pers:ZorgverlenerFunctie ;
                        onz-g:startDatum ?start_functie_datum .
                    OPTIONAL {
                        ?functie onz-g:eindDatum ?eind_functie_datum .
                    }
                    FILTER(?start_functie_datum <= ?eind_periode_datum && (?eind_functie_datum >= ?start_periode_datum || !BOUND(?eind_functie_datum)))
                    BIND(IF(?start_functie_datum < ?start_periode_datum, ?start_periode_datum, ?start_functie_datum) AS ?start_functie_datum_corr)
                    BIND(IF(!BOUND(?eind_functie_datum) || ?eind_functie_datum > ?eind_periode_datum, ?eind_periode_datum, ?eind_functie_datum) AS ?eind_functie_datum_corr)
                    ?start_functie_datum_corr ^time:inXSDDate/time:inTemporalPosition/time:numericPosition ?start_functie_corr .
                    ?eind_functie_datum_corr ^time:inXSDDate/time:inTemporalPosition/time:numericPosition ?eind_functie_corr .
                    BIND(?eind_functie_corr - ?start_functie_corr + 1 AS ?dagen_functie)

                    ?overeenkomst
                        a onz-pers:ArbeidsOvereenkomst ;
                        onz-g:isAbout ?functie ;
                        onz-pers:heeftOpdrachtnemer ?persoon .
                }	
                GROUP BY ?persoon ?dagen_periode ?start_functie_corr
                ORDER BY ?persoon
            }
            GROUP BY ?persoon
        }
    }
}
