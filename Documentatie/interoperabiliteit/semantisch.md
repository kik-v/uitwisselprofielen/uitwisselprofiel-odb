---
title: Semantische interoperabiliteit
weight: 3
---

## 1. Informatievragen & definities

De informatievragen per indicator en de bijbehorende definities staan beschreven in het handboek ‘Verpleeghuiszorg kwaliteitskader indicatoren’ en handboek 'Verpleeghuiszorg Personeelssamenstelling indicatoren'.

* Indicatoren Personeelssamenstelling [handboek](https://www.zorginzicht.nl/kwaliteitsinstrumenten/verpleeghuiszorg-personeelssamenstelling-indicatoren)
* Indicatoren Basisveiligheid, Totaalscore cliëntervaring [handboek](https://www.zorginzicht.nl/kwaliteitsinstrumenten/verpleeghuiszorg-kwaliteitskader-indicatoren)

## 2. Algemene uitgangspunten

Voor de berekening van de indicatoren en informatievragen in de verschillende uitwisselprofielen worden algemene uitgangspunten gehanteerd. Uitgangspunten die gelden voor specifieke indicatoren of informatievragen worden bij de functionele beschrijving van de betreffende indicator beschreven. Indicator-specifieke uitgangspunten gaan voor op algemene uitgangspunten van een uitwisselprofiel.

Voor alle uitwisselprofielen gelden onderstaande algemene uitgangspunten:

[Algemene uitgangspunten](https://kik-v-publicatieplatform.nl/documentatie/Algemene%20uitgangspunten)

Specifiek uitgangspunt voor dit uitwisselprofiel
Toerekening naar de Wlz en cliënten met een Wlz-indicatie met zorgprofiel VV4 t/m VV10 (cliëntpopulatie):
Met betrekking tot de indicatoren personele samenstelling is niet altijd een zuivere toerekening te maken naar de Wlz (cliënten met een Wlz-indicatie met zorgprofiel VV4 t/m VV10). Voor deze indicatoren kan dan gebruik gemaakt worden van een zogenaamde verdeelsleutel. Voor de indicatoren waar dit toegepast kan worden, is in de functionele berekening de stap beschreven om een verdeelsleutel toe te passen. Op deze pagina staan beschrijvingen van verdeelsleutels die toegepast kunnen worden om een toerekening naar de Wlz VV te doen: https://kik-v-publicatieplatform.nl/documentatie/Verdeelsleutels/1.1.0/Documentatie/introductie  

## 3. Benodigde gegevens

De volgende gegevens zijn benodigd voor de berekeningen Personeel en Basisveiligheid:

### 3.1. Personeelssamenstelling
De concepten, eigenschappen en relaties die nodig zijn om de indicatoren Personeelssamenstelling te berekenen staan hier:

[klik hier](/Gevalideerde_vragen_technisch/Modelgegevensset/)

### 3.2. Basisveiligheid
De concepten, eigenschappen en relaties die nodig zijn om de indicatoren Basisveiligheid te berekenen staan hier:

[klik hier](/Gevalideerde_vragen_technisch/Modelgegevensset)

## 4. Functionele beschrijvingen gevalideerde vragen

[Klik hier](/Gevalideerde_vragen_functioneel/personeelssamenstelling) voor de personeelssamenstelling en [hier](/Gevalideerde_vragen_functioneel/basisveiligheid) voor de basisveiligheid.

Bij de berekeningen wordt rekening gehouden met de volgende validaties:

### 4.1. Validatie

#### 4.1.1. Algemeen

* Vraag: levert u zorg volgens de reikwijdte van het Kwaliteitskader Verpleeghuiszorg? Alleen als hierop ‘ja’ wordt geantwoord, kunnen gegevens worden aangeleverd;
* De zorgaanbieder vult ten minste twee keuze indicatoren in, voordat verder kan worden gegaan met het aanleveren van gegevens;
* Als bij de noemer ‘0' wordt ingevuld, wordt een foutmelding weergegeven. Als bij een keuze indicator ‘0’ of 'n.v.t.’ wordt ingevuld, wordt de zorgaanbieder teruggeleid naar start;
* Indien de noemer die wordt ingevuld met meer dan 10 afwijkt van de noemer bij een eerdere indicator, wordt gevraagd of de ingevulde noemer correct is.

#### Per thema

* Decubitus
  * Indien er geen cliënten met decubitus op de afdeling waren in het verslagjaar, is het niet de bedoeling dat deze indicator gekozen wordt
  * **Let op:** als de organisatie uit één afdeling of locatie bestaat, is de score bij indicator 1.2 altijd 0% of 100%
* Advance Care Planning: weergave van extra informatie: "Bij deze indicator moeten zowel cliënten met een indicatie Wlz Zorgprofiel VV 4 t/m 10 **zonder behandeling**, als **met behandeling** worden meegenomen.”
* Medicatieveiligheid:
  * **Let op:** als de organisatie uit één afdeling of locatie bestaat, is de score bij indicator 3.1 altijd 0% of 100%
  * De indicator 3.2 kan alleen worden gekozen als er cliënten **met behandeling** op de locatie aanwezig zijn en deze minimaal zes maanden in zorg zijn
* Continentie: **NB:** Deze indicator vraagt een toetsing van de vastgelegde afspraken rondom de toiletgang per cliënt. Het betreft hier uitdrukkelijk niet de globale afspraken over het proces zoals deze in de instelling zijn vastgelegd.
* Kwaliteitsverslag: **let op:** als het Kwaliteitsverslag op moment van aanleveren nog niet gepubliceerd is, vul dan indien mogelijk de URL in waar deze gepubliceerd zal worden.

## 5. Contextinformatie

Voor contextinformatie in het geval van afwijkingen/ afwijkende cijfers, bijvoorbeeld ten opzichte van het vorige verslagjaar, worden de toelichtingenvelden bij de indicator zelf gebruikt.  
Voor contextinformatie bij de aangeleverde cijfers kan het kwaliteitsverslag van de zorgaanbieder worden gebruikt. De zorgaanbieder bepaalt zelf welke contextinformatie meegegeven dient te worden bij bepaalde indicatoren.