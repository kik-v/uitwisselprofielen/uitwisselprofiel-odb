---
title: Organisatorische interoperabiliteit
weight: 2
---
## 1. Aanlevering

U kunt vanaf 8 januari tot 25 juni 2024 de kwaliteitsgegevens aanleveren over het jaar 2023.

## 2. Meetperiode

De meting voor de indicatoren Basisveiligheid vindt plaats in de periode van 1 januari 2024 t/m 28 februari 2024.
De meetperiode voor de indicatoren Personeelssamenstelling is van 1 januari 2023 t/m 31 december 2023.

## 3. Terugkoppeling

De volgende terugkoppeling volgt ten aanzien van de aangeleverde gegevens:

* Een bedankkaart aan de zorgaanbieder vanuit het Zorginstituut na aanlevering van de gegevens. Voor zorgaanbieders die gebruik maken van een datastation voor de aanlevering wordt een bevestiging digitaal verstrekt via (een e-mail vanuit) de KIK-starter. 
* Een analyse door het Zorginstituut ten aanzien van de kwaliteit van de aangeleverde gegevens met name gericht op de volledigheid van de aanlevering. In de Stuurgroep Kwaliteitskader verpleeghuiszorg worden de resultaten van de analyse gepresenteerd. Zorgaanbieders ontvangen per brief een samenvatting van de analyse. Indien gewenst kunnen zorgaanbieders de volledige analyse opvragen bij het Zorginstituut. NB: Dit is een analyse over de gehele aanlevering, dus niet per zorgaanbieder. Dit vindt plaats na aanlevering en publicatie in het ODB.
* Verkenning naar draagvlak en mogelijkheden voor de organisatie van een jaarlijkse informatie dag voor zorgaanbieders van verpleeghuiszorg i.s.m. veldpartijen.
* In het aanleverportaal (van gegevensmakelaar Desan) zijn diverse validaties t.b.v. de kwaliteit van de gegevens ingebouwd, waaronder controles op de invoervelden. Deze validaties worden beschreven op de semantische laag van dit uitwisselprofiel. Voor zorgaanbieders die gebruik maken van een datastation kunnen deze validaties gedaan worden met behulp van het datastation.

## 4. In- en exclusiecriteria

De uitvraag is bedoeld voor organisaties die onder onderstaande inclusiecriteria vallen (en daarmee gebonden zijn aan het kwaliteitskader verpleeghuiszorg)

De uitvraag van deze indicatoren betreft de zorg die aan cliënten verleend wordt volgens de reikwijdte van het Kwaliteitskader: “cliënten met Wlz-indicatie met zorgprofiel VV4 t/m VV10, die 24 uur aangewezen zijn op Wlz zorg en ondersteuning. Dit gaat over zorg die geboden wordt aan groepen van cliënten, dan wel zorg die voor een deel van de tijd geclusterd geboden wordt, zoals tijdelijke opnamen. Het kader geldt ook voor situaties waarin mensen kiezen om met een persoonsgebonden budget in groepsverband zorg te krijgen”.

### 4.1. Sector (type)

inclusie: sector Verpleging en verzorging (VV), zorgprofielen VV4 t/m VV10.  
exclusie: sectoren GGZ B-groep (GGZ-B), Lichamelijk gehandicapt (LG), Licht verstandelijk gehandicapt (LVG), Verstandelijk gehandicapt (VG), Zintuiglijk gehandicapt, auditief en communicatief (ZGaud) en Zintuiglijk gehandicapt, visueel (ZGvis).

### 4.2. Profiel (cliënt)

* VV04 Beschut wonen met intensieve begeleiding en uitgebreid wonen  
* VV05 Beschermd wonen met intensieve dementiezorg  
* VV06 Beschermd wonen met intensieve verzorging en verpleging  
* VV07 Beschermd wonen met zeer intensieve zorg, vanwege specifieke aandoeningen, met de nadruk op begeleiding  
* VV08 Beschermd wonen met zeer intensieve zorg, vanwege specifieke aandoeningen, met de nadruk op verzorging/ verpleging  
* VV09B Herstelgerichte behandeling met verpleging en verzorging  
* VV10 / PTZ Beschermd verblijf met intensief palliatief-terminale zorg