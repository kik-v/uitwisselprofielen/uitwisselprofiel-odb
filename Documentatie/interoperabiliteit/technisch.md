---
title: Technische interoperabiliteit
weight: 4
---
## 1. Via Desan portaal
Aanlevering van de uitkomsten van beschreven berekeningen (de indicatoren), verwijzing naar het kwaliteitsverslag en totaalscore cliëntervaring vindt plaats aan het Zorginstituut via de gegevensmakelaar (Desan). Zie voor meer informatie en instructies voor de aanlevering: [Zorginzicht](https://www.zorginzicht.nl/ondersteuning/aanleveren-kwaliteitsgegevens-per-sector/verpleeghuiszorg).

Zorgaanbieders die al eerder kwaliteitsgegevens hebben aangeleverd krijgen inloggegevens voor de portal van Desan automatisch toegezonden in januari 2024. Indien dit niet het geval is, neemt de zorgaanbieder contact op met Desan (verpleeghuiszorg@desan.nl) voor het aanvragen van inloggegevens. Gegevens kunnen door Zorgaanbieder worden aangeleverd aan de portal in de periode 8 januari tot 25 juni 2024. Verpleeghuiszorgorganisaties bevestigen in de portal of gegevens na controle kunnen worden doorgeleverd aan het ODB loket.

Individuele zorgaanbieders zijn verantwoordelijk voor de tijdigheid, volledigheid en juistheid van de aangeleverde gegevens. Indien verpleeghuiszorgorganisaties gegevens aanleveren die een correctie vormen op eerder aangeboden gegevens, dan worden de oude/eerder aangeleverde gegevens vernietigd.

## 2. Via KIK-starter mbv datastation

Indien een zorgaanbieder indicatoren kan berekenen en aanleveren via een datastation, dan kunnen die worden aangeleverd middels de KIK-starter. Voor meer informatie hierover zie [het Publicatieplatform](https://kik-v-publicatieplatform.nl/).

De zorgaanbieder gebruikt hiervoor de volgende SPARQL queries:

[Klik hier voor personeelssamenstelling](/Gevalideerde_vragen_technisch/Personele%20samenstelling)

[Klik hier voor basisveiligheid](/Gevalideerde_vragen_technisch/Basisveiligheid)

Voor aanlevering middels de KIK-starter gelden de volgende afspraken:

* Voor verslagjaar 2023 kunnen gegevens worden aangeleverd via KIK-starter. Zorgaanbieders die zich hiervoor aanmelden krijgen van het programma KIK-V toegang tot de KIK-starter via een daarvoor ingericht aansluitproces.
* Voor gebruik van de KIK-starter worden geen kosten in rekening gebracht.
* Gegevens kunnen door Zorgaanbieder worden aangeleverd aan de KIK-starter in de periode 8 januari tot 25 juni 2024.  Verpleeghuiszorgorganisaties bevestigen in de KIK-starter of gegevens na controle kunnen worden doorgeleverd aan het ODB loket.
* Herstel/correctie is mogelijk tot het moment dat het ODB loket over gaat tot publicatie. Na dit moment alleen in onderling overleg. Over de wens tot herstel/correctie is onderling contact tussen het programma KIK-V en het ODB loket.
* Op het moment dat de zorgaanbieder de gegevens via de KIK-starter verzendt, verklaart zij door of namens de Raad van Bestuur dat zij akkoord gaat met het aanleveren van de data aan Zorginstituut Nederland voor het landelijk beschikbaar stellen van de data via de Openbare Database. Meer informatie hierover is weergegeven in de KIK-starter.