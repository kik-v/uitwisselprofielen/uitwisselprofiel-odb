---
title: Juridische interoperabiliteit
weight: 1
---
## Grondslag

Zie Juridisch kader (Tabel rij 2 voor grondslag en randvoorwaarden: [Wkkgz](https://kik-v-publicatieplatform.nl/afsprakenset/_/content/docs/juridisch-kader/aggregatie/wkkgz), artikel 11g, 11h en 11i; Tabel rij 3 voor verplichting van de aanlevering: [Zvw artikel 88 en 89](https://kik-v-publicatieplatform.nl/afsprakenset/_/content/docs/juridisch-kader/verstrekking/zvw)).