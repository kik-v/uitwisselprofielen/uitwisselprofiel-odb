---
title: Release- en versiebeschrijving
weight: 3
---
| **Releaseinformatie**| |
|---|---|
| Release| 1.2.3 |  
| Versie | 1.2.3 versie 0.9 ter vaststelling door de Ketenraad |
| Doel | Release 1.2.3 betreft het uitwisselprofiel dat hoort bij het handboek aanlevering ODB voor de indicatoren verslagjaar 2023. |
| Doelgroep | Zorginstituut, Zorgaanbieders verpleeghuiszorg, Betrokken partijen kwaliteitskader verpleeghuiszorg |
| Totstandkoming | De ontwikkeling van release 1.2.3 is uitgevoerd door het programma KIK-V in afstemming met de werkgroepen onder de stuurgroep kwaliteitskader verpleeghuiszorg en het Zorginstituut. |
| Operationeel toepassingsgebied | Aanlevering indicatoren kwaliteitskader verpleeghuiszorg. Zie inhoud van het uitwisselprofiel. |
| Status | Voor advies naar het Tactisch Overleg voorafgaand aan vaststelling door de Ketenraad. |
| Functionele scope | Release 1.2.3 omvat Wijzigingen en aanvullingen als gevolg van besluitvorming over het nieuwe handboek voor verslagjaar 2023. Belangrijkste wijziging is de wijziging van het verslagjaar. De inhoud is verder hetzelfde gebleven. |
| Licentie | Creative Commons: Naamsvermelding-GeenAfgeleideWerken 4.0 Internationaal (CC BY-ND 4.0) |

