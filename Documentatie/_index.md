---
title: Introductie
weight: 1
---
Het Uitwisselprofiel ZIN Openbaarmaking kwaliteitsindicatoren verpleeghuiszorg bestaat uit drie secties:

* de algemene documentatie, bestaande uit een sectie: [algemeen](/Documentatie/algemeen/), [release-informatie](/Documentatie/release-info/), [interoperabiliteit](/Documentatie/interoperabiliteit/) en [privacy & informatiebeveiliging](/Documentatie/privacy%26informatiebeveiliging/);
* de [functionele berekeningen](/Gevalideerde_vragen_functioneel/) van de gevalideerde vragen;
* de [technische uitwerking](/Gevalideerde_vragen_technisch/) van de gevalideerde vragen in de vorm van SPARQL's.

Let op: Uitwisselprofiel ZIN Openbaarmaking kwaliteitsindicatoren verpleeghuiszorg komt per 31-12-2024 te vervallen.
