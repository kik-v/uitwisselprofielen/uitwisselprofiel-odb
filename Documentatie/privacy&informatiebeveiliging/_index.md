---
title: Privacy- en informatiebeveiliging
weight: 5
---
De zorgaanbieder levert alleen de afzonderlijke teller en noemer plus de uitkomsten van de berekeningen op, de indicatoren. Deze aanlevering bevat geen persoonsgegevens.

Alle zorgaanbieders leveren hun meetgegevens aan in het Desanportaal. Echter, de gegevens van de zorgaanbieders met **minder dan tien cliënten** worden in verband met herleidbaarheid **niet** doorgeleverd aan het ODB. In het Desanportaal wordt in het betreffende veld een codering (‘Waarneming kleiner dan 10’) opgenomen die aangeeft dat er wel gegevens zijn aangeleverd, maar dat de N lager is dan tien. Dit wordt ook op deze manier zichtbaar in het ODB.

Zorgaanbieders met (een vestiging met) minder dan tien cliënten, die gebruik maken van een datastation voor de aanlevering, leveren voor een indicator waar dit van toepassing is een code ('Waarneming kleiner dan 10') aan als antwoord op de indicator(en). Dit is ook opgenomen in de desbetreffende SPARQL queries.