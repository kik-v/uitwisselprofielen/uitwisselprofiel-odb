---
title: Algemeen
weight: 2
---
# Algemeen
De sectorpartijen (Patiëntenfederatie Nederland, LOC, Zorgverzekeraars Nederland, ActiZ, ZorgthuisNL, V&VN en Verenso) verenigd in de Stuurgroep Verpleeghuiszorg hebben bepaald met welke indicatoren de kwaliteit van zorg gemeten wordt. De indicatoren zijn vastgesteld op voordracht van deze Stuurgroep en aangeboden voor opname in het kwaliteitsregister. Op grond van de Wet kwaliteit, klachten en geschillen zorg (artikel 11i tweede lid) is aanlevering aan het Zorginstituut van de informatie in het kwaliteitsregister verplicht voor zorgaanbieders. 
Het uitwisselprofiel bevat de afspraken over de uitwisseling van gegevens op het thema Personeel en Basisveiligheid in het kwaliteitskader ten behoeve van publicatie in het Openbare Databestand (en daarmee zorginzicht.nl). De afspraken zijn gericht op de aanlevering in het jaar 2024 over het jaar 2023. Dit betekent tevens dat dit uitwisselprofiel een jaarlijkse aanpassing vergt.

## 1.1. Doel van de uitvraag

### Indicatoren Kwaliteitskader verpleeghuiszorg (Basisveiligheid, verwijzing kwaliteitsverslag, Totaalscore cliëntervaring)

Zorgverleners hebben de verantwoordelijkheid om in hun dagelijkse praktijk, samen met collega’s continu aan de verbetering van de zorg te werken. Eén van de hulpmiddelen hierbij is regelmatig metingen te doen. De uitslag van deze metingen brengt in teams het gesprek op gang: wat vertelt deze uitkomst? Herkennen we deze uitslag? Zien we een trend? Vinden we het goed genoeg of gaan we actie ondernemen? Het kwaliteitskader verpleeghuiszorg heeft zes thema’s benoemd die belangrijk zijn voor de veiligheid van cliënten: decubitus, advance care planning, medicatieveiligheid, gemotiveerd omgaan met vrijheidsbeperking, continentie en aandacht voor eten en drinken. Op deze thema’s zijn door Verenso en V&VN indicatoren ontwikkeld met als doel het leren en verbeteren in teams op deze thema’s een impuls te geven.

Naast de aanlevering van de indicatoren basisveiligheid leveren zorgaanbieders voor de overige indicatoren direct vanuit het Kwaliteitskader, het volgende aan:

* de URL naar het kwaliteitsverslag van de zorgorganisatie;
* de gemiddelde totaalscore cliëntervaring op locatieniveau. Meting kan plaatsvinden via ZorgkaartNederland. Een meting kan ook plaatsvinden zonder ZorgkaartNederland, bijvoorbeeld met een meetbureau of een meting in eigen beheer.

### Indicatoren Personeelssamenstelling

Het kwaliteitskader verpleeghuiszorg draait om samen leren en verbeteren. Dit vraagt onder meer met elkaar in gesprek te gaan over de relatie tussen de aard van de te verlenen zorg en de daarvoor wenselijke personeelssamenstelling. Deze relatie is van meerdere facetten afhankelijk en daarmee niet uniform. Het is vanuit meerdere perspectieven gewenst gebruik te kunnen maken van sectorale inzichten. Om trends, ontwikkelingen en waardevolle informatie te kunnen analyseren binnen de sector, is afgesproken dat zorgorganisaties op organisatieniveau gegevens aanleveren.

## 1.2. Wat gebeurt er met de gegevens?

Zorginstituut publiceert de gegevens die worden aangeleverd in het Openbaar Databestand (ODB), zodat ze openbaar worden gemaakt en door iedereen te gebruiken zijn. De gegevens zijn bedoeld voor onder andere de professionele gebruiker die deze gegevens bijvoorbeeld beschikbaar kan maken als (keuze-)informatie voor cliënten. Daarnaast kunnen deze gegevens ook worden gebruikt voor zorginkoop en toezicht.

## 1.3. Doel per indicator van de uitvraag

### 1.3.1. Indicatoren Personeelssamenstelling

#### Aard van de aanstellingen

De aard van de aanstellingen zegt met name iets over de kwantitatieve personele bezetting en de mate waarin sprake is van continuïteit in de relatie tussen personeel, organisatie en cliënten. Ook zegt het iets over de wijze waarop qua personele inzet ingespeeld wordt c.q. gespeeld kan worden op onplanbare veranderingen in de cliëntvraag. Daarnaast heeft het een relatie met de aantrekkelijkheid van een organisatie op de arbeidsmarkt.

#### Kwalificatieniveaus zorgverleners en vrijwilligers

Het kwalificatieniveau van de zorgverleners geeft aan welk niveau aan kwaliteitseisen (middels opleiding en scholing) gesteld wordt aan het personeel c.q. functies die ingezet worden bij het leveren van zorg- en dienstverlening aan de cliënten. De door een organisatie gewenste kwalificatieniveaumix vormt idealiter een doorvertaling van de zorgvisie naar een ideale personeelssamenstelling. De kwantitatieve beschikbaarheid van vrijwilligers zegt iets over de extra aandacht die geschonken kan worden aan cliënten buiten de reguliere, professionele zorg- en dienstverlening.

#### Ziekteverzuim

Het ziekteverzuim zegt iets over de mate waarin personeelsleden op basis van fysieke- en/of psychische klachten besluiten niet te kunnen werken. In sommige gevallen ligt de oorzaak of een deel van de oorzaak in het werk of de werkomstandigheden. Ziekteverzuim en de wijze waarop ziekteverzuim herbezet wordt, heeft invloed op de personele inzet in relatie tot de cliëntsamenstelling.

#### In-, door- en uitstroom

De in-, door- en uitstroom zegt iets over de stabiliteit van de personele bezetting in relatie tot de cliënten. Een doelgerichte balans tussen enerzijds continuïteit en anderzijds vernieuwing afgestemd op de cliëntbehoefte en de ontwikkelingen hieromtrent vormt de uitdaging. Idealiter geeft in-, door- en uitstroom bewegingsruimte om te komen tot een verbeterde personeelssamenstelling en illustreert het de mate waarin de organisatie weet aan te sluiten op de loopbaanwensen van personeelsleden.

#### Ratio personeelskosten/opbrengsten

Hierbij gaat het om de kwantitatieve personele inzet bij de zorg- en dienstverlening aan de gemiddelde cliënt.

### 1.3.2. Indicatoren Basisveiligheid

#### Thema Decubitus

De laatste jaren is de prevalentie van decubitus flink gedaald. Echter, in de langdurige zorg is het percentage wonden met een duur van zes maanden of langer toegenomen en komen de meer ernstige vormen van decubitus ook vaker voor. Dit kan erop duiden dat de behandeling van decubitus verbeterd kan worden (Landelijke Prevalentiemeting Zorgproblemen, 2015). Daarbij komt dat decubitus ook een grote impact heeft op de kwaliteit van leven van de cliënt. De uitkomstindicator over decubitus (aantal cliënten met decubitus vanaf categorie 2) geeft informatie over het vóórkomen (prevalentie) van decubitus en is internationaal gevalideerd. De uitkomsten (bijvoorbeeld een stijging ten opzichte van een eerdere meting) kunnen voor teams aanleiding zijn om in multidisciplinair verband, bijvoorbeeld met de wondverpleegkundige en de specialist ouderengeneeskunde, in te zoomen op casussen in het eigen team en lessen te trekken uit die casussen. Consultatie en reflectie vergroten het bewustzijn en de kennis over de toestand van de huid van degene die aan het bed staat en dat is volgens professionals een belangrijke sleutel tot decubituspreventie. Met name het bespreken van casuïstiek met betrekking tot huidletsel is een goed middel om te leren en verbeteren op dit thema en zo de deskundigheid te vergroten. Om deze reden is een procesindicator over casuïstiekbespreking bij decubitus aan de indicatorenset toegevoegd.

#### Thema Advance Care Planning

In het kwaliteitskader wordt preventie van acute ziekenhuisopnames genoemd als belangrijk thema. Voor het zoveel als mogelijk voorkómen van dit soort opnames is een doorlopend gesprek met cliënt en familie nodig over de door hen gewenste en medisch gezien mogelijke zorg, ook rondom het levenseinde. De moderne geneeskunde kan steeds meer. Deze verbeterde mogelijkheden om ziektes te genezen en levens te verlengen heeft ook een keerzijde. Sommige behandelingen zijn ingrijpend, hebben grote bijwerkingen en mogelijk schadelijke gevolgen. Dit roept vragen op als: wanneer houdt medisch ingrijpen op om zinvol te zijn en is andere zorg passender? Staan arts en cliënt stil bij de kwaliteit die het leven voor de cliënt heeft na een voorgenomen behandeling? Gaat het gesprek ook over de vraag of de cliënt zo’n behandeling nog wil en of dat realistisch is? Over wat de cliënt werkelijk belangrijk vindt en met welke zorg dit het best te bereiken is? Het zijn onderwerpen die professionals op tijd moeten bespreken met cliënten die door een ziekte of een kwetsbare situatie het levenseinde in zicht hebben. Want zorg is alleen passend als deze aansluit bij de wens van de cliënt. Ook is het van belang dat de uitkomsten van deze gesprekken goed worden vastgelegd. Op het moment dat de vraag naar verder behandelen namelijk acuut aan de orde is, kan die belangrijke vraag, waarover een ander onmogelijk kan beslissen, meestal niet meer worden gesteld. Om een indruk te krijgen van de mate waarin gesprekken worden gevoerd en afspraken worden vastgelegd, is ervoor gekozen om een indicator op cliëntniveau te maken die kijkt naar de verslaglegging hiervan in het zorgdossier. De indicator geldt voor cliënten met een Wlz-indicatie, zowel met als zonder indicatie voor behandeling. De indicator heeft als doel een impuls te geven aan het voeren van de goede gesprekken op het juiste moment. Het is niet de bedoeling dat de indicator gaat werken als een met de cliënt af te vinken lijstje. Dit staat haaks op de zorgvuldigheid waarmee de gesprekken over dit onderwerp moeten worden gevoerd.

#### Thema Medicatieveiligheid

De ‘Veilige Principes’ in de medicatieketen doen verschillende aanbevelingen voor een veilig medicatieproces (zie https://www.zorgvoorbeter.nl/medicatieveiligheid/richtlijnen-veilige-principes). Twee van deze randvoorwaarden zijn omgezet in een indicator voor de verpleeghuiszorg. Het gaat om: 1. het op teamniveau bespreken van medicatiefouten, en 2. het uitvoeren van medicatiereviews met een vertegenwoordiger van het zorgteam. Professionals geven aan dat als het gaat om leren en verbeteren het cruciaal is dat degene die de medicatie geeft, ook weet wat zij of hij geeft. Bekwaam zijn is essentieel. De indicatoren zijn daarom gericht op randvoorwaarden van medicatieveiligheid, bekwaam zijn. En  het leren en verbeteren hiervan. Indicator 3.1 richt zich op het leren van medicatiefouten op de afdeling. De indicator geeft antwoord op de vraag of teams ten minste ieder kwartaal informatie over incidenten en bijna incidenten krijgen teruggekoppeld en op basis daarvan verbeteracties kunnen uitzetten. Indicator 3.2 geeft antwoord op de vraag of er minstens één keer per jaar een formele medicatiereview plaatsvindt met een zorgmedewerker die medicijnen aan de cliënt verstrekt. Het bespreken van de medicatie met specialist ouderengeneeskunde, apotheker en verpleegkundige en verzorgende vergroot het inzicht in de werking van medicatie bij alle betrokkenen. Het is voor de verpleegkundige en verzorgende als het ware een korte bijscholing, tegelijkertijd doen specialist ouderengeneeskunde en apotheker uit de observaties van de verpleegkundige meer kennis op over de werking van de medicatie.

#### Thema Gemotiveerd omgaan met vrijheidsbeperking

Het Kwaliteitskader Verpleeghuiszorg benoemt als thema ‘Gemotiveerd omgaan met vrijheidsbeperkende interventies’. Dit thema bestaat uit 2 indicatoren. Een procesindicator die op afdelingsniveau vraagt hoe hieraan wordt gewerkt. In de andere indicator wordt op locatieniveau gevraagd hoe in het verpleeghuis wordt gewerkt aan vrijheidsbevordering (structuurindicator). Ondanks de invoering van de Wet Zorg en Dwang zijn deze twee indicatoren m.b.t. vrijheidsbeperking onveranderd. Omdat het een keuze-indicator is, kunnen zorgorganisaties en zorgprofessionals zelf beslissen of zij deze indicator willen kiezen.

#### Thema Continentie

Incontinentie is een omvangrijk gezondheidsprobleem dat vaak leidt tot een afname in de kwaliteit van leven van mensen. Incontinentie komt relatief vaak voor in verpleeghuizen. Urine-incontinentie komt voor bij 57% van de cliënten; enige vorm van fecale incontinentie bij 30% van de cliënten (Bron: LPZ). Het Kwaliteitskader Verpleeghuiszorg benoemt continentie als belangrijk thema voor de basisveiligheid. Professionals geven aan dat de term incontinentie klinkt alsof een  zorgverlener er niet meer zoveel aan kan doen, terwijl de sector juist een impuls wil geven aan leren en verbeteren op de werkvloer. Daarom is het thema hernoemd naar continentie. Professionals geven aan dat de behoefte aan continentiezorg in kaart kan worden gebracht  door het opstellen van een plan voor zorg rondom de toiletgang waarin de preferenties van de cliënt zijn vastgelegd. In dit plan worden de afspraken over voorkeuren en gewoontes van de  cliënt betreffende de toiletgang genoteerd. Gaat een cliënt bijvoorbeeld meteen bij het  opstaan naar het toilet of later? Heeft de cliënt hulp nodig bij de toiletgang?  Incontinentiemateriaal wordt alleen gebruikt indien dit ondersteunend is voor de cliënt. Bijvoorbeeld wanneer een cliënt ’s nachts wil doorslapen. Ook deze afspraak wordt vastgelegd in het plan voor zorg rondom de toiletgang. Deze indicator gaat in op de gewoonten van cliënten met betrekking tot de toiletgang en in  hoeverre cliënten die gewoonten kunnen behouden, zodat minder snel overgegaan hoeft te  worden op bijvoorbeeld incontinentiemateriaal.

#### Thema Aandacht voor eten en drinken

Eten en drinken is voor alle cliënten in de verpleeghuiszorg een belangrijk aandachtspunt. Daarbij gaat het niet alleen om het voorkomen van ondervoeding of overgewicht bij de cliënt, maar ook om het genieten van eten en drinken en de ambiance tijdens de maaltijden. Professionals geven aan dat het voor een team prettig is te weten wat de voedselvoorkeuren van een cliënt zijn. Deze voorkeuren omvatten een scala aan onderwerpen, zoals het bespreken van voorkeuren voor bepaald eten en drinken of hoeveelheden. Ook voorkeuren voor tijdstippen, plaats en tussendoortjes vallen hieronder. Of een cliënt hulp bij eten en drinken wenst of nodig heeft, is ook een onderwerp dat onder voedselvoorkeuren valt. Voedselvoorkeuren van cliënten kunnen veranderen met de tijd, om deze reden is het  belangrijk hierover regelmatig in gesprek te blijven. Deze indicator gaat in op hoe wordt omgegaan met de voorkeuren van de cliënt m.b.t. eten en  drinken in het dagelijks leven. Het betreft niet de organisatiekenmerken, zoals bijvoorbeeld het aanbieden van een keuzemenu.
