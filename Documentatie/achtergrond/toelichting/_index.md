---
title: Toelichting
weight: 1
---
# Toelichting

## Scope

Het uitwisselprofiel volgt de informatievragen per indicator en de bijbehorende definities zoals beschreven staan in het handboek ‘Verpleeghuiszorg kwaliteitskader indicatoren’ en handboek 'Verpleeghuiszorg Personeelssamenstelling indicatoren'.

Momenteel zijn de verwijzing naar het kwaliteitsverslag, de totaalscore cliëntervaring en de mogelijkheid tot opmerkingen bij een indicator buiten scope van de functionele en technische uitwerking van de indicatoren. Deze informatie komt niet uit de data in het operationele proces van de zorgaanbieder. Er wordt gekeken naar een manier om deze informatie op een andere manier via de KIK-starter aan te leveren. Deze informatie is in ieder geval via het DESAN portaal aan te leveren.

## Proces

Bij de ontwikkeling van de functionele vragen en technische queries KIK-V in het uitwisselprofiel zijn de volgende partijen betrokken:

- Voor de indicatoren personeelssamenstelling: ActiZ en betrokken partijen uit de stuurgroep Kwaliteitskader.
- Voor de indicatoren basisveiligheid: de werkgroep onder aansturing van Verenso en V&VN.

Voor de uitwerking van de tekst in het uitwisselprofiel is ook het Zorginstituut ODB loket (Zorginzicht) betrokken geweest als ontvanger van de antwoorden en het publiceren daarvan.

De indicatoren zijn bij implementatie getoetst en verder doorontwikkeld.

## Gemaakte keuzes

- T.a.v. indicatoren en concepten: welke definitie uit verschillende definities is gekozen en waarom? Wat is wel/niet meegenomen in het antwoord en waarom? Waarom is een bepaald uitgangspunt gekozen? Welke strategische afweging gemaakt? Waarom zijn bepaalde elementen niet opgenomen? Waarom zijn bepaalde vragen niet opgenomen? Of niet als directe vraag opgenomen, maar op een andere manier? Waar zijn de antwoorden voor bedoeld? In het uitwisselprofiel staat deze informatie al kort en bondig beschreven, maar hier staat nog meer informatie over het waarom en hoe.

| Informatievraag                                | Bestaande cq gewijzigde cq nieuwe informatievraag | Toelichting vraaganalyse                                                                                                                                         | Toelichting resultaten praktijktoets                                                                                                                     |
| ---------------------------------------------- | ------------------------------------------------- | ---------------------------------------------------------------------------------------------------------------------------------------------------------------- | ---------------------------------------------------------------------------------------------------------------------------------------------------------- |
| Voor de indicatoren die gaan over de gesprekken op afdelingsniveau, namelijk 1.2 Casuïstiekbespreking Decubitus en 3.1 Bespreken medicatiefouten in het team en indicator 3.2 Medicatiereview, gericht op de cliënt | Bestaande informatievragen | Registratie van decubitus en medicatiefouten vindt gestructureerd plaats, maar niet dat daarover het gesprek is gevoerd en met wie. Dat wordt veelal bijgehouden in verslagen die apart worden gearchiveerd en daarmee onvoldoende bruikbaar zijn als bron voor geautomatiseerde vaststelling van diverse feiten omtrent dat gesprek en of het voldoet aan de juiste criteria om mee te nemen als antwoord op de indicator. Met andere woorden: uitkomsten zijn veelal gestructureerd vastgelegd, processen niet. Wat in ieder geval niet de bedoeling is van het kwaliteitskader en het leren&verbeteren is dat deze indicatoren een soort ‘afvinklijstje’ worden. De vraag is dan of het goede gesprek wel is gevoerd, of alleen geregistreerd om te registreren. Er is door de organisatie in de bedoelde gevallen juist opgetreden als de organisatie vindt dat er juist is opgetreden. Wat dus de essentie is, is dat je er als organisatie beleid voor hebt en controleert of dat op de juiste manier wordt uitgevoerd. Met die blik is nog eens naar de voorstellen voor de voorbeeldberekeningen gekeken, maar de indicatoren zijn niet aangepast. ||
| Indicator 4.1 vrijheidsbeperkende maatregelen    | Geschrapt | De huidige indicator sluit minder goed aan bij de registratie van zorgaanbieders, omdat die nu plaatsvindt in het kader van de nieuwe WZD. De werkgroep heeft voorgesteld deze indicator voor verslagjaar 2022 te laten vallen. Er zal een doorontwikkeling op het thema plaatsvinden waarbij tevens wordt bepaald of en in welke vorm hiervoor een indicator terugkeert in het kwaliteitskader. ||
| Indicatoren basisveiligheid 1.1, 2, 3.2, 5 en 6  | Bestaande informatievraag | Hiervoor gold in de voorstellen voor de berekeningen KIK-V het uitgangspunt: ‘Cliënten die in de verslagperiode van afdeling wisselen tellen bij elk van deze afdelingen als ‘1’ keer mee in de noemer.’ Dit is aangepast om te voorkomen dat er een verkeerd beeld ontstaat van het aantal gevallen. Dezelfde persoon telt dan namelijk meerdere keren mee in de indicatoren. Voor al deze indicatoren worden cliënten die verhuizen van vestiging in de verslagperiode maar 1x meegeteld in de noemer bij de laatste vestiging. Het uitgangspunt is dan nu: ‘Cliënten worden ingedeeld op de meeste recente vestiging waar de cliënt (o.b.v. de indicatie) zorg ontving binnen de verslagperiode.’ ||
| Indicatoren Basisveiligheid 1.1, 2, 5 en 6       | Bestaande informatievraag | In geval van 'Meting in de periode van 1 januari-28 februari 2023' (handboek) betreft het een peildatum in die periode met de stand van zaken op die datum (peildatum). Voor de gegevensuitwisseling KIK-V is het noodzakelijk om een vaste peildatum te kiezen. Er is gekozen voor peildatum 1 maart. Voor de andere indicatoren basisveiligheid betreft het ‘Terugkijkend op de periode januari tm december 2022, vaststellen in de periode van 1 januari-28 februari 2023' (handboek) en gaat het dus over een berekening over een gehele jaar (meetperiode). ||
| Indicator 3.2 basisveiligheid                   | Bestaande informatievraag | Extra uitgangspunt opnemen t.o.v. de voorgestelde berekening KIK-V: ‘Als de noemer 0 is, dan is de indicator niet van toepassing’ (komt in de praktijk overigens nauwelijks voor is de verwachting en je levert dan waarschijnlijk een andere keuzeindicator aan). ||
| Indicatoren personeelssamenstelling             | Bestaande informatievraag | De indicatoren personeelssamenstelling veranderen niet. Er zijn geen activiteiten gepland om hier voor verslagjaar 2022 aanpassingen in aan te brengen. ||
| Algemene uitgangspunten                         | Bestaande informatievraag | Aanpassingen algemene uitgangspunten uitwisselprofiel als gevolg van inzichten uit toepassing daarvan in andere uitwisselprofielen. In ieder geval is een nieuw uitgangspunt opgenomen in lijn met de afspraak over aanlevering van gegevens van vestigingen met minder dan 10 cliënten: Bij een populatie van 10 cliënten of minder (n =< 10) wordt de indicator niet berekend. In dat geval wordt het volgende getoond: 'Waarneming kleiner dan 10'. Daarnaast zijn de volgende algemene punten aangepast:  • Op vestigingsniveau (locatie) berekenen (want op dit niveau ook aangeleverd) en afdeling niet gebruiken in de berekeningen  • Bij alle indicatoren gebruiken zorgprofielen VV4 tm VV10 (ipv ZZP's)||

Zie verder: Algemene uitgangspunten en: Veelgestelde vragen
